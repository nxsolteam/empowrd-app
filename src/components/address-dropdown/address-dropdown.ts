import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Refresher } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';
import { Events } from 'ionic-angular';
/**
 * Generated class for the AddressDropdownComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
 declare var google: any;
@Component({
  selector: 'address-dropdown',
  templateUrl: 'address-dropdown.html'
})
export class AddressDropdownComponent {
  headerAddressTitle='';
  showEventAddress='false';
  showconfirmonHeader = false;
  addressExist=true;
  popupstatus=false;
  user:any;
  volunteerDetails='';
  addressOnHeader='';
  volunteerList=[];
  volunteerAdminId;
  usersEvents:any;
  addresses:any;
  addressId:string;
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  addresslist:any;
  constructor(public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, private addresslistProvider: AddresslistProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public geolocation: Geolocation,private diagnostic: Diagnostic, public events: Events) {
    this.addressId = localStorage.getItem('addressId');
    console.log('Hello AddressDropdownComponent Component');
    events.subscribe('addressDropDown:created', (data, time) => {
      if(data.status==true) {
        this.openPopup();
      } else {
        this.hidepopup();
      }
    });
    this.user =  JSON.parse(localStorage.getItem('user'));
    if (localStorage.getItem('headerAddressTitle') !==null) {
    	this.headerAddressTitle=localStorage.getItem('headerAddressTitle');
    }
    if (localStorage.getItem('addresstext') !==null) {
      this.addressOnHeader=localStorage.getItem('addresstext');
    }
    if(localStorage.getItem('showEventAddress') !== null){
      this.showEventAddress=localStorage.getItem('showEventAddress');
    }
    this.sendrequest.getResult('user/getvolunteerlist/' + this.user.id,'get',{}).then((response:any) => {     
      	if(response.data.noresult==0){
            this.volunteerDetails="VOLUNTEER ASSIGNMENT";
            localStorage.setItem('volunteerDetails',"VOLUNTEER ASSIGNMENT");
        } else {
            if(response.data.length > 0) {
                this.volunteerList['0']=response.data[0].name+" VOLUNTEER";
            } else {
                this.volunteerList['0']="VOLUNTEER ASSIGNMENT";
            }
            response.data.forEach(function(value){
              console.log(value);
               this.volunteerList[value.id]=value.name+" VOLUNTEER";
            }, this);
            this.volunteerDetails="VOLUNTEER ASSIGNMENT";
            localStorage.setItem('volunteerDetails',"VOLUNTEER ASSIGNMENT");
        }
  	},
  	error => {
      
      this.sendrequest.presentToast("Error occured. Please try again");
  	})

    // Source data for refreshing the cache data and first load.
    
    //const sourceData = this.addresslistProvider.random();
    
    //console.log(sourceData);
    // Register the cache instance using 'contact' as identifier, so we get the same data on next load.
    
    this.cacheService.get('addresslist')
      .mergeMap((cache: Cache<Addresslist[]>) => {
          this.cache = cache;
          return this.cache.get$;
      })
      .subscribe((addresslist:any) => {
       
        this.addresslist=addresslist.data.addresses;
        this.addressId = localStorage.getItem('addressId');
          if (localStorage.getItem('headerAddressTitle') ==null) {
            localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
            this.headerAddressTitle=localStorage.getItem('headerAddressTitle');
          }
          if (localStorage.getItem('addresstext') ==null) {
            localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address);

            this.addressOnHeader=localStorage.getItem('addresstext');
          }
      });
    
  }
  changeOpportunity(type) {

  }
  ShowData(data){
  	console.log(data);
  }
  
  openPopup() {
    this.popupstatus = true;
  }
  hidepopup() {
    this.popupstatus = false;
  }
  
  checkDeviceSetting(){
    this.diagnostic.isLocationEnabled().then((isEnabled) => {
      if(!isEnabled){
          //handle confirmation window code here and then call switchToLocationSettings
        this.diagnostic.switchToLocationSettings();
      }
      else{
        return true;
      }
    })
  }
  changeEvent(object){
    
    let event_name = object.event_name;
    let address = object.address;
    this.showEventAddress='true';
    localStorage.setItem('eventId',object.id);
    localStorage.setItem('showEventAddress','true');
    this.addressExist = true;
    this.headerAddressTitle=event_name;
    localStorage.setItem('headerAddressTitle',event_name);
    this.addressOnHeader = address;
    localStorage.setItem('addresstext',address);
    this.events.publish('addressChanged:created', object, Date.now());
    this.hidepopup();
  }
  addAddress() {
    this.hidepopup();
    // this.sendrequest.goToPage('AddEditAddressPage');
    this.events.publish('gotoPage:created', 'AddEditAddressPage', {});
  }
  votingPrimary(object) {
    
    let add = object.type;
    let address = object.address;
    let id = object.id;
    this.addressId = id;
    localStorage.setItem('validateAddress',object.validateAddress);
    this.volunteerAdminId=object.admin_id;
    
    localStorage.setItem('currentLocationClicked','false');
    localStorage.setItem('addressId',id);
    localStorage.setItem('volunteerAdminId',object.admin_id);
   
    localStorage.setItem('addresstext',address);
    this.addressOnHeader = address;
    
    this.showEventAddress='false';
    localStorage.setItem('showEventAddress','false');
    this.addressExist = true;
    
    localStorage.setItem('headerAddressTitle',add);
    
    this.headerAddressTitle=add;
    this.sendrequest.getResult('office/findlatlong/'+ address + '/' + this.user.id,'get',{}).then((response:any) => {     
      var results = response.data.response.results[0];
      
      localStorage.setItem('latitude',results.location.lat.toString());
      localStorage.setItem('longitude',results.location.lng.toString());
      this.events.publish('setLatLon:created', this.user, Date.now());
      this.leaderlistProvider.leaderlistRefresh();
      this.hidepopup();
      this.events.publish('addressChanged:created', object, Date.now());
      
    },
    error => {

    })
    
  };
  currentLocationGPS() {
    
   // if(this.checkDeviceSetting()) {
     
      this.geolocation.getCurrentPosition().then((response) => {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...',
            spinner: 'bubbles'
        });
        loading.present();
        console.log(response);
        var geocoder = new google.maps.Geocoder();
          var latlng = new google.maps.LatLng(response.coords.latitude, response.coords.longitude);
          localStorage.setItem('latitude',response.coords.latitude.toString());
          localStorage.setItem('longitude',response.coords.longitude.toString());
          geocoder.geocode(
            {'latLng': latlng}, 
             (results, status) => {
              console.log(results);
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log("Location : ");
                    this.volunteerAdminId='';
                    let object = {'address': results[0].formatted_address, 'type': 'CURRENT LOCATION'};
                    
                    this.showEventAddress='false';
                    localStorage.setItem('showEventAddress','false');
                    localStorage.setItem('headerAddressTitle','CURRENT LOCATION');
                    this.headerAddressTitle='CURRENT LOCATION';
                    localStorage.setItem('currentLocationClicked','true');
                    localStorage.setItem('addressId','0');
                    localStorage.setItem('volunteerAdminId','');
                    this.leaderlistProvider.leaderlistRefresh();
                    localStorage.setItem('addresstext',results[0].formatted_address);
                    this.addressOnHeader = results[0].formatted_address;
                    this.events.publish('addressChanged:created', object, Date.now());
                }
                else {
                  this.sendrequest.presentToast("Location can not getting. Please try again.")
                }
                loading.dismiss();
               
            }
            
        );

      });
    //}
  }

}
