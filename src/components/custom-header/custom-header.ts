import { Component,Input } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the CustomHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'custom-header',
  templateUrl: 'custom-header.html'
})
export class CustomHeaderComponent {

  header_data: any;
  countNewMessage='0';
  constructor(public navCtrl: NavController, public sendrequest: SendrequestProvider, public events: Events) {
    this.countNewMessage = localStorage.getItem('countNewMessage');
    events.subscribe('countNewMessage:created', (user, time) => {
    console.log("countNewMessage "+localStorage.getItem('countNewMessage'));
      this.countNewMessage = localStorage.getItem('countNewMessage');
    });
  }
  @Input() 
  set headerData(header_data: any) {
    this.header_data=header_data;
  }
  get headerData() {
    return this.header_data;
  } 

  goToPage(page) {
    //this.sendrequest.goToPage(page,{},true);
    this.events.publish('goToPage:params', page, { });
    //this.navCtrl.setRoot(page);
  }
}
