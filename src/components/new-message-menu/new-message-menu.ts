import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the NewMessageMenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'new-message-menu',
  templateUrl: 'new-message-menu.html'
})
export class NewMessageMenuComponent {

  message: any;
  index: any;
  all: string='0';
  type='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    console.log('Hello NewMessageMenuComponent Component');

    if(navParams.get('activity')==undefined) {
      this.type='Message';
      if(navParams.get('message')==undefined) {
        this.all = navParams.get('all');
      } else {
        this.message = navParams.get('message');
        this.index = navParams.get('index');
      }
    } else {
      this.type='Activity';
      this.message = navParams.get('activity');
    }
  }
  deleteMessage() {
  	let data:any = {'message': this.message,'index': this.index};
    this.viewCtrl.dismiss(data);
  }
  deleteAllMessage() {
    let data:any = {'all': '1' };
    this.viewCtrl.dismiss(data);
  }

}
