import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { CustomHeaderComponent } from './custom-header/custom-header';
import { AddressDropdownComponent } from './address-dropdown/address-dropdown';
import { EmojiPickerComponent } from './emoji-picker/emoji-picker';
//import { NewMessageMenuComponent } from './new-message-menu/new-message-menu';
//import { NewMessageComponent } from './new-message/new-message';
@NgModule({
	declarations: [
    CustomHeaderComponent,
    AddressDropdownComponent,
    EmojiPickerComponent
    ],
	imports: [CommonModule],
	exports: [
    CustomHeaderComponent,
    AddressDropdownComponent,
    EmojiPickerComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ComponentsModule {}
