import { Component, ViewChild } from '@angular/core';
import { ModalController, ViewController, NavParams, NavController, Events, PopoverController, Slides } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { PlaceholderProvider } from '../../providers/placeholder/placeholder';
import { NewMessageMenuComponent } from '../../components/new-message-menu/new-message-menu';

/**
 * Generated class for the NewMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'new-message',
  templateUrl: 'new-message.html'
})
export class NewMessageComponent {
  @ViewChild('mainSlider') mainCarousel: Slides;
  text: string;
  multipleMessage='0';
  senderName='';
  newMessagePopupSub='';
  newMessagePopupText='';
  message_type_inbox='';
  class='';
  MultipleUnReadMessage:any=[];
  adminName='';
  umid='';
  user:any;
  constructor( private modalCtrl: ModalController,  public viewCtrl: ViewController, public navParams: NavParams, public navCtrl: NavController, private events: Events, public sendrequest: SendrequestProvider, public popoverCtrl: PopoverController, private placeholderProvider: PlaceholderProvider) {
    console.log('Hello NewMessageComponent Component');
    this.text = 'Hello World';
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.senderName = navParams.get('senderName');
    this.adminName = navParams.get('adminName');
    this.umid = navParams.get('umid');
    this.message_type_inbox = navParams.get('message_type_inbox');
    this.class = navParams.get('class');
    this.multipleMessage = navParams.get('multipleMessage');
    this.newMessagePopupSub = navParams.get('newMessagePopupSub');

    this.newMessagePopupText = navParams.get('newMessagePopupText').trim();
    console.log(" this.newMessagePopupText ",  this.newMessagePopupText);
    this.MultipleUnReadMessage = navParams.get('MultipleUnReadMessage');
    console.log("MultipleUnReadMessage", this.MultipleUnReadMessage);
    this.sendrequest.getResult('user/updateMessagePopupStatus','get',{}).then((response:any) => { 
    },
    error => {
    }) 
  }
  ionViewDidLoad() {
    this.mainCarousel.centeredSlides = false;
    this.mainCarousel.direction = 'horizontal';
    this.mainCarousel.autoHeight = false;
    this.mainCarousel.pager = false;
    this.mainCarousel.loop = false;
    this.mainCarousel.zoom = false;
    this.mainCarousel.speed = 500;
    if(this.multipleMessage!='0') {
      this.mainCarousel.spaceBetween = 24;
      this.mainCarousel.slidesPerView = 1.3;
      this.mainCarousel.slidesOffsetBefore = 37;
    }
    this.mainCarousel.effect = 'slide';
  }
  removeMessage(message,index) {
    this.MultipleUnReadMessage.splice(index, 1);
    if(this.MultipleUnReadMessage.length==0) {
      this.closepopup();
    }
    /* this.sendrequest.getResult('user/deletemail/'+message.umid,'get',{}).then((response:any) => { 
      this.placeholderProvider.refresh();
    },
    error => {
    }) */
  }
  openSingleMessagePopup(myEvent) {
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, {  'message' : {}, 'index':0 }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        console.log(this.umid);
        this.sendrequest.getResult('user/deletemail/'+this.umid,'get',{}).then((response:any) => { 
          this.placeholderProvider.refresh();
          this.updateMessages();
        },
        error => {
        }) 
        this.closepopup();
      }
    });
  }
  updateMessages() {
    this.sendrequest.getResult('user/fetchLatestNewMailCountNew/' + this.user.id+'/2','get',{}).then((response:any) => { 

        localStorage.setItem('getNewMailListOrg', JSON.stringify(response.data.getNewMailListOrg));

        localStorage.setItem('getNewMailByTypes', JSON.stringify(response.data.getNewMailByTypes));
       
        localStorage.setItem('getAllMessageTypes', JSON.stringify(response.data.getAllMessageTypes));
        
        this.events.publish('messageCountChange:created', localStorage.getItem('messageCount'), Date.now()); 
      },
      error => {
       
      })
    
  }
  openMenuPopup(myEvent, message, index) {
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, {  'message' : message, 'index':index }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        this.removeMessagePermanent(data.message, data.index);
      }
    });
  }
  getMessageType(type) {
      if(type=='ANNOUNCEMENT'){
        return 'BULLETINS';
      } else if(type=='MEMBERSHIP INVITE') {
        return 'INVITES';
      } else {
        return type;
      }
  }
  removeMessagePermanent(message,index) {
    this.MultipleUnReadMessage.splice(index, 1);
    if(this.MultipleUnReadMessage.length==0) {
      this.closepopup();
    }
    this.sendrequest.getResult('user/deletemail/'+message.umid,'get',{}).then((response:any) => { 
      this.placeholderProvider.refresh();
    },
    error => {
    }) 
  }
  closepopup() {
    let data = {'messageDetail':'0', 'message_type_inbox': this.message_type_inbox };
    this.viewCtrl.dismiss(data);
  }
  viewSingleMessage() {
    let data = {'messageDetail':'1', 'message_type_inbox': this.message_type_inbox };
    this.viewCtrl.dismiss(data);
  }
  messageDetails(message) {
    let data = {'messageDetail':'3','message_id':message.umid, 'message_type_inbox': this.message_type_inbox };
    this.viewCtrl.dismiss(data);
  }
  goToMessage() {
  	let data = {'messageDetail':'2', 'message_type_inbox': this.message_type_inbox };
    this.viewCtrl.dismiss(data);
  }

}
