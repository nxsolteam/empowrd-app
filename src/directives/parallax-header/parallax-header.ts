import { Directive, ElementRef, Renderer } from '@angular/core';
import { Events  } from 'ionic-angular';
/**
* Generated class for the ParallaxHeaderDirective directive.
*
* See https://angular.io/api/core/Directive for more info on Angular
* Directives.
*/
@Directive({
	selector: '[parallax-header]', // Attribute selector
	host: {
		'(ionScroll)': 'onContentScroll($event)',
		'(window:resize)': 'onWindowResize($event)'
	}
})
export class ParallaxHeaderDirective
{
	header: any;
    mainContent:any;
    main_contentBGOne:any;
    main_contentBGTwo:any;
    headerHeight: any;
    translateAmt: any;
    scaleAmt: any;
    incresedWidth=true;
    homeOpportunityScreen=false;
	constructor(public element: ElementRef, public renderer: Renderer, public events: Events)
	{
		console.log('Hello ParallaxHeaderDirective Directive');
        events.subscribe('incresedWidth:created', (type, date) => {
        console.log("Events call");
            if(type=='true') {
                this.incresedWidth=true;
            } else {
                this.incresedWidth=false;
            }
        });
        events.subscribe('homeOpportunityScreen:created', (type, date) => {
            console.log("call home opportunity event");
            if(type=='true') {
                this.homeOpportunityScreen=true;
                this.renderer.setElementStyle(this.mainContent, 'background-position', 'center 0px');
                 let content = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
                this.main_contentBGOne = content.getElementsByClassName('main-contentBG1')[0];
                this.main_contentBGTwo = content.getElementsByClassName('main-contentBG2')[0];

                this.renderer.setElementStyle(this.main_contentBGOne, 'background-position', 'center 0px');
                this.renderer.setElementStyle(this.main_contentBGTwo, 'background-position', 'center 0px');
            } else {
                this.homeOpportunityScreen=false;
            }
        });
	}

	ngOnInit()
	{
        let content = this.element.nativeElement.getElementsByClassName('scroll-content')[0];
        this.header = content.getElementsByClassName('header-image')[0];
        let mainContent = content.getElementsByClassName('main-content')[0];
        this.mainContent = mainContent;
        this.headerHeight = this.header.clientHeight;

        this.renderer.setElementStyle(this.header, 'webkitTransformOrigin', 'center bottom');
        // this.renderer.setElementStyle(this.header, 'background-size', 'cover');
        this.renderer.setElementStyle(mainContent, 'position', 'relative');
        if(this.homeOpportunityScreen==true) {
            this.renderer.setElementStyle(this.mainContent, 'background-position', 'center 0px');
        }
    }

    onWindowResize(ev)
    {
        this.headerHeight = this.header.clientHeight;
    }

    onContentScroll(ev)
    {
        ev.domWrite(() => 
        {
            this.updateParallaxHeader(ev);
        });

    }

    updateParallaxHeader(ev)
    {

        if(ev.scrollTop >= 0){
            this.translateAmt = ev.scrollTop / 4;
            this.scaleAmt = 1;
        } else {
            this.translateAmt = 0;
            this.scaleAmt = -ev.scrollTop / this.headerHeight + 1;
        }
      
        this.renderer.setElementStyle(this.header, 'webkitTransform', 'translate3d(0,'+this.translateAmt+'px,0) ');

        //this.renderer.setElementStyle(this.header, 'webkitTransform', 'translate3d(0,0,0) scale('+this.scaleAmt+','+this.scaleAmt+')');

       // this.renderer.setElementStyle(this.header, 'top', this.translateAmt+'px');

        if(this.homeOpportunityScreen==true) {
            let bgAmt = ev.scrollTop / 4;
            this.renderer.setElementStyle(this.mainContent, 'background-position', 'center -'+bgAmt+'px');
            let bgAmtOne = ev.scrollTop / 5;
            this.renderer.setElementStyle(this.main_contentBGOne, 'background-position', 'center -'+bgAmtOne+'px');
            let bgAmtTwo = ev.scrollTop / 3;
            this.renderer.setElementStyle(this.main_contentBGTwo, 'background-position', 'center -'+bgAmtTwo+'px');
        }
    }

}
