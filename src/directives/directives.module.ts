import { NgModule } from '@angular/core';
import { ParallaxHeaderDirective } from './parallax-header/parallax-header';
import { OpenExternalurlDirective } from './open-externalurl/open-externalurl';
@NgModule({
	declarations: [ParallaxHeaderDirective,
    OpenExternalurlDirective],
	imports: [],
	exports: [ParallaxHeaderDirective,
    OpenExternalurlDirective]
})
export class DirectivesModule {}
