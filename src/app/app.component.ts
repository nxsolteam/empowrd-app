import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, Events, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LandingPage } from '../pages/landing/landing';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { NewMessageComponent } from '../components/new-message/new-message';

import { Deeplinks } from '@ionic-native/deeplinks';
import { MainTabPage } from '../pages/main-tab/main-tab';

//import { LeaderDetailPage } from '../pages/leader-detail/leader-detail';
import { SendrequestProvider } from '../providers/sendrequest/sendrequest';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'LandingPage';
  name: string ='';
  firstname: string ='';
  lastname: string ='';
  profileImage: string ='';
  showEventAddress =false;
  addressTitile: string='';
  user:any;
  usercomposedmsgId:any;
  composeMsg=false;
  msgId:any;
  newMessage:any;
  pages: Array<{title: string, component: any}>;
  addressText='';
  powerScore='0';

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public mobileAccessibility: MobileAccessibility, private nativePageTransitions: NativePageTransitions, private deeplinks: Deeplinks, public alertCtrl: AlertController,  public events: Events, private push: Push, private modalCtrl: ModalController, public sendrequest: SendrequestProvider) {
    if(localStorage.getItem('powerScore') != undefined) {
      this.powerScore = localStorage.getItem('powerScore');
    } 
    events.subscribe('powerScoreUpdate:created', (user, time) => {
      this.powerScore = localStorage.getItem('powerScore');
    });
    events.subscribe('updateProfie:created', (user, time) => {
     
      this.user = JSON.parse(localStorage.getItem('user'));;
      this.firstname = this.user.first_name;
      this.lastname = this.user.last_name;
      this.name=this.user.first_name.charAt(0)+""+this.user.last_name.charAt(0);
      if(user.photo !='' && user.photo !=null && user.photo !='null'){
          this.profileImage= user.photo;
      }
    });
    events.subscribe('gotoPage:created', (page, data) => {
      if(page=='CivicProfilePage') {
        this.nav.push(page,data,{animate:false});
      } else {
        this.nav.push(page,data,{animate:true, animation: 'ios-transition'});
      }
    });
    /* this.cacheService
        .get('addresslist')
        .mergeMap((cache: Cache<Addresslist[]>) => {
            this.cache = cache;
            return this.cache.get$;
        })
        .subscribe((addresslist:any) => {
            console.log("addresslist ", addresslist);
        });*/
    events.subscribe('setLatLon:created', (page, data) => {
      this.addressText = localStorage.getItem('addresstext');
    });
    if(localStorage.getItem('addresstext') != undefined) {
      this.addressText = localStorage.getItem('addresstext');
    }
    this.initializeApp();

   // this.logout(); 
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' },
      { title: 'List', component: 'ListPage' }
    ];
    this.platform.ready().then(() => {
      this.pushSetup();
     
      if(localStorage.getItem('countNewMessage') == undefined) {
        localStorage.setItem('countNewMessage','0');
      }
      if(localStorage.getItem('messageCount') == undefined) {
        localStorage.setItem('messageCount','0');
      }
      // this.fetchLatestMessage();
      setInterval(() => {this.fetchLatestMessage()},11000);
    })
     
    if(localStorage.getItem('user') !==null && localStorage.getItem('user') !='undefined'){
      let  user = JSON.parse(localStorage.getItem('user'));
      let firstname= user.first_name;
      let lastname= user.last_name;
      this.firstname = firstname;
      this.lastname = lastname;
      this.user = user;
      this.name=firstname.charAt(0)+""+lastname.charAt(0);
      if(user.photo !='' && user.photo !=null && user.photo !='null'){
          this.profileImage= user.photo;
      }
    }
    if (localStorage.getItem('showEventAddress') !==null && localStorage.getItem('showEventAddress') !='undefined') {
      if(localStorage.getItem('showEventAddress')=='true'){
        this.showEventAddress= true;
      }
      else {
        this.showEventAddress= false;
      }
    }

    if (localStorage.getItem('headerAddressTitle') !==null) {
    
        this.addressTitile = '';
    } else {
        if(localStorage.getItem('headerAddressTitle')  == 'voter') {
          localStorage.setItem('headerAddressTitle','PRIMARY RESIDENCE');
        }
        else if(localStorage.getItem('headerAddressTitle')  == 'registered') {
          localStorage.setItem('headerAddressTitle','VOTING ADDRESS');
        }            
        this.addressTitile=localStorage.getItem('headerAddressTitle') ;
    }
  }
  fetchLatestMessage() {
    
    if(localStorage.getItem('user') !==null && localStorage.getItem('user') !='undefined' && localStorage.getItem('onboardingStart') ==undefined){
      this.user =  JSON.parse(localStorage.getItem('user'));
      this.sendrequest.getResult('user/fetchLatestNewMailCountNew/' + this.user.id+'/2','get',{}).then((response:any) => { 
        let newMessage = 0;
        if(localStorage.getItem('user') !==null && localStorage.getItem('user') !='undefined'){
          if(localStorage.getItem('countNewMessage') != undefined) {
            newMessage = parseInt(localStorage.getItem('countNewMessage'));
          }
          console.log("newMessage : ", newMessage);
          console.log("getNewMailCount : ", response.data.getNewMailCount );
          localStorage.setItem('countNewMessage',response.data.getNewMailCount.toString());
          localStorage.setItem('suportOpportunity',response.data.suportOpportunity.toString());

          localStorage.setItem('getAllMessageTypes', JSON.stringify(response.data.getAllMessageTypes));
          console.log("getNewMailByTypes : ");
          localStorage.setItem('getNewMailListOrg', JSON.stringify(response.data.getNewMailListOrg));
          if(localStorage.getItem('getNewMailByTypes') != undefined && localStorage.getItem('getNewMailByTypes') != null) {
            localStorage.setItem('getNewMailByTypes', JSON.stringify(response.data.getNewMailByTypes));
          } else {
            localStorage.setItem('getNewMailByTypes', JSON.stringify(response.data.getNewMailByTypes));
            this.events.publish('messageCountChange:created', localStorage.getItem('messageCount'), Date.now()); 
          }
          localStorage.setItem('empowrdUnivesity',response.data.empowrdUnivesity.toString());
          localStorage.setItem('votinDateCount',response.data.votinDateCount.toString());
          localStorage.setItem('messages',response.data.messages.toString());
          localStorage.setItem('messageCount',response.data.messageCount.toString());
          this.events.publish('countNewMessage:created', localStorage.getItem('countNewMessage'), Date.now()); 
         
          if(response.data.getNewMailCount > newMessage) {

            this.newMessage =response.newMessageCount;
            this.msgId = response.data.id;
            if(response.data.composeMsg){
              this.composeMsg = true;
            } else{
              this.composeMsg = false;
            }

            let params:any = {
              'newMessagePopupSub': response.data.getLatestUnreadMessage.subject,
              'umid': response.data.id,
              'message_type_inbox': response.data.getLatestUnreadMessage.message_type_inbox,
              'newMessagePopupText': response.data.getLatestUnreadMessage.body,
              'senderName': response.data.getLatestUnreadMessage.senderName,
              'class': response.data.getLatestUnreadMessage.class,
              'adminName': response.data.getLatestUnreadMessage.adminName,
              'multipleMessage': response.data.getLatestUnreadMessage.multipleMessage,
              'MultipleUnReadMessage': response.data.MultipleUnReadMessage,
            }
            this.openNewMessagePopup(params);
           

            this.events.publish('messageCountChange:created', localStorage.getItem('messageCount'), Date.now()); 
          }
        }
      },
      error => {
       
      })
    }
  }
  openNewMessagePopup(params) {
    console.log(params);
    let modal = this.modalCtrl.create(NewMessageComponent, params,{
      cssClass: 'notice-approval-modal',
      enableBackdropDismiss : true,
    });
    modal.onDidDismiss((data) => {
      console.log(data);
      if(data!=null && data != undefined){
        if(data.messageDetail=='1') {
          if(this.composeMsg == false){
            this.nav.push('MsgDetailPage', { 'msgId':   this.msgId});
          } else {
            this.nav.push('MsgDetailPage', {'composeMessage':this.composeMsg, 'msgId':this.msgId }); //this.usercomposedmsgId
          }
          this.events.publish('msglistRefresh:created', this.user.id, Date.now());
        } 
        else if(data.messageDetail=='3') {
          
            this.nav.push('MsgDetailPage', { 'msgId':   data.message_id});
         
          this.events.publish('msglistRefresh:created', this.user.id, Date.now());
        } 
        else if(data.messageDetail=='2') {
          let view = this.nav.getActive();
          
          if(data.message_type_inbox != undefined) {
            localStorage.setItem('messageSelectedType', data.message_type_inbox);
            this.events.publish('messageSelectedType:created', this.user.id, Date.now());
          }
          if(view.component.name !='MsglistPage') {
              this.nav.push('MsglistPage',{},{animate:true, animation: 'ios-transition'});
          }
        }
        
      }
    });
    modal.present();
  }
  goToEventPage(pageIndex) {
    this.goToTabPage(pageIndex);
  }
  goToTabPage(pageIndex) {
    console.log(pageIndex);
    let view = this.nav.getActive();
    console.log(this.nav);
    if(view.component.name =='MainTabPage') {
      this.events.publish('selectedTabs:data',pageIndex, Date.now());
    } else {
      console.log("Elseee");
      this.nav.setRoot('MainTabPage',{ 'selectedIndex':pageIndex});
    }
  }
  goToUpcommingRace(pageIndex,type) {
    this.goToTabPage(pageIndex);
    if(pageIndex=='2') {
        this.events.publish('changeCivicDataNavTab:created',type, Date.now());
     
    } else {
      this.events.publish('changeElectionNavTab:created',type, Date.now());
    }
  }
  pushSetup() {
    const options: PushOptions = {
       android: {
        senderID:'526033488492'
       },
       ios: {
           alert: 'true',
           badge: true,
           sound: 'true'
       }
    }

    const pushObject: PushObject = this.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

    pushObject.on('registration').subscribe((registration: any) => {
      console.log("Registered Push notification : ");
     // alert(JSON.stringify(registration));
      console.log(registration.registrationId);
      console.log(registration);
      localStorage.setItem('registereddevice',registration.registrationId);
      console.log(localStorage.getItem('registereddevice'));
     
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

  }
  appClosePromt() {
    let alert = this.alertCtrl.create({
      title: "Empowrd",
      message: 'Do you want to exit?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            // Dismiss
          }
        },
        {
          text: 'Exit',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();  
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      let deeplinkActive=false;
      this.deeplinks.route({
        '/':{},
        '/emailverified/': {'page':'EmailVerifiedPage'},
        '/emailverified': {'page':'EmailVerifiedPage'},
        '/getemail/': {'page':'MyEmailAccountPage'},
        '/getemail': {'page':'MyEmailAccountPage'},
        '/main-tab/tab-2/home-opportunity/:card': {'page':'MainTabPage'},
        '/main-tab/tab-2/home-opportunity/:card/': {'page':'MainTabPage'},
        "/leader-detail/:position": {'page':'LeaderDetailPage'},
        "/leader-detail/:position/": {'page':'LeaderDetailPage'},

       }).subscribe((match:any) => {
        deeplinkActive=true;
        console.log( "match : ");
        console.log( match);
        if(match.$args.email != undefined) {
          localStorage.setItem('activePage','EmailVerifiedPage');
          this.nav.setRoot('EmailVerifiedPage',{'userId':match.$args.email,'verifyEmail':'1'},{animate:true, animation: 'ios-transition'});
        }
        if(match.$args.i != undefined) {
          this.nav.setRoot('MyEmailAccountPage',{'userId':match.$args.i},{animate:true, animation: 'ios-transition'});
        }
        if(match.$args.position != undefined) {
          this.nav.setRoot('LeaderDetailPage',{'position':match.$args.position},{animate:true, animation: 'ios-transition'});
        }
        if(match.$args.card != undefined) {
          this.nav.setRoot('MainTabPage',{'card':match.$args.card},{animate:true, animation: 'ios-transition'});
        }
        
       }, (nomatch:any) => {
         console.error("nomatch : ");
         console.error(nomatch);
         if(nomatch.$args !==null && nomatch.$args !='undefined' && nomatch.$args != undefined){
          deeplinkActive=true;
          if(nomatch.$args.email != undefined) {
            localStorage.setItem('activePage','EmailVerifiedPage');
            this.nav.setRoot('EmailVerifiedPage',{'userId':nomatch.$args.email,'verifyEmail':'1'},{animate:true, animation: 'ios-transition'});
          }
          if(nomatch.$args.i != undefined) {
            this.nav.setRoot('MyEmailAccountPage',{'userId':nomatch.$args.i},{animate:true, animation: 'ios-transition'});
          }
          if(nomatch.$args.position != undefined) {
            this.nav.setRoot('LeaderDetailPage',{'position':nomatch.$args.position},{animate:true, animation: 'ios-transition'});
          }
          if(nomatch.$args.card != undefined) {
            this.nav.setRoot('MainTabPage',{'card':nomatch.$args.card},{animate:true, animation: 'ios-transition'});
          }
         }
       });
       
       this.platform.registerBackButtonAction(() => {
          if (this.nav.canGoBack()) {
            
            this.nav.pop({ animate:true, duration:800, direction: 'back', animation: 'ios-transition'});
          } else {
            // Currently on root page
            this.appClosePromt();
          }
      });
      if(deeplinkActive==false) {
        if(localStorage.getItem('user') ===null || localStorage.getItem('user') =='undefined'){
          this.nav.setRoot('LandingPage');
          // this.nav.setRoot('InviteviaSmsPage');
        } else {
          this.buttonClicked();
          this.nav.setRoot('MainTabPage');
          // this.nav.setRoot('InviteviaEmailPage');
        }
      }
     // this.mobileAccessibility.usePreferredTextZoom(true);
      this.statusBar.styleDefault();
      //this.statusBar.styleBlackOpaque();
      //this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }
  buttonClicked() {
    this.sendrequest.getResult('user/buttonClicked/' + this.user.id+'/App Opened','get',{}).then((response:any) => { 
      this.sendrequest.updatePowerScore(this.user.id);
    },
    error => {
       
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component,{},{animate:true, animation: 'ios-transition'});
  }
  goToPage(page){
    this.nav.push(page,{},{animate:true, animation: 'ios-transition'});
  }
  goToCivicProfilePage() {
    this.nav.push('CivicProfilePage',{},{animate:false});
  }
  goToHelpPage() {
    this.nav.push('HelpPage',{},{animate:false});
  }
  goToPrivacyPolicy() {
    this.nav.push('PrivacyPolicyPage',{},{animate:false});
  }
  confirmCloseAccount() {
    let alert = this.alertCtrl.create({
      title: "Are you sure you want to close an account?",
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            // Dismiss
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.closeAccount();
          }
        }
      ]
    });
    alert.present();  
  }
  closeAccount() {
    this.sendrequest.getResult('user/deleteuser/' + this.user.id,'post',{}).then((response:any) => { 

    },
    error => {
       
    })
    this.logout();
  }
  goToMyDataSourcesPage() {
    this.nav.push('DataSourcesPage',{},{animate:false});
  }
  goToMyDataPage() {
    this.nav.push('MyDataPage',{},{animate:false});
  }
  logout(){
    localStorage.removeItem('auth-token');
    localStorage.removeItem('user');
    localStorage.removeItem('countNewMessage');
    localStorage.removeItem('registerStep');
    let registerId = localStorage.getItem('registereddevice');
    localStorage.clear();
    localStorage.setItem('registereddevice', registerId);
    //localStorage.clearCache();
    this.events.publish('getNewMessage:created', 'user', Date.now());
    this.nav.setRoot('LandingPage');
  }
}
