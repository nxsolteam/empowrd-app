import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IonicStorageModule } from '@ionic/storage';
import { CacheModule } from 'ionic-cache-observable';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SendrequestProvider } from '../providers/sendrequest/sendrequest';
import { CryptProvider } from '../providers/crypt/crypt';
import { PlaceholderProvider } from '../providers/placeholder/placeholder';
import { InterceptorProvider } from '../providers/interceptor/interceptor';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { AddresslistProvider } from '../providers/addresslist/addresslist';
import { LeaderlistProvider } from '../providers/leaderlist/leaderlist';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AutoCompleteModule } from 'ionic2-auto-complete';
import { OfficeSearchAutoCompleteProvider } from '../providers/office-search-auto-complete/office-search-auto-complete';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Deeplinks } from '@ionic-native/deeplinks';
import { Keyboard } from '@ionic-native/keyboard';
import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { DirectivesModule } from '../directives/directives.module';
import { Push } from '@ionic-native/push';
import { GooglePlus } from '@ionic-native/google-plus';
import { NewMessageComponent } from '../components/new-message/new-message';
import { NewMessageMenuComponent } from '../components/new-message-menu/new-message-menu';
// import { SuperTabsModule } from 'ionic2-super-tabs';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Contacts } from '@ionic-native/contacts';   
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { Facebook } from '@ionic-native/facebook';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { ElectionRacesProvider } from '../providers/election-races/election-races';
import { SelectedEventActivitiesProvider } from '../providers/selected-event-activities/selected-event-activities';
import { GetCausesListProvider } from '../providers/get-causes-list/get-causes-list';
import { GetOrgListProvider } from '../providers/get-org-list/get-org-list';
import { LocationStrategy, PathLocationStrategy, APP_BASE_HREF } from '@angular/common';
import { MyCommunityProvider } from '../providers/my-community/my-community';
import { EmojiProvider } from '../providers/emoji/emoji';
import { ChatServiceProvider } from '../providers/chat-service/chat-service';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';


@NgModule({
  declarations: [
    MyApp,
  //  ParallaxHeaderDirective,
  // CustomHeaderComponent,
    NewMessageComponent,
    NewMessageMenuComponent,

  ],
  imports: [
    BrowserModule,
    CacheModule,
    HttpClientModule,
    HttpModule,
    AutoCompleteModule,
  //  DirectivesModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp,{ locationStrategy: 'path' } ),
    NgxMaskIonicModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NewMessageComponent,
    NewMessageMenuComponent
  
  ],
  providers: [
    StatusBar,
    Geolocation,
    SplashScreen,
    Network,
    Camera,
    File,
    Push,
    Diagnostic,
    FileTransfer,
    MobileAccessibility,
    NativePageTransitions,
    Deeplinks,
    Keyboard,
    GooglePlus,
    Facebook,
    Contacts,
    SocialSharing,
    InAppBrowser,
    CallNumber,
    EmailComposer,
    LaunchNavigator,
    Location,
    {provide: LocationStrategy, useClass: PathLocationStrategy}, {provide: APP_BASE_HREF, useValue : '/' },
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SendrequestProvider,
    CryptProvider,
    PlaceholderProvider,
    InterceptorProvider,
    AddresslistProvider,
    LeaderlistProvider,
    OfficeSearchAutoCompleteProvider,
    ElectionRacesProvider,
    SelectedEventActivitiesProvider,
    GetCausesListProvider,
    GetOrgListProvider,
    MyCommunityProvider,
    EmojiProvider,
    ChatServiceProvider
  ],
  
})
export class AppModule {}
