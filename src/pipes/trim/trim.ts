import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TrimPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'trim',
})
export class TrimPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any) {
        if (!value) {
            return '';
        }

        return value.trim();
  }
}
