import { NgModule } from '@angular/core';
import { TrimPipe } from './trim/trim';
import { SafeHtmlPipe } from './safe-html/safe-html';
import { RelativeTimePipe } from './relative-time/relative-time';
@NgModule({
	declarations: [TrimPipe,
    SafeHtmlPipe,
    RelativeTimePipe],
	imports: [],
	exports: [TrimPipe,
    SafeHtmlPipe,
    RelativeTimePipe]
})
export class PipesModule {}
