import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySocialFollowersPage } from './my-social-followers';

@NgModule({
  declarations: [
    MySocialFollowersPage,
  ],
  imports: [
    IonicPageModule.forChild(MySocialFollowersPage),
  ],
})
export class MySocialFollowersPageModule {}
