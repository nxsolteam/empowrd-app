import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';   

/**
 * Generated class for the MySocialFollowersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-social-followers',
  templateUrl: 'my-social-followers.html',
})
export class MySocialFollowersPage {

  socialFollowers=true;
  user:any;
  checkShareTwitter=false;
  checkShareFacebook=false;
  invitedSMS=false;
  invitedEmail=false;
  checkShare=false;
  powerScore=0;
  private contactlist: any=[]; 
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, private socialSharing: SocialSharing, private sendrequest: SendrequestProvider, private contacts: Contacts) {

    this.user =  JSON.parse(localStorage.getItem('user'));
    this.powerScore = parseInt(localStorage.getItem('powerScore'));
  	events.subscribe('socialFollowers:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.socialFollowers=false;
      setTimeout(() => {
        this.socialFollowers=true;
      }, 500);
    });
    events.subscribe('invitedContacts:created', (user, time) => {
      this.invitedSMS=true;
      this.powerScore += (user * 5);
      this.sendrequest.updatePowerScore(this.user.id);
    });
    events.subscribe('invitedEmailContacts:created', (user, time) => {
      this.powerScore += (user * 5);
      this.invitedEmail=true;
      this.sendrequest.updatePowerScore(this.user.id);
    });
  }
  showPhonebook() {
    this.navCtrl.push('InviteviaSmsPage' , {animate:false});
  }
  showEmailContacts() {
    this.navCtrl.push('InviteviaEmailPage' , {animate:false});
  }
  ionViewDidLoad() {
    this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {
      for (var i = 0; i < contacts.length; i++) {
        if(contacts[i].phoneNumbers != null ){
          contacts[i].selected=false;
          contacts[i].showContacts=true;
          if(contacts[i].name==null) {
            contacts[i].name ={'formatted': this.getName(contacts[i])};
          } else if(contacts[i].name.formatted=='' || contacts[i].name.formatted==null) {
             contacts[i].name.formatted = (contacts[i].phoneNumbers.length > 0)?contacts[i].phoneNumbers[0].value:'';
          }
          this.contactlist.push({ 'emails':contacts[i].emails, 'phoneNumbers':contacts[i].phoneNumbers, 'name':contacts[i].name, 'displayName':contacts[i].displayName } );
        }
      }
      this.addPhoneBookContacts();
    }, (error) => {
      console.log(error);
    }); 
    /* console.log("Invite contacts ");
    let phoneArr = [];
    let contact = [];
    phoneArr.push({ 'value': 8160650688, 'id':111 });
    phoneArr.push({ 'value': 9913179768, 'id':113 });
    contact.push({ 'selected': false, 'showContacts':true, 'phoneNumbers':phoneArr, 'name': { 'formatted': 'Bhavesh Rabadiya'} });
    phoneArr = [];
    phoneArr.push({ 'value': 8000343638, 'id':112 });
    contact.push({ 'selected': false, 'showContacts':true, 'phoneNumbers':phoneArr, 'name': { 'formatted': 'Jatin Bhatt'} });
    this.contactlist=[];
    for (var i = 0; i < contact.length; i++) {
      if(contact[i].phoneNumbers != null ){
        contact[i].selected=false;
        contact[i].showContacts=true;
        if(contact[i].name==null) {
          contact[i].name ={'formatted': this.getName(contact[i])};
        } else if(contact[i].name.formatted=='' || contact[i].name.formatted==null) {
           contact[i].name.formatted = (contact[i].phoneNumbers.length > 0)?contact[i].phoneNumbers[0].value:'';
        }
        this.contactlist.push(contact[i]);
      }
    }
    this.addPhoneBookContacts(); */
  }
  getName(contact) {
    if(contact.name!=null) {
      if(contact.name.formatted=='') {
        return (contact.phoneNumbers.length > 0)?contact.phoneNumbers[0].value:'';
      } else {
        return contact.name.formatted;
      }
    } else if(contact.displayName!=null) {
      return contact.displayName;
    } else if(contact.phoneNumbers.length > 0) {
      return contact.phoneNumbers[0].value;
    } else {
      return '';
    }
  }
  addPhoneBookContacts() {
    this.sendrequest.getResult('user/addPhoneBookContacts','post',{ 'contacts': this.contactlist }).then((response:any) => {
    },
    error => {
    });
  }
  shareTwitter() {
    this.socialSharing.shareViaTwitter('Join me on EMPOWRD and become a more engaged voter, citizen and community member. Download today! http://onelink.to/xzexga', '', 'http://onelink.to/xzexga').then((result:any) => {
      console.log('share result ', result);
      if(this.checkShareTwitter==false) {
        this.powerScore += 5;
      }
      this.checkShareTwitter=true;
      this.buttonClicked('Twitter Share');
    },
    error => {
          console.log('share error ', error)
    })
  }
  share() {
    this.socialSharing.share('Join me on EMPOWRD and become a more engaged voter, citizen and community member. Download today! http://onelink.to/xzexga', '', null, null).then((result:any) => {
      console.log('share result ', result);
      if(this.checkShare==false) {
        this.powerScore += 5;
      }
      this.checkShare=true;
      this.buttonClicked('Share');
    },
    error => {
          console.log('share error ', error)
    })
  }
  buttonClicked(type) {
    this.sendrequest.getResult('user/buttonClicked/' + this.user.id+'/'+type,'get',{}).then((response:any) => { 
    	this.sendrequest.updatePowerScore(this.user.id);
    },
    error => {
    }) 
  }
  shareFacebook() {
    this.socialSharing.shareViaFacebook('Join me on EMPOWRD and become a more engaged voter, citizen and community member. Download today! http://onelink.to/xzexga', null, null).then((result:any) => {
      console.log('share result ', result);
      if(this.checkShareFacebook==false) {
        this.powerScore += 5;
      }
      this.checkShareFacebook=true;
      this.buttonClicked('Facebook Share');
    },
    error => {
          console.log('share error ', error)
    })
  }
  GoToNext(value){
  	this.navCtrl.pop({animate:true, duration:900, animation: 'ios-transition'});
    setTimeout(() => {
       this.socialFollowers=true;
    }, 1500);
  }
  goBack() {
  	this.events.publish('workwithOrganizationContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
  	setTimeout(() => {
  		this.socialFollowers=false;
    }, 100);
	this.navCtrl.pop({animate:true, duration:900, animation: 'ios-transition'});
	setTimeout(() => {
	   this.socialFollowers=true;
	}, 1500);
  }

}
