import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeaderListOnboardingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leader-list-onboarding',
  templateUrl: 'leader-list-onboarding.html',
})
export class LeaderListOnboardingPage {
  cityCheckbox =false;
  searchBarSection=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderListOnboardingPage');
  }
  showSearchBarSection() {
    this.searchBarSection= !this.searchBarSection;
  }
}
