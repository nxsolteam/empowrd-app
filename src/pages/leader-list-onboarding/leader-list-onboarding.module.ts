import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaderListOnboardingPage } from './leader-list-onboarding';

@NgModule({
  declarations: [
    LeaderListOnboardingPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaderListOnboardingPage),
  ],
})
export class LeaderListOnboardingPageModule {}
