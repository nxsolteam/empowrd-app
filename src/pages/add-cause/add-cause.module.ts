import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectivesModule } from '../../directives/directives.module';
import { AddCausePage } from './add-cause';


@NgModule({
  declarations: [
    AddCausePage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(AddCausePage),
  ],
})
export class AddCausePageModule {}
