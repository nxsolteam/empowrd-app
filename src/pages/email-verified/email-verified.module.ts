import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailVerifiedPage } from './email-verified';


@NgModule({
  declarations: [
    EmailVerifiedPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailVerifiedPage),
  ],
})
export class EmailVerifiedPageModule {}
