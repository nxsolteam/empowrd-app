import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { GetCausesListProvider } from '../../providers/get-causes-list/get-causes-list';
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';

/**
 * Generated class for the EmailVerifiedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-email-verified',
  templateUrl: 'email-verified.html',
})
export class EmailVerifiedPage {
  user:any;
  name:string;
  emailVerifiedShowContent=true;
  addresslist:any;
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, public loadingCtrl: LoadingController, private leaderlistProvider: LeaderlistProvider, private addresslistProvider: AddresslistProvider, private cacheService: CacheService, private electionRacesProvider: ElectionRacesProvider, private getCausesListProvider: GetCausesListProvider, private getOrgListProvider: GetOrgListProvider) {
    events.subscribe('emailVerifiedShowContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.emailVerifiedShowContent=false;
      setTimeout(() => {
        this.emailVerifiedShowContent=true;
      }, 500);
    });
     console.log("Email Verified");
    console.log(localStorage.getItem('user') );
    if(localStorage.getItem('user') !=undefined ){ 
  	 this.user =  JSON.parse(localStorage.getItem('user'));
     if(this.user.first_name==undefined) {
      this.registerUser();
     } else {
      this.name = this.user.first_name;
      }
    } else {
      this.registerUser();
    }
  }
  registerUser() {
    this.events.publish('emailVerifySubscription:created', '', Date.now());
      let responseData:any =  JSON.parse(localStorage.getItem('responseData'));
      this.name = responseData.first_name;
     
      this.sendrequest.authregister(responseData).then((response:any) => { 
        
        this.sendrequest.authtoken = response.data.token;
        localStorage.setItem('user', JSON.stringify(response.data.user));
        localStorage.setItem('auth-token', response.data.token);
        this.user = response.data.user;
        localStorage.setItem('currentLocationClicked','false');
        let type:string="voter";
        localStorage.setItem('headerAddressTitle',"PRIMARY RESIDENCE");
        this.getCausesListProvider.getCausesListRefresh();
        this.getOrgListProvider.getOrgListRefresh();
        this.sendrequest.getResult("user/saveAddress/" + this.user.id,'post',{
            address: localStorage.getItem('addresstext'),
            type: type
        }).then((response:any) => { 
          this.cacheService
          .get('addresslist')
          .mergeMap((cache: Cache<Addresslist[]>) => {
              this.cache = cache;
              return this.cache.get$;
          })
          .subscribe((addresslist:any) => {
               console.log("register address list cache register");
              this.addresslist=addresslist;
              localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
              localStorage.setItem('addressId' , this.addresslist.data.addresses[0].id); 
              localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
              localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
              localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);

              if(localStorage.getItem('latitude') ==null || localStorage.getItem('latitude') =='undefined'){
                this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + this.user.id,'get',{}).then((response:any) => {     
                  var results = response.data.response.results[0];
                  localStorage.setItem('latitude',results.location.lat.toString());
                  localStorage.setItem('longitude',results.location.lng.toString());
                  this.leaderlistProvider.leaderlistRefresh();
                  this.electionRacesProvider.electionRaceRefresh();
                });
              } else {
                this.leaderlistProvider.leaderlistRefresh();
                this.electionRacesProvider.electionRaceRefresh();
              }
          });
           
        },
        error => {
          
        });
        this.sendrequest.sendFriendNotification();
      },
      error => {
        
          this.sendrequest.presentToast('Error Occurred. Please check your connection!');
      })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EmailVerifiedPage');
  }
  goBack() {
    console.log("clicked");
    // this.events.publish('verifyEmailContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, duration:900, animation: 'ios-transition'});
  }
  goToRoot() {
    //this.navCtrl.popToRoot({animate:false});
    setTimeout(() => {
      this.emailVerifiedShowContent=false;
    }, 100);
    if(localStorage.getItem('modelRegistration')=='true') {
      localStorage.removeItem('modelRegistration');
      if(localStorage.getItem('position_id')!=undefined) {
        this.navCtrl.setRoot('LeaderDetailPage',{ 'position':localStorage.getItem('position_id') },{animate:true, animation: 'ios-transition'});
      } else {
        this.navCtrl.setRoot('MainTabPage',{ 'powerScore': 0 },{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
      }
    } else {
      this.navCtrl.setRoot('MainTabPage',{ 'powerScore': 0 },{animate:true, direction: 'forward', animation: 'ios-transition'});
    }
    setTimeout(() => {
      this.emailVerifiedShowContent=true;
    }, 1500);
  }
  getStarted() {
    localStorage.removeItem('responseData');
  	localStorage.setItem('registerStep','getstarted');
    setTimeout(() => {
      this.emailVerifiedShowContent=false;
    }, 100);
    if(localStorage.getItem('modelRegistration')=='true') {
      localStorage.removeItem('modelRegistration');
      this.navCtrl.setRoot('LeaderDetailPage',{ 'position':localStorage.getItem('position_id') },{animate:true, animation: 'ios-transition'});
    } else {
      this.navCtrl.push('CreateProfilePage',{},{animate:true, animation: 'ios-transition'});
    }
    setTimeout(() => {
      this.emailVerifiedShowContent=true;
    }, 1500);
  }
}
