import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { SocialSharing } from '@ionic-native/social-sharing';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {
  @ViewChild(Slides) slides: Slides;

  public zoomLevel: number = 100;
  currentIndex:number=1;
  option = 
    {
      loop: false,
      prerender: false,
      autoplay: true,
      autoloadSegments: false,
      path: 'assets/img/landing-gif/EMPOWRD-LogoAnimation.js'
    };
  constructor(public navCtrl: NavController, public navParams: NavParams,public mobileAccessibility: MobileAccessibility, public platform: Platform, private nativePageTransitions: NativePageTransitions, private fb: Facebook, private socialSharing: SocialSharing, private statusBar: StatusBar) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    

    //this.goToSlide();
   // this.mobileAccessibility.isScreenReaderRunning(this.isScreenReaderRunningCallback);
   /* this.platform.ready().then(() => {
      if(this.mobileAccessibility.isScreenReaderRunning()){
        console.log("screen reader is running");
      } else {
        console.log("screen reader isn't running");
      }

      this.mobileAccessibility.getTextZoom().then(e => this.zoomLevel = e);
    }) */

  }
  fbLogin() {
    this.fb.login(['public_profile', 'email'])
    .then((res: FacebookLoginResponse) => {
      //alert(JSON.stringify(res));
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then((profile:any) => {
      //  alert(JSON.stringify(profile));
      });
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }
  slideReachStart() {
   this.currentIndex = this.slides.getActiveIndex();
  }
  ionViewWillEnter() {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
  }
  ionViewDidEnter() {
    // this.slides.autoplayDisableOnInteraction = false;
  }
  slideDidChange() {
  }
  goToTab(index){
     this.slides.slideTo(index, 0);
  }
  slideChanged() {
  }
  
  updateZoom() {
    this.mobileAccessibility.setTextZoom(this.zoomLevel);
  }

    

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
   
  }
  gotoLogin(){
  	this.navCtrl.push("LoginPage",{},{animate:false}); 
    /*
    this.socialSharing.canShareVia('com.facebook.katana', "Join me on EMPOWRD and become a more engaged voter, citizen and community member. Download today!", null, null, 'https://www.empowrd.com/').then((result:any) => {
      console.log('share result ', result);
      
    },
    error => {
          console.log('share error ', error)
    }).catch((error) => {
      console.log('catch error ', error)

    }); */
  }
  gotoSignupPage(){
  	this.navCtrl.push('MobileNumberVerificationPage',{},{animate:false});
    /* this.socialSharing.shareViaFacebook('Join me on EMPOWRD and become a more engaged voter, citizen and community member. Download today! http://onelink.to/xzexga', null, null).then((result:any) => {
      
    },
    error => {
          console.log('share error ', error)
    }) */
  }

}
