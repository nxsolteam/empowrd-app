import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandingPage } from './landing';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    LandingPage,
  ],
  imports: [
    IonicPageModule.forChild(LandingPage),
    LottieAnimationViewModule.forRoot(),
  ],
})
export class LandingPageModule {}
