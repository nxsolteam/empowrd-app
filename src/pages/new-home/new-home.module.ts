import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewHomePage } from './new-home';
import { ComponentsModule }from '../../components/components.module';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    NewHomePage,
  ],
  imports: [
    IonicPageModule.forChild(NewHomePage),
    DirectivesModule,
    ComponentsModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class NewHomePageModule {}
