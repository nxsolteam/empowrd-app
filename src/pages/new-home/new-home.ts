import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Events, Content, Select, AlertController, Platform } from 'ionic-angular';
import { Map, tileLayer, marker, geoJSON, LayerOptions, icon, circleMarker } from 'leaflet';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Slides } from 'ionic-angular';
declare var google;
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header'
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { PlaceholderProvider } from '../../providers/placeholder/placeholder';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the NewHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-home',
  templateUrl: 'new-home.html',
})
export class NewHomePage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('electionDrownDown') electionDrownDown: Select;

  map: any;
  messageCount='0';
  loadPage=false;
  latLons:any=[];
  maillatLons:any=[];
  spaceBetween:any;
  virtualTranslate:any;
  options:any;
  PrimarylatLons:any=[];
  @ViewChild('orgMessageSlider') orgMessageSlider: Slides;
  showLoader=true;
  slideSecondMapElement:any=[];
  slideFirstMap: any;
  slideFiveMap: any;
  slideSecondMap: any=[];
  slideThirdMap: any=[];
  slideFourMap: any=[];
  newMarker:any;
  shortAddress='';
  addressOnHeader='';
  votingaddressdate='';
  mailInvotingaddressdate='';
  electionDateType='';
  mailInelectionDateType='';
  activeIndexOfSlide=0;//MAULIK
  scrallDirection='';//MAULIK
  user:any;
  validateAddress=false;
  showLobbyCard=true;
  validAddressIcon="assets/img/group-5.svg";
  votingLocationData:any = [];
  mailInVotingLocationData:any = [];
  primaryLocationData:any = [];
  generalLocationData:any = [];
  specialLocationData:any = [];
  primarySelectedData:any = {};
  generalSelectedData:any = {};
  specialSelectedData:any = {};
  voting:any = {};
  mailInVoting:any = {};

  primarySelectedIndex=0;
  votingSelectedIndex=0;
  mailInVotingSelectedIndex=0;
  specialSelectedIndex=0;
  generalSelectedIndex=0;
  firstmoredays=0;
  earlyVotingDays='';
  earlyPrimaryDays='';
  earlyGeneralDays='';
  specialElectionDays='';
  generalElectionDate='';
  primaryElectionDate='';
  specialElectionDate='';
  showDropDown = false;
  lobbyCardMoreDaysText='';
  lobbyMessageCount='0';
  lobbyNewMessageCount='0';
  lobbyCardState='';
  lobbyCardDate='';
  nonVerifyUrl='';
  hideFirstSlide=false;
  earlyVotingLocationEnable=true;
  earlyMailInVotingLocationEnable=true;
  earlyPrimaryEnable=true;
  earlySpecialEnable=true;
  earlyGeneralEnable=true;
  firstSlideText='';
  firstSlideTitle='CHECK YOUR VOTER REGISTRATION STATUS';
  powerScore='0';
  earlyVotingLocationDate='';
  earlyMailInVotingLocationDate='';
  votingOpportunity = '0';
  suportOpportunity = '0';
  empowrdUnivesity= '0';
  messages = '0';
  initialName = '';
  messageTitle = '';
  messageText = '';
  navMenu='getEngaged';
  empowrdUnivesityMessageData:any=[];
  text='';
  problem='';
  cardCount=0;
  getNewMailByTypes:any=[];
  getNewMailListOrg:any=[];
  currentDay:any;
  currentDate:any;
  engagedNewMessage=0;
  primaryElectionData:any =[];
  specialElectionData:any =[];
  generalElectionData:any =[];
  firstmoredaysText='';
  earlyVotingDaysText='';
  earlyMailInVotingDaysText='';
  loadedOrgMessageSlider=false;

  congrationalCardMoreDaysText='';
  congrationalCardDate='';
  congressionalTextTitle='';
  congressionalText='';
  lobbyTextTitle='';
  lobbyText='';
  showCongrationalCard=false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public events: Events, private iab: InAppBrowser, private placeholderProvider: PlaceholderProvider, private launchNavigator: LaunchNavigator, private alertCtrl: AlertController, private el:ElementRef, private statusBar: StatusBar, public platform: Platform, private sanitizer: DomSanitizer) {
   this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.messageCount = localStorage.getItem('messageCount');
    this.currentDay = moment().format("dddd");
    this.currentDate = moment().format("MMM. DD, YYYY");

    events.subscribe('countNewMessage:created', (user, time) => {
      this.suportOpportunity = localStorage.getItem('suportOpportunity');
      this.messages = localStorage.getItem('messages');
      this.empowrdUnivesity = localStorage.getItem('empowrdUnivesity');
      this.votingOpportunity = localStorage.getItem('votinDateCount');
      this.engagedNewMessage = 0;
      for(var i=0; i< this.getNewMailByTypes.length; i++) {
        this.engagedNewMessage += this.getNewMailByTypes[i].new_message_count;
        if(this.getNewMailByTypes[i].message_type_inbox=='LOBBY'){
          this.lobbyNewMessageCount=this.getNewMailByTypes[i].new_message_count.toString();
          this.lobbyMessageCount=this.getNewMailByTypes[i].message_count.toString();
        }
      }
      
    });
    events.subscribe('messageCountChange:created', (user, time) => {
      this.empowrdUnivesityMessageData = JSON.parse(localStorage.getItem('empowrdUnivesityMessageData'));
      this.getNewMailByTypes = JSON.parse(localStorage.getItem('getNewMailByTypes'));
      this.engagedNewMessage = 0;
      for(var i=0; i< this.getNewMailByTypes.length; i++) {
        this.engagedNewMessage += this.getNewMailByTypes[i].new_message_count;
        if(this.getNewMailByTypes[i].message_type_inbox=='LOBBY'){
          this.lobbyNewMessageCount=this.getNewMailByTypes[i].new_message_count.toString();
          this.lobbyMessageCount=this.getNewMailByTypes[i].message_count.toString();
        }
      }
      this.getNewMailListOrg = JSON.parse(localStorage.getItem('getNewMailListOrg'));
      setTimeout(() => {
       this.loadedOrgMessageSlider=true;
      },400); 
    });
    if(localStorage.getItem('getNewMailListOrg') != undefined) {
      this.getNewMailListOrg = JSON.parse(localStorage.getItem('getNewMailListOrg'));
      setTimeout(() => {
       this.loadedOrgMessageSlider=true;
      },400);
    }
    if(localStorage.getItem('empowrdUnivesityMessageData') != undefined) {
      this.empowrdUnivesityMessageData = JSON.parse(localStorage.getItem('empowrdUnivesityMessageData'));
    }
    if(localStorage.getItem('getNewMailByTypes') != undefined) {
      this.getNewMailByTypes = JSON.parse(localStorage.getItem('getNewMailByTypes'));
      this.engagedNewMessage = 0;
      for(var i=0; i< this.getNewMailByTypes.length; i++) {
        this.engagedNewMessage += this.getNewMailByTypes[i].new_message_count;
        if(this.getNewMailByTypes[i].message_type_inbox=='LOBBY'){
          this.lobbyNewMessageCount=this.getNewMailByTypes[i].new_message_count.toString();
          this.lobbyMessageCount=this.getNewMailByTypes[i].message_count.toString();
        }

      }
    }
   
    if(localStorage.getItem('votinDateCount') != undefined) {
     this.votingOpportunity = localStorage.getItem('votinDateCount');
    }
    if(localStorage.getItem('suportOpportunity') != undefined) {
      this.suportOpportunity = localStorage.getItem('suportOpportunity');
    }
    if(localStorage.getItem('empowrdUnivesity') != undefined) {
      this.empowrdUnivesity = localStorage.getItem('empowrdUnivesity');
    }
    if(localStorage.getItem('messages') != undefined) {
      this.messages = localStorage.getItem('messages');
    }
    let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
    if(totalpowerScore > 999) {
      this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
    } else {
      this.powerScore = localStorage.getItem('powerScore');
    }
    
    events.subscribe('setLatLon:created', (user, time) => {
      if(localStorage.getItem('validateAddress')!= undefined) {
        if(localStorage.getItem('validateAddress')=='0') {
          this.validateAddress=false;
          this.validAddressIcon="assets/img/group-5.svg";
        } else {
          this.validateAddress=true;
          this.validAddressIcon="assets/img/group-5.svg";
        }
      }
      this.lobbyCardState=localStorage.getItem('stateShortName');
      if(localStorage.getItem('addresstext')!= undefined) {
      console.log('addresstext', localStorage.getItem('addresstext'));
        this.addressOnHeader = localStorage.getItem('addresstext');
        this.shortAddress = this.addressOnHeader.substring(0, 22);
      }
      this.earlyVotingLocationEnable=true;
    });
    if(localStorage.getItem('validateAddress')!= undefined) {
      if(localStorage.getItem('validateAddress')=='0') {
        this.validateAddress=false;
      } else {
        this.validateAddress=true;
      }
    }
    if(localStorage.getItem('addresstext')!= undefined) {
      this.addressOnHeader = localStorage.getItem('addresstext');
      this.shortAddress = this.addressOnHeader.substring(0, 22);
      this.lobbyCardState=localStorage.getItem('stateShortName');
    }
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.initialName = this.user.first_name[0]+""+this.user.last_name[0];
  }
  getMessageType(type) {
      if(type=='ANNOUNCEMENT'){
        return 'BULLETINS';
      } else if(type=='MEMBERSHIP INVITE') {
        return 'INVITES';
      } else {
        return type;
      }
  }
  handleClick(event) {
    console.log(event);
    console.log(event.target.parentNode.tagName);
    let url='';
    if (event.target.tagName == "A") { 
      // url = event.target.innerText.trim();
      url = event.target.getAttribute("open-externalurl");

    } else if (event.target.parentNode.tagName == "A") { 
      url = event.target.parentNode.getAttribute("open-externalurl");

      //url = event.target.parentNode.innerText.trim();
    }
    console.log(url);
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    if(url != '' && url != undefined) {
      this.iab.create(url,target,options); 
    }
  }
  backAddress(type) {
    let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
    if(type=='voting') {
      if(this.votingSelectedIndex!=0) {
        this.votingSelectedIndex--;
      }
      if(this.votingSelectedIndex < this.votingLocationData.length) {
        this.voting = this.votingLocationData[this.votingSelectedIndex];
        
        let earlylatVoting=this.voting.latitude;
        let earlylonVoting=this.voting.longitude;
        let mapOptionsVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatVoting, earlylonVoting), // New York
          styles: mapStyle
        };
        this.slideFirstMap = new google.maps.Map(document.getElementById('slideFirstMapElement'), mapOptionsVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatVoting, earlylonVoting),
            map: this.slideFirstMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    }
    else if(type=='mailInVoting') {
      if(this.mailInVotingSelectedIndex!=0) {
        this.mailInVotingSelectedIndex--;
      }
      if(this.mailInVotingSelectedIndex < this.mailInVotingLocationData.length) {
        this.mailInVoting = this.mailInVotingLocationData[this.mailInVotingSelectedIndex];
        
        let earlylatmailVoting=this.mailInVoting.latitude;
        let earlylonmailVoting=this.mailInVoting.longitude;
        let mapOptionsmailVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting), // New York
          styles: mapStyle
        };
        this.slideFiveMap = new google.maps.Map(document.getElementById('slideFiveMapElement'), mapOptionsmailVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting),
            map: this.slideFiveMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    }
  }
  nextAddress(type) {
    let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
    if(type=='voting') {
      this.votingSelectedIndex++;
      if(this.votingSelectedIndex < this.votingLocationData.length) {
        this.voting = this.votingLocationData[this.votingSelectedIndex];
        
        let earlylatVoting=this.voting.latitude;
        let earlylonVoting=this.voting.longitude;
        let mapOptionsVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatVoting, earlylonVoting), // New York
          styles: mapStyle
        };
        this.slideFirstMap = new google.maps.Map(document.getElementById('slideFirstMapElement'), mapOptionsVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatVoting, earlylonVoting),
            map: this.slideFirstMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    } else if(type=='mailInVoting') {
      this.mailInVotingSelectedIndex++;
      if(this.mailInVotingSelectedIndex < this.mailInVotingLocationData.length) {
        this.mailInVoting = this.mailInVotingLocationData[this.mailInVotingSelectedIndex];
        
        let earlylatmailVoting=this.mailInVoting.latitude;
        let earlylonmailVoting=this.mailInVoting.longitude;
        let mapOptionsmailVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting), // New York
          styles: mapStyle
        };
        this.slideFiveMap = new google.maps.Map(document.getElementById('slideFiveMapElement'), mapOptionsmailVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting),
            map: this.slideFiveMap,
            icon:'assets/img/early_vote_4.svg',
          });
        }
      }
    } else if(type=='primary') {
      this.primarySelectedIndex++;
      if(this.primarySelectedIndex < this.primaryLocationData.length) {
        this.primarySelectedData = this.primaryLocationData[this.primarySelectedIndex];
      }
      let primarylatVoting=this.primarySelectedData.latitude;
      let primarylonVoting=this.primarySelectedData.longitude;
      
      let primaryEleMapOptions = {
        zoom: 16,
        gestureHandling: 'none',
        zoomControl: false,
        fullscreenControl: false,
        mapTypeControl: false, 
        center: new google.maps.LatLng(primarylatVoting, primarylonVoting), // New York
        styles: mapStyle
      };
      this.slideFourMap = [];
      for(var j=0; j < this.primaryElectionData.length; j++) {
        this.slideFourMap[j] = new google.maps.Map(document.getElementById('slideFourthMapElement_'+this.primaryElectionData[j].id), primaryEleMapOptions);
      }
      for(var i=0; i< this.PrimarylatLons.length; i++) {
                  
        
        for(var k=0; k < this.slideFourMap.length; k++) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(this.PrimarylatLons[i].latitude, this.PrimarylatLons[i].longitude),
            map: this.slideFourMap[k],
            icon:'assets/img/early_vote_4.svg',
          });
        } 
      }

    } else if(type=='special') {
      this.specialSelectedIndex++;
      if(this.specialSelectedIndex < this.specialLocationData.length) {
        this.specialSelectedData = this.specialLocationData[this.specialSelectedIndex];
        let speciallatVoting=this.specialSelectedData.latitude;
        let speciallonVoting=this.specialSelectedData.longitude;
        let specialEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(speciallatVoting, speciallonVoting), // New York
          styles: mapStyle
        };
        this.slideSecondMap=[];
        for(var j=0; j < this.specialElectionData.length; j++) {
          this.slideSecondMap[j] = new google.maps.Map(document.getElementById('slideSecondMapElement_'+this.specialElectionData[j].id), specialEleMapOptions);
        }
        
        for(var k=0; k < this.slideSecondMap.length; k++) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(speciallatVoting, speciallonVoting),
            map: this.slideSecondMap[k],
            icon:'assets/img/early_vote_4.svg',
          });
        }
      }
    } else {
      this.generalSelectedIndex++;
      if(this.generalSelectedIndex < this.generalLocationData.length) {
        this.generalSelectedData = this.generalLocationData[this.generalSelectedIndex];
      }
      let generallatVoting=this.generalSelectedData.latitude;
      let generallonVoting=this.generalSelectedData.longitude;

      let generalEleMapOptions = {
        zoom: 16,
        gestureHandling: 'none',
        zoomControl: false,
        fullscreenControl: false,
        mapTypeControl: false, 
        center: new google.maps.LatLng(generallatVoting, generallonVoting), // New York
        styles: mapStyle
      };
      this.slideThirdMap=[];
      for(var j=0; j < this.generalElectionData.length; j++) {
        this.slideThirdMap[j] = new google.maps.Map(document.getElementById('slideThirdMapElement_'+this.generalElectionData[j].id), generalEleMapOptions);
      }
      for(var k=0; k < this.slideThirdMap.length; k++) {
        var markerTwo = new google.maps.Marker({
          position: new google.maps.LatLng(generallatVoting, generallonVoting),
          map: this.slideThirdMap[k],
          icon:'assets/img/early_vote_4.svg',
        });
      }
    }
  }
  removeOrgMessage(getNewMail) {
    let umid = getNewMail.umid;
    this.getNewMailListOrg.some(function(item, index) {
      if(this.getNewMailListOrg[index].umid === getNewMail.umid){
        this.getNewMailListOrg.splice(index, 1);
      }
    }, this);
    if(this.getNewMailListOrg.length==1) {
      this.orgMessageSlider.slideTo(0);
    }
    localStorage.setItem('getNewMailListOrg', JSON.stringify(this.getNewMailListOrg));
  
    /*
    this.sendrequest.getResult('user/removeMessage','post',{ 'umid': umid }).then((response:any) => { 

    },
    error => {
      
    }) */
  }
  removeEmpMessage(empowrdUnivesityMessage) {
    let umid = empowrdUnivesityMessage.umid;
    this.empowrdUnivesityMessageData.some(function(item, index) {
      if(this.empowrdUnivesityMessageData[index].umid === empowrdUnivesityMessage.umid){
        this.empowrdUnivesityMessageData.splice(index, 1);
      }
    }, this);
    this.sendrequest.getResult('user/removeMessage','post',{ 'umid': umid }).then((response:any) => { 

    },
    error => {
      
    })
  }
  segmentChanged(event) {

  }
  editAddress() {
    this.events.publish('gotoPage:created', 'AddEditAddressPage', {});
  }
  sendCommunity() {
    this.sendrequest.getResult('user/sendhelp/' + this.user.id,'post',{ 'problem': this.problem, 'text': this.text, 'user': this.user }).then((response:any) => { 

    },
    error => {
      
    })
    this.problem='';
    this.text='';
    this.sendrequest.presentToast('Thank you for given feedback.');
  }
  ionViewDidEnter() {
    console.log("ionViewDidEnter New Home Page");
    //this.loadmap();
    
    // this.events.publish('homeOpportunityScreen:created', 'true', Date.now());
  }
  public getSafehtml(html:string){
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  viewBallotDetails(date, type) {
    date =  moment(date, "MM.DD.YYYY").format('YYYY-MM-DD');
    this.events.publish('gotoPage:created', 'ElectionsPage',  { 'date': date, 'type': type, 'menuType' : 'UPCOMING RACES'});

    //this.events.publish('selectedTabs:data','4', { 'date': date, 'type': type, 'menuType' : 'UPCOMING RACES'});
    setTimeout(() => {
      this.events.publish('changeElectionNavTab:created','UPCOMING RACES',{ 'date': date, 'type': type});
    },400);
  }
 
  goToContactUs() {
   this.events.publish('gotoPage:created', 'HelpPage', {});
  }
  showMapDirections(address) {
    let lat=parseFloat(localStorage.getItem('latitude'));
    let lon=parseFloat(localStorage.getItem('longitude'));
    let options: LaunchNavigatorOptions = { 
      start:[lat, lon]
      // app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    console.log("address", address);
    console.log(options);
    this.launchNavigator.navigate(address, options)
    .then(
      (success:any) => console.log('Launched navigator'),
      (error:any) => console.log('Error launching navigator', error)
    );
  }
  openMapPopUp(address) {
    let alert = this.alertCtrl.create({
      title: 'Would you like driving directions to this location?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.showMapDirections(address);
          }
        }
      ]
    });
    alert.present();
  }
  messageDetail(umid) {
    this.events.publish('gotoPage:created', 'MsgDetailPage', { 'msgId': umid });
    this.placeholderProvider.refresh();
  }
  showOpportunity() {
    this.events.publish('selectedTabs:data','3', Date.now());
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NewHomePage');
    this.orgMessageSlider.freeMode = false;
    this.orgMessageSlider.centeredSlides = true;
    this.orgMessageSlider.direction = 'horizontal';
    this.orgMessageSlider.autoHeight = false;
    this.orgMessageSlider.pager = false;
    this.orgMessageSlider.loop = false;
    this.orgMessageSlider.spaceBetween = 17;
    this.orgMessageSlider.zoom = false;
    this.orgMessageSlider.speed = 500;
    this.orgMessageSlider.slidesPerView = '1.2';
    this.orgMessageSlider.effect = 'slide';
      setTimeout(() => {this.loadPage=true},1000);

    
  }
  gotoElectionsPage() {
    this.events.publish('selectedTabs:data','6', Date.now());
  }
  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
    this.events.publish('addressDropDown:created', {'status':this.showDropDown }, Date.now());
  }
  logScrollStart() {
    // console.log("Scroll Start");
    // document.getElementById("headerNavbarHomeOpportunity").classList.add("fadeUp");
    // document.getElementById("hidetabs").classList.add("inProgress");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("headerNavbarHomeOpportunity").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
    this.scrallDirection = $event.directionY;
    if (this.scrallDirection == "down") {
      // document.getElementById("hidetabs").classList.add("inProgress");
      // document.getElementById("headerNavbarHomeOpportunity").classList.add("fadeUp");
    }
    if (this.scrallDirection == "up") {
      // document.getElementById("hidetabs").classList.remove("inProgress");
      // document.getElementById("headerNavbarHomeOpportunity").classList.remove("fadeUp");
    }
    if ($event.scrollTop > 100) {
      // document.getElementById("headerNavbarHomeOpportunity").classList.add("shadow");
    } else {
      // document.getElementById("headerNavbarHomeOpportunity").classList.remove("shadow");
    }

  }
  verifyLocation() {

    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(this.nonVerifyUrl,target,options);
  }
  showMessages(type, count) {
    //type
    if(count!='0') {
      localStorage.setItem('messageSelectedType', type);
      this.events.publish('messageSelectedType:created', type, Date.now());
      this.events.publish('gotoPage:created', 'MsglistPage', {});
    }
  }
}
