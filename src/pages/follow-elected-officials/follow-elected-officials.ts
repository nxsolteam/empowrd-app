import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the FollowElectedOfficialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-follow-elected-officials',
  templateUrl: 'follow-elected-officials.html',
})
export class FollowElectedOfficialsPage {

  followElectedOfficials=true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
  	events.subscribe('followElectedOfficials:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.followElectedOfficials=false;
      setTimeout(() => {
        this.followElectedOfficials=true;
      }, 300);
    });
  }

  ionViewDidLoad() {
    
  }
  GoToNext(value){
  	setTimeout(() => {
  		this.followElectedOfficials=false;
    }, 100);
	this.navCtrl.push('SocialFollowersPage',{},{animate:true, animation: 'ios-transition'});
	setTimeout(() => {
	   this.followElectedOfficials=true;
	}, 1000);
  }
  goBack() {
  	this.events.publish('createProfileContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
  	setTimeout(() => {
  		this.followElectedOfficials=false;
    }, 100);
	this.navCtrl.setRoot('MainTabPage',{},{animate:true, animation: 'ios-transition'});
	setTimeout(() => {
	   this.followElectedOfficials=true;
	}, 1000);
  }

}
