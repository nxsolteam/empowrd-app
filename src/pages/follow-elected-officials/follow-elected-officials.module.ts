import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowElectedOfficialsPage } from './follow-elected-officials';

@NgModule({
  declarations: [
    FollowElectedOfficialsPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowElectedOfficialsPage),
  ],
})
export class FollowElectedOfficialsPageModule {}
