import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { SelectedEventActivitiesProvider } from '../../providers/selected-event-activities/selected-event-activities';

/**
 * Generated class for the AddEventsActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-events-activities',
  templateUrl: 'add-events-activities.html',
})
export class AddEventsActivitiesPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;

  selectedEvent:any=[];
  user:any;
  showall='0';
  allEventActivity:any;
  eventActivity:any;
  selectedEventActivity = [];
  searchValue='';
  eventName='';
  totalSelected=0;
  powerScore='0';
  loaderShow=true;
  fromEditActivity=false;
  events=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private selectedEventActivitiesProvider: SelectedEventActivitiesProvider, public event: Events) {
  	this.powerScore = localStorage.getItem('powerScore');
  	this.selectedEvent = navParams.get('selectedEvent');
    this.eventName=localStorage.getItem('selectedEventName');
    if(navParams.get('events')!=undefined) {
      this.events = navParams.get('events');
      if(this.selectedEvent[0]!= localStorage.getItem('selectedEventId')) {
        for(var i=0; i < this.events.length; i++) {
          if(this.selectedEvent[0]==this.events[i].id) {
            localStorage.setItem('selectedEventId',this.events[i].id);
            localStorage.setItem('selectedEventName',this.events[i].event_name);
            this.eventName=localStorage.getItem('selectedEventName');
          }
        }
      }
    }
    
    if(navParams.get('fromSupportPage')!=undefined) {
      this.addAndRemoveSelectedEvent();
    }
    if(navParams.get('fromEditActivity')!=undefined) {
      this.fromEditActivity = true;
    }

    console.log( " last : ");
    console.log( " previous : ",this.navCtrl.getPrevious());
      	//this.selectedEvent =[7];
  	this.user =  JSON.parse(localStorage.getItem('user'));
    let params:any;
    if(this.fromEditActivity==false){
      params ={ 'eventids' : this.selectedEvent, 'deleteUnselected':1 };
    } else {
      params ={ 'eventids' : this.selectedEvent };
    } 
  	this.sendrequest.getResult('events/addAndGetSelectedEventActivityList','post',params).then((response:any) => {     
	  	response.data.selectedActivity.forEach(function(item){ 
        this.selectedEventActivity.push(item.activity_id);
      },this);
      this.totalSelected = this.selectedEventActivity.length;
      this.eventActivity = response.data.EventActivityData;
      this.eventActivity.forEach(function(item){ 
          item.isChecked=false; 
          this.selectedEventActivity.forEach(function(pitem){ 
              if(pitem == item.id) {
                  item.isChecked=true; 
              }
          });
      },this);
      this.loaderShow=false;
      this.allEventActivity =this.eventActivity;
  	},
  	error => {
  		this.sendrequest.presentToast("Error occured. Please try again");
  	})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventsActivitiesPage');
  }
  addAndRemoveSelectedEvent() {
    this.sendrequest.getResult('user/addAndRemoveSelectedEvent','post',{
      selectedEvents:this.selectedEvent
    }).then((response:any) => { 
      this.sendrequest.updateCivicProfileData();   
    },
    error => {
      
    });
  }
  searchOrganization() {
    this.eventActivity = this.allEventActivity.filter((v) => {
	    if (v.activity_title.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
	       return true;
	    }
	    return false;
    })
  }
  insertActivity(activity){
    console.log(activity.isChecked);
    if(activity.isChecked==false){
        this.selectedEventActivity.splice(this.selectedEventActivity.indexOf(activity.id), 1);
    } else {
        this.selectedEventActivity.push(activity.id);
    }
   // activity.isChecked = !activity.isChecked;
    this.totalSelected = this.selectedEventActivity.length;
  }
  cancel() {
    this.event.publish('selectedEventContent:created', 'user', Date.now());
  	this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goBack() {
    this.event.publish('selectedEventContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    if(this.navParams.get('powerScore')==undefined) {
      this.navCtrl.popToRoot({animate:false});
    } else {
      this.navCtrl.setRoot('MainTabPage',{'powerScore': this.navParams.get('powerScore')},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
    }
  }
  addToProfile(){
    if(this.selectedEventActivity.length!=0){
	  	this.sendrequest.getResult('user/addEventActivity/' + this.user.id,'post',{
	  		activity: this.selectedEventActivity,
            selectedeventids: this.selectedEvent
        }).then((response:any) => {  
            if(response.data.showError =='1'){
				this.sendrequest.presentToast(response.data.errorMessage);
            } else {
              this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
              this.navCtrl.pop({animate:true, animation: 'ios-transition'});
            }
        },
		error => {
			this.sendrequest.presentToast("Error occured. Please try again");
		});
    } else {
      this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
      if(this.fromEditActivity==false) {
        if(this.navParams.get('powerScore')==undefined) {
          let views=this.navCtrl.getViews();
          let CivicProfilePageIndex=0;
          for (var i = views.length - 1; i >= 0; i--) {
            if(views[i].name=="CivicProfilePage") {
              CivicProfilePageIndex=views[i].index;
              break;
            }
          }
          this.navCtrl.popTo(this.navCtrl.getByIndex(CivicProfilePageIndex));
        } else {
           this.navCtrl.push('SocialFollowersPage',{ 'powerScore' : this.navParams.get('powerScore') },{animate:true, duration:900, animation: 'ios-transition'});
        }
      } else {
        this.navCtrl.pop({animate:true, animation: 'ios-transition'});
      }
    }
     
  }

}
