import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEventsActivitiesPage } from './add-events-activities';

@NgModule({
  declarations: [
    AddEventsActivitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEventsActivitiesPage),
  ],
})
export class AddEventsActivitiesPageModule {}
