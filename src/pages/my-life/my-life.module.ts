import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyLifePage } from './my-life';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    MyLifePage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(MyLifePage),
  ],
})
export class MyLifePageModule {}
