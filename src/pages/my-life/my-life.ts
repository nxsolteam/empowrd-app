import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, Select, DateTime, ActionSheetController, Platform } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject  } from '@ionic-native/file-transfer';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import * as moment from 'moment';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

/**
 * Generated class for the MyLifePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-life',
  templateUrl: 'my-life.html',
})
export class MyLifePage {
  private win: any = window;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('pronounSelect') pronounSelect: Select;
  @ViewChild('engamentLevelSelect') engamentLevelSelect: Select;
  @ViewChild('genderSelect') genderSelect: Select;
  @ViewChild('employmentSelect') employmentSelect: Select;
  @ViewChild('interestSelect') interestSelect: Select;
  @ViewChild('partySelect') partySelect: Select;
  @ViewChild('disabilitySelect') disabilitySelect: Select;
  @ViewChild('ageSelect') ageSelect: DateTime;
  createProfileContent=true;
  age= '';
  genderText='';
  gender='';
  engamentLevel='';
  engamentLevelText='';
  interestText='';
  employment='';
  employmentText='';
  interest=[];
  pronoun='';
  disability='';
  party='';
  partyText='';
  profileImage='';
  profileImageUrl:any;
  initial='';
  pronounShortName='';
  user:any;
  ageNumeric=0;
  civicData:any;
  profileImageUploaded=false;

  selectOptions = {
    cssClass: 'election-date-dropdown'
  };
  customPickerOptions = {
        cssClass: 'custom-date-picker'
  };
  employeements=[];
  // maxDate:string = (new Date()).getFullYear()-100;
  // minDate:string = new Date().toISOString();
  powerScore='0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public sendrequest: SendrequestProvider, private camera: Camera, private transfer: FileTransfer, public platform: Platform,
    public actionSheetController: ActionSheetController, private sanitize: DomSanitizer) {
  	this.powerScore = localStorage.getItem('powerScore');
    this.user = JSON.parse(localStorage.getItem('user'));
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    this.initial=this.user.first_name.charAt(0)+"."+this.user.last_name.charAt(0)+".";
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    if(this.civicData.userData.date_of_birth != '') {
      this.age = this.civicData.userData.date_of_birth;
      this.changeAgeValue();
    }
    if(this.civicData.userData.photo_url != null) {
      this.profileImage=this.civicData.userData.photo_url;
      this.profileImageUrl=this.civicData.userData.photo_url;
    }
    
    if(this.civicData.userData.party != null && this.civicData.userData.party != '' && this.civicData.userData.party != 'notselected') {
      this.party=this.civicData.userData.party;
      this.changeParty();
    }
    if(this.civicData.userData.employment != null && this.employeements.length > 0) {
      this.employment=this.civicData.userData.employment;
      this.changeEmployment();
    }
    this.sendrequest.getResult('user/getJobList','get', {} ).then((response:any) => { 
      this.employeements=response.data;
      if(this.civicData.userData.employment != null) {
        this.employment=this.civicData.userData.employment;
        this.changeEmployment();
      }
    },
    error => {
    });
    if(this.civicData.userData.disabled != null) {
      this.disability=this.civicData.userData.disabled;
    }
    if(this.civicData.userData.most_interest_in != null) {
      this.interest = this.civicData.userData.most_interest_in.split(",");
      this.changeInterest();
    }
    if(this.civicData.userData.engagement_level != null) {
      this.engamentLevel = this.civicData.userData.engagement_level;
      this.changeEngagementLevel();
    }
    if(this.civicData.userData.gender != null) {
      this.gender=this.civicData.userData.gender.toString();
      console.log("this.gender : ",this.gender);
      this.changeGender();
    }
    if(this.civicData.userData.prononouns != null) {
      this.pronoun=this.civicData.userData.prononouns;
      this.changePronoun();
    }
  }
  
  changeAgeValue() {
    console.log(this.age);
    this.ageNumeric = moment().diff(this.age, 'years');

  }
  changeParty() {
    console.log(this.age);
    this.partyText = this.party.substring(0, 3);

  }
  changeGender() {  
    if(this.gender=='1') {
      this.genderText = 'MALE';
    } else if(this.gender=='0') {
      this.genderText = 'FEMALE';
    } else if(this.gender=='3') {
      this.genderText = 'TRANS';
    } else {
      this.genderText = 'NON-BINARY';
    }
  }
  changeEmployment() {
    for(var i=0; i < this.employeements.length; i++) {
      if(this.employment==this.employeements[i].name) {
        this.employmentText = this.employeements[i].acronym;
      }
    }
  }
  changePronoun() {
    if(this.pronoun=='Him/He') {
      this.pronounShortName='H|H';
    } else if(this.pronoun=='She/Her') {
      this.pronounShortName='S|H';
    } else {
      this.pronounShortName='T|T';
    }
  }
  changeEngagementLevel() {
    console.log(this.engamentLevel);
    this.engamentLevelText = this.engamentLevel;
  }
  changeInterest() {
    console.log(this.interest);
    if(this.interest.length > 1) {
      this.interestText = this.interest.length+"";
    } else {
      this.interestText = this.interest.join(' ');
    }
  }
  uploadProfileImage() {
    // this.profileImage='./assets/img/horaceWilliams.jpg';
    // this.profileImageUrl='./assets/img/horaceWilliams.jpg';
     this.addPhoto();
    /* const actionSheet = this.actionSheetController.create({
      title: 'Choose Option',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take photo',
          icon: 'ios-camera-outline',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Choose photo from Gallery',
          icon: 'ios-images-outline',
          handler: () => {
            this.addPhoto();
          }
        },
      ]
    });
     actionSheet.present(); */
  }
  takePhoto() {
    //Final Working on phone.
     const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageUri) => {
      console.log(imageUri);
      let composed = imageUri.replace("file:///", "").split("/");
      let fileName = composed.pop();
      this.sendrequest.cacheObject(imageUri, fileName).then((result: any) => {

        console.log("take---> " + result);
        let img_uri = result.nativeURL;
        this.profileImageUploaded=true;
        this.profileImage = result.nativeURL;
        if (this.platform.is('ios') || this.platform.is('ipad')) {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage.replace(/^file:\/\//, ''));
        } else {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage);
        }
        
      })
    }, (err) => {
       this.sendrequest.presentToast("There was an error getting your image, please try again.");

    })   
  }
  addPhoto() { 
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0
    }

    this.camera.getPicture(options).then((imageUri: string) => {

      
      console.log(imageUri);
      if (imageUri.split("?")[0] != undefined) {
        imageUri = imageUri.split("?")[0];
      }

      let composed = imageUri.replace("file:///", "").split("/");
      let fileName = composed.pop();


      console.log("fileName->");
      console.log(fileName);

      this.sendrequest.cacheObject(imageUri, fileName).then((result: any) => {
        console.log("cacheObject---> ");
        console.log(result);
        this.profileImageUploaded=true;
        this.profileImage = result.nativeURL;
        if (this.platform.is('ios') || this.platform.is('ipad')) {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage.replace(/^file:\/\//, ''));
        } else {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage);
        }
        
      })

    }, (err) => {
        this.sendrequest.presentToast("There was an error getting your image, please try again.");
    });
    
  }
  updateProfilePicture() {
     let composed = this.profileImage.replace("file:///", "").split("/");
      let fileName = composed.pop();
      let filePath = "file:///" + composed.join("/");
      const fileTransfer: FileTransferObject = this.transfer.create();
      let options: FileUploadOptions = {
                fileKey: 'image',
                fileName: fileName,
                headers: {'Authorization': 'Bearer '+this.sendrequest.authtoken, 'Accept': 'application/json'},
                mimeType: "multipart/form-data",
                params: {},
                chunkedMode: false
      }
      console.log(options);
      fileTransfer.upload(this.profileImage, this.sendrequest.api_url + "user/updateProfileImage/" + this.user.id, options)
      .then((data:any) => {
        let response=JSON.parse(data.response);
        console.log(response);
        this.sendrequest.updateCivicProfileData();
        this.sendrequest.updatePowerScore(this.user.id);
        if(response.data.photo !='' && response.data.photo !=null && response.data.photo !='null'){
          if(this.user.photo != response.data.photo){
            this.user.photo=response.data.photo;
            localStorage.setItem('user', JSON.stringify(this.user));
          }
        }
       
      }, (err) => {
        
        this.sendrequest.presentToast('Profile image not uploaded. Please try again');
        console.log(err);
        
       });
  }
  updateProfile() {
    let data = {
      'disability': this.disability,
      'interest': this.interest,
      'party': this.party,
      'engamentLevel': this.engamentLevel,
      'gender': this.gender,
      'age': this.age,
      'employment': this.employment,
      'pronoun': this.pronoun
    }
    this.sendrequest.getResult('user/updateProfile/' + this.user.id,'post', data ).then((response:any) => { 
      this.sendrequest.updateCivicProfileData();
      this.sendrequest.updatePowerScore(this.user.id);
      if(response.data.photo !='' && response.data.photo !=null && response.data.photo !='null'){
        if(this.user.photo != response.data.photo){
            this.user.photo=response.data.photo;
            localStorage.setItem('user', JSON.stringify(this.user));
        }
      }           
      
    },
    error => {
          this.sendrequest.presentToast("Error occured. Please try again");
    });
    if(this.profileImageUploaded==true) {
      this.updateProfilePicture();
    }
    
    this.navCtrl.pop({animate:true, duration:900, animation: 'ios-transition'});
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyLifePage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    console.log($event);
  }  
  GoToNext(value){
  	setTimeout(() => {
  		this.createProfileContent=false;
    }, 100);
  	this.navCtrl.push('AddCausePage',{},{animate:true, animation: 'ios-transition'});
  	setTimeout(() => {
  	   this.createProfileContent=true;
  	}, 1500);
  }
  goBack() {
    //this.events.publish('createProfileContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
  togglePronoun(){
    setTimeout(() => {
      this.pronounSelect.open();
    },150); 
  }
  toggleEmployment(){
    setTimeout(() => {
      this.employmentSelect.open();
    },150); 
  }
  toggleAge(){
    setTimeout(() => {
      this.ageSelect.open();
    },150); 
  }
  toggleGender() {
    setTimeout(() => {
      this.genderSelect.open();
    },150); 
  }
  toggleEngagementLevel() {
    setTimeout(() => {
      this.engamentLevelSelect.open();
    },150);
  }
  toggleParty() {
    setTimeout(() => {
      this.partySelect.open();
    },150);
  }
  toggleInterest() {
    setTimeout(() => {
      this.interestSelect.open();
    },150);
  }
  toggleDisability() {
    setTimeout(() => {
      this.disabilitySelect.open();
    },150);
  }
}
