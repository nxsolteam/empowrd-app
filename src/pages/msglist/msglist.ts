import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, PopoverController, Platform } from 'ionic-angular';
import { Refresher } from 'ionic-angular';
import { Cache, CacheService } from 'ionic-cache-observable';
import { PlaceholderProvider } from '../../providers/placeholder/placeholder';
import { Observable } from 'rxjs/Observable';
import { Placeholder } from '../../providers/placeholder/placeholder.model';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { NewMessageMenuComponent } from '../../components/new-message-menu/new-message-menu';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the MsglistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-msglist',
  templateUrl: 'msglist.html',
})
export class MsglistPage {
  @ViewChild(ParallaxHeaderDirective) directive = null;
  scrallDirection='';
  header_data:any;
  showDropDown = false;
  shortAddress='';
  addressOnHeader='';
  messageCount='0';
  initial='';
  user:any;
  validateAddress=false;
  messageList:any = [];
  allMessages:any = [];
  messageSelectedType = '';
  eventMessages=false;
  eventId='';
  selectedOpportunity='support opportunity';
  /**
   * The placeholder data to present.
   *
   * @type {Observable<Placeholder>}
   */
  public placeholder$: Observable<Placeholder[]>;

  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<Placeholder>}
   */
  public cache: Cache<Placeholder[]>;

  /**
   * Refresh subscription that can be cancelled when leaving the view.
   *
   * @type {Subscription}
   */
  public refreshSubscription: Subscription;

  /**
   * ContactPage constructor.
   *
   * @param {PlaceholderProvider} placeholderProvider
   * @param {CacheService} cacheService
   */
  getAllMessageTypes:any = [];
  messageSelectedTypeText='';
  constructor(public navCtrl: NavController, public navParams: NavParams, private placeholderProvider: PlaceholderProvider, private cacheService: CacheService, public events: Events,  public sendrequest: SendrequestProvider, private alertCtrl: AlertController, public popoverCtrl: PopoverController, private statusBar: StatusBar, public platform: Platform) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    if(navParams.get('eventMessages')!=undefined) {
      this.eventMessages=true;
      this.eventId=navParams.get('event_id');
    }



    if(localStorage.getItem('getAllMessageTypes')!= undefined) {
      this.getAllMessageTypes = JSON.parse(localStorage.getItem('getAllMessageTypes'));
    }
    if(localStorage.getItem('messageSelectedType')!= undefined) {
      this.messageSelectedType = localStorage.getItem('messageSelectedType');
      
      if(this.messageSelectedType=='ANNOUNCEMENT'){
        this.messageSelectedTypeText = 'BULLETINS';
      } else if(this.messageSelectedType=='MEMBERSHIP INVITE') {
        this.messageSelectedTypeText = 'INVITES';
      } else {
        this.messageSelectedTypeText = this.messageSelectedType;
      }
    }
    events.subscribe('messageSelectedType:created', (type, time) => {
      this.getAllMessageTypes = JSON.parse(localStorage.getItem('getAllMessageTypes'));
      this.messageSelectedType = type;
      if(this.messageSelectedType=='ANNOUNCEMENT'){
        this.messageSelectedTypeText = 'BULLETINS';
      } else if(this.messageSelectedType=='MEMBERSHIP INVITE') {
        this.messageSelectedTypeText = 'INVITES';
      } else {
        this.messageSelectedTypeText = this.messageSelectedType;
      }
      this.getMessageList();
    });
    this.header_data={ismenu:true,ishome:false,title:"INBOX"};
    this.addressOnHeader=localStorage.getItem('addresstext');
    this.messageCount = localStorage.getItem('messageCount');
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.initial=this.user.first_name.charAt(0)+""+this.user.last_name.charAt(0);
    events.subscribe('messageCountChange:created', (user, time) => {
       this.getAllMessageTypes = JSON.parse(localStorage.getItem('getAllMessageTypes'));
      this.messageCount = localStorage.getItem('messageCount');
    });
    events.subscribe('msglistRefresh:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.placeholderProvider.refresh();
    });
    events.subscribe('setLatLon:created', (user, time) => {
      if(localStorage.getItem('validateAddress')!= undefined) {
        if(localStorage.getItem('validateAddress')=='0') {
          this.validateAddress=false;
          
        } else {
          this.validateAddress=true;
          
        }
      }
      if(localStorage.getItem('addresstext')!= undefined) {
        this.addressOnHeader = localStorage.getItem('addresstext');
        this.shortAddress = this.addressOnHeader.substring(0, 15);
      }
    });
    
    if(localStorage.getItem('addresstext')!= undefined) {
      this.addressOnHeader = localStorage.getItem('addresstext');
      this.shortAddress = this.addressOnHeader.substring(0, 15);
    }
    if(localStorage.getItem('validateAddress')!= undefined) {
      if(localStorage.getItem('validateAddress')=='0') {
        this.validateAddress=false;
      } else {
        this.validateAddress=true;
      }
    }
  }
  civicProfilePage() {
    this.events.publish('gotoPage:created', 'CivicProfilePage', {});
  }
  confirmDeleteAllMessagepopup() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteAll();
          }
        }
      ]
    });
    alert.present();
  }
  openMenuPopup(myEvent, message, index) {
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, {  'message' : message, 'index':index }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        this.deleteMessage(data.message, data.index);
      }
    });
  }
  openMenuPopupForDeleteAll(myEvent) {
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, { 'all': '1' }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        this.confirmDeleteAllMessagepopup();
      }
    });
  }
  deleteAll() {
    this.messageList = [];
    let deleteMessageList = [];
    for(var i = 0; i < this.allMessages.length; i++) {
      if(this.allMessages[i].message_type_inbox==this.messageSelectedType ) {
        let composeMsg = ( this.allMessages[i].usercomposed==false)?'0':'1';
        deleteMessageList.push({ 'id' : this.allMessages[i].id, 'umid': this.allMessages[i].umid, 'usercomposed' : composeMsg })
      }
    }
     console.log(deleteMessageList);
    if(deleteMessageList.length > 0) {
      this.sendrequest.getResult('user/deleteAllMail','post',{ 'maillist': deleteMessageList }).then((response:any) => { 
        this.placeholderProvider.refresh();
        this.updateMessages();
      },
      error => {
      }) 
    }
    //
  }
  confirmDeleteMessagepopup(mail, index) {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteMessage(mail, index);
          }
        }
      ]
    });
    alert.present();
  }
  removeMessageFromList(message,index) {
    this.messageList.splice(index, 1);
  }
  deleteMessage(mail, index) {
    // this.messageList.push(this.allMessages[i]);
     this.messageList.splice(index, 1);
    if (mail.usercomposed == false) {
      this.sendrequest.getResult('user/deletemail/'+mail.umid,'get',{}).then((response:any) => { 
        this.placeholderProvider.refresh();
        this.updateMessages();
      },
      error => {
      })
    } else {
      this.sendrequest.getResult('user/deletemailcomposed/'+mail.umid,'get',{}).then((response:any) => { 
        this.placeholderProvider.refresh();
        this.updateMessages();
      },
      error => {
      })
    } 
  }
  updateMessages() {
    this.sendrequest.getResult('user/fetchLatestNewMailCountNew/' + this.user.id+'/2','get',{}).then((response:any) => { 

        localStorage.setItem('getNewMailListOrg', JSON.stringify(response.data.getNewMailListOrg));

        localStorage.setItem('getNewMailByTypes', JSON.stringify(response.data.getNewMailByTypes));
       
        localStorage.setItem('getAllMessageTypes', JSON.stringify(response.data.getAllMessageTypes));
        
        this.events.publish('messageCountChange:created', localStorage.getItem('messageCount'), Date.now()); 
      },
      error => {
       
      })
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MsglistPage');
  }
  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
  }
  /**
   * We will refresh the data automatically every time the view is entered.
   */
  ionViewWillEnter() {
    // Source data for refreshing the cache data and first load.
    this.getMessageList();
    
  }
  getMessageList() {
    this.placeholderProvider.refresh();
    this.cacheService
    .get('msglist')
    .mergeMap((cache: Cache<Placeholder[]>) => {
        this.cache = cache;
        return this.cache.get$;
    }).subscribe((response:any) => {
      this.allMessages = response.data;
      localStorage.setItem("lastupdated_message", this.allMessages[0].created_at);
      if(this.messageSelectedType=='') {
        if(this.allMessages.length > 0) {
          this.messageSelectedType=this.allMessages[0].message_type_inbox;
        }
      }
      this.getMessageByType();
    })
  }
  getMessageByType() {
    this.messageList = [];
    if(this.eventMessages==true) {
      for(var i = 0; i < this.allMessages.length; i++) {
        if(this.allMessages[i].type==2 && this.allMessages[i].event_id==this.eventId) {
          this.messageList.push(this.allMessages[i]);
        }
      }
    } else {
      for(var i = 0; i < this.allMessages.length; i++) {
      console.log(this.allMessages[i].message_type_inbox, this.allMessages[i]);
        if(this.allMessages[i].message_type_inbox!=undefined){
          if(this.allMessages[i].message_type_inbox.toLowerCase()==this.messageSelectedType.toLowerCase() ) {
            this.messageList.push(this.allMessages[i]);
          }
        }
      }
    }
  }
  ShowData(data){
  	console.log(data);
  }
  /**
   * Handle refresh event.
   *
   * @param {Refresher} refresher
   */
  public onRefresh(refresher: Refresher): void {
    this.placeholderProvider.refresh(refresher);
  }

  
  changeOpportunity(type) {
    this.messageSelectedType = type;
    this.selectedOpportunity=type;
    this.getMessageByType();
    this.toggleDropDown();
    localStorage.setItem('messageSelectedType', type);
  }
  gotoMailDetail(umid, usercomposed, msgid, mail) {
    console.log("Clicked : ");
    mail.status = 1;
    if (usercomposed == false) {
      this.events.publish('gotoPage:created', 'MsgDetailPage', { 'msgId': umid, 'mail':mail });
    } else {
      this.events.publish('gotoPage:created', 'MsgDetailPage', { 'composeMessage': usercomposed, 'msgId': msgid, 'mail':mail });
    }
  }
  logScrollStart() {
    // console.log("Scroll Start");
    // document.getElementById("hidetabs").classList.add("inProgress");
    // document.getElementById("msgListheaderNavbar").classList.add("fadeUp");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("hidetabs").classList.remove("inProgress");
    // document.getElementById("msgListheaderNavbar").classList.remove("fadeUp"); 
  }
  logScrolling($event) {
    // console.log($event);
     /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
     this.scrallDirection=$event.directionY;
     if(this.scrallDirection=="down")
     {
       // document.getElementById("hidetabs").classList.add("inProgress");
       // document.getElementById("msgListheaderNavbar").classList.add("fadeUp");
     }
     if(this.scrallDirection=="up")
     {
       // document.getElementById("hidetabs").classList.remove("inProgress");      
       // document.getElementById("msgListheaderNavbar").classList.remove("fadeUp");        
     }
     /*
     MAULIK
     */
     /*
     if ($event.scrollTop > 100){
       document.getElementById("msgListheaderNavbar").classList.add("shadow");
     } else{
       document.getElementById("msgListheaderNavbar").classList.remove("shadow");
     }
     */
  }
  goBack() {
    if(this.eventMessages==false) {
      this.navCtrl.pop();
    } else {
      this.events.publish('selectedEventContent:created', 'user', Date.now());
      this.navCtrl.pop({animate:true, animation: 'ios-transition'});
    }
    //this.events.publish('selectedTabs:data','2', Date.now());
  }
  goToRoot() {
    this.navCtrl.pop();
    //this.events.publish('selectedTabs:data','2', Date.now());
  }
}
