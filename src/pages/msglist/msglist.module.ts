import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MsglistPage } from './msglist';
import { ComponentsModule }from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    MsglistPage,
    
  ],
  imports: [
    DirectivesModule,
  	ComponentsModule,
    IonicPageModule.forChild(MsglistPage),
  ],
   schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class MsglistPageModule {}
