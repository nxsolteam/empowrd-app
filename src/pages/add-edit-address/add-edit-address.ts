import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, Events } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';

/**
 * Generated class for the AddEditAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
@IonicPage()
@Component({
  selector: 'page-add-edit-address',
  templateUrl: 'add-edit-address.html',
})
export class AddEditAddressPage {
  latitude=0;;
  longitude=0;
  location:any;
  user:any;
  @ViewChild('Address') Address;
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController,  private leaderlistProvider: LeaderlistProvider, private addresslistProvider: AddresslistProvider, private events: Events, private alertCtrl: AlertController) {
  	this.user =  JSON.parse(localStorage.getItem('user'));

  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.Address.setFocus();
    }, 700);
  }
  ionViewWillEnter() {
     // Google Places API auto complete
    let input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];
    let autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        // retrieve the place object for your use
        let place = autocomplete.getPlace();
        let state = '';
        for(var i=0; i < place.address_components.length; i++) {
          if(place.address_components[i].types[0] =='administrative_area_level_1') {
            state = place.address_components[i].short_name;
          }
        }
        // if(state=='GA' || state=='FL') {
          localStorage.setItem('stateShortName',state);
          localStorage.setItem('latitude', place.geometry.location.lat());
          localStorage.setItem('longitude', place.geometry.location.lng());
          localStorage.setItem('addresstext', place.formatted_address);
          this.saveAddress(place.formatted_address);
        /* } else {
          this.location='';
          this.confirmPopUp();
        } */
    });
  }
  confirmPopUp() {
    let alert = this.alertCtrl.create({
      title: "Today, EMPOWRD only engages via Florida and Georgia locations.",
      subTitle: "If you would like to continue, please provide a Florida or Georgia address.",
      cssClass: 'address-popup',
      buttons: [
        {
          text: 'DO NOT CHANGE',
          handler: () => {
            this.goBack();
          }
        },
        {
          text: 'CHANGE ADDRESS',
          handler: () => {
            
          }
        }
      ]
    });
    alert.present();
  }
  goBack() {
    this.navCtrl.pop();
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
  saveAddress(address) {
    localStorage.setItem('currentLocationClicked','false');
    localStorage.setItem('showEventAddress','false');
    
      this.leaderlistProvider.leaderlistRefresh();
    
    this.events.publish('setLatLon:created', this.user, Date.now());
    this.sendrequest.getResult('user/updateAddress/' + this.user.id,'post',{
      address: address,
      id: localStorage.getItem('addressId'),
  
    }).then((response:any) => { 
     
      this.addresslistProvider.refresh();
    },
    error => {
    });
    this.sendrequest.getResult('user/checkVoterValidationAddress/' + this.user.id,'post',{ 'address': address }).then((response:any) => {     
      if(response.data.record==0) {
        localStorage.setItem('validateAddress','0');
      } else {
        localStorage.setItem('validateAddress','1');
      }  
    },
    error => {
    })
    
    this.navCtrl.pop({ animate:true, animation: 'ios-transition' });
  }
}
