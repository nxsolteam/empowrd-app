import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEditAddressPage } from './add-edit-address';

@NgModule({
  declarations: [
    AddEditAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEditAddressPage),
  ],
})
export class AddEditAddressPageModule {}
