import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestBPage } from './test-b';

@NgModule({
  declarations: [
    TestBPage,
  ],
  imports: [
    IonicPageModule.forChild(TestBPage),
  ],
})
export class TestBPageModule {}
