import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditEmailAddressModalPage } from './edit-email-address-modal';

@NgModule({
  declarations: [
    EditEmailAddressModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditEmailAddressModalPage),
  ],
})
export class EditEmailAddressModalPageModule {}
