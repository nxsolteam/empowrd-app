import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, ViewController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
/**
 * Generated class for the EditEmailAddressModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-email-address-modal',
  templateUrl: 'edit-email-address-modal.html',
})
export class EditEmailAddressModalPage {

  @ViewChild('email') firstNameInput;
  successEmailClass='';
  emailFocusClass='';
  userData:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public fb: FormBuilder, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, public events: Events) {
  	this.userData = navParams.get('userData');
  }
  public registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")]]
  });
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditEmailAddressModalPage');
    this.registerForm.patchValue({email : this.userData.email });
    this.inputBlurEmail();
    setTimeout(() => {
      this.firstNameInput.setFocus();
    }, 1100);
  }

  inputFocusEmail() {
     this.emailFocusClass='has-focus';
  }
  inputBlurEmail() {
    this.emailFocusClass='';
    this.successEmailClass='';
    if(this.registerForm.controls['email'].valid){
      this.successEmailClass='has-success';
    }
    if(this.registerForm.controls['email'].errors) {
      this.successEmailClass='has-error';
    }
  }
  registerMember(form){ 
    if(this.registerForm.valid){
        let data = { 'email': form.email };
   		this.viewCtrl.dismiss(data);
    }
  }
  close() {
   	this.viewCtrl.dismiss();
  }

}
