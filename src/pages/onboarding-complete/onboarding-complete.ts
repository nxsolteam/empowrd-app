import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

/**
 * Generated class for the OnboardingCompletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-onboarding-complete',
  templateUrl: 'onboarding-complete.html',
})
export class OnboardingCompletePage {
  onboardingComplete = true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
  	
  }

  ionViewDidLoad() {
    
  }
  GoToNext(value){
  	//setTimeout(() => {
  		this.onboardingComplete=false;
   // }, 100);
  	this.navCtrl.push('MainTabPage',{},{animate:true, duration:900, animation: 'ios-transition'});
  	setTimeout(() => {
  	   this.onboardingComplete=true;
  	}, 1500);
  }
  goBack() {
  	this.events.publish('socialFollowers:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
  	//setTimeout(() => {
  		this.onboardingComplete=false;
    // }, 100);
  	this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900, animation: 'ios-transition'});
  	setTimeout(() => {
  	   this.onboardingComplete=true;
  	}, 1500);
  }
}
