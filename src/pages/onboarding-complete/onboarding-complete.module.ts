import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnboardingCompletePage } from './onboarding-complete';

@NgModule({
  declarations: [
    OnboardingCompletePage,
  ],
  imports: [
    IonicPageModule.forChild(OnboardingCompletePage),
  ],
})
export class OnboardingCompletePageModule {}
