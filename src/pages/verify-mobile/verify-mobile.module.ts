import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyMobilePage } from './verify-mobile';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  declarations: [
    VerifyMobilePage,
  ],
  imports: [
    IonicPageModule.forChild(VerifyMobilePage),
    NgxMaskIonicModule,
  ],
})
export class VerifyMobilePageModule {}
