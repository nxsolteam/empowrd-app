import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, ViewController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { GetCausesListProvider } from '../../providers/get-causes-list/get-causes-list';
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';
import { MyCommunityProvider } from '../../providers/my-community/my-community';

/**
 * Generated class for the VerifyMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-mobile',
  templateUrl: 'verify-mobile.html',
})
export class VerifyMobilePage {
  verifyMobileShowContent=true;
  code:string;
  phoneNumber:string;
  email:string;
  responseData:any;
  goToRegistration=true;
  showError=false;
  @ViewChild('phone1') phone1;
  @ViewChild('phone2') phone2;
  phoneNumber1='';
  phoneNumber2='';
  sendButtonText="SEND VERIFICATION CODE";
  sendButtonEnabled=true;
  addresslist:any;
  social='';
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, private addresslistProvider: AddresslistProvider, private cacheService: CacheService, private leaderlistProvider: LeaderlistProvider, private electionRacesProvider: ElectionRacesProvider, private platform: Platform, private getCausesListProvider: GetCausesListProvider, private getOrgListProvider: GetOrgListProvider, public viewCtrl: ViewController, public myCommunityProvider: MyCommunityProvider) {
  	 events.subscribe('verifyMobileContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.verifyMobileShowContent=false;
      setTimeout(() => {
        this.verifyMobileShowContent=true;
      }, 500);
    });
    if(navParams.get('email') != null && navParams.get('email') != undefined) {
      this.email = navParams.get('email');
    }
    this.phoneNumber = navParams.get('phoneNumber');
    if(navParams.get('response') != null && navParams.get('response') != undefined) {
      this.responseData = navParams.get('response');
      this.goToRegistration=false;
    }
    if(navParams.get('social') != null && navParams.get('social') != undefined) {
      this.social = navParams.get('social');
    }
    if(navParams.get('fromLogin') != null && navParams.get('fromLogin') != undefined) {
      this.responseData = JSON.parse(localStorage.getItem('response'));
      if(navParams.get('fromLogin') ==true){
        this.goToRegistration=false;
      }
    }
    events.subscribe('smsReaded:created', (message, time) => {
      this.phoneNumber1=localStorage.getItem('code');
      setTimeout(() => {
        this.verifyCode();
      }, 2000);
    });
  }
  ionViewDidEnter()
  {

  }
  
  ionViewDidLoad() {

     setTimeout(() => {
      this.phone1.setFocus();
    }, 1200);
    console.log('ionViewDidLoad VerifyMobilePage');
    if(this.goToRegistration==false) {
      if(this.responseData.token == undefined && this.responseData.register!='1') {
        setTimeout(() => {
        this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
        this.goToRoot();
         }, 500);
      }
    }
  }
  setFocusElement() {
    this.phone1.setFocus();
  }
  checkLength(event,type) {
    this.showError=false;
    /* if(event.key !="Backspace") {
        var numbers=this.phoneNumber1;
        if(numbers.length > 5)
        {
          return false;
        }
        */
        /*
        if(numbers.length == 3) {
          this.phoneNumber1= numbers+'-';
        }*/
       /*
         if(this.phoneNumber1.length ==6) {
            this.sendButtonEnabled=true;
            
        } else {
            this.sendButtonEnabled=false;
        }
       */
        
      // }
      /*
       if(event.code =="Backspace") {
       if(this.phoneNumber1.length ==5) {
            this.sendButtonEnabled=true;
            
        } else {
            this.sendButtonEnabled=false;
        }
       }*/
   
  }
  checkbackslash(event){

  	 if(event.code =="Backspace") {
       if(this.phoneNumber1.length ==7) {
            this.sendButtonEnabled=true;
            
        } else {
            this.sendButtonEnabled=false;
        }
       }

  }
  verifyMobile(){
  	this.sendrequest.sendVerificationCode({
      email:this.email, 
      phoneNumber:this.phoneNumber
    }).then((response:any) => { 
    	this.code = response.data.code;
      this.sendrequest.presentToast("Code sent");
    },
  	error => {
  		
  	});
  }
  goBack() {
    //this.events.publish('hideMobileContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    if(localStorage.getItem('modelRegistration')=='true') {
      this.viewCtrl.dismiss();
    } else {
      this.navCtrl.popToRoot({animate:false});
    }
  }
  verifyCode(){
    this.code =localStorage.getItem('code');
    let phonenumber = this.phoneNumber1.replace("-","");
  	if(this.code == phonenumber){
      this.events.publish('stopSmsWatch:created', 'message', Date.now());
  		console.log("Match Code");
  		this.showError=false;
  		if(this.goToRegistration==false) {
        if(this.responseData.register=='1') {

          let data ={};
          if(this.responseData.data.social=='apple') {
            data = {
              email:this.responseData.data.email,
              first_name:this.responseData.data.first_name,
              last_name:this.responseData.data.last_name,
              social:this.responseData.data.social,
              apple:this.responseData.data.apple,
              home:"ecu",
              phone: this.phoneNumber
            }
          } else if(this.responseData.data.social=='google') {
            data = {
              email:this.responseData.data.email,
              first_name:this.responseData.data.first_name,
              last_name:this.responseData.data.last_name,
              social:this.responseData.data.social,
              google:this.responseData.data.google,
              home:"ecu",
              phone: this.phoneNumber
            }
          } else {
            data = {
              email:this.responseData.data.email,
              first_name:this.responseData.data.first_name,
              last_name:this.responseData.data.last_name,
              social:this.responseData.data.social,
              facebook:this.responseData.data.facebook,
              home:"ecu",
              phone: this.phoneNumber
            }
          }
          localStorage.setItem('onboardingStart','1'); 
          setTimeout(() => {
           this.verifyMobileShowContent=false;
          }, 100);
          this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
          setTimeout(() => {
           this.verifyMobileShowContent=true;
          }, 1500);
        }
        else if(this.responseData.token != undefined) {
          this.sendrequest.authtoken = this.responseData.token;
          localStorage.setItem('user', JSON.stringify(this.responseData.user));
          localStorage.setItem('auth-token', this.responseData.token);
          console.log( JSON.parse(localStorage.getItem('user')));
          this.addresslistProvider.refresh();
          this.getCausesListProvider.getCausesListRefresh();
          this.getOrgListProvider.getOrgListRefresh();
          //const sourceData = this.addresslistProvider.random();
          this.cacheService
          .get('addresslist')
          .mergeMap((cache: Cache<Addresslist[]>) => {
              this.cache = cache;
              
              return this.cache.get$;
          })
          .subscribe((addresslist:any) => {
              this.addresslist=addresslist;
             
              if(localStorage.getItem('addressId')!=this.addresslist.data.addresses[0].id) {
                localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
                localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address); 
                localStorage.setItem('addressId' , this.addresslist.data.addresses[0].id); 
                localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
                localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
                localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
                let stateShortName = '';
                if(this.addresslist.data.addresses[0].state=='Georgia') {
                  stateShortName='GA';
                } else if(this.addresslist.data.addresses[0].state=='Florida') {
                  stateShortName='FL';
                }
                localStorage.setItem('stateShortName' ,stateShortName); 
                localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);
              
                this.events.publish('setAddressTitle:created', 'user', Date.now());
                
                this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + this.responseData.user.id,'get',{}).then((response:any) => {     
                  var results = response.data.response.results[0];
                  localStorage.setItem('latitude',results.location.lat.toString());
                  localStorage.setItem('longitude',results.location.lng.toString());
                  this.events.publish('setLatLon:created', 'user', Date.now());
                  this.leaderlistProvider.leaderlistRefresh();
                  this.electionRacesProvider.electionRaceRefresh();
                });
              }
             
          });
          this.myCommunityProvider.myCommunityRefresh();
           setTimeout(() => {
           this.verifyMobileShowContent=false;
          }, 100);
          this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
          setTimeout(() => {
           this.verifyMobileShowContent=true;
          }, 1200);
        } else {
          this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
          this.events.publish('loginPageContent:created', 'user', Date.now());
          this.navCtrl.push('LoginPage',{},{ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
        }
      } else {
        setTimeout(() => {
         this.verifyMobileShowContent=false;
        }, 100);
        this.navCtrl.push('RegisterPage',{ 'phoneNumber': this.phoneNumber },{ animate:true, duration:900, animation: 'ios-transition'});
        setTimeout(() => {
         this.verifyMobileShowContent=true;
        }, 1500);
        //this.sendrequest.goToPage('RegisterPage',{'phoneNumber': this.phoneNumber });
      }
  	} else {
      this.sendButtonText="SUBMIT VERIFICATION CODE";
  		this.showError=true;
  	}

  }

}
