import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the NumberSubmittedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-number-submitted',
  templateUrl: 'number-submitted.html',
})
export class NumberSubmittedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events) {
  	/* events.subscribe('verifyMobileContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.verifyMobileShowContent=false;
      setTimeout(() => {
        this.verifyMobileShowContent=true;
      }, 500);
    }); 
    if(navParams.get('email') != null && navParams.get('email') != undefined) {
      this.email = navParams.get('email');
    } */
    // this.phoneNumber = navParams.get('phoneNumber');
    
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NumberSubmittedPage');
  }
  goBack() {
    this.events.publish('hideMobileContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
  }

}
