import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NumberSubmittedPage } from './number-submitted';

@NgModule({
  declarations: [
    NumberSubmittedPage,
  ],
  imports: [
    IonicPageModule.forChild(NumberSubmittedPage),
  ],
})
export class NumberSubmittedPageModule {}
