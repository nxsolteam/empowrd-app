import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { OfficeSearchAutoCompleteProvider } from '../../providers/office-search-auto-complete/office-search-auto-complete';
/**
 * Generated class for the LeaderFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leader-filter',
  templateUrl: 'leader-filter.html',
})
export class LeaderFilterPage {
  header_data:any;
  resultData:any;
  user:any;
  states:any;
  selectedItem:any;
  campaignOnly='2';
  showCampaignData:string='';
  searchbyfirstname:string='';
  nextElectionYear:string='';
  juri_type:string='';
  juriName:string='';
  state_name:string='';
  juriCoverageArea:string='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController) {
  	
  	let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
  	this.sendrequest.getResult('user/showCityCountyLeaderData','get',{}).then((response:any) => {     
      	loading.dismiss();
  	  	this.showCampaignData =response.data.showData;
	    if(this.showCampaignData =='false'){
	        this.campaignOnly='1';
	    }
  	},
  	error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
  	})
  	
    this.header_data={ismenu:false,ishome:false,title:"LEADER SEARCH"};
  	this.states = this.loadAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderFilterPage');
  }
  loadAll() {
        var allStates = 'Adjutant General, Advisory Neighborhood Commissioner, Agriculture Commissioner, Alderman, Assembly Member, Attorney General, Board of Review Commissioner, Board of Trustees, Board of Trustees, Borough President, Chairperson, Chief Judge, Circuit Court Judge, City Assessor, City Attorney, City Auditor, City Clerk, City Collector of Revenue, City Commissioner, City Comptroller, City Controller, City Council Member, City License Collector, City Prosecutor, City Recorder of Deeds, City Sheriff, City Tax Commissioner, City Treasurer, Commonwealth\'s Attorney, Constable, Coroner, Corporation Commissioner, County Assessor, County Attorney, County Auditor, County Circuit Clerk, County Clerk, County Commissioner, County Court Judge, County Engineer, County Executive, County Judge, County Recorder, County Register of Deeds, County Surveyor, County Tax Commissioner, County Treasurer, Court Clerk, Court of Appeals Judge, Court of Common Pleas Judge, Criminal Court Clerk, Criminal Court Judge, Criminal District Judge, District Attorney, District Clerk, District Court Judge, Domestic Relations Judge, Elections Director, Estimate & Taxation Board Commissioner, Family Court Judge, General Sessions Court Clerk, Governor, Health System Board Director, Insurance Commissioner, Jury Commissioner, Justice of the Peace, Juvenile Court Clerk, Juvenile Court Judge, Labor Commissioner, Lieutenant Governor, Magistrate Judge, Mayor, Metro Auditor, Metro Councilor, Missouri, Montana, Municipal Court Clerk, Municipal Court Judge, Parks & Recreation Board Commissioner, President, Probate Court Clerk, Probate Court Judge, Property Valuation Administrator, Public Administrator, Public Advocate, Public Defender, Public Lands Commissioner, Public Service Commissioner, Public Utilities Commissioner, Railroad Commissioner, Register, Register of Wills, School Board Member, School and Public Lands Commissioner, Secretary of State, Shadow Senator, Sheriff, Soil & Water Supervisor, Solicitor General, State Auditor, State Comptroller, State Controller, State Court Judge, State Delegate, State Mine Inspector, State Representative, State Senator, State Treasurer, Superintendent of Public Instruction, Superior Court Clerk, Superior Court Judge, Supervisor, Supreme Court Judge, Tax Commissioner, U.S. Representative, U.S. Senator, Vice Mayor, Vice President, Water Conservation Board Director';
        return allStates.split(/, +/g).map(function(state) {
            return {
                value: state.toLowerCase(),
                display: state
            };
        });
  }
  getSearchResult() {
    
    localStorage.setItem('nextElectionYear',this.nextElectionYear);
    localStorage.setItem('campaignOnly',this.campaignOnly);
    localStorage.setItem('first_name_search',this.searchbyfirstname);
    localStorage.setItem('last_name_search', '');
    localStorage.setItem('juriName',this.juriName);
    localStorage.setItem('juriCoverageArea',this.juriCoverageArea);
    localStorage.setItem('state_name',this.state_name);
    localStorage.setItem('juri_type',this.juri_type);
    let offieTitle:any;
    if (this.selectedItem != null) {
        offieTitle = this.selectedItem.display;
    } else {
        offieTitle = '';
    }

    localStorage.setItem('office_title_search',offieTitle);
    // vm.searchresult = localStorage.searchLeader;
    this.sendrequest.goToPage('LeaderSearchResultPage');
   
    
  };

}
