import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeadersPageOnboardingPage } from './leaders-page-onboarding';

@NgModule({
  declarations: [
    LeadersPageOnboardingPage,
  ],
  imports: [
    IonicPageModule.forChild(LeadersPageOnboardingPage),
  ],
})
export class LeadersPageOnboardingPageModule {}
