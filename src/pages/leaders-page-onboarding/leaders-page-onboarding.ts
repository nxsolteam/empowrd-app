import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeadersPageOnboardingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaders-page-onboarding',
  templateUrl: 'leaders-page-onboarding.html',
})
export class LeadersPageOnboardingPage {
  totalLeaderSelected = 0;
  showLeaders =false;
  cityCheckbox =false;
  stateCheckbox =false;
  countyCheckbox =false;
  countryCheckbox =false;
  searchBarSection=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeadersPageOnboardingPage');
  }
  viewAll(value){
  	this.showLeaders =true;
  }
  changeCheckbox(event,name){
  	if( event.checked) {
  		this.totalLeaderSelected += 1;
  	} else {
  		this.totalLeaderSelected -= 1;
  	}
  }
  showSearchBarSection() {
    this.searchBarSection= !this.searchBarSection;
  }
}
