import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the LeaderSearchResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leader-search-result',
  templateUrl: 'leader-search-result.html',
})
export class LeaderSearchResultPage {
  header_data:any;
  user:any;
  searchstring:string;
  
  showCampaignData:any;
  campaignOnly:string;
  campaignValue:string;
  nextElectionYear:string;
  advanceSearch:boolean;
  firstName:string;
  lastName:string;
  searchLeader:string;
  officeTitle:string;
  stateName:string;
  juryType:string;
  juryName:string;
  juryCoverageArea:string;
  searchdata:any;
  serchdatacount:number;
  pagenext:number;
  pageprev:number;
  total_pagination:number;
  total_record_per_page:number;
  header_string_in_searchresult:any=[];
  array_string_or_not:any;
  stateshow:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController) {
  	this.header_data={ismenu:false,ishome:false,title:"SEARCH RESULTS"};
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	var searchLeader =localStorage.getItem('searchLeader');
        if (searchLeader) {
            searchLeader = searchLeader;
            this.searchstring = searchLeader;
           
        } else {
            searchLeader = 'none';
            this.searchstring = '';
        }
        let loading = this.loadingCtrl.create({
	        content: 'Please wait...',
	        spinner: 'bubbles'
	    });
	    loading.present();
	  	this.sendrequest.getResult('user/showCityCountyLeaderData','get',{}).then((response:any) => {     
	      	loading.dismiss();
	  	  	this.showCampaignData =response.data.showData;
	  	},
	  	error => {
	      loading.dismiss();
	      this.sendrequest.presentToast("Error occured. Please try again");
	  	});
        this.campaignValue='';
        var campaignOnly = localStorage.getItem('campaignOnly');
        if (campaignOnly != '') {
            this.campaignOnly = campaignOnly;
            if(this.campaignOnly=='2') {
                this.campaignValue='Incumbent Official';
            }
            else if(this.campaignOnly=='1') {
                this.campaignValue='Incumbents & Candidates';
            }
            else if(this.campaignOnly=='0') {
                this.campaignValue='Candidates Only';
            }
        } else {
            this.campaignOnly = '2';
            this.campaignValue='Incumbent Official';
        }
        var nextElectionYear = localStorage.getItem('nextElectionYear');
        if (nextElectionYear) {
            this.nextElectionYear = nextElectionYear;
        } else {
            this.nextElectionYear = '0';
        }
        
        var stateName = localStorage.getItem('state_name');
        if (stateName) {
            stateName = stateName;
            this.advanceSearch = false;
        } else {
            stateName = 'none';
            this.stateName = '';
            this.stateshow = false;
        }
        var firstName = localStorage.getItem('first_name_search');
        if (firstName) {
            firstName = firstName;
            this.advanceSearch = false;
        } else {
            firstName = 'none';
        }
        console.log("first_name_search");
        console.log( localStorage.getItem('first_name_search'));
        var lastName = localStorage.getItem('last_name_search');
        if (lastName) {
            lastName = lastName;
            this.advanceSearch = false;
        } else {
            lastName = 'none';
        }
        var officeTitle = localStorage.getItem('office_title_search');
        if (officeTitle) {
            officeTitle = officeTitle;
            this.advanceSearch = false;
        } else {
            officeTitle = 'none';
        }
        var juryType = localStorage.getItem('juri_type');
        if (juryType) {
            juryType = juryType;
            this.advanceSearch = false;
        } else {
            juryType = 'none';
        }
        var juryName = localStorage.getItem('juriName');
        if (juryName) {
            juryName = juryName;
            this.advanceSearch = false;
        } else {
            juryName = 'none';
        }
        var juryCoverageArea = localStorage.getItem('juriCoverageArea');
        if (juryCoverageArea) {
            juryCoverageArea = juryCoverageArea;
            this.advanceSearch = false;
        } else {
            juryCoverageArea = 'none';
        }
        this.firstName = firstName;
        this.lastName = lastName;
        this.searchLeader = searchLeader;
        this.officeTitle = officeTitle;
        this.stateName = stateName;
        this.juryType = juryType;
        this.juryName = juryName; 
        this.juryCoverageArea = juryCoverageArea;
        this.searchResult();
  }
  searchResult(){
  	let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
   	this.sendrequest.getResult('leaders/getSearchResult/' + this.searchLeader + '/' + this.stateName + '/' + this.firstName + '/' + this.lastName + '/' + this.officeTitle + '/' + this.juryType + '/' + this.juryName + '/' + this.juryCoverageArea + '/' +this.campaignOnly+'/'+this.nextElectionYear+'/' + this.user.id + '/1','get',{}).then((response:any) => {     
      	loading.dismiss();
  	  	localStorage.setItem('page','2');
        this.searchdata = response.data.searchresult;
        this.serchdatacount = response.data.searchresult.length;
       
        this.pagenext = 2;
        this.pageprev = 1;
        this.total_pagination = response.data.total_pagination;
        this.total_record_per_page = response.data.total_record;
        this.header_string_in_searchresult = response.data.header_string_in_searchresult;
        this.array_string_or_not = response.data.array;
        //$scope.lastpagination = true;
        /*if (response.data.total_record <= 50) {
            $scope.loadingmoretextend = false;
        }*/
        /*setTimeout(function() {
            $scope.loading = false;
        }, 500);
        if (vm.serchdatacount == 0) {
            $scope.noresultdiv = false;
        }*/
  	},
  	error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
  	});

  	
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderSearchResultPage');
  }
  searchAdjust(string) {
    if (string == "firstname") {
      this.firstName = 'none';
    }
    if (string == "lastname") {
      this.lastName = 'none';
    }
    if (string == "officetitle") {
      this.officeTitle = 'none';
    }
    if (string == "jurytype") {
      this.juryType = 'none';
    }
    if (string == "juryname") {
      this.juryName = 'none';
    }
    if (string == "jurycoveragearea") {
      this.juryCoverageArea = 'none';
    }
    if (string == "campaignOnly") {
      this.campaignOnly = '0';
    }
    if (string == "nextElectionYear") {
      this.nextElectionYear = 'none';
    }
    
    if (this.firstName == 'none' && this.lastName == 'none' && this.officeTitle == 'none' && this.juryType == 'none' && this.juryCoverageArea == 'none' && this.juryName == 'none' && this.nextElectionYear =='none') {
     
      this.sendrequest.goToPage('');
    }
    
    localStorage.setItem('nextElectionYear',this.nextElectionYear);
    localStorage.setItem('campaignOnly',this.campaignOnly);
    localStorage.setItem('first_name_search',this.firstName);
    localStorage.setItem('last_name_search', this.lastName);
    localStorage.setItem('juriName',this.juryName);
    localStorage.setItem('juriCoverageArea',this.juryCoverageArea);
    localStorage.setItem('state_name',this.stateName);
    localStorage.setItem('juri_type',this.juryType);
    localStorage.setItem('office_title_search',this.officeTitle );
    this.searchResult();
  }
  logScrollEnd(event){

  }
}
