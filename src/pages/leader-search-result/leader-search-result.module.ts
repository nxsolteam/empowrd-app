import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaderSearchResultPage } from './leader-search-result';
import { ComponentsModule }from '../../components/components.module';
@NgModule({
  declarations: [
    LeaderSearchResultPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(LeaderSearchResultPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class LeaderSearchResultPageModule {}
