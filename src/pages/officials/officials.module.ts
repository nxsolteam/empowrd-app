import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfficialsPage } from './officials';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { DirectivesModule } from '../../directives/directives.module';
import { ComponentsModule }from '../../components/components.module';

@NgModule({
  declarations: [
    OfficialsPage,
  //  ParallaxHeaderDirective,
  ],
  imports: [
  	DirectivesModule,
    IonicPageModule.forChild(OfficialsPage),
   	ComponentsModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class OfficialsPageModule {}
