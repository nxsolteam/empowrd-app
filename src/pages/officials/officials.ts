import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content, Select, Platform  } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { Observable } from 'rxjs/Observable';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';

/**
 * Generated class for the OfficialsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-officials',
  templateUrl: 'officials.html',
})
export class OfficialsPage {
  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('electionDrownDown') electionDrownDown: Select;
  public leaderlistCache: Cache<Leaderlist[]>;
  public leaderlist$: Observable<Leaderlist[]>;
  selectedLeaders=[];
  leadersdata:any=[];
  statename:string;
  header_data:any={};
  resultData:any;
  user:any;
  addressOnHeader='';
  leaderSelectedArray=[]; 
  totalstate:number;
  leadercountydata:any=[];
  countyname:string;
   
  totalcounty:number;
  leadercitydata:any=[];
  navMenu='CURRENT REPS';
   
  totalcity:number;
  leaderusdata:any=[];
  totalusleaders:number;
  totalLeaders=20;
  cityname:string='';
  shortAddress='';
  scrallDirection='';
  showDropDown = false;
  messageCount='0';
  powerScore='0';
  initial='';
  initialName='';
  validateAddress=false;
  forceUpdateRaces=false;

  ElectionDateOptions = {
    cssClass: 'election-date-dropdown'
  };
  
  showElectionsDropDown = false;
  gridthird=false;
  gridsecond=false;
  gridfirst=false;
  dropDownDate="NOV. 23, 2020";
  racesResult:any=[];
  electionDates:any=[];
  selectedDate='';
  racesCount=0;
  measureCount=0;
  selectedDateType='';
  allMeasuresData:any=[];
  measuresData:any=[];
  leaderLoaderShow=true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private statusBar: StatusBar, public platform: Platform) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.initialName = this.user.first_name[0]+""+this.user.last_name[0];
    let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
    if(totalpowerScore > 999) {
      this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
    } else {
      this.powerScore = localStorage.getItem('powerScore');
    }
    events.subscribe('powerScoreUpdate:created', (user, time) => {
      let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
      if(totalpowerScore > 999) {
        this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
      } else {
        this.powerScore = localStorage.getItem('powerScore');
      }
    });
    this.initial=this.user.first_name.charAt(0)+""+this.user.last_name.charAt(0);
    this.messageCount = localStorage.getItem('messageCount');
    events.subscribe('messageCountChange:created', (user, time) => {
      this.messageCount = localStorage.getItem('messageCount');
    });
    events.subscribe('setLatLon:created', (user, time) => {
  console.log(" setLatLon 0 ");

      if(localStorage.getItem('validateAddress')!= undefined) {
        if(localStorage.getItem('validateAddress')=='0') {
          this.validateAddress=false;
        } else {
          this.validateAddress=true;
        }
      }
      if(localStorage.getItem('addresstext')!= undefined) {
        this.addressOnHeader = localStorage.getItem('addresstext');
        this.shortAddress = this.addressOnHeader.substring(0, 22);
      }
      this.leaderLoaderShow=true;
    });
    if(localStorage.getItem('validateAddress')!= undefined) {
      if(localStorage.getItem('validateAddress')=='0') {
        this.validateAddress=false;
      } else {
        this.validateAddress=true;
      }
    }
    this.addressOnHeader=localStorage.getItem('addresstext');
    this.shortAddress = this.addressOnHeader.substring(0, 22);
    this.getLeaderData();
    
  }
  civicProfilePage() {
    this.events.publish('gotoPage:created', 'CivicProfilePage', {});
  }
  segmentChanged(event) {

  }
  ionViewWillEnter() {
   this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
  }
  ionViewDidEnter() {
    this.leaderlistProvider.leaderlistRefresh();
  }
  getLeaders(leaders) {
    let date = this.selectedDate;
    if(this.selectedDateType=='general') {
      leaders = leaders.filter((v) => {
        if(v.next_general_election_date==date) {
          return true;
        }
        return false;
      })
    } else if(this.selectedDateType=='primary') {
      leaders = leaders.filter((v) => {
        if(v.next_primary_election_date==date) {
          return true;
        }
        return false;
      })
    } else {
      leaders = leaders.filter((v) => {
        if(v.next_special_election_date==date) {
          return true;
        }
        return false;
      })
    }
    return leaders;
  }
 
  toggleElectionDropDown() {
    // this.showElectionsDropDown = !this.showElectionsDropDown;
    setTimeout(() => {
      this.electionDrownDown.open();
    },150);
  }
  measureDetail(measure) {
    localStorage.setItem('backFromElection', '1');
    localStorage.setItem('raceDetail', JSON.stringify(measure));
    localStorage.setItem('showRace', 'false');
    this.events.publish('raceDetails:created', measure, Date.now());
    this.events.publish('selectedTabs:data', '5', Date.now());
  }
  raceDetail(race) {
    localStorage.setItem('backFromElection', '1');
    localStorage.setItem('raceDetail', JSON.stringify(race));
    localStorage.setItem('showRace', 'true');
    this.events.publish('raceDetails:created', race, Date.now());
    this.events.publish('selectedTabs:data', '5', Date.now());

  }
  
  editAddress() {
    this.events.publish('gotoPage:created', 'AddEditAddressPage', {});
  }
  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
    this.events.publish('addressDropDown:created', {'status':this.showDropDown }, Date.now());
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OfficialsPage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    //document.getElementById("officialheaderNavbar").classList.add("fadeUp");
    //document.getElementById("hidetabs").classList.add("inProgress");
    // document.getElementById("hidetabs").classList.add("inProgress");
    // document.getElementById("officialheaderNavbar").classList.add("fadeUp");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("officialheaderNavbar").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    
  }
  
  getTitle(title,index) {
    if(index=='0') {
      return title.substring(0, title.lastIndexOf(" ") + 1).trim();
    } else {
      return title.substring(title.lastIndexOf(" ") + 1).trim();
 
    }
  }
  
  getLeaderData() {
  console.log(" leader response 0 ");
    
    this.cacheService
        .get('leaderlist')
        .mergeMap((leaderlistCacheResponse: Cache<Leaderlist[]>) => {
            console.log(" leader response 1 ");
            //this.leaderlistCache = leaderlistCacheResponse;
            return leaderlistCacheResponse.get$;
        }).subscribe((response:any) => {
            console.log(" leader response 2 ");
            this.leadersdata = response.data.leaders;
            this.leaderLoaderShow=false;
            console.log(this.leadersdata);
            this.statename = response.data.state_name;
            this.totalstate = response.data.statedatacount;
            
            this.countyname = response.data.county_name;
            this.totalcounty = response.data.countydatacount;

            this.cityname = response.data.city_name;
            this.cityname=response.data.city_name.replace("City", "").trim();
            this.totalcity = response.data.citydatacount;
            this.totalusleaders = response.data.usdatacount;
            this.jurisdictionsearchresult((this.totalusleaders+this.totalcity+this.totalcounty+this.totalstate));
            localStorage.setItem('statenametodispalyonmap',response.data.state_name);
            localStorage.setItem('countynametodispalyonmap',response.data.county_name);
            localStorage.setItem('citynametodispalyonmap', response.data.city_name);
console.log(" leader response 3 ");
        });
  }
  jurisdictionsearchresult(totalLeaders) {
    this.sendrequest.getResult('leaders/jurisdictionsearchresult/'+this.user.id, 'post', { 'address': this.addressOnHeader, 'latitude': localStorage.getItem('latitude'), 'longitude': localStorage.getItem('longitude'), 'status': 1, 'total_leader': totalLeaders }).then((response: any) => {
    },
    error => {
    })
  }
  leaderDetailPage(leader) {
  	this.events.publish('gotoPage:created', 'LeaderDetailPage', { 'leader':leader });
  }

}
