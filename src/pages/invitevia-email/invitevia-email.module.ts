import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InviteviaEmailPage } from './invitevia-email';

@NgModule({
  declarations: [
    InviteviaEmailPage,
  ],
  imports: [
    IonicPageModule.forChild(InviteviaEmailPage),
  ],
})
export class InviteviaEmailPageModule {}
