import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';  
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { GooglePlus } from '@ionic-native/google-plus';

/**
 * Generated class for the InviteviaEmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invitevia-email',
  templateUrl: 'invitevia-email.html',
})
export class InviteviaEmailPage {

  @ViewChild(ParallaxHeaderDirective) directive = null;
  user:any;
  checkShareTwitter=false;
  checkShareFacebook=false;
  inviteContactsCount=0;
  private inviteContactsList: any=[];   
  private contactlist: any=[];   
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, private googlePlus: GooglePlus ) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.googleLogin();
    /* this.inviteContactsList.push({'name':'Bhavesh', 'email':'bhavesh.r.php@gmail.com'});
        this.inviteContactsCount = this.inviteContactsList.length;
        */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InviteviaEmailPage');
  }
  cancel() {
  	this.navCtrl.pop({animate:false});
  }
  inviteFriends() {
    if(this.inviteContactsCount > 0){
    	// alert(JSON.stringify(this.inviteContactsList));
      this.events.publish('invitedEmailContacts:created', this.inviteContactsCount, Date.now());
      this.sendrequest.getResult('user/inviteEmailContacts/' + this.user.id,'post',{ invite: this.inviteContactsList }).then((response:any) => {
          },
      error => {
        });
     } else {
    }
    this.navCtrl.pop({animate:false});
  }
  googleLogin(){
   // https://www.google.com/m8/feeds
    this.googlePlus.login(
     {'scopes': 'https://www.googleapis.com/auth/contacts.readonly'})
    .then((res:any) => {
      console.log(res);
      this.importEmail(res)
    })
    .catch(err => console.log(err));
  }
  clearAll() {
    this.inviteContactsList = [];
    this.inviteContactsCount = this.inviteContactsList.length;
    for (var i = 0; i < this.contactlist.length; i++) {
      this.contactlist[i].selected=false;
    }
  }
  addAll() {
    for (var i = 0; i < this.contactlist.length; i++) {
      if(this.contactlist[i].selected==false) {
        this.contactlist[i].selected=true;
        this.inviteContactsList.push({'name':this.contactlist[i].name, 'email':this.contactlist[i].email});
      } 
    }
    this.inviteContactsList = [];
    this.inviteContactsCount = this.inviteContactsList.length;
  }
  inviteContacts(contact) {
    if(contact.selected==true) {
      //contact.selected=true;
      this.inviteContactsList.push({'name':contact.name, 'email':contact.email});
    } else {
      //contact.selected=false;
      var index = this.inviteContactsList.map(function(o) { return o.email; }).indexOf(contact.email);
      this.inviteContactsList.splice( index, 1 );
    }
    this.inviteContactsCount = this.inviteContactsList.length;
  }
  importEmail(obj) {
    this.sendrequest.getEmails(obj).then((response:any) => { 
      console.log(response);
      console.log(response.feed.entry);
      let contactsArray = [];
      let contacts:any = response.feed.entry;
      for(var i=0; i< contacts.length;i++) {
        let user = {
          'name': contacts[i].title.$t,
          'email': contacts[i].gd$email[0].address,
          'selected':false
        };
        this.contactlist.push(user);
      }
      // alert(JSON.stringify(this.contactlist));
      this.contactlist.sort(this.compare);
    
    },
    error => {
     console.log(error);
    }) 
  }
  compare(a,b) {
    if (a.name < b.name)
      return -1;
    if (a.name > b.name)
      return 1;
    return 0;
  }

}
