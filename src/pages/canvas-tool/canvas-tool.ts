import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Events, Content, AlertController, Slides, Platform } from 'ionic-angular';
import { Map, tileLayer, marker, geoJSON, LayerOptions, icon, circleMarker } from 'leaflet';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

declare var google;

/**
 * Generated class for the CanvasToolPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-canvas-tool',
  templateUrl: 'canvas-tool.html',
})
export class CanvasToolPage {

  @ViewChild('canvasToolMap') mapElement: ElementRef;
  map: any;
  showSignUpList=false;
  numberOfSignUp=0;
  addressDropdown=[];
  user:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, public sendrequest: SendrequestProvider) {
    this.user =  JSON.parse(localStorage.getItem('user'));
  }
  gotoNewSignup() {
    let params={};
    let newSignUpModal = this.modalCtrl.create('NewSignUpWebPage', params);
    newSignUpModal.onDidDismiss(data => {
       console.log(data);
    });
    newSignUpModal.present();

  }
  editNewSignup(jurisdictions) {
    let newSignUpModal = this.modalCtrl.create('NewSignUpWebPage', { 'jurisdictions': jurisdictions });
    newSignUpModal.onDidDismiss(data => {
       console.log(data);
    });
    newSignUpModal.present();
  }
  loadNewSignUpData() {
    this.sendrequest.getResult('user/GetnewSignupLive/' + this.user.id,'post',{ 'addressId': localStorage.getItem('addressId') }).then((response:any) => { 
      this.addressDropdown = response.data;
    },
    error => {
      
    });
  }
  ionViewDidLoad() {
    this.loadNewSignUpData();    
    console.log('ionViewDidLoad CanvasToolPage');
    console.log("this.mapElement : ", this.mapElement);
  	let lat=parseFloat(localStorage.getItem('latitude'));
    let lon=parseFloat(localStorage.getItem('longitude'));
  	let mapOptions = {
      zoom: 9,
      gestureHandling: 'none',
      zoomControl: false,
      fullscreenControl: false,
      mapTypeControl: false,
      center: new google.maps.LatLng(lat, lon), // New York
      styles: [{"featureType":"road","stylers":[{"hue":"#5e00ff"},{"saturation":-79}]},{"featureType":"poi","stylers":[{"saturation":-78},{"hue":"#6600ff"},{"lightness":-47},{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"lightness":22}]},{"featureType":"landscape","stylers":[{"hue":"#6600ff"},{"saturation":-11}]},{},{},{"featureType":"water","stylers":[{"saturation":-65},{"hue":"#1900ff"},{"lightness":8}]},{"featureType":"road.local","stylers":[{"weight":1.3},{"lightness":30}]},{"featureType":"transit","stylers":[{"visibility":"simplified"},{"hue":"#5e00ff"},{"saturation":-16}]},{"featureType":"transit.line","stylers":[{"saturation":-72}]},{}]
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

}
