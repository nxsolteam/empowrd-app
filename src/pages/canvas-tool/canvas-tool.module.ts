import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanvasToolPage } from './canvas-tool';

@NgModule({
  declarations: [
    CanvasToolPage,
  ],
  imports: [
    IonicPageModule.forChild(CanvasToolPage),
  ],
})
export class CanvasToolPageModule {}
