import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventReviewPage } from './event-review';

@NgModule({
  declarations: [
    EventReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(EventReviewPage),
  ],
})
export class EventReviewPageModule {}
