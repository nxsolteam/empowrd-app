import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InviteviaSmsPage } from './invitevia-sms';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    InviteviaSmsPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(InviteviaSmsPage),
  ],
})
export class InviteviaSmsPageModule {}
