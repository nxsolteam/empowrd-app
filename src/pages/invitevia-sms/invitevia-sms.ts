import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';   
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the InviteviaSmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invitevia-sms',
  templateUrl: 'invitevia-sms.html',
})
export class InviteviaSmsPage {
  @ViewChild(ParallaxHeaderDirective) directive = null;
  user:any;
  checkShareTwitter=false;
  checkShareFacebook=false;
  inviteContactsCount=0;
  private inviteContactsList: any=[];   
  private contactlist: any=[]; 
  loaderShow=true;  
  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing, private contacts: Contacts, public sendrequest: SendrequestProvider, public events: Events ) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {
      for (var i = 0; i < contacts.length; i++) {
        if(contacts[i].phoneNumbers != null ){
          contacts[i].selected=false;
          contacts[i].showContacts=true;
          if(contacts[i].name==null) {
            contacts[i].name ={'formatted': this.getName(contacts[i])};
          } else if(contacts[i].name.formatted=='' || contacts[i].name.formatted==null) {
             contacts[i].name.formatted = (contacts[i].phoneNumbers.length > 0)?contacts[i].phoneNumbers[0].value:'';
          }
          this.contactlist.push(contacts[i]);
        }
      }
      this.contactlist.sort(this.compare);
      this.loaderShow=false;
    }, (error) => {
      console.log(error);
      this.loaderShow=false;
    });
    /*
      let phoneArr = [];
      let contact = [];
      phoneArr.push({ 'value': 8160650688, 'id':111 });
      phoneArr.push({ 'value': 9913179768, 'id':113 });
      contact.push({ 'selected': false, 'showContacts':true, 'phoneNumbers':phoneArr, 'name': { 'formatted': 'Bhavesh Rabadiya'} });
      phoneArr = [];
      phoneArr.push({ 'value': 8000343638, 'id':112 });
      contact.push({ 'selected': false, 'showContacts':true, 'phoneNumbers':phoneArr, 'name': { 'formatted': 'Jatin Bhatt'} });
      this.contactlist=[];
      for (var i = 0; i < contact.length; i++) {
        if(contact[i].phoneNumbers != null ){
          contact[i].selected=false;
          contact[i].showContacts=true;
          if(contact[i].name==null) {
            contact[i].name ={'formatted': this.getName(contact[i])};
          } else if(contact[i].name.formatted=='' || contact[i].name.formatted==null) {
             contact[i].name.formatted = (contact[i].phoneNumbers.length > 0)?contact[i].phoneNumbers[0].value:'';
          }
          this.contactlist.push(contact[i]);
        }
      }
      this.contactlist.sort(this.compare);
      this.loaderShow=false; 
    */
  }
  
  getName(contact) {
    if(contact.name!=null) {
      if(contact.name.formatted=='') {
        return (contact.phoneNumbers.length > 0)?contact.phoneNumbers[0].value:'';
      } else {
        return contact.name.formatted;
      }
    } else if(contact.displayName!=null) {
      return contact.displayName;
    } else if(contact.phoneNumbers.length > 0) {
      return contact.phoneNumbers[0].value;
    } else {
      return '';
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InviteviaSmsPage');
  }

  compare(a,b) {
   if (a.name.formatted < b.name.formatted)
     return -1;
   if (a.name.formatted > b.name.formatted)
     return 1;
   return 0;
  }
  cancel() {
  	this.navCtrl.pop({animate:false});
  }
  inviteFriends() {
    if(this.inviteContactsCount > 0){
      this.events.publish('invitedContacts:created', this.inviteContactsCount, Date.now());
      this.sendrequest.getResult('user/inviteContacts/' + this.user.id,'post',{ invite: this.inviteContactsList }).then((response:any) => {
          },
      error => {
        });
     } else {
    }
    this.navCtrl.pop({animate:false});
  }
  clearAll() {
    this.inviteContactsList = [];
    this.inviteContactsCount = this.inviteContactsList.length;
    for (var i = 0; i < this.contactlist.length; i++) {
      this.contactlist[i].selected=false;
    }
  }
  addAll() {
    // this.inviteContactsList = [];
    for (var i = 0; i < this.contactlist.length; i++) {
      if(this.contactlist[i].selected==false) {
        this.contactlist[i].selected=true;
        this.inviteContactsList.push({'displayName':this.contactlist[i].name.formatted,'id':this.contactlist[i].id,'phoneNumbers':this.contactlist[i].phoneNumbers[0].value});
      } 
    }
    this.inviteContactsCount = this.inviteContactsList.length;
  }
  inviteContacts(contact) {
    if(contact.selected==true) {
      //contact.selected=true;
      this.inviteContactsList.push({'displayName': '' ,'id':contact.id,'phoneNumbers':contact.phoneNumbers[0].value});
    } else {
      //contact.selected=false;
      var index = this.inviteContactsList.map(function(o) { return o.id; }).indexOf(contact.id);
      this.inviteContactsList.splice( index, 1 );
    }
    this.inviteContactsCount = this.inviteContactsList.length;
  }

  shareTwitter() {
  	this.socialSharing.shareViaTwitter(this.user.first_name+' '+this.user.last_name+' invites you to join my constituency on EMPOWRD.  EMPOWRD is an easy to use mobilization platform for elected leaders, influencers, and organizations.  Sign up, select the causes and leaders you want to follow and use your power to make change.  Click the link below to download the app. Download EMPOWRD APP From http://onelink.to/xzexga', null, null).then((result:any) => {
	    console.log('share result ', result);
	    this.checkShareTwitter=true;
  	},
  	error => {
  	      console.log('share error ', error)
  	})
  }
  shareFacebook() {
  	this.socialSharing.shareViaFacebook(this.user.first_name+' '+this.user.last_name+' invites you to join my constituency on EMPOWRD.  EMPOWRD is an easy to use mobilization platform for elected leaders, influencers, and organizations.  Sign up, select the causes and leaders you want to follow and use your power to make change.  Click the link below to download the app. Download EMPOWRD APP From http://onelink.to/xzexga, null, http://onelink.to/xzexga').then((result:any) => {
	    console.log('share result ', result);
	    this.checkShareFacebook=true;
  	},
  	error => {
  	      console.log('share error ', error)
  	})
  }

}
