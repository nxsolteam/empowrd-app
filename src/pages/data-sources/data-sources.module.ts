import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataSourcesPage } from './data-sources';

@NgModule({
  declarations: [
    DataSourcesPage,
  ],
  imports: [
    IonicPageModule.forChild(DataSourcesPage),
  ],
})
export class DataSourcesPageModule {}
