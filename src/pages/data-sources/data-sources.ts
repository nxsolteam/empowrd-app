import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
/**
 * Generated class for the DataSourcesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data-sources',
  templateUrl: 'data-sources.html',
})
export class DataSourcesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  private callNumber: CallNumber, private emailComposer: EmailComposer, private iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataSourcesPage');
  }
  callToNumber(number) {
      this.callNumber.callNumber(number, true)
      .then((res:any) => {  })
      .catch(err => console.log('Error launching dialer', err));
   }

  sendEmail(email) {
    let emailOptions = {
      to: email,
    };
    // Send a text message using default options
    this.emailComposer.open(emailOptions);
  }
  openUrl(url){
    if (url.substring(0, 4) !== 'http') {
        url = 'http://' + url;
    }
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  goBack() {
    this.navCtrl.pop({ animate:true, direction: 'forward', animation: 'ios-transition'});
  }
}
