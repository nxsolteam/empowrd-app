import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PowerscorePage } from './powerscore';

@NgModule({
  declarations: [
    PowerscorePage,
  ],
  imports: [
    IonicPageModule.forChild(PowerscorePage),
  ],
})
export class PowerscorePageModule {}
