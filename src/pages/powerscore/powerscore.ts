import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PowerscorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-powerscore',
  templateUrl: 'powerscore.html',
})
export class PowerscorePage {

  powerScoreList:any =[];
  powerScore='0';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.powerScore = localStorage.getItem('powerScore');
  	this.powerScoreList = JSON.parse(localStorage.getItem('powerScorePoint'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PowerscorePage');
  }
  goBack() {
    //this.events.publish('createProfileContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }

}
