import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivityEvaluationPage } from './activity-evaluation';

@NgModule({
  declarations: [
    ActivityEvaluationPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivityEvaluationPage),
  ],
})
export class ActivityEvaluationPageModule {}
