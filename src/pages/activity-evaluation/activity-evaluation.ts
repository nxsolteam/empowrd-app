import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { SelectedEventActivitiesProvider } from '../../providers/selected-event-activities/selected-event-activities';

/**
 * Generated class for the ActivityEvaluationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-evaluation',
  templateUrl: 'activity-evaluation.html',
})
export class ActivityEvaluationPage {

  activityDetail:any;
  objectiveValueQue1 = [];
  objectiveValueQue2 = [];
  questionFirstObj = [];
  questionSecondObj = [];
  question3='';
  question4='';
  question5='';
  question6='';
  question7='';
  question8='';
  question9='';
  question10='';
  activityMessageId:number; //3862
  userMessageId:number;
  activityComplete=false;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private selectedEventActivitiesProvider: SelectedEventActivitiesProvider,public modalCtrl: ModalController) {
  	this.activityDetail = navParams.get('activity');
  	this.questionFirstObj = navParams.get('questionFirstObj');
  	this.questionSecondObj = navParams.get('questionSecondObj');
  	this.user =  JSON.parse(localStorage.getItem('user'));

  }
  goToEventList() {
	this.activityComplete = false;
    this.navCtrl.remove(this.navCtrl.getActive().index - 1, 2,{animate:true, animation: 'ios-transition'});
  }
  showactivityComplete() {
  	var log = [];
	let evaluation={};
	let evaluationquestion_1=[];
	let evaluationquestion_2=[];
	let evaluationSend=true;
	this.objectiveValueQue1.forEach(function(value, key){
	    if (value) {
	       this.push({'id':key,'value':value});
	    } else {
	        evaluationSend=false;
	    }
	}, evaluationquestion_1);
	this.objectiveValueQue2.forEach(function(value, key){
	    if (value) {
	       this.push({'id':key,'value':value});
	    }
	    else {
	            evaluationSend=false;
	    }
	}, evaluationquestion_2);
	if(this.question3 == ''){
	    evaluationSend=false;
	}
	evaluation["question_3"]= {'id':'3','value':this.question3};
	if(this.question4 == ''){
	    evaluationSend=false;
	}
	evaluation["question_4"]={'id':'4','value':this.question4};
	if(this.question5 == ''){
	    evaluationSend=false;
	}
	evaluation["question_5"]= {'id':'5','value':this.question5};
	if(this.question6 == ''){
	    evaluationSend=false;
	}
	evaluation["question_6"]= {'id':'6','value':this.question6};
	evaluation["question_7"]=  {'id':'7','value':this.question7};
	evaluation["question_8"]= {'id':'8','value':this.question8};
	evaluation["question_9"]= {'id':'9','value':this.question9};
	evaluation["question_10"]= {'id':'10','value':this.question10};
	if(evaluationSend==false){
	    this.sendrequest.presentToast('Please complete evaluation.');
	    return;
	}
	console.log(evaluation);
	
	this.sendrequest.getResult('user/userEvaluation/' + this.user.id,'post',{activityId:this.activityDetail.id,
	    evaluation:evaluation,
	    evaluationquestion_1:evaluationquestion_1,
	    evaluationquestion_2:evaluationquestion_2 }).then((response:any) => { 
        this.activityMessageId=response.data.messageId;
        this.userMessageId=response.data.userMessageId;
		this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    },
    error => {
      
    })
	//this.activityComplete = true;
	let activityModal = this.modalCtrl.create('ActivityCompletedPage', { activityMessageId: this.activityMessageId });
	 activityModal.onDidDismiss(data => {
     console.log(data);
     	if(data!=null){
     		if(data.type=='eventCompleted') {
     			this.goToEventCompleted();
     		} else {
     			this.goToEventList();
     		}
     	}
   	});
   	activityModal.present();

  }
  goToEventCompleted() {
	// this.events.publish('gotoPage:created', 'MsgDetailPage', { 'msgId': umid });
    this.navCtrl.push('MsgDetailPage',{ 'activityMessageId': this.activityMessageId, 'activityDetail': this.activityDetail,'gotoEventActivityPage':'1', 'msgId': this.userMessageId },{animate:true, animation: 'ios-transition'});
  }
  goBack() {
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityEvaluationPage');
  }

}
