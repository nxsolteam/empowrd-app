import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content, Platform } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { Observable } from 'rxjs/Observable';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { StatusBar } from '@ionic-native/status-bar';

import * as moment from 'moment';

/**
 * Generated class for the RaceDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-race-details',
  templateUrl: 'race-details.html',
})
export class RaceDetailsPage {
  // @ViewChild(Content) content: Content;

  @ViewChild(ParallaxHeaderDirective) directive = null;
  selectedLeaders=[];
  leadersdata:any=[];
  statename:string;
  header_data:any={};
  resultData:any;
  user:any;
  addressOnHeader='';
  leaderSelectedArray=[]; 
  totalstate:number;
  leadercountydata:any=[];
  countyname:string;
   
  totalcounty:number;
  leadercitydata:any=[];
  
   
  totalcity:number;
  leaderusdata:any=[];
  totalusleaders:number;
  totalLeaders=0;
  cityname:string='';
  shortAddress='';
  raceDetail:any = {};
  showDescription=false;
  scrallDirection='';
  selectedDate='';
  selectedDateType='';
  showRace=true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private statusBar: StatusBar, public platform: Platform) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#EEE3CD');
    }
    // this.header_data={ismenu:true,ishome:true,title:"MY ELECTED LEADERS"};
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.addressOnHeader=localStorage.getItem('addresstext');
    this.shortAddress = this.addressOnHeader.substring(0, 15);
    this.getRaceDetails();
    events.subscribe('raceDetails:created', (race, time) => {
      this.getRaceDetails();
    });
    this.sendrequest.buttonClicked(this.user.id,'view_race_detail');
  }
  getRaceDetails() {
    if(localStorage.getItem('selectedDate') != undefined) {
      this.selectedDate = localStorage.getItem('selectedDate');
    }
    if(localStorage.getItem('selectedDateType') != undefined) {
      this.selectedDateType = localStorage.getItem('selectedDateType');
    }
    if(localStorage.getItem('showRace') != undefined) {
      if(localStorage.getItem('showRace')=='false') {
        this.showRace=false;
      } else {
        this.showRace=true;
      }
    }
    if(localStorage.getItem('raceDetail') != undefined) {
      this.raceDetail = JSON.parse(localStorage.getItem('raceDetail'));
      console.log(this.raceDetail);
    }
  }
  ionViewWillEnter() {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#EEE3CD');
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RaceDetailsPage');
  }
  getLeaders(leaders) {
    let date = this.selectedDate;
    if(this.selectedDateType=='general') {
      leaders = leaders.filter((v) => {
        if(v.next_general_election_date==date) {
          return true;
        }
        return false;
      })
    } else if(this.selectedDateType=='primary') {
      leaders = leaders.filter((v) => {
        if(v.next_primary_election_date==date) {
          return true;
        }
        return false;
      })
    } else {
      leaders = leaders.filter((v) => {
        if(v.next_special_election_date==date) {
          return true;
        }
        return false;
      })
    }
    return leaders;
  }
  getElectionDate(leader) {
    if(this.selectedDateType=='general') {
      return leader.next_general_election_date;
    } else if(this.selectedDateType=='special') {
      return leader.next_special_election_date;
    } else {
      return leader.next_primary_election_date;
    }
  }

  logScrollStart() {
    // console.log("Scroll Start");
    // document.getElementById("raceheaderNavbar").classList.add("fadeUp");
    // document.getElementById("hidetabs").classList.add("inProgress");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("raceheaderNavbar").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    // console.log("Scrolling");
    // console.log($event);
    /* if ($event.scrollTop > 100){
      document.getElementById("raceheaderNavbar").classList.add("shadow");
    } else{
      document.getElementById("raceheaderNavbar").classList.remove("shadow");
    } */
     // console.log("Scrolling");
    // console.log($event);
    /* if ($event.scrollTop > 100){
      document.getElementById("raceheaderNavbar").classList.add("shadow");
    } else{
      document.getElementById("raceheaderNavbar").classList.remove("shadow");
    } */
      // console.log("Scrolling");
     //console.log($event);
     
     /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
     this.scrallDirection=$event.directionY;
     if(this.scrallDirection=="down")
     {
       // document.getElementById("hidetabs").classList.add("inProgress");
       // document.getElementById("raceheaderNavbar").classList.add("fadeUp");
     }
     if(this.scrallDirection=="up")
     {
       // document.getElementById("hidetabs").classList.remove("inProgress");      
       // document.getElementById("raceheaderNavbar").classList.remove("fadeUp");        
     }
     /*
     MAULIK
     */
     /*
     if ($event.scrollTop > 100){
       document.getElementById("raceheaderNavbar").classList.add("shadow");
     } else{
       document.getElementById("raceheaderNavbar").classList.remove("shadow");
     } */
  }

  goBack() {
     this.navCtrl.pop({animate:true, animation: 'ios-transition'});
    /*if(localStorage.getItem('backFromElection') =='1') {
      this.events.publish('selectedTabs:data', '4', Date.now());
    } else {
      this.events.publish('selectedTabs:data', '3', Date.now());
    }*/
  }

  getFormattedDate(date){
    // return '';
    return moment(date).format('MM.DD.YYYY');

  }
  leaderDetailPage(leader) {
  	this.events.publish('gotoPage:created', 'LeaderDetailPage', { 'raceDetail': this.raceDetail, 'leaderDetail': leader });
  }
  
}
