import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RaceDetailsPage } from './race-details';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    RaceDetailsPage,
  ],
  imports: [
  	DirectivesModule,
    IonicPageModule.forChild(RaceDetailsPage),
  ],
})
export class RaceDetailsPageModule {}
