import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { Observable } from 'rxjs/Observable';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

import { HttpClient } from '@angular/common/http';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  resultData:any;
  header_data:any={};
  user:any;
  headerAddressTitle='';
  addressOnHeader='';
  newMessagePopup:boolean = false;
  showEventAddress='false';
  yourEngagementStart='';
  public leaderlistCache: Cache<Leaderlist[]>;
  public leaderlist$: Observable<Leaderlist[]>;
  leaderlist:any;
  titleonHeader='';
  newMessageSubscription:any;
  newMessagePopupSub:string ='';
  newMessagePopupText:string ='';
  senderName:string ='';
  adminName:string ='';
  adminPhoto:string ='';
  getNewMailCount = 0;
  composeMsg:boolean = false;
  msgId;
  multipleMessage:string ='';
  county='';
  city='';
  state='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private nativePageTransitions: NativePageTransitions) {
    this.user =  JSON.parse(localStorage.getItem('user'));
    if(localStorage.getItem('countNewMessage')== null || localStorage.getItem('countNewMessage')== undefined) {
      localStorage.setItem('countNewMessage','0');
    }
    // localStorage.setItem('countNewMessage','0');
  	this.cacheService.get('leaderlist').subscribe((leaderlistCache) => {
      this.leaderlist$ = leaderlistCache.get$;      
    });
    console.log("State : ");
    console.log(localStorage.getItem('state'));
    
    this.getLatestMessage();
    //this.newMessageSubscription.unsubscribe();
    this.newMessageSubscription = Observable.interval(10000).subscribe(x => { 
     console.log("call this getLatestMessage");
      this.getLatestMessage();
    });
    this.setAddressTitle();
    events.subscribe('getNewMessage:created', (user, time) => {
      this.newMessageSubscription.unsubscribe();
    });
    events.subscribe('setAddressTitle:created', (user, time) => {
      this.setAddressTitle();
    });
  }
  setAddressTitle() {
    if(localStorage.getItem('city')!= null && localStorage.getItem('city')!= undefined) {
      this.city =localStorage.getItem('city');
    }
    if(localStorage.getItem('county')!= null && localStorage.getItem('county')!= undefined) {
      this.county =localStorage.getItem('county');
    }
    if (localStorage.getItem('addresstext') !==null) {
      this.addressOnHeader=localStorage.getItem('addresstext');
    }
    if(localStorage.getItem('state')!= null && localStorage.getItem('state')!= undefined) {
      this.state =localStorage.getItem('state');
    }
  }
  getLatestMessage() {
 
    this.sendrequest.getResult('user/fetchLatestNewMailCount/' + this.user.id+'/2','get',{}).then((response:any) => { 
        console.log(response.data);
        console.log(parseInt(localStorage.getItem('countNewMessage')));
        this.getNewMailCount = parseInt(response.data.getNewMailCount);
        if(this.getNewMailCount > parseInt(localStorage.getItem('countNewMessage'))) {
          this.newMessagePopup=true;
        }
        console.log("newMessagePopup  : " + this.newMessagePopup);
        
        if(response.data.composeMsg){
            this.composeMsg = true;
        } else{
            this.composeMsg  = false;
        }
        this.msgId = response.data.id;
        this.newMessagePopupSub = response.data.getLatestUnreadMessage.subject;
        this.newMessagePopupText = response.data.getLatestUnreadMessage.body;
        this.senderName = response.data.getLatestUnreadMessage.senderName;
        this.adminName = response.data.getLatestUnreadMessage.adminName;
        this.adminPhoto = response.data.getLatestUnreadMessage.adminPhoto;
        this.multipleMessage = response.data.getLatestUnreadMessage.multipleMessage;
        localStorage.setItem('countNewMessage',response.data.getNewMailCount.toString());
        this.events.publish('countNewMessage:created', 'user', Date.now());
      });
  }
  closeNewmessageModal() {
    this.newMessagePopup=false;
  }
  goToMessages() {
    this.events.publish('goToPage:params', 'MsglistPage', {});
  }
  goToMessageList() {
    this.closeNewmessageModal();
    if(this.composeMsg == true){
      //this.navCtrl.push('MsgDetailPage', {'composeMessage':this.composeMsg,'msgId':this.msgId});
      this.events.publish('goToPage:params', 'MsgDetailPage', {'composeMessage':this.composeMsg,'msgId':this.msgId});
    } else {
      //this.navCtrl.push('MsgDetailPage', {'msgId':this.msgId});
      this.events.publish('goToPage:params', 'MsgDetailPage', {'msgId':this.msgId});

    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

}
