import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivityCompletedPage } from './activity-completed';

@NgModule({
  declarations: [
    ActivityCompletedPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivityCompletedPage),
  ],
})
export class ActivityCompletedPageModule {}
