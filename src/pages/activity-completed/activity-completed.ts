import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ActivityCompletedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-completed',
  templateUrl: 'activity-completed.html',
})
export class ActivityCompletedPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityCompletedPage');
  }
  goToEventCompleted() {
  	this.viewCtrl.dismiss({ 'type': 'eventCompleted'});
  }
  goToEventList() {
    this.viewCtrl.dismiss({ 'type': 'eventList'});
  }

}
