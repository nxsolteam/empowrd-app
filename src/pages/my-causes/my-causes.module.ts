import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCausesPage } from './my-causes';

@NgModule({
  declarations: [
    MyCausesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCausesPage),
  ],
})
export class MyCausesPageModule {}
