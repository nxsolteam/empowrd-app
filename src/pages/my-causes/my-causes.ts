import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { GetCausesListProvider } from '../../providers/get-causes-list/get-causes-list';
import { Cache, CacheService } from 'ionic-cache-observable';
import { GetCausesList } from '../../providers/get-causes-list/get-causes-list.model';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the MyCausesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-causes',
  templateUrl: 'my-causes.html',
})
export class MyCausesPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  public getCausesListCache: Cache<GetCausesList[]>;
  public getCausesList$: Observable<GetCausesList[]>;
  addCauseContent=true;
  selectedArray = [];
  totalSelected=0;
  showall='0';
  user:any;
  searchValue='';
  causes:any;
  allcauses:any;
  previouspowerScore=0;
  selectedArrayId=[]; 
  civicData:any;
  selectedCausesArray=[];
  loaderShow=true;
  powerScore='0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, public events: Events, private getCausesListProvider: GetCausesListProvider, private cacheService: CacheService) {
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.powerScore = localStorage.getItem('powerScore');
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    for(var i=0; i < this.civicData.causes.length; i++) {
      this.selectedCausesArray.push(this.civicData.causes[i].name);
    }
    console.log(this.selectedCausesArray);
    /* this.sendrequest.getResult('user/getcausesFour/' + this.user.id,'get',{}).then((response:any) => { 
      this.causes = response.data;
      this.causes.forEach(function(item){ 
        if(this.selectedCausesArray.indexOf(item.name) !== -1){
          item.isChecked=true;
          this.insertinvited(item.isChecked,item.accronym,item.name,item.id);
        } else {
          item.isChecked=false;
        }
      }, this); 
      this.loaderShow=false;
      this.allcauses=this.causes;
    },
    error => {
      this.sendrequest.presentToast("Error occured. Please try again");
    }); */
    this.getCausesListProvider.getCausesListRefresh();
    this.cacheService
      .get('getCausesList')
      .mergeMap((getCausesListCacheResponse: Cache<GetCausesList[]>) => {
          this.getCausesListCache = getCausesListCacheResponse;
          return this.getCausesListCache.get$;
      }).subscribe((response:any) => {
        this.causes = response.data;
        this.causes.forEach(function(item){ 
          if(this.selectedCausesArray.indexOf(item.name) !== -1){
            item.isChecked=true;
            this.insertinvited(item.isChecked,item.accronym,item.name,item.id);
          } else {
            item.isChecked=false;
          }
        }, this); 
        this.loaderShow=false;
        this.allcauses=this.causes;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCausesPage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    console.log($event);
  } 

  GoToNext(value){
    if(this.selectedArray.length!=0){
      this.sendrequest.getResult('user/addMultipleCauses/' + this.user.id,'post',{ organization:this.selectedArray}).then((response:any) => {
        this.sendrequest.updateCivicProfileData();
        this.sendrequest.updatePowerScore(this.user.id);
        },
    error => {
      });
    } 
    
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
    
  }
  goBack() {
    //this.events.publish('createProfileContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
  
  searchCauses() {
    this.causes = this.allcauses.filter((v) => {
    if (v.name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
       return true;
      }

      return false;
    })
  };
  showSelected(){
    this.showall='1';
    this.causes=[];
    for(var i=0;i < this.allcauses.length;i++){
        if(this.selectedArrayId.indexOf(this.allcauses[i].id) != -1){
            this.allcauses[i].isChecked=true; 
            this.causes.push(this.allcauses[i]);
        } else {
            this.allcauses[i].isChecked=false;
        }
    }
  };
  showAll() {
    this.showall='0';
    this.searchValue='';
    this.causes = this.allcauses;
  };
        
  insertinvited(v,a,f,id) {
    
    if(v){
        this.selectedArrayId.push(id);
        this.selectedArray.push({
            accronym: a, 
            full:  f,
            accronym_model:v
        });
        this.selectedCausesArray.push(f);
     } else {
        this.selectedCausesArray.splice(this.selectedCausesArray.indexOf(f), 1);
        this.selectedArrayId.splice(this.selectedArrayId.indexOf(id), 1);
        this.selectedArray =  this.removeByKey(this.selectedArray,{
            accronym: a, 
            full:  f,
            accronym_model:v
        });
      
     }
     this.totalSelected = this.selectedArray.length;
     //this.powerScore = this.previouspowerScore + (this.totalSelected * 5);
  }

  removeByKey(array, params){
   
    array.some(function(item, index) {
        console.log(array[index].accronym);
    if(array[index].accronym === params.accronym){
        // found it!
        array.splice(index, 1);
             }
    });
    return array;
  } 
}
