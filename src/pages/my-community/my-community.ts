import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Content, ModalController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { MyCommunityProvider } from '../../providers/my-community/my-community';
import { Cache, CacheService } from 'ionic-cache-observable';
import { MyCommunity } from '../../providers/my-community/my-community.model';
import { Observable } from 'rxjs/Observable';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';   

/**
 * Generated class for the MyCommunityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-community',
  templateUrl: 'my-community.html',
})
export class MyCommunityPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  powerScore='0';
  public myCommunityCache: Cache<MyCommunity[]>;
  public myCommunity$: Observable<MyCommunity[]>;
  communityData:any=[];
  showLoader=true;
  contactlist:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public myCommunityProvider: MyCommunityProvider, private cacheService: CacheService,  private statusBar: StatusBar, public platform: Platform, private contacts: Contacts, public sendrequest: SendrequestProvider, public modalCtrl: ModalController) {
  	this.powerScore = localStorage.getItem('powerScore');
  	this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.getCommunityData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCommunityPage');
  }
  update() {
  	this.showLoader=true;
  	this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {
      for (var i = 0; i < contacts.length; i++) {
        if(contacts[i].phoneNumbers != null ){
          contacts[i].selected=false;
          contacts[i].showContacts=true;
          if(contacts[i].name==null) {
            contacts[i].name ={'formatted': this.getName(contacts[i])};
          } else if(contacts[i].name.formatted=='' || contacts[i].name.formatted==null) {
             contacts[i].name.formatted = (contacts[i].phoneNumbers.length > 0)?contacts[i].phoneNumbers[0].value:'';
          }
          this.contactlist.push({ 'emails':contacts[i].emails, 'phoneNumbers':contacts[i].phoneNumbers, 'name':contacts[i].name, 'displayName':contacts[i].displayName } );
        }
      }
      this.addPhoneBookContacts();
    }, (error) => {
      console.log(error);
      this.myCommunityProvider.myCommunityRefresh();
    });
  }
  goBack() {
    this.navCtrl.pop({ animate:true, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
	this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  getName(contact) {
    if(contact.name!=null) {
      if(contact.name.formatted=='') {
        return (contact.phoneNumbers.length > 0)?contact.phoneNumbers[0].value:'';
      } else {
        return contact.name.formatted;
      }
    } else if(contact.displayName!=null) {
      return contact.displayName;
    } else if(contact.phoneNumbers.length > 0) {
      return contact.phoneNumbers[0].value;
    } else {
      return '';
    }
  }

  addPhoneBookContacts() {
    this.sendrequest.getResult('user/addPhoneBookContacts','post',{ 'contacts': this.contactlist }).then((response:any) => {
    	this.myCommunityProvider.myCommunityRefresh();
    },
    error => {
    });
  }
  comunityDetail(community) {
  	let powerscoreModal = this.modalCtrl.create('PowerscoreModalPage', { community: community });
	powerscoreModal.present();
  }
  getCommunityData() {
  this.myCommunityProvider.myCommunityRefresh();
	this.cacheService
    .get('myCommunity')
    .mergeMap((myCommunityCacheResponse: Cache<MyCommunity[]>) => {
        this.myCommunityCache = myCommunityCacheResponse;
        return this.myCommunityCache.get$;
    }).subscribe((response:any) => {
        this.communityData = response.data;
        this.showLoader=false;
    });
  }

}
