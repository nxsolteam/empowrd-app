import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';   
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';


/**
 * Generated class for the InviteContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite-contacts',
  templateUrl: 'invite-contacts.html',
})
export class InviteContactsPage {

  user:any;
  checkShareTwitter=false;
  checkShareFacebook=false;
  inviteContactsCount=0;
  private inviteContactsList: any=[];   
  private contactlist: any=[];   
  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing, private contacts: Contacts, public sendrequest: SendrequestProvider ) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.contacts.find(["displayName", "phoneNumbers"], {multiple: true}).then((contacts) => {
      console.log(contacts);
      for (var i = 0; i < contacts.length; i++) {
        if(contacts[i].phoneNumbers != null ){
          contacts[i].selected=false;
          contacts[i].showContacts=true;
          this.contactlist.push(contacts[i]);
        }
      }
      console.log(this.contactlist)
      // this.contactlist = contacts;       
      this.contactlist.sort(this.compare);
    });
  }
  compare(a,b) {
   if (a.name.formatted < b.name.formatted)
     return -1;
   if (a.name.formatted > b.name.formatted)
     return 1;
   return 0;
  }
  inviteFriends() {
    if(this.inviteContactsCount > 0){
      this.sendrequest.getResult('user/inviteContacts/' + this.user.id,'post',{ invite: this.inviteContactsList }).then((response:any) => {
          },
      error => {
        });
     } else {
     }
  }
  clearAll() {
    this.inviteContactsList = [];
    this.inviteContactsCount = this.inviteContactsList.length;
    for (var i = 0; i < this.contactlist.length; i++) {
      this.contactlist[i].selected=false;
    }
  }
  addAll() {
    for (var i = 0; i < this.contactlist.length; i++) {
      if(this.contactlist[i].selected==false) {
        this.contactlist[i].selected=true;
        this.inviteContactsList.push({'displayName':this.contactlist[i].name.formatted,'id':this.contactlist[i].id,'phoneNumbers':this.contactlist[i].phoneNumbers[0].value});
      } 
    }
    this.inviteContactsList = [];
    this.inviteContactsCount = this.inviteContactsList.length;
  }
  inviteContacts(contact) {
    if(contact.selected==false) {
      contact.selected=true;
      this.inviteContactsList.push({'displayName':contact.name.formatted,'id':contact.id,'phoneNumbers':contact.phoneNumbers[0].value});
    } else {
      contact.selected=false;
      var index = this.inviteContactsList.map(function(o) { return o.id; }).indexOf(contact.id);
      this.inviteContactsList.splice( index, 1 );
    }
    this.inviteContactsCount = this.inviteContactsList.length;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InviteContactsPage');
  }

  shareTwitter() {
  	this.socialSharing.shareViaTwitter(this.user.first_name+' '+this.user.last_name+' invites you to join my constituency on EMPOWRD.  EMPOWRD is an easy to use mobilization platform for elected leaders, influencers, and organizations.  Sign up, select the causes and leaders you want to follow and use your power to make change.  Click the link below to download the app. Download EMPOWRD APP From http://onelink.to/xzexga', null, null).then((result:any) => {
	    console.log('share result ', result);
	    this.checkShareTwitter=true;
  	},
  	error => {
  	      console.log('share error ', error)
  	})
  }
  shareFacebook() {
  	this.socialSharing.shareViaFacebook(this.user.first_name+' '+this.user.last_name+' invites you to join my constituency on EMPOWRD.  EMPOWRD is an easy to use mobilization platform for elected leaders, influencers, and organizations.  Sign up, select the causes and leaders you want to follow and use your power to make change.  Click the link below to download the app. Download EMPOWRD APP From http://onelink.to/xzexga, null, http://onelink.to/xzexga').then((result:any) => {
	    console.log('share result ', result);
	    this.checkShareFacebook=true;
  	},
  	error => {
  	      console.log('share error ', error)
  	})
  }

}
