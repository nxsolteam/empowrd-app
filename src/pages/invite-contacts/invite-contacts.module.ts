import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InviteContactsPage } from './invite-contacts';

@NgModule({
  declarations: [
    InviteContactsPage,
  ],
  imports: [
    IonicPageModule.forChild(InviteContactsPage),
  ],
})
export class InviteContactsPageModule {}
