import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EndorseModalPage } from './endorse-modal';

@NgModule({
  declarations: [
    EndorseModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EndorseModalPage),
  ],
})
export class EndorseModalPageModule {}
