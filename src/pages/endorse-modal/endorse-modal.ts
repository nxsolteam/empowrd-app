import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the EndorseModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-endorse-modal',
  templateUrl: 'endorse-modal.html',
})
export class EndorseModalPage {

  onlyFollow=false;
  endorseFollow=true;
  statement='';
  leaderDetail:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.leaderDetail = navParams.get('leaderDetail');
    console.log(this.leaderDetail);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EndorseModalPage');
  }
  closeModel(){
    this.viewCtrl.dismiss();
  }
  submitDetails() {
    let data = {'statement': this.statement, 'endorseFollow': this.endorseFollow, 'onlyFollow': this.onlyFollow };
    this.viewCtrl.dismiss(data);
  }

}
