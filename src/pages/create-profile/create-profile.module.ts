import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProfilePage } from './create-profile';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    CreateProfilePage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(CreateProfilePage),
  ],
})
export class CreateProfilePageModule {}
