import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, Select, DateTime, ActionSheetController, Platform } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject  } from '@ionic-native/file-transfer';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import * as moment from 'moment';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

/**
 * Generated class for the CreateProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-profile',
  templateUrl: 'create-profile.html',
})
export class CreateProfilePage {
  private win: any = window;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('pronounSelect') pronounSelect: Select;
  @ViewChild('engamentLevelSelect') engamentLevelSelect: Select;
  @ViewChild('genderSelect') genderSelect: Select;
  @ViewChild('interestSelect') interestSelect: Select;
  @ViewChild('partySelect') partySelect: Select;
  @ViewChild('disabilitySelect') disabilitySelect: Select;
  @ViewChild('ageSelect') ageSelect: DateTime;
  @ViewChild('employmentSelect') employmentSelect: Select;
  createProfileContent=true;
  age= '';
  genderText='';
  gender='';
  engamentLevel='';
  engamentLevelText='';
  interestText='';
  interest=[];
  employment='';
  employmentText='';
  pronoun='';
  disability='';
  party='';
  powerScore=0;
  partyText='';
  profileImage='';
  profileImageUrl:any;
  initial='';
  pronounShortName='';
  selectOptions = {
    cssClass: 'election-date-dropdown'
  };
  customPickerOptions = {
        cssClass: 'custom-date-picker'
  };
  user:any;
  ageNumeric=0;
  employeements=[];
  // maxDate:string = (new Date()).getFullYear()-100;
  // minDate:string = new Date().toISOString();
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public sendrequest: SendrequestProvider, private camera: Camera, private transfer: FileTransfer, public platform: Platform,
    public actionSheetController: ActionSheetController, private sanitize: DomSanitizer) {
  	events.subscribe('createProfileContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      //this.createProfileContent=false;
      setTimeout(() => {
      //  this.createProfileContent=true;
      }, 500);
    });
    
    this.sendrequest.getResult('user/getJobList','get', {} ).then((response:any) => { 
      this.employeements=response.data;
    },
    error => {
    });
    this.user = JSON.parse(localStorage.getItem('user'));
    this.initial=this.user.first_name.charAt(0)+"."+this.user.last_name.charAt(0)+".";
  }
  
  changeAgeValue() {
    console.log(this.age);
    this.ageNumeric = moment().diff(this.age, 'years');
    this.calculatePowerScore();
  }
  changeDisability() {
    this.calculatePowerScore();
  }
  changeEmployment() {
    for(var i=0; i < this.employeements.length; i++) {
      if(this.employment==this.employeements[i].name) {
        this.employmentText = this.employeements[i].acronym;
      }
    }
    this.calculatePowerScore();
  }
  calculatePowerScore() {
    this.powerScore=0;
    this.powerScore += (this.age!='')?5:0;
    this.powerScore += (this.disability!='')?5:0;
    this.powerScore += (this.genderText!='')?5:0;
    this.powerScore += (this.partyText!='')?5:0;
    this.powerScore += (this.pronounShortName!='')?5:0;
    this.powerScore += (this.interestText!='')?5:0;
    this.powerScore += (this.engamentLevelText!='')?5:0;
    this.powerScore += (this.profileImage!='')?5:0;
    this.powerScore += (this.employment!='')?5:0;
  }
  changeParty() {
    console.log(this.age);
    this.partyText = this.party.substring(0, 3);
    this.calculatePowerScore();
  }
  changeGender() {  
    if(this.gender=='1') {
      this.genderText = 'MALE';
    } else if(this.gender=='0') {
      this.genderText = 'FEMALE';
    } else if(this.gender=='3') {
      this.genderText = 'TRANS';
    } else {
      this.genderText = 'NON-BINARY';
    }
    this.calculatePowerScore();
  }
  changePronoun() {
    if(this.pronoun=='Him/He') {
      this.pronounShortName='H|H';
    } else if(this.pronoun=='She/Her') {
      this.pronounShortName='S|H';
    } else {
      this.pronounShortName='T|T';
    }
    this.calculatePowerScore();
  }
  changeEngagementLevel() {
    console.log(this.engamentLevel);
    this.engamentLevelText = this.engamentLevel;
    this.calculatePowerScore();
  }
  changeInterest() {
    console.log(this.interest);
    if(this.interest.length > 1) {
      this.interestText = this.interest.length+"";
    } else {
      this.interestText = this.interest.join(' ');
    }
    this.calculatePowerScore();
  }
  uploadProfileImage() {
    // this.profileImage='./assets/img/horaceWilliams.jpg';
    // this.profileImageUrl='./assets/img/horaceWilliams.jpg';
    this.addPhoto();
    /*  const actionSheet = this.actionSheetController.create({
      title: 'Choose Option',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take photo',
          icon: 'ios-camera-outline',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Choose photo from Gallery',
          icon: 'ios-images-outline',
          handler: () => {
            this.addPhoto();
          }
        },
      ]
    });
     actionSheet.present(); */
  }
  takePhoto() {
    //Final Working on phone.
     const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageUri) => {
      console.log(imageUri);
      let composed = imageUri.replace("file:///", "").split("/");
      let fileName = composed.pop();
      this.sendrequest.cacheObject(imageUri, fileName).then((result: any) => {
        console.log("take---> " + result);
        let img_uri = result.nativeURL;
        this.profileImage = result.nativeURL;
        this.calculatePowerScore();
        if (this.platform.is('ios') || this.platform.is('ipad')) {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage.replace(/^file:\/\//, ''));
        } else {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage);
        }
        
      })
    }, (err) => {
       this.sendrequest.presentToast("There was an error getting your image, please try again.");

    })   
  }
  addPhoto() { 
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0
    }

    this.camera.getPicture(options).then((imageUri: string) => {

      
      console.log(imageUri);
      if (imageUri.split("?")[0] != undefined) {
        imageUri = imageUri.split("?")[0];
      }

      let composed = imageUri.replace("file:///", "").split("/");
      let fileName = composed.pop();


      console.log("fileName->");
      console.log(fileName);

      this.sendrequest.cacheObject(imageUri, fileName).then((result: any) => {
        console.log("cacheObject---> ");
        console.log(result);
        this.profileImage = result.nativeURL;
        this.calculatePowerScore();
        if (this.platform.is('ios') || this.platform.is('ipad')) {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage.replace(/^file:\/\//, ''));
        } else {
          this.profileImageUrl = this.win.Ionic.WebView.convertFileSrc(this.profileImage);
        }
        
      })

    }, (err) => {
        this.sendrequest.presentToast("There was an error getting your image, please try again.");
    });
    
  }
  updateProfilePicture() {
     let composed = this.profileImage.replace("file:///", "").split("/");
      let fileName = composed.pop();
      let filePath = "file:///" + composed.join("/");
      const fileTransfer: FileTransferObject = this.transfer.create();
      let options: FileUploadOptions = {
                fileKey: 'image',
                fileName: fileName,
                headers: {'Authorization': 'Bearer '+this.sendrequest.authtoken, 'Accept': 'application/json'},
                mimeType: "multipart/form-data",
                params: {},
                chunkedMode: false
      }
      console.log(options);
      fileTransfer.upload(this.profileImage, this.sendrequest.api_url + "user/updateProfileImage/" + this.user.id, options)
      .then((data:any) => {
        let response=JSON.parse(data.response);
        console.log(response);
        if(response.data.photo !='' && response.data.photo !=null && response.data.photo !='null'){
          if(this.user.photo != response.data.photo){
            this.user.photo=response.data.photo;
            localStorage.setItem('user', JSON.stringify(this.user));
          }
        }
       
      }, (err) => {
        
        this.sendrequest.presentToast('Profile image not uploaded. Please try again');
        console.log(err);
        
       });
  }
  toggleEmployment(){
    setTimeout(() => {
      this.employmentSelect.open();
    },150); 
  }
  updateProfile() {
    let data = {
      'disability': this.disability,
      'interest': this.interest,
      'party': this.party,
      'engamentLevel': this.engamentLevel,
      'gender': this.gender,
      'age': this.age,
      'pronoun': this.pronoun,
      'employment': this.employment
    }
    this.sendrequest.getResult('user/updateProfile/' + this.user.id,'post', data ).then((response:any) => { 
      if(response.data.photo !='' && response.data.photo !=null && response.data.photo !='null'){
        if(this.user.photo != response.data.photo){
            this.user.photo=response.data.photo;
            localStorage.setItem('user', JSON.stringify(this.user));
        }
      }           
      
    },
    error => {
          this.sendrequest.presentToast("Error occured. Please try again");
    });
    if(this.profileImage!='') {
      this.updateProfilePicture();
    }
    setTimeout(() => {
     // this.createProfileContent=false;
    }, 100);
    this.navCtrl.push('AddCausePage',{ 'powerScore' : this.powerScore },{animate:true, animation: 'ios-transition'});
    setTimeout(() => {
      // this.createProfileContent=true;
    }, 1500);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProfilePage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    console.log($event);
  }  
  GoToNext(value){
  	setTimeout(() => {
  		this.createProfileContent=false;
    }, 100);
  	this.navCtrl.push('AddCausePage',{ 'powerScore' : this.powerScore },{animate:true, animation: 'ios-transition'});
  	setTimeout(() => {
  	   this.createProfileContent=true;
  	}, 1500);
  }
  goBack() {
  	this.events.publish('emailVerifiedShowContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
  	setTimeout(() => {
  		this.createProfileContent=false;
    }, 100);
  
    this.navCtrl.setRoot('MainTabPage',{'powerScore': 0 },{animate:true, direction: 'forward', animation: 'ios-transition'});
	setTimeout(() => {
	   this.createProfileContent=true;
	}, 1500);
  }
  togglePronoun(){
    setTimeout(() => {
      this.pronounSelect.open();
    },150); 
  }
  toggleAge(){
    setTimeout(() => {
      this.ageSelect.open();
    },150); 
  }
  toggleGender() {
    setTimeout(() => {
      this.genderSelect.open();
    },150); 
  }
  toggleEngagementLevel() {
    setTimeout(() => {
      this.engamentLevelSelect.open();
    },150);
  }
  toggleParty() {
    setTimeout(() => {
      this.partySelect.open();
    },150);
  }
  toggleInterest() {
    setTimeout(() => {
      this.interestSelect.open();
    },150);
  }
  toggleDisability() {
    setTimeout(() => {
      this.disabilitySelect.open();
    },150);
  }
}
