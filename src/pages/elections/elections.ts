import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content, Select, Platform  } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { ElectionRaces } from '../../providers/election-races/election-races.model';
import { Observable } from 'rxjs/Observable';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';

/**
 * Generated class for the ElectionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-elections',
  templateUrl: 'elections.html',
})
export class ElectionsPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('electionDrownDown') electionDrownDown: Select;
  public leaderlistCache: Cache<Leaderlist[]>;
  public electionRaceCache: Cache<Leaderlist[]>;
  public leaderlist$: Observable<ElectionRaces[]>;
  public electionRace$: Observable<ElectionRaces[]>;
  selectedLeaders=[];
  leadersdata:any=[];
  statename:string;
  header_data:any={};
  resultData:any;
  user:any;
  addressOnHeader='';
  leaderSelectedArray=[]; 
  totalstate:number;
  leadercountydata:any=[];
  countyname:string;
   
  totalcounty:number;
  leadercitydata:any=[];
  navMenu='CURRENT REPS';
   
  totalcity:number;
  leaderusdata:any=[];
  totalusleaders:number;
  totalLeaders=20;
  cityname:string='';
  shortAddress='';
  scrallDirection='';
  showDropDown = false;
  messageCount='0';
  powerScore='0';
  initial='';
  initialName='';
  validateAddress=false;
  forceUpdateRaces=false;
  ElectionDateOptions = {
    cssClass: 'election-date-dropdown'
  };
  showElectionsDropDown = false;
  gridthird=false;
  gridsecond=false;
  gridfirst=false;
  dropDownDate="NOV. 23, 2020";
  
  racesResult:any=[];
  allracesResult:any=[];
  electionDates:any=[];
  selectedDate='';
  racesCount=0;
  measureCount=0;
  selectedDateType='';
  allMeasuresData:any=[];
  measuresData:any=[];
  racesLoaderShow=true;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private electionRacesProvider: ElectionRacesProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private statusBar: StatusBar, public platform: Platform) {
    
    this.statusBar.overlaysWebView(false);
    if(navParams.get('date')!= undefined) {
      this.selectedDate = navParams.get('date');
      this.selectedDateType = navParams.get('type');
      this.navMenu=navParams.get('menuType');
      this.forceUpdateRaces=true;
    }
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    // this.header_data={ismenu:true,ishome:true,title:"MY ELECTED LEADERS"};
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.initialName = this.user.first_name[0]+""+this.user.last_name[0];
    let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
    if(totalpowerScore > 999) {
      this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
    } else {
      this.powerScore = localStorage.getItem('powerScore');
    }
    events.subscribe('changeElectionNavTab:created', (type, date) => {
      this.navMenu=type;
      console.log("changeElectionNavTab", date);
      if(date.date != undefined) {
        this.selectedDate = date.date;
        this.selectedDateType = date.type;
        this.selectElectionDate(this.selectedDate, this.selectedDateType);
      }
    });
    events.subscribe('powerScoreUpdate:created', (user, time) => {
      let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
      if(totalpowerScore > 999) {
        this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
      } else {
        this.powerScore = localStorage.getItem('powerScore');
      }
    });
    this.initial=this.user.first_name.charAt(0)+""+this.user.last_name.charAt(0);
    this.messageCount = localStorage.getItem('messageCount');
    events.subscribe('messageCountChange:created', (user, time) => {
      this.messageCount = localStorage.getItem('messageCount');
    });
    events.subscribe('setLatLon:created', (user, time) => {
      if(localStorage.getItem('validateAddress')!= undefined) {
        if(localStorage.getItem('validateAddress')=='0') {
          this.validateAddress=false;
        } else {
          this.validateAddress=true;
        }
      }
      if(localStorage.getItem('addresstext')!= undefined) {
        this.addressOnHeader = localStorage.getItem('addresstext');
        this.shortAddress = this.addressOnHeader.substring(0, 22);
      }
      this.racesLoaderShow=true;
      
      this.electionRacesProvider.electionRaceRefresh();
      
    });
    if(localStorage.getItem('validateAddress')!= undefined) {
      if(localStorage.getItem('validateAddress')=='0') {
        this.validateAddress=false;
      } else {
        this.validateAddress=true;
      }
    }
    this.addressOnHeader=localStorage.getItem('addresstext');
    this.shortAddress = this.addressOnHeader.substring(0, 22);
    this.fetchRacesListData();

  }
  civicProfilePage() {
    this.events.publish('gotoPage:created', 'CivicProfilePage', {});
  }
  segmentChanged(event) {

  }
  ionViewWillEnter() {
   this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
  }
  ionViewDidEnter() {
    this.electionRacesProvider.electionRaceRefresh();
  }
  getLeaders(leaders) {
    let date = this.selectedDate;
    if(this.selectedDateType=='general') {
      leaders = leaders.filter((v) => {
        if(v.next_general_election_date==date) {
          return true;
        }
        return false;
      })
    } else if(this.selectedDateType=='primary') {
      leaders = leaders.filter((v) => {
        if(v.next_primary_election_date==date) {
          return true;
        }
        return false;
      })
    } else {
      leaders = leaders.filter((v) => {
        if(v.next_special_election_date==date) {
          return true;
        }
        return false;
      })
    }
    return leaders;
  }
 
  toggleElectionDropDown() {
    // this.showElectionsDropDown = !this.showElectionsDropDown;
    setTimeout(() => {
      this.electionDrownDown.open();
    },150);
  }
  measureDetail(measure) {
    localStorage.setItem('backFromElection', '1');
    localStorage.setItem('raceDetail', JSON.stringify(measure));
    localStorage.setItem('showRace', 'false');
    this.events.publish('raceDetails:created', measure, Date.now());
    this.events.publish('gotoPage:created', 'RaceDetailsPage', {});
    //this.events.publish('selectedTabs:data', '5', Date.now());
  }
  raceDetail(race) {
    localStorage.setItem('backFromElection', '1');
    localStorage.setItem('raceDetail', JSON.stringify(race));
    localStorage.setItem('showRace', 'true');
    this.events.publish('raceDetails:created', race, Date.now());
    this.events.publish('gotoPage:created', 'RaceDetailsPage', {});
    //this.events.publish('selectedTabs:data', '5', Date.now());
  }
  goBack() {
     this.navCtrl.pop({animate:true, animation: 'ios-transition'});
    /*if(localStorage.getItem('backFromElection') =='1') {
      this.events.publish('selectedTabs:data', '4', Date.now());
    } else {
      this.events.publish('selectedTabs:data', '3', Date.now());
    }*/
  }
  fetchRacesListData() {
   
    // this.electionRacesProvider.electionRaceRefresh();
    this.cacheService
      .get('electionRace')
      .mergeMap((electionRaceCacheResponse: Cache<Leaderlist[]>) => {
          this.electionRaceCache = electionRaceCacheResponse;
          return this.electionRaceCache.get$;
      }).subscribe((response:any) => {
         
          this.allracesResult = response.data.addressList;
          this.allMeasuresData = response.data.measuresData;
          this.electionDates = response.data.electionDate;
          this.racesLoaderShow=false;
          console.log("this.electionDates : ", this.electionDates);
          if(this.electionDates.length > 0 && this.selectedDate=='') {
            let currentDate = moment(). format('YYYY-MM-DD');
            for(var i=0; i < this.electionDates.length; i++) {
              if(this.selectedDate=='') {
                if(this.electionDates[i].date >= currentDate) {
                  this.selectedDate =this.electionDates[i].date;
                  this.selectedDateType = this.electionDates[i].type;
                 // this.selectElectionDate(this.selectedDate, this.selectedDateType);
                 // break;
                }
              }  
            }

            localStorage.setItem("selectedDate",this.selectedDate);
            localStorage.setItem("selectedDateType",this.selectedDateType);
          }
          //if(this.forceUpdateRaces==true) {
            this.selectElectionDate(this.selectedDate, this.selectedDateType);
            //this.forceUpdateRaces=false;
          //}
      });

  }
  selectElectionDate(date, type) {
    this.selectedDate =date;
    this.selectedDateType =type;
    localStorage.setItem("selectedDate",this.selectedDate);
    localStorage.setItem("selectedDateType",this.selectedDateType);
   
    if(type=='general') {
      this.racesResult = this.allracesResult.filter((v) => {
        if(v.next_general_election_date==date && v.leaders.length > 0) {
          return true;
        }
        return false;
      })
      this.measuresData = this.allMeasuresData.filter((v) => {
        if(v.next_general_election_date==date) {
          return true;
        }
        return false;
      })
    } else if(type=='primary') {
      this.racesResult = this.allracesResult.filter((v) => {
        if(v.next_primary_election_date==date && v.leaders.length > 0) {
          return true;
        }
        return false;
      })
      this.measuresData = this.allMeasuresData.filter((v) => {
        if(v.next_primary_election_date==date) {
          return true;
        }
        return false;
      })
    } else {
      this.racesResult = this.allracesResult.filter((v) => {
        if(v.next_special_election_date==date && v.leaders.length > 0) {
          return true;
        }
        return false;
      })
      this.measuresData = this.allMeasuresData.filter((v) => {
        if(v.next_special_election_date==date) {
          return true;
        }
        return false;
      })
    }
    
    this.racesCount = this.racesResult.length;
    this.measureCount = this.measuresData.length;
  }
  editAddress() {
    this.events.publish('gotoPage:created', 'AddEditAddressPage', {});
  }
  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
    this.events.publish('addressDropDown:created', {'status':this.showDropDown }, Date.now());
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OfficialsPage');
    this.sendrequest.buttonClicked(this.user.id,'view_all_my_races');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    //document.getElementById("officialheaderNavbar").classList.add("fadeUp");
    //document.getElementById("hidetabs").classList.add("inProgress");
    // document.getElementById("hidetabs").classList.add("inProgress");
    // document.getElementById("officialheaderNavbar").classList.add("fadeUp");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("officialheaderNavbar").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    // console.log("Scrolling");
    // console.log($event);
    /* if ($event.scrollTop > 100){
      document.getElementById("officialheaderNavbar").classList.add("shadow");
    } else{
      document.getElementById("officialheaderNavbar").classList.remove("shadow");
    } */
      // console.log("Scrolling");
     //console.log($event);
     
     /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
     this.scrallDirection=$event.directionY;
     if(this.scrallDirection=="down")
     {
       //document.getElementById("hidetabs").classList.add("inProgress");
       // document.getElementById("officialheaderNavbar").classList.add("fadeUp");
     }
     if(this.scrallDirection=="up")
     {
      // document.getElementById("hidetabs").classList.remove("inProgress");      
      // document.getElementById("officialheaderNavbar").classList.remove("fadeUp");        
     }
     /*
     MAULIK
     */

    //  if ($event.scrollTop > 100){
    //    document.getElementById("officialheaderNavbar").classList.add("shadow");
    //  } else{
    //    document.getElementById("officialheaderNavbar").classList.remove("shadow");
    //  }
  }
  
  getTitle(title,index) {
    if(index=='0') {
      return title.substring(0, title.lastIndexOf(" ") + 1).trim();
    } else {
      return title.substring(title.lastIndexOf(" ") + 1).trim();
 
    }
  }

  leaderDetailPage(leader) {
    this.events.publish('gotoPage:created', 'LeaderDetailPage', { 'leader':leader });
  }

}
