import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElectionsPage } from './elections';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { DirectivesModule } from '../../directives/directives.module';
import { ComponentsModule }from '../../components/components.module';

@NgModule({
  declarations: [
    ElectionsPage,
  //  ParallaxHeaderDirective,
  ],
  imports: [
    DirectivesModule,
    ComponentsModule,
    IonicPageModule.forChild(ElectionsPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class ElectionsPageModule {}
