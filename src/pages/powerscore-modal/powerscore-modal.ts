import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PowerscoreModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-powerscore-modal',
  templateUrl: 'powerscore-modal.html',
})
export class PowerscoreModalPage {

  powerScoreList:any =[];
  community:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
  	this.community = navParams.get('community');
    this.powerScoreList = this.community.powerScore.powerScorePoint;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PowerscoreModalPage');
  }
  
  close() {
   this.viewCtrl.dismiss();
  }

}

