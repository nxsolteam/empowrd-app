import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PowerscoreModalPage } from './powerscore-modal';

@NgModule({
  declarations: [
    PowerscoreModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PowerscoreModalPage),
  ],
})
export class PowerscoreModalPageModule {}
