import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowLeaderModalPage } from './follow-leader-modal';

@NgModule({
  declarations: [
    FollowLeaderModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowLeaderModalPage),
  ],
})
export class FollowLeaderModalPageModule {}
