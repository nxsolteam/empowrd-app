import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
/**
 * Generated class for the FollowLeaderModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-follow-leader-modal',
  templateUrl: 'follow-leader-modal.html',
})
export class FollowLeaderModalPage {
  leaderid='0';
  onlyfollowers=false;
  followers=false;
  followersvalue:number;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loadingCtrl: LoadingController, public sendrequest: SendrequestProvider) {
  	this.user = JSON.parse(localStorage.getItem('user'));
  	this.leaderid=navParams.get('leaderid');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FollowLeaderModalPage');
  }
  followedLeader(value) {
    if(value == '0') {
        this.onlyfollowers=true;
  		this.followers=false;
        this.followersvalue=0;
    } else if(value =="1") {
        this.onlyfollowers=false;
  		this.followers=true;
        this.followersvalue=1;
    }
  }
    finishedfollowprocess(){
    	let loading = this.loadingCtrl.create({
	      content: '',
	      spinner: 'dots'
	    });
	    loading.present();
    	this.sendrequest.getResult("user/followLeaders/" + this.user.id,'post',{ leader_id: this.leaderid,status:this.followersvalue}).then((response:any) => {
    		loading.dismiss();
    		let data ={followLeader:'1'};
    		this.viewCtrl.dismiss(data);
    	},
	    error => {
	        loading.dismiss();
	        this.sendrequest.presentToast("Error occured. Please try again");
	        
	    });
    	
    }


}
