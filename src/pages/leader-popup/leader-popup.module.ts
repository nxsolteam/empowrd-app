import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaderPopupPage } from './leader-popup';

@NgModule({
  declarations: [
    LeaderPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaderPopupPage),
  ],
})
export class LeaderPopupPageModule {}
