import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

/**
 * Generated class for the LeaderPopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leader-popup',
  templateUrl: 'leader-popup.html',
})
export class LeaderPopupPage {

  leaderDetail:any;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,  private callNumber: CallNumber, private emailComposer: EmailComposer, private sendrequest: SendrequestProvider, private iab: InAppBrowser) {
    this.leaderDetail = navParams.get('leaderDetail');
    console.log(this.leaderDetail);
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderPopupPage');
  }
  closeModel(){
    this.viewCtrl.dismiss();
  }
  callToNumber(number) {
      this.callNumber.callNumber(number, true)
      .then((res:any) => { this.leaderEngament('phone'); })
      .catch(err => console.log('Error launching dialer', err));
    //  Request URL: https://prod1.empowrd.com/api/office/callEmailButtonLeader/phone/1505798595/1132

   }
  leaderEngament(type) {
     this.sendrequest.getResult('office/callEmailButtonLeader/'+type+'/'+this.leaderDetail.position_id+'/'+ this.user.id , 'get', { }).then((response: any) => {
        this.sendrequest.updatePowerScore(this.user.id);
    },
      error => {
      })
  }
  sendEmail(email) {
    let emailOptions = {
      to: email,
    };
    // Send a text message using default options
    this.emailComposer.open(emailOptions);
    this.leaderEngament('email');
  }
  facebook(url){
    if (url.substring(0, 4) !== 'http') {
        url = 'http://' + url;
    }
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  twitter(url){
    url = 'https://twitter.com/'+url;
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  instagram(url){
    url = "https://www.instagram.com/"+url;
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  website(url){
    if (url.substring(0, 4) !== 'http') {
        url = 'http://' + url;
    }
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  
}
