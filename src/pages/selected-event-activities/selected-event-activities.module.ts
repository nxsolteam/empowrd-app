import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedEventActivitiesPage } from './selected-event-activities';
import { ComponentsModule }from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    SelectedEventActivitiesPage,
  ],
  imports: [
     DirectivesModule,
  	ComponentsModule,
    IonicPageModule.forChild(SelectedEventActivitiesPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class SelectedEventActivitiesPageModule {}
