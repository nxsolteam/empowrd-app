import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content, Select, Platform, AlertController, PopoverController  } from 'ionic-angular';
import { SelectedEventActivitiesProvider } from '../../providers/selected-event-activities/selected-event-activities';
import { Cache, CacheService } from 'ionic-cache-observable';
import { SelectedEventActivities } from '../../providers/selected-event-activities/selected-event-activities.model';
import { Observable } from 'rxjs/Observable';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';
import { NewMessageMenuComponent } from '../../components/new-message-menu/new-message-menu';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the SelectedEventActivitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selected-event-activities',
  templateUrl: 'selected-event-activities.html',
})
export class SelectedEventActivitiesPage {
  @ViewChild(Content) content: Content;
  eventName='';
  activityList:any=[];
  public selectedEventActivitiesCache: Cache<SelectedEventActivities[]>;
  public selectedEventActivities$: Observable<SelectedEventActivities[]>;
  todayDate = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private selectedEventActivitiesProvider: SelectedEventActivitiesProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private statusBar: StatusBar, public popoverCtrl: PopoverController, private alertCtrl: AlertController, public sendrequest: SendrequestProvider) {
    this.todayDate = moment(). format('MM/DD/YY');
  	this.eventName=localStorage.getItem('selectedEventName');
  	this.getActivityData();
  }
  openMenuPopup(myEvent, activity, index) {
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, {  'activity' : activity, 'index':index }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        this.confirmDeleteMessagepopup(data.message, data.index);
        
      }
    });
  }
  confirmDeleteMessagepopup(activity,index) {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteActivity(activity,index);
          }
        }
      ]
    });
    alert.present();
  }
  deleteActivity(activity, index) {
      console.log(activity);
      this.activityList.splice(index, 1);
      this.sendrequest.getResult('user/deleteUserActivity','post',{ 'event_id': activity.event_id, 'activity_id' : activity.id }).then((response:any) => { 
        this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
      },
      error => {
      })
  }
  goToActivityList() {
    
    let selectedEvent=[];
    selectedEvent.push(localStorage.getItem('selectedEventId'));
    this.navCtrl.push('AddEventsActivitiesPage', {'selectedEvent': selectedEvent },{animate:true, animation: 'ios-transition'});
  }
  editActivity() {
  	let selectedEvent=[];
  	selectedEvent.push(localStorage.getItem('selectedEventId'));
  	this.navCtrl.push('AddEventsActivitiesPage', {'selectedEvent': selectedEvent, 'fromEditActivity' : '1'},{animate:true, animation: 'ios-transition'});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectedEventActivitiesPage');
  }
  goBack() {
    this.events.publish('selectedEventContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToActivityDetails(activity) {
  	this.navCtrl.push('ActivityDetailPage', {'activity': activity},{animate:true, animation: 'ios-transition'});
  }
  getActivityData() {
    this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    this.cacheService
        .get('selectedEventActivities')
        .mergeMap((selectedEventActivitiesCacheResponse: Cache<SelectedEventActivities[]>) => {
            this.selectedEventActivitiesCache = selectedEventActivitiesCacheResponse;
            return this.selectedEventActivitiesCache.get$;
        }).subscribe((response:any) => {
            //this.activityList = response.data;
            let activities = [];
            for(var i = 0; i < response.data.length; i++) {
              if(response.data[i].event_id==localStorage.getItem('selectedEventId')) {
                activities.push(response.data[i]);
              }
            }
            this.activityList = activities;
        });
  }

}
