import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, ModalController, ViewController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import {Observable} from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';


/**
 * Generated class for the VerifyEmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-email',
  templateUrl: 'verify-email.html',
})
export class VerifyEmailPage {
  
  verifyEmailShowContent=true;
  subscription:any;
  responseData:any;
  userId:number;
  userData:any;
  refresherEnabled=true;
  constructor(public http: HttpClient,public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public sendrequest: SendrequestProvider, public events: Events, public modalCtrl: ModalController, public viewCtrl: ViewController) {
    this.userData = navParams.get('userData');
    console.log("this.userData : ", this.userData);
  	//this.responseData = navParams.get('response');
    
  	this.subscription = Observable.interval(5000).subscribe(x => { 
	  	this.sendrequest.verifyEmail(this.userData).then((response:any) => { 
	  		if(response.data.verifyEmail ==1){
	  			this.subscription.unsubscribe();
          if(localStorage.getItem('activePage') =='VerifyEmailPage'){
            localStorage.setItem('activePage','EmailVerifiedPage');
            
  			    this.navCtrl.push('EmailVerifiedPage',{},{animate:true, duration:900, animation: 'ios-transition'});
          }
	  		}
	  	},
	    error => {
	        //this.sendrequest.presentToast('Error Occurred. Please check your connection!');
	    }) 
	  });
    events.subscribe('verifyEmailContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.verifyEmailShowContent=false;
      setTimeout(() => {
        this.verifyEmailShowContent=true;
      }, 500);
    });
	  events.subscribe('emailVerifySubscription:created', (user, time) => {
      this.subscription.unsubscribe();
    }); 
  }
  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }
  goBack() {
    // this.events.publish('stepTwoContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    if(localStorage.getItem('modelRegistration')=='true') {
      this.viewCtrl.dismiss();
    } else {
      this.navCtrl.popToRoot({animate:false});
    }
  }
  skipThisStep(){
    this.subscription.unsubscribe();
    localStorage.setItem('activePage','EmailVerifiedPage');
    //setTimeout(() => {
      this.verifyEmailShowContent=false;
    //}, 100);

    this.navCtrl.push('EmailVerifiedPage',{},{animate:true, duration:900, animation: 'ios-transition'});
    setTimeout(() => {
      this.verifyEmailShowContent=true;
    }, 1500);
  }
  
  doRefresh(refresher) {
	this.sendrequest.verifyEmail(this.userData).then((response:any) => { 
  		if(response.data.verifyEmail ==1){
  			this.subscription.unsubscribe();
  			
  			if(localStorage.getItem('activePage') =='VerifyEmailPage'){
          localStorage.setItem('activePage','EmailVerifiedPage');
          setTimeout(() => {
            this.verifyEmailShowContent=false;
          }, 100);

          this.navCtrl.push('EmailVerifiedPage',{},{animate:true, duration:900, animation: 'ios-transition'});
          setTimeout(() => {
            this.verifyEmailShowContent=true;
          }, 1500);
        }

  		}
  		this.refresherEnabled=false;
  		refresher.complete();
  	},
    error => {
    	refresher.complete();
        this.sendrequest.presentToast('Error Occurred. Please check your connection!');
    }) 
    //return false;
  }
  editEmailAddress() {
    let profileModal = this.modalCtrl.create('EditEmailAddressModalPage', { 'userData': this.userData });
    profileModal.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null && data != '') {
        this.userData.email = data.email;
        localStorage.setItem('responseData',JSON.stringify(this.userData));
        this.sendVerificationEmail();     
      }
    });
    profileModal.present();
  }
  sendVerificationEmail() {
  	this.sendrequest.sendVerificationEmail(this.userData).then((response:any) => { 
      this.sendrequest.presentToast("Resent Email");
    },
    (error:any) => {
    }) 
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyEmailPage');
  }

}
