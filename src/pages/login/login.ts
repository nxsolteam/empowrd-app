import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events, AlertController, LoadingController, Nav, Platform } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { CryptProvider } from '../../providers/crypt/crypt';
import { HomePage } from '../home/home';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { StatusBar } from '@ionic-native/status-bar';
import { GetCausesListProvider } from '../../providers/get-causes-list/get-causes-list';
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';

declare var cordova: any;

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
	@ViewChild(Nav) nav: Nav;
  @ViewChild('emailAddress') emailAddress;
  failureInLogin=false;
  loginShowContent=true;
  emailFocusClass='';
  showLoader=false;
  successEmailClass='';
  showAppleLoginButton=false;
  addresslist:any;
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder, public sendrequest: SendrequestProvider, public crypt: CryptProvider, public alertCtrl: AlertController, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public events: Events, private nativePageTransitions: NativePageTransitions, private googlePlus: GooglePlus, private facebook: Facebook, public platform: Platform, private addresslistProvider: AddresslistProvider, private cacheService: CacheService, private leaderlistProvider: LeaderlistProvider, private electionRacesProvider: ElectionRacesProvider, private statusBar: StatusBar, private getCausesListProvider: GetCausesListProvider, private getOrgListProvider: GetOrgListProvider) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    if (this.platform.is('ios') || this.platform.is('ipad')) {
      this.showAppleLoginButton=true;
    }
    events.subscribe('loginPageContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      //this.loginShowContent=false;
      setTimeout(() => {
        //this.loginShowContent=true;
      }, 300);
    });
  }
  public loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")]]
  });
  ionViewDidLoad() {
    setTimeout(() => {
      this.emailAddress.setFocus();
    }, 700);
    console.log('ionViewDidLoad LoginPage');
  }
  googleLogin(){
    console.log("Google Login");
    this.googlePlus.login(
      {
    })
    .then((res:any) => {
      // alert(JSON.stringify(res));
      
      let data = {
        email:res.email,
        first_name:res.givenName,
        last_name:res.familyName,
        home:"ecu",
        phone: '',
        photo_url: res.imageUrl,
        social: 'google',
        google: res.userId,
      }
      this.sendrequest.login(data).then((response:any) => { 
        localStorage.setItem('response',JSON.stringify(response));
      },
      error => {
        /* this.failureInLogin=true;
        this.sendrequest.presentToast(error.error.errors.message[0]);
        */
       
        setTimeout(() => {
          this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
          this.goToRoot();
        }, 500);
      }) 
      setTimeout(() => {
       this.loginShowContent=false;
      }, 100);
      this.navCtrl.push('MobileNumberVerificationPage',{'fromLogin':'1','email':res.email, 'social': 'google', 'google': res.userId },{animation: 'ios-transition', duration:900, animate:true});
      setTimeout(() => {
       this.loginShowContent=true;
      }, 1500);

    })
    .catch(err =>  this.sendrequest.presentToast('Unable to login with google'));
  }
  fbLogin() {
    this.facebook.login(['public_profile', 'email'])
    .then((res: FacebookLoginResponse) => {
      // alert(JSON.stringify(res));
      this.facebook.api('me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)', []).then((profile:any) => {
        let data = {
          email:profile.email,
          first_name:profile.first_name,
          last_name:profile.last_name,
          photo_url:profile.picture_large.data.url,
          social: 'facebook',
          facebook: profile.id,
          phone: '',
        }
        this.sendrequest.login(data).then((response:any) => { 
          localStorage.setItem('response',JSON.stringify(response));
        },
        error => {
          /* this.failureInLogin=true;
          this.sendrequest.presentToast(error.error.errors.message[0]);
          */
         
          setTimeout(() => {
            this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
            this.goToRoot();
          }, 500);
        }) 
        setTimeout(() => {
         this.loginShowContent=false;
        }, 100);
        this.navCtrl.push('MobileNumberVerificationPage',{'fromLogin':'1','email':profile.email, 'social': 'facebook', 'facebook': profile.id },{animation: 'ios-transition', duration:900, animate:true});
        setTimeout(() => {
         this.loginShowContent=true;
        }, 1500);

      });
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }
  appleLogin() {
   console.log("Apple Login");
    /*
    let appleLoginResponse = {'email' : 'bhaveshr59@gmail.com', 'fullName': {'givenName':'B', 'familyName':'R' }, 'user': '001771.e3bba37ec09a4a70a57227647ebbe336.1059', 'authorizationCode': '' };
      console.log(appleLoginResponse);
      this.showLoader=true;
     let data = {
        email:appleLoginResponse.email,
        first_name:appleLoginResponse.fullName.givenName,
        last_name:appleLoginResponse.fullName.familyName,
        home:"ecu",
       // phone: '',
        social: 'apple',
        apple: appleLoginResponse.user,
      }
      this.sendrequest.appleLogin(data).then((responseData:any) => {
        localStorage.setItem('response',JSON.stringify(responseData));
        localStorage.setItem('onboardingStart','1'); 

          if(responseData.register=='1') {
            setTimeout(() => {
             this.loginShowContent=false;
            }, 100);
            this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
            setTimeout(() => {
             this.loginShowContent=true;
            }, 1500);
          } else {
            this.sendrequest.authtoken = responseData.token;
            localStorage.setItem('user', JSON.stringify(responseData.user));
            localStorage.setItem('auth-token', responseData.token);
            console.log( JSON.parse(localStorage.getItem('user')));
            this.addresslistProvider.refresh();
            this.getCausesListProvider.getCausesListRefresh();
            this.getOrgListProvider.getOrgListRefresh();
            //const sourceData = this.addresslistProvider.random();
            this.cacheService
            .get('addresslist')
            .mergeMap((cache: Cache<Addresslist[]>) => {
                this.cache = cache;
                return this.cache.get$;
            })
            .subscribe((addresslist:any) => {
                this.addresslist=addresslist;
               
                if(localStorage.getItem('addressId')!=this.addresslist.data.addresses[0].id) {
                  localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
                  localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address); 
                  localStorage.setItem('addressId' , this.addresslist.data.addresses[0].id); 
                  localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
                  localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
                  localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
                  localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);
                
                  this.events.publish('setAddressTitle:created', 'user', Date.now());
                  
                  this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + responseData.user.id,'get',{}).then((response:any) => {     
                    var results = response.data.response.results[0];
                    localStorage.setItem('latitude',results.location.lat.toString());
                    localStorage.setItem('longitude',results.location.lng.toString());
                    this.events.publish('setLatLon:created', 'user', Date.now());
                    this.leaderlistProvider.leaderlistRefresh();
                    this.electionRacesProvider.electionRaceRefresh();
                  });
                }
               
            });
            
             setTimeout(() => {
             this.loginShowContent=false;
            }, 100);
            this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
            setTimeout(() => {
             this.loginShowContent=true;
            }, 1200);
          }
          this.showLoader=false;
      },
      error => {
      this.showLoader=false;
       setTimeout(() => {
         this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
         this.goToRoot();
       }, 500);
      }) */
      /*setTimeout(() => {
        this.loginShowContent=false;
      }, 100);
      this.navCtrl.push('MobileNumberVerificationPage',{'fromLogin':'1','email':appleLoginResponse.email, 'social': 'apple', 'apple': appleLoginResponse.id },{animation: 'ios-transition', duration:900, animate:true});
      setTimeout(() => {
        this.loginShowContent=true;
      }, 1500); */
      
  cordova.plugins.SignInWithApple.signin(
    { requestedScopes: [0, 1] },
    (appleLoginResponse: any) => {
      console.log(appleLoginResponse);
      this.showLoader=true;
       let data = {
          email:appleLoginResponse.email,
          first_name:appleLoginResponse.fullName.givenName,
          last_name:appleLoginResponse.fullName.familyName,
          home:"ecu",
          // phone: '',
          social: 'apple',
          apple: appleLoginResponse.user
        }
       this.sendrequest.appleLogin(data).then((responseData:any) => {
        localStorage.setItem('response',JSON.stringify(responseData));
        localStorage.setItem('onboardingStart','1'); 

          if(responseData.register=='1') {
            setTimeout(() => {
             this.loginShowContent=false;
            }, 100);
            this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
            setTimeout(() => {
             this.loginShowContent=true;
            }, 1500);
          } else {
            this.sendrequest.authtoken = responseData.token;
            localStorage.setItem('user', JSON.stringify(responseData.user));
            localStorage.setItem('auth-token', responseData.token);
            console.log( JSON.parse(localStorage.getItem('user')));
            this.addresslistProvider.refresh();

            //const sourceData = this.addresslistProvider.random();
            this.cacheService
            .get('addresslist')
            .mergeMap((cache: Cache<Addresslist[]>) => {
                this.cache = cache;
                return this.cache.get$;
            })
            .subscribe((addresslist:any) => {
                this.addresslist=addresslist;
               
                if(localStorage.getItem('addressId')!=this.addresslist.data.addresses[0].id) {
                  localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
                  localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address); 
                  localStorage.setItem('addressId' , this.addresslist.data.addresses[0].id); 
                  localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
                  localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
                  localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
                  localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);
                
                  this.events.publish('setAddressTitle:created', 'user', Date.now());
                  
                  this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + responseData.user.id,'get',{}).then((response:any) => {     
                    var results = response.data.response.results[0];
                    localStorage.setItem('latitude',results.location.lat.toString());
                    localStorage.setItem('longitude',results.location.lng.toString());
                    this.events.publish('setLatLon:created', 'user', Date.now());
                    this.leaderlistProvider.leaderlistRefresh();
                    this.electionRacesProvider.electionRaceRefresh();
                  });
                }
               
            });
            
             setTimeout(() => {
             this.loginShowContent=false;
            }, 100);
            this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
            setTimeout(() => {
             this.loginShowContent=true;
            }, 1200);
          }
          this.showLoader=false;
      },
      error => {
      this.showLoader=false;
       setTimeout(() => {
         this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
         this.goToRoot();
       }, 500);
      })
    },
    (err:any) => {
      console.error(err);
      console.log(JSON.stringify(err));
    }
  )
  
 }
  onForgotEmailPage(){
    setTimeout(() => {
     this.loginShowContent=false;
    }, 100);
    this.navCtrl.push('MobileNumberVerificationPage',{'fromLogin':'1','forgotEmail': '1'},{animation: 'ios-transition', duration:900, animate:true});
    setTimeout(() => {
     this.loginShowContent=true;
    }, 1500);
  }
  inputFocusEmail() {
    this.emailFocusClass='has-focus';
  }
   inputBlurEmail() {
    this.emailFocusClass='';
    if(this.loginForm.controls['email'].valid){
      this.successEmailClass='has-success';
    }
    if(this.loginForm.controls['email'].errors) {
      this.successEmailClass='has-error';
    }
  }
  goToRoot() {
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#282333');
    this.navCtrl.popToRoot({animate:false});
  }
  login(form) { 
    let data = {
    	email:form.email
    }
    this.sendrequest.login(data).then((response:any) => { 
      localStorage.setItem('response',JSON.stringify(response));
    },
    error => {
      /* this.failureInLogin=true;
      this.sendrequest.presentToast(error.error.errors.message[0]);
      */
     
      setTimeout(() => {
        this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
        this.goToRoot();
      }, 500);
    }) 
    setTimeout(() => {
     this.loginShowContent=false;
    }, 100);
    this.navCtrl.push('MobileNumberVerificationPage',{'fromLogin':'1','email':form.email},{animation: 'ios-transition', duration:900, animate:true});
    setTimeout(() => {
     this.loginShowContent=true;
    }, 1500);
  }
}
