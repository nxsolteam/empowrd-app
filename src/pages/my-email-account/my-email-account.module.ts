import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyEmailAccountPage } from './my-email-account';

@NgModule({
  declarations: [
    MyEmailAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(MyEmailAccountPage),
  ],
})
export class MyEmailAccountPageModule {}
