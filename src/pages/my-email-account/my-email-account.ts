import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { GetCausesListProvider } from '../../providers/get-causes-list/get-causes-list';
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';

/**
 * Generated class for the MyEmailAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-email-account',
  templateUrl: 'my-email-account.html',
})
export class MyEmailAccountPage {

  code:string;
  phoneNumber:string;
  userId:any;
  email='';
  responseData:any;
  goToRegistration=true;
  showError=false;
  phoneNumber1='';
  phoneNumber2='';
  sendButtonText="SEND VERIFICATION CODE";
  sendButtonEnabled=false;
  myEmailShowContent=true;
  addresslist:any;
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, private addresslistProvider: AddresslistProvider, private cacheService: CacheService, private leaderlistProvider: LeaderlistProvider, private electionRacesProvider: ElectionRacesProvider, private getCausesListProvider: GetCausesListProvider, private getOrgListProvider: GetOrgListProvider) {
  	this.userId = 
    // if(navParams.get('userId') != null && navParams.get('userId') != undefined) {

    this.userId = navParams.get('userId');
    //this.userId = '3854';
    
    let data = {
	    user_id:this.userId
	}
	  this.sendrequest.userData(data).then((response:any) => { 
	    
	    this.responseData = response;
	    this.email = response.user.email;
	  },
	  error => {
	    
	    setTimeout(() => {
	      this.sendrequest.presentToast("Failed To Getting Email Address. Try Again.");
	      this.goToRoot();
	    }, 500);
	  }) 
    
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyEmailAccountPage');
  }

  login() {
  	 this.sendrequest.authtoken = this.responseData.token;
          localStorage.setItem('user', JSON.stringify(this.responseData.user));
          localStorage.setItem('auth-token', this.responseData.token);
          console.log( JSON.parse(localStorage.getItem('user')));
          this.addresslistProvider.refresh();
          this.getCausesListProvider.getCausesListRefresh();
          this.getOrgListProvider.getOrgListRefresh();
          //const sourceData = this.addresslistProvider.random();
          this.cacheService
          .get('addresslist')
          .mergeMap((cache: Cache<Addresslist[]>) => {
              this.cache = cache;
              
              return this.cache.get$;
          })
          .subscribe((addresslist:any) => {
               console.log("login address list cache register");
              this.addresslist=addresslist;
                for(var i=0; i < this.addresslist.data.addresses.length; i++) {
                  if(this.addresslist.data.addresses[i].type=='PRIMARY RESIDENCE') {
                    localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[i].type);
                    localStorage.setItem('addresstext' , this.addresslist.data.addresses[i].address); 
                    localStorage.setItem('city' , this.addresslist.data.addresses[i].city); 
                    localStorage.setItem('county' , this.addresslist.data.addresses[i].county); 
                    localStorage.setItem('state' , this.addresslist.data.addresses[i].state); 
                    localStorage.setItem('validateAddress',this.addresslist.data.addresses[i].validateAddress);
                  }
                }
                if(localStorage.getItem('headerAddressTitle')==undefined) {
                  localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
                  localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address); 
                  localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
                  localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
                  localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
                  localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);
                }
                this.events.publish('setAddressTitle:created', 'user', Date.now());
             if(localStorage.getItem('latitude') ==null || localStorage.getItem('latitude') ==undefined){
                this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + this.responseData.user.id,'get',{}).then((response:any) => {     
                  var results = response.data.response.results[0];
                  localStorage.setItem('latitude',results.location.lat.toString());
                  localStorage.setItem('longitude',results.location.lng.toString());
                  this.events.publish('setLatLon:created', 'user', Date.now());
                  this.leaderlistProvider.leaderlistRefresh();
                  this.electionRacesProvider.electionRaceRefresh();
                });
              }
          });
          
           setTimeout(() => {
           this.myEmailShowContent=false;
          }, 100);
          this.navCtrl.setRoot('MainTabPage',{},{animate:true, animation: 'ios-transition'});
          setTimeout(() => {
           this.myEmailShowContent=true;
          }, 1200);
  }

  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
}
