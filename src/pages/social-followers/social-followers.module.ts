import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialFollowersPage } from './social-followers';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    SocialFollowersPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(SocialFollowersPage),
  ],
})
export class SocialFollowersPageModule {}
