import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

/**
 * Generated class for the AccountSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-setting',
  templateUrl: 'account-setting.html',
})
export class AccountSettingPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  user:any;
  showLoader=false;
  showError=false;
  errorMessage='';
  firstnameFocusClass='';
  successFirstnameClass='';
  successLastnameClass='';
  successEmailClass='';
  lastnameFocusClass='';
  emailFocusClass='';
  public member:any={'email':''};
  phoneNumber1='';
  powerScore='0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, public events: Events, public fb: FormBuilder) {
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.powerScore = localStorage.getItem('powerScore');
  }
  public registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")]],
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    phone_number: ['', Validators.required]
  });

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountSettingPage');
    this.registerForm.patchValue({email : this.user.email });
    this.registerForm.patchValue({first_name : this.user.first_name });
    this.registerForm.patchValue({last_name : this.user.last_name });
    var phone =this.user.phone.toString();
    var first = phone.substring(0, 3);
    var second = phone.substring(3, 6);
    var third = phone.substring(6, 10);
    this.phoneNumber1=first+"-"+second+"-"+third;
    this.registerForm.patchValue({phone_number : this.phoneNumber1});
    
  }
  goBack() {
  	this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  update() {
    if(this.registerForm.valid) {
      this.showLoader=true;
      this.showError=false;
      //var res = str.replace("Microsoft", "W3Schools");

      let phone = this.registerForm.value.phone_number.replace("-", "");
      phone = phone.replace("-", "");
      let data = { 'email': this.registerForm.value.email, 'phone': phone, 'first_name': this.registerForm.value.first_name, 'last_name': this.registerForm.value.last_name }
      this.sendrequest.getResult('user/' + this.user.id+'/settings','post',{'settings': data}).then((response:any) => { 
        this.showLoader=false;
        localStorage.setItem('user', JSON.stringify(response.data.user));
        this.events.publish('updateProfie:created',response.data.user, Date.now()); 
        this.navCtrl.pop({animate:true, animation: 'ios-transition'});
      },
      (error:any) => {
      console.log(error);
        this.errorMessage= error.error.message;
        this.showError=true;
        this.showLoader=false;
      })
    }
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
  checkLength(event,type)
  {
     if(event.key != "Backspace")
     {
       var numbers=this.phoneNumber1;
       if(numbers.length > 11)
       {
        return false;
       }
       if(numbers.length == 3)
       {
         this.phoneNumber1= numbers+'-';
       }
       if(numbers.length == 7)
       {
         this.phoneNumber1= numbers+'-';
       }
    }

  }
  inputFocusFirstname() {
    this.firstnameFocusClass='has-focus';
  }
  inputBlurLastname() {
    this.lastnameFocusClass='';
    this.successLastnameClass='';
    if(this.registerForm.controls['last_name'].valid){
      this.successLastnameClass='has-success';
    }
    if(this.registerForm.controls['last_name'].errors ){
      this.successLastnameClass='has-error';
    }
  }
  inputFocusLastname() {
    this.lastnameFocusClass='has-focus';
  }
  inputBlurFirstname() {
    this.firstnameFocusClass='';
    this.successFirstnameClass='';
    if(this.registerForm.controls['first_name'].valid){
      this.successFirstnameClass='has-success';
    }
    if(this.registerForm.controls['first_name'].errors ){
      this.successFirstnameClass='has-error';
    }
  }
  //MAULIK

  inputFocusEmail() {
     this.emailFocusClass='has-focus';
  }
  inputBlurEmail() {
    this.emailFocusClass='';
    this.successEmailClass='';
    if(this.registerForm.controls['email'].valid){
      this.successEmailClass='has-success';
    }
    if(this.registerForm.controls['email'].errors) {
      this.successEmailClass='has-error';
    }
  }
  logScrollStart() {
    // console.log("Scroll Start");
    document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    console.log($event);
  } 

}
