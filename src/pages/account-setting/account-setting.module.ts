import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountSettingPage } from './account-setting';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  declarations: [
    AccountSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountSettingPage),
    NgxMaskIonicModule,
  ],
})
export class AccountSettingPageModule {}
