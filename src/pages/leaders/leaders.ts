import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, Content  } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the LeadersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaders',
  templateUrl: 'leaders.html',
})
export class LeadersPage {
  @ViewChild(Content) content: Content;
  header_data:any={};
  resultData:any;
  user:any;
  addressOnHeader='';
  leaderSelectedArray=[];
  //public leaderList:Leaderlist=[];
  totalLeaderSelected = 0;
  showLeaders =false;
  showCityLeaders =false;
  showStateLeaders =false;
  showCountyLeaders =false;
  showCountryLeaders =false;
  showSelectedLeaders=false;
  cityCheckbox =false;
  stateCheckbox =false;
  countyCheckbox =false;
  countryCheckbox =false;
  leaderListCheckbox =false;
  leaderListCheckbox1 =false;
  leaderListCheckbox2 =false;
  leaderListCheckbox3 =false;
  selectedLeaders=[];
  leaderstatedata:any=[];
  statename:string;
   
  totalstate:number;
  leadercountydata:any=[];
  countyname:string;
   
  totalcounty:number;
  leadercitydata:any=[];
  cityname:string;
   
  totalcity:number;
  leaderusdata:any=[];
  totalusleaders:number;
    
  introBigText = "";
  introSmallText = "";
   
  public leaderlistCache: Cache<Leaderlist[]>;
  public leaderlist$: Observable<Leaderlist[]>;
  leaderlist:any;
  checkedAll=0;
  searchValue='';
  searchBarSection=false;
  IsVisibleCity = false;
  IsVisibleCounty = false;
  IsVisibleState = false;
  IsVisibleUSA = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events) {
  	// this.header_data={ismenu:true,ishome:true,title:"MY ELECTED LEADERS"};
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.addressOnHeader=localStorage.getItem('addresstext');
    this.getLeaderData();
    
  }
  getLeaderData() {
    this.cacheService
        .get('leaderlist')
        .mergeMap((leaderlistCacheResponse: Cache<Leaderlist[]>) => {
            this.leaderlistCache = leaderlistCacheResponse;
            return this.leaderlistCache.get$;
        }).subscribe((response:any) => {
            this.leaderstatedata = response.data.state;
            this.statename = response.data.state_name;
            localStorage.setItem('statenametodispalyonmap',this.statename);
            this.totalstate = this.leaderstatedata.length;
            this.leadercountydata = response.data.county;
            this.countyname = response.data.county_name;
            localStorage.setItem('countynametodispalyonmap',this.countyname);
            this.totalcounty = this.leadercountydata.length;
            this.leadercitydata = response.data.city;
            this.cityname = response.data.city_name;
            localStorage.setItem('citynametodispalyonmap', this.cityname);
            this.totalcity = this.leadercitydata.length;
            this.leaderusdata = response.data.us;
            this.totalusleaders = this.leaderusdata.length;
            var totalLeader = (this.totalstate+this.totalcounty + this.totalcity + this.totalusleaders);
            console.log(" Leader List response ");
            console.log(response);
            this.leadercitydata.forEach(function(item){
              item.checked=false;
            }, this);
            this.leadercountydata.forEach(function(item){
              item.checked=false;
            }, this);
            this.leaderstatedata.forEach(function(item){
              item.checked=false;
            }, this);
            this.leaderusdata.forEach(function(item){
              item.checked=false;
            }, this);
        
        });
  }
  clicked() {
   console.log('clicked LeadersPage');
  }
  goBack() {
    if(this.showLeaders==true){
      this.showLeaders=false;
      this.showCityLeaders =false;
      this.showStateLeaders =false;
      this.showCountyLeaders =false;
      this.showCountryLeaders =false;
      this.showSelectedLeaders = false;
      return;
    }
    else {
      //this.navCtrl.push('TabsHomePage');
    }
  }
  viewAll(name){
    this.showLeaders =true;
    if(name=='city'){
      this.showCityLeaders =true;
    }
    else if(name=='county'){
      this.showCountyLeaders =true;
    }
    else if(name=='state'){
      this.showStateLeaders =true;
    }
    else if(name=='country'){
      this.showCountryLeaders =true;
    }
    
    
  }
  showSearchBarSection() {
    this.searchBarSection= !this.searchBarSection;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LeadersPage');
  }
  logScrolling(event){
    
  }
  logScrollStart(event){
  
  }
  goToDetail(id) {
    this.events.publish('goToPage:params', 'LeaderDetailPage', { 'id': id });
  };
  ShowHideCity() {
      this.IsVisibleCity = this.IsVisibleCity ? false : true;
      return;
  }
  ShowHideCounty() {
      this.IsVisibleCounty = this.IsVisibleCounty ? false : true;
      return;
  }
  ShowHideState() {
      this.IsVisibleState = this.IsVisibleState ? false : true;
      return;
  }
  ShowHideUSA() {
      this.IsVisibleUSA = this.IsVisibleUSA ? false : true;
      return;
  }
  appliedActiveClassCity(myObj) {
    if (this.IsVisibleCity) {
        return "active";
    } else {
        return "";
    }
  }
  changeLeaderCheckbox(event,item,type){
  

    this.checkedAll=0;
    if( event) {
      this.totalLeaderSelected += 1;
      item.checked=true;
      this.leaderSelectedArray.push(item.position_id);
      if(type=='city'){
        
        this.leadercitydata.forEach(function(item){
          if(item.checked==false) {
            this.cityCheckbox =false;
          } 
          else {
             this.checkedAll +=1;
          }
        }, this);
        if(this.checkedAll ==  this.totalcity) {
          this.cityCheckbox =true;
        }
      }
      else if(type=='county'){
        
        this.leadercountydata.forEach(function(item){
          if(item.checked==false){
            this.countyCheckbox =false;
          } else {
             this.checkedAll +=1;
          }
        }, this);
        if(this.checkedAll ==  this.totalcounty) {
          this.countyCheckbox =true;
        }
      }
      else if(type=='state'){
        
        this.leaderstatedata.forEach(function(item){
          if(item.checked==false) {
            this.stateCheckbox =false;
          } else {
             this.checkedAll +=1;
          }
        }, this);
        if(this.checkedAll ==  this.totalstate) {
          this.stateCheckbox =true;
        }
      }
      else if(type=='country'){
        
        this.leaderusdata.forEach(function(item){
          if(item.checked==false) {
            this.countryCheckbox =false;
          } else {
             this.checkedAll +=1;
          }
        }, this);
        if(this.checkedAll ==  this.totalusleaders) {
          this.countryCheckbox =true;
        }
      } 
       
    } else {
       this.totalLeaderSelected -= 1;
        item.checked=false;
        this.leaderSelectedArray.splice(this.leaderSelectedArray.indexOf(item.position_id), 1);
      if(type=='city'){
        this.cityCheckbox =false;
      }
      else if(type=='county'){
        
        this.countyCheckbox =false;
      }
      else if(type=='state'){
        
        this.stateCheckbox =false;
      }
      else if(type=='country'){
        this.countryCheckbox =false;
      } 
    }
  }
  searchLeader() {
    this.selectedLeaders=[];
    this.showSearchBarSection();
    if(this.showLeaders == false || this.showCityLeaders ==true){
      this.leadercitydata.forEach(function(item){
         if (item.first_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1 || item.last_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
          this.selectedLeaders.push(item);
        }
      }, this);
    }
    if(this.showLeaders == false || this.showCountyLeaders ==true){
      this.leadercountydata.forEach(function(item){
        if (item.first_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1 || item.last_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
          this.selectedLeaders.push(item);
        }
      }, this);
    }
    if(this.showLeaders == false || this.showStateLeaders ==true){
      this.leaderstatedata.forEach(function(item){
        if (item.first_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1 || item.last_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
          this.selectedLeaders.push(item);
        }
      }, this);
    }
    if(this.showLeaders == false || this.showCountryLeaders ==true){
      this.leaderusdata.forEach(function(item){
        if (item.first_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1 || item.last_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
          this.selectedLeaders.push(item);
        }
      }, this);
    }
    this.showLeaders =true;
    this.showCityLeaders =false;
    this.showStateLeaders =false;
    this.showCountyLeaders =false;
    this.showCountryLeaders =false;
    this.showSelectedLeaders = true;
  }
  viewSelected() {
    this.selectedLeaders=[];
    this.leadercitydata.forEach(function(item){
      if(item.checked==true){
        this.selectedLeaders.push(item);
      }
    }, this);
    this.leadercountydata.forEach(function(item){
      if(item.checked==true){
        this.selectedLeaders.push(item);
      }
    }, this);
    this.leaderstatedata.forEach(function(item){
      if(item.checked==true){
        this.selectedLeaders.push(item);
      }
    }, this);
    this.leaderusdata.forEach(function(item){
      if(item.checked==true){
        this.selectedLeaders.push(item);
      }
    }, this);
    this.showLeaders =true;
    this.showCityLeaders =false;
    this.showStateLeaders =false;
    this.showCountyLeaders =false;
    this.showCountryLeaders =false;
    this.showSelectedLeaders = true;
  }
  addSelected() {
    //
    this.getLeaderData();
    console.log(this.leaderSelectedArray);
    this.sendrequest.presentToast('Selected leaders followed.');
    
    this.sendrequest.getResult('user/followMultipleLeaders/' + this.user.id,'post',{'leaders':this.leaderSelectedArray}).then(result => { 
      this.leaderSelectedArray = [];
      this.totalLeaderSelected = 0;
    },
    error => {
      this.sendrequest.presentToast("Error occured. Please try again");
    }) 
    this.events.publish('changeTab:created', 0,Date.now());
  }
  changeCheckbox(event,name){
    
    if( event) {
      if(name=='city'){
        
        this.leadercitydata.forEach(function(item){
          if(item.checked ==false){
            this.leaderSelectedArray.push(item.position_id);
            this.totalLeaderSelected += 1;
            item.checked=true;
          }
        }, this);
      }
      else if(name=='county'){
        
        this.leadercountydata.forEach(function(item){
          if(item.checked ==false){
            this.leaderSelectedArray.push(item.position_id);
            this.totalLeaderSelected += 1;
            item.checked=true;
          }
        }, this);
      }
      else if(name=='state'){
        
        this.leaderstatedata.forEach(function(item){
          if(item.checked ==false){
            this.leaderSelectedArray.push(item.position_id);
            this.totalLeaderSelected += 1;
            item.checked=true;
          }
        }, this);
      }
      else if(name=='country'){
       
        this.leaderusdata.forEach(function(item){
          if(item.checked ==false){
            this.leaderSelectedArray.push(item.position_id);
            this.totalLeaderSelected += 1;
            item.checked=true;
          }
        }, this);
      } 
    } else {
      
      if(name=='city'){
        
        this.leadercitydata.forEach(function(item){
          if(item.checked ==true){
            this.leaderSelectedArray.splice(this.leaderSelectedArray.indexOf(item.position_id), 1);
            this.totalLeaderSelected -= 1;
            item.checked=false;
          }
        }, this);
      }
      else if(name=='county'){
        this.leadercountydata.forEach(function(item){
          if(item.checked ==true){
            this.leaderSelectedArray.splice(this.leaderSelectedArray.indexOf(item.position_id), 1);
            this.totalLeaderSelected -= 1;
            item.checked=false;
          }
        }, this);
      }
      else if(name=='state'){
        this.leaderstatedata.forEach(function(item){
          if(item.checked ==true){
            this.leaderSelectedArray.splice(this.leaderSelectedArray.indexOf(item.position_id), 1);
            this.totalLeaderSelected -= 1;
            item.checked=false;
          }
        }, this);
      }
      else if(name=='country'){
        this.leaderusdata.forEach(function(item){
          if(item.checked ==true){
            this.leaderSelectedArray.splice(this.leaderSelectedArray.indexOf(item.position_id), 1);
            this.totalLeaderSelected -= 1;
            item.checked=false;
          }
        }, this);
      } 
    }
  }
  

}
