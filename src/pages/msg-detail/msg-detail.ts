import { Component, ElementRef, ViewChild, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Platform, Content, ModalController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { OpenExternalurlDirective } from '../../directives/open-externalurl/open-externalurl';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { ChatServiceProvider, ChatMessage, UserInfo } from "../../providers/chat-service/chat-service";
import * as moment from 'moment';

/**
 * Generated class for the MsgDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-msg-detail',
  templateUrl: 'msg-detail.html',
})
export class MsgDetailPage {
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild(OpenExternalurlDirective) openexternal = null;
  @ViewChild(Content) content: Content;

  
  @ViewChild('chat_input') messageInput: ElementRef;
  msgList: ChatMessage[] = [];
  userInfo: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;


  copyContentText="Copy Content";
  hideResponse = true;
  showResponseDivDetail = true;
  user:any;
  msgId;
  mailDetail:any={};
  countNewMessage:number;
  mid:number;
  adminname:string;
  searchdata:any;
  usermaildetails:any;
  responsetext='';
  typeValues = [];
  optionValues='';
  multipleAnswer=false;
  singleAnswer=false;
  showThanx = false;
  userCompose=false;
  loaderShow=true;
  mail:any={};
  activityCertificateContent='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, private sanitizer: DomSanitizer, private iab: InAppBrowser, private statusBar: StatusBar, public platform: Platform, public modalCtrl: ModalController, public chatService: ChatServiceProvider) {
    console.log("messageInput : ",this.messageInput);
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.msgId = navParams.get('msgId');
    this.mail = navParams.get('mail');
    console.log("this.mail : ", this.mail);
    if( navParams.get('composeMessage') != null && navParams.get('composeMessage') != undefined) {
      this.userCompose=true;
      this.mailDetail = this.mail;
      this.usermaildetails = this.mail.mailuserreply;
        this.scrollToBottom();
      this.mailDetail.status=1;
      if(this.mailDetail.status=='1'){
        /* let dimensions = this.content.getContentDimensions();
        this.content.scrollToBottom(0);
        setTimeout(()=>{this.content.scrollToBottom();},200); */
      }
      this.loaderShow=false;
      this.sendrequest.getResult('user/updateComposeMessageStatus' ,'post',{ 'id': this.msgId }).then((response:any) => { 
        
        this.sendrequest.updatePowerScore(this.user.id);
      },
      error => {
        
      }); 
    }  else {
    	   
        if(this.mail.MessageDetails==undefined) {
          this.mailDetail = this.mail;
    	   // this.countNewMessage = this.mail.mailTotal;
    	    this.mid = this.mailDetail.mid;
    	    this.adminname = this.mail.adminname;
    	    this.searchdata = this.mail.leaders;
    	    this.usermaildetails = this.mail.mailuserreply;
          if(this.mailDetail.options != undefined) {
            if(this.mailDetail.options.length > 0) {
              if(this.mailDetail.options[0].type==0) {
                this.multipleAnswer =true;
              } else {
                this.singleAnswer =true;
              }
            }
          }
        } else {
          this.mailDetail=this.mail.MessageDetails;
          console.log("this.mail.MessageDetails : ", this.mail.MessageDetails);
          this.activityCertificateContent=this.mail.messageContent;

        }
        this.sendrequest.getResult('user/updateMessageStatus' ,'post',{ 'id': this.msgId, 'mid': this.mail.message_id }).then((response:any) => { 
        
          this.sendrequest.updatePowerScore(this.user.id);
        },
        error => {
          
        });
        this.scrollToBottom();
        this.mailDetail.status=1;
        if(this.mailDetail.status=='1'){
          /* let dimensions = this.content.getContentDimensions();
          this.content.scrollToBottom(0);
          setTimeout(()=>{this.content.scrollToBottom();},200); */
        }
        this.loaderShow=false;
        this.updateMessages();
        this.sendrequest.updatePowerScore(this.user.id);
    }
  	
  }
  ionViewWillLeave() {
    // unsubscribe
    this.events.unsubscribe('chat:received');
  }

  ionViewDidEnter() {
    //get message list
    // this.getMsg();

    // Subscribe to received  new message events
    this.events.subscribe('chat:received', msg => {
     // this.pushNewMsg(msg);
    })
  }
  SaveCertificate() {
    let url="https://prod1.empowrd.com/api/auth/downloadPDF/" + this.mailDetail.id + '/' + this.user.id;
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
  updateMessages() {
    this.sendrequest.getResult('user/fetchLatestNewMailCountNew/' + this.user.id+'/2','get',{}).then((response:any) => { 

        localStorage.setItem('getNewMailListOrg', JSON.stringify(response.data.getNewMailListOrg));

        localStorage.setItem('getNewMailByTypes', JSON.stringify(response.data.getNewMailByTypes));
       
        localStorage.setItem('getAllMessageTypes', JSON.stringify(response.data.getAllMessageTypes));
        
        this.events.publish('messageCountChange:created', localStorage.getItem('messageCount'), Date.now()); 
      },
      error => {
       
      })
    
  }
  public getSafehtml(html:string){
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  handleClick(event) {
    console.log(event);
    console.log(event.target.parentNode.tagName);
    let url='';
    if (event.target.tagName == "A") { 
      // url = event.target.innerText.trim();
      url = event.target.getAttribute("open-externalurl");

    } else if (event.target.parentNode.tagName == "A") { 
      url = event.target.parentNode.getAttribute("open-externalurl");

      //url = event.target.parentNode.innerText.trim();
    }
    console.log(url);
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    if(url != '' && url != undefined) {
      this.iab.create(url,target,options);
      let params = {
        'user_id': this.user.id,
        'msg_id': this.mid,
        'url': url
      }
      this.sendrequest.getResult('user/msgTrack' ,'post',params).then((response:any) => { 
        
        this.sendrequest.updatePowerScore(this.user.id);
      },
      error => {
        this.sendrequest.presentToast("Error occured. Please try again");
      }) 
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MsgDetailPage');
        console.log("messageInput : ",this.messageInput.nativeElement);
        console.log("messageInput : ",this.messageInput.nativeElement);
        console.log("messageInput : ",this.messageInput.nativeElement.scrollHeight);

  }
  copyClipboard() {

  }
  getValues() {
    console.log("I am here");
    console.log(this.typeValues);
    console.log(this.optionValues);
    let result:any;
    if(this.multipleAnswer ==true) {
        let log = [];
        this.typeValues.forEach(function(value, key) {
           if (value) {
                this.push(key);
                console.log(key + ': ' + value)
            }
        }, log);
        result = log;
    } else {
        result = this.optionValues;
    }
    console.log("result : ");
    console.log(result);

    this.sendrequest.getResult('user/addvoteresult/' + this.user.id,'post',{
      options: result,
      mailid: this.mid
    }).then((response:any) => { 
      if (response != "dup") {
          this.showThanx = true;
      } else {
        this.sendrequest.presentToast("You already voted for this question.");
      }
      
      this.events.publish('msglistRefresh:created', this.user.id, Date.now());
    },
    error => {
      this.sendrequest.presentToast("Error occured. Please try again");
    });  
  }
  returnInbox() {
    if(this.navParams.get('gotoEventActivityPage')==undefined) {
      this.navCtrl.pop();
    } else {
      let views=this.navCtrl.getViews();
      let SelectedEventActivitiesPageIndex=0;
      for (var i = views.length - 1; i >= 0; i--) {
        if(views[i].name=="SelectedEventActivitiesPage") {
          SelectedEventActivitiesPageIndex=views[i].index;
          break;
        }
      }
      this.navCtrl.popTo(this.navCtrl.getByIndex(SelectedEventActivitiesPageIndex));
    }
  }
  gotoCivicDashboard() {
    this.events.publish('goToPage:params', 'TabsHomePage', { });
  }
  sendResponse() {
  	let data:any;
    if (!this.editorMsg.trim()) return;
    // Mock message
    const id = Date.now().toString();
    let newMsg = {
      date: moment().format("MMMM DD, YYYY"),
      first_name: this.user.first_name,
      last_name: this.user.last_name,
      message: this.editorMsg,
      user_id: this.user.id
    };
    let messageResponse = this.editorMsg;
    this.mail.mailuserreply.push(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }
    if(this.userCompose==false) {
      data = {
    		'user_id': this.user.id,
    		'msg_id': this.msgId,
        'mid': this.mid,
    		'message' : messageResponse
    	};
    	this.sendrequest.getResult('leaders/sendresponse','post',data).then((response:any) => { 
    		
      },
      error => {
        
      })
    } else { 
      data = {
        'user_id': this.user.id,
        'msg_id': this.msgId,
        'message' : messageResponse
      };
      this.sendrequest.getResult('leaders/sendresponsecomposed','post',data).then((response:any) => { 
        
      },
      error => {
        
      })
    }

  }
  showFormMailDetail(leader) {
    console.log(leader);
    let profileModal = this.modalCtrl.create('LeaderPopupPage', { 'leaderDetail': leader });
    profileModal.present();
    this.sendrequest.getResult('office/leaderengagement', 'post', { 'position_id': leader.position_id, 'user_id': this.user.id }).then((response: any) => {
      this.sendrequest.updatePowerScore(this.user.id);
    },
    error => {
    })
    // this.events.publish('gotoPage:created', 'LeaderDetailPage', { 'leader':leader });
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  getMsg() {
    // Get mock message list
    return this.chatService
    .getMsgList()
    .subscribe(res => {
      this.msgList = res;
      this.scrollToBottom();
    });
  }

  /**
   * @name sendMsg
   */
  sendMsg() {
    if (!this.editorMsg.trim()) return;

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: this.user.id,
      userName: this.user.name,
      userAvatar: this.user.avatar,
      toUserId: this.toUser.id,
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }

    this.chatService.sendMsg(newMsg)
    .then(() => {
      let index = this.getMsgIndexById(id);
      if (index !== -1) {
        this.msgList[index].status = 'success';
      }
    })
  }

  /**
   * @name pushNewMsg
   * @param msg
   */
  pushNewMsg(msg: ChatMessage) {
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    /* if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    } */
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea =this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }
}
