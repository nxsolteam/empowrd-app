import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MsgDetailPage } from './msg-detail';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';
import { ChatServiceProvider } from "../../providers/chat-service/chat-service";
import { RelativeTimePipe } from "../../pipes/relative-time/relative-time";
import { EmojiPickerComponentModule } from "../../components/emoji-picker/emoji-picker.module";
import { EmojiProvider } from "../../providers/emoji/emoji";
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    MsgDetailPage,
    // RelativeTimePipe,
  ],
  imports: [
    // EmojiPickerComponentModule,
    IonicPageModule.forChild(MsgDetailPage),
  	PipesModule,
    DirectivesModule,
    ComponentsModule,
  ],
  providers: [
    ChatServiceProvider,
    EmojiProvider
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MsgDetailPageModule {}
