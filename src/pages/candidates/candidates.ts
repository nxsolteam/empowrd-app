import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CandidatesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-candidates',
  templateUrl: 'candidates.html',
})
export class CandidatesPage {

  showDropDown = false

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CandidatesPage');
  }

}
