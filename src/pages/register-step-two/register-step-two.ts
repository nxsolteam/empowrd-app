import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the RegisterStepTwoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-step-two',
  templateUrl: 'register-step-two.html',
})
export class RegisterStepTwoPage {
  @ViewChild('firstName') firstNameInput;
  public submitAttempt=false;
  registerStepTwoMobileShowContent=true;
  failure=false;
  errorMessage:string;
  firstnameFocusClass='';
  successFirstnameClass='';
  successLastnameClass='';
  successEmailClass='';
  lastnameFocusClass='';
  emailFocusClass='';
  phoneNumber;
  email;
    public member:any={'email':'','first_name':'','last_name':'','password':'','confirmPassword':''};
   constructor(public navCtrl: NavController, public navParams: NavParams,public fb: FormBuilder, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, public events: Events) {
    if(navParams.get('phoneNumber') != null && navParams.get('phoneNumber') != undefined) {
      this.phoneNumber = navParams.get('phoneNumber');
    }
    if(navParams.get('email') != null && navParams.get('email') != undefined) {
      this.email = navParams.get('email');
    }
    events.subscribe('registeStepTwoContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.registerStepTwoMobileShowContent=false;
      setTimeout(() => {
        this.registerStepTwoMobileShowContent=true;
      }, 500);
    });
  }
  public registerForm = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required]
  });
  inputFocusFirstname() {
    this.firstnameFocusClass='has-focus';
  }
  inputBlurLastname() {
    this.lastnameFocusClass='';
    this.successLastnameClass='';
    if(this.registerForm.controls['last_name'].valid){
      this.successLastnameClass='has-success';
    }
    if(this.registerForm.controls['last_name'].errors ){
      this.successLastnameClass='has-error';
    }
  }
  inputFocusEmail() {
    this.emailFocusClass='has-focus';
  }
 
  inputFocusLastname() {
    this.lastnameFocusClass='has-focus';
  }
  inputBlurFirstname() {
    this.firstnameFocusClass='';
    this.successFirstnameClass='';
    if(this.registerForm.controls['first_name'].valid){
      this.successFirstnameClass='has-success';
    }
    if(this.registerForm.controls['first_name'].errors ){
      this.successFirstnameClass='has-error';
    }
  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.firstNameInput.setFocus();
    }, 1200);
     
    console.log('ionViewDidLoad RegisterPage');
  }
  goBack() {
    this.events.publish('registerContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }
  registerMember(form){
    this.submitAttempt = true;
    
    if(this.registerForm.valid){
      //console.log("Valid Form");
     
      let data = {
        email:this.email,
        first_name:form.first_name,
        last_name:form.last_name,
        home:"ecu",
        phone: this.phoneNumber
      }
      setTimeout(() => {
        this.registerStepTwoMobileShowContent=false;
      }, 100);
      this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
      setTimeout(() => {
        this.registerStepTwoMobileShowContent=true;
      }, 1500);
      
    } else {
      
    }
    
  }

}
