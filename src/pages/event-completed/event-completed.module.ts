import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventCompletedPage } from './event-completed';

@NgModule({
  declarations: [
    EventCompletedPage,
  ],
  imports: [
    IonicPageModule.forChild(EventCompletedPage),
  ],
})
export class EventCompletedPageModule {}
