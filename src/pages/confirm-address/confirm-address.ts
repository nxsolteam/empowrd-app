import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';

/**
 * Generated class for the ConfirmAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-address',
  templateUrl: 'confirm-address.html',
})
export class ConfirmAddressPage {
  latitude:any;
  longitude:any;
  address:string='';
  user:any;
  location='';
  customLocation='';
  volunteerList:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController,  private leaderlistProvider: LeaderlistProvider, private addresslistProvider: AddresslistProvider) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.address = navParams.get('addresstext');
  	this.latitude = navParams.get('latitude');
  	this.longitude = navParams.get('longitude');
  	
    this.sendrequest.getResult('user/getvolunteerlist/' + this.user.id,'get',{}).then((response:any) => {     
      
  	  	if(response.data.noresult==0){
            this.volunteerList=[];
        } else {
            this.volunteerList=response.data;
        }
  	},
  	error => {
     // loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
  	});
  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmAddressPage');
  }
  donotsaveaddress() {
  	this.sendrequest.goToPage('Search1Page',{},true);
  }	
  registeredHere(type) {
        let adminId='';
        localStorage.setItem('volunteerAdminId','');
    	if(type=="Custom Location") {
    		type=this.customLocation;
    	}
      else if(type != 'Registered Voting Location' && type != 'Work Location' && type != 'Primary Residence' && type != 'School Location') {
          localStorage.setItem('volunteerAdminId',type);
          type='Volunteer Assignment';
          adminId=type.toString();
      }
     	localStorage.setItem('currentLocationClicked','false');
	    localStorage.setItem('addresstext',this.address);
	    localStorage.setItem('showEventAddress','false');
	    localStorage.setItem('headerAddressTitle',type);
      this.sendrequest.getResult('user/checkVoterValidationAddress/' + this.user.id,'post',{ 'address': this.address }).then((response:any) => {     
        if(response.data.record==0) {
          localStorage.setItem('validateAddress','0');
        } else {
          localStorage.setItem('validateAddress','1');
        }  
      },
      error => {
      })
    
      this.sendrequest.getResult('user/saveAddress/' + this.user.id,'post',{
          address: this.address,
          type: type,
          admin_id:adminId,
          firstname:this.user.first_name,
          lastname:this.user.last_name
      }).then((response:any) => {     
      
  	  	if(response.data.type =="Validated Voting Address") {
          localStorage.setItem('headerAddressTitle',"Validated Voting Address");
        }
        localStorage.setItem('latitude', this.latitude);
  	    localStorage.setItem('longitude', this.longitude);
        localStorage.setItem('addressId',response.data.insertId.toString());
        this.leaderlistProvider.leaderlistRefresh();
        this.addresslistProvider.refresh();
        this.sendrequest.goToPage('NewHomePage',{},true);
	  	},
	  	error => {
	      
	      this.sendrequest.presentToast("Error occured. Please try again");
	  	});
    };
}
