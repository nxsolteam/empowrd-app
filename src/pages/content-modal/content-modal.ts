import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
/**
 * Generated class for the ContentModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-content-modal',
  templateUrl: 'content-modal.html',
})
export class ContentModalPage {
  data:any;
  user:any;
  letsGetStartedChecked=0;
  officeData=false;
  showRunForThisOfficeThankYou=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController) {
  	this.data=navParams.get('data');
  	this.officeData=navParams.get('officeData');
  	this.user = JSON.parse(localStorage.getItem('user'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContentModalPage');
  }
  hideFullRoleDescription() {
  	this.viewCtrl.dismiss();
  }
  	LetsStarted(value) {
	    if(this.letsGetStartedChecked == 1) {
	        this.letsGetStartedChecked=0;
	    } else {
	        this.letsGetStartedChecked=1;
	    }
	}
	sendMyInfoToRep(){
	    this.showRunForThisOfficeThankYou=true;
	    this.sendrequest.getResult("user/sendMessageToRGA/" + this.user.id,'post',{leader_id: this.data.position_id,
	        letsGetStartedChecked: this.letsGetStartedChecked}).then((response:any) => {
	     
	    },
	    error => {
	        this.sendrequest.presentToast("Error occured. Please try again");
	    });
	   
	}

}
