import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header'

/**
 * Generated class for the MyDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-data',
  templateUrl: 'my-data.html',
})
export class MyDataPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  civicData:any;
  user:any;
  powerScore='0';
  selectedOrganizationArray:any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public sendrequest: SendrequestProvider) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    this.powerScore = localStorage.getItem('powerScore');
    /*
    for(var i=0; i < this.civicData.organizations.length; i++) {
      this.civicData.organizations[i].call_text_me = true;
      this.civicData.organizations[i].email_me = true;
      this.civicData.organizations[i].view_org = true;
      this.civicData.organizations[i].support_endorse = true;
    } */
    this.selectedOrganizationArray = this.civicData.organizations;
    console.log(this.selectedOrganizationArray)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyDataPage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    console.log($event);
  }
  update() {
    this.sendrequest.getResult('user/addUserOrganizationAccess','post',{
      organization:this.selectedOrganizationArray
    }).then((response:any) => { 
      this.sendrequest.updateCivicProfileData();
      // this.sendrequest.updatePowerScore(this.user.id);
    },
    error => {
     
    })
    this.goBack();
  }
  goBack() {
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }

}
