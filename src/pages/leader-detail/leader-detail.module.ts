import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaderDetailPage } from './leader-detail';
import { PipesModule } from '../../pipes/pipes.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    LeaderDetailPage,
  ],
  imports: [
  	PipesModule,
  	DirectivesModule,
    IonicPageModule.forChild(LeaderDetailPage),
  ],
   schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class LeaderDetailPageModule {}
