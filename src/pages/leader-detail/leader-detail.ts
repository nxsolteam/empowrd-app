import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Events, Content, AlertController, Slides, Platform } from 'ionic-angular';
import { Map, tileLayer, marker, geoJSON, LayerOptions, icon, circleMarker } from 'leaflet';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
//import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import * as moment from 'moment';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';

declare var google;
declare var document;



/**
 * Generated class for the LeaderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
 // http://localhost:8100/leader-detail/1609189749
  segment: 'leader-detail/:position'
})
@Component({
  selector: 'page-leader-detail',
  templateUrl: 'leader-detail.html',
})
export class LeaderDetailPage {
  @ViewChild('mainCarousel') mainCarousel: Slides;
  @ViewChild('mapIdLeader') mapElement: ElementRef;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  map: any;
  newMarker: any;
  raceDetail: any;
  leaderDetail: any;
  scrallDirection = '';
  boundarylat;
  boundarylon;
  boundarydata;
  user: any;
  electionYear;
  showDescription = false;
  endorsed=false;
  followers:any = [];
  endorsement:any = [];
  allendorsement:any = [];
  followerText = '';
  endorsementText = '';
  endorseFirstname = '';
  endorseLastname = '';
  endorseStatement = '';
  endorsementLength=0;
  followersLength=0;
  endorseParty='';
  endorsementImage='';
  election_date:string='';
  election_type:string ='';
  ballot=false;
  loaderShow=true;
  showContactDetailButton=false;
  followEndorseText = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, private sendrequest: SendrequestProvider, public modalCtrl: ModalController, private launchNavigator: LaunchNavigator, private alertCtrl: AlertController, private callNumber: CallNumber, private emailComposer: EmailComposer, private statusBar: StatusBar, public platform: Platform,  private socialSharing: SocialSharing) {
  
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log("users ",this.user); 
    if (navParams.get('leader') == undefined && navParams.get('leaderDetail') == undefined) {
      this.leaderDetail = { abbreviation:"",address_line1:"",address_line2:"",alt_address_line1:"",alt_address_line2:"",alt_city:"",alt_phone:"",alt_state:"",alt_zip:"",boundary_type:"",city:"",cityjurisdiction:"",coverage_area:"",description:"",email:"",facebook:"",first_name:"",instagram:"",jurisdiction_name:"",jurisdiction_type:"",last_name:"",lat:"",lon:"",middle_name:"",next_election:"",next_election_date:"",next_election_format:"",next_election_yes:"",next_general_election_date:"",next_primary_election_date:"",next_special_election_date:"",nick_name:"",party:"",phone:"",photo_url:"",pid:"",pos_jid:"",position_id:"",position_title:"",position_type:"",prefix:"",short_description:"",showCandidate:"",state:"",state_code:"",stateforfront:"",suffix:"",supplemental_title:"",twitter:"",website:"",weight:"",youtube:"",zip:"" };
      this.raceDetail = this.leaderDetail;
    }
    
    let position_id;
    if(this.user==null) {
      if(navParams.get('position') != undefined) {
        position_id = navParams.get('position');
      } else {
        position_id = this.getParams();
      }
      console.log("position_id : ", position_id);
      this.sendrequest.getResult('office/getCdmLeaderDetailByPositionId/' + position_id, 'get', {}).then((response: any) => {
          this.boundarylat = response.data.stgeometry[0].latitude;
          this.boundarylon = response.data.stgeometry[0].longitude;
          this.boundarydata = response.data.stgeometry[0].boundary;
          this.leaderDetail = response.data.leaderDetail;
          this.raceDetail = this.leaderDetail;
          this.leaderDetail.boundary_type = response.data.boundary_type;
          this.setFormattedElectionDate();
          this.getEndorseAndFollowers();
          this.loaderShow=false;
          this.loadmap(response.data.mapZoomLevel);
          
      });
      localStorage.setItem('modelRegistration','true');
      
      localStorage.setItem('position_id',position_id.toString());
      //    this.navCtrl.push('MobileNumberVerificationPage',{},{animate:false});

      let registerModal = this.modalCtrl.create('MobileNumberVerificationPage', {  });
      registerModal.onDidDismiss((data:any) => {
        console.log(data);
        if(data != null && data != '') {
          
        } else {
          this.navCtrl.setRoot('LandingPage');
        }
      });
      registerModal.present();

    } else {
      console.log("call else block");
      if (navParams.get('leader') != undefined) {
        this.leaderDetail = navParams.get('leader');
        this.raceDetail = this.leaderDetail;
        this.setFormattedElectionDate();
        this.getEndorseAndFollowers();
        console.log("1 ");
      } else if (navParams.get('leaderDetail') != undefined) {
        this.raceDetail = navParams.get('raceDetail');
        this.leaderDetail = navParams.get('leaderDetail');
        console.log("2 ");
        this.setFormattedElectionDate();
        this.getEndorseAndFollowers();
      } else {
        if(navParams.get('position') != undefined) {
          position_id = navParams.get('position');
        } else {
          position_id = this.getParams();
        }
        console.log(" deeplink position_id : ", position_id);
          this.sendrequest.getResult('office/getCdmLeaderDetailByPositionId/' + position_id, 'get', {}).then((response: any) => {
          this.boundarylat = response.data.stgeometry[0].latitude;
          this.boundarylon = response.data.stgeometry[0].longitude;
          this.boundarydata = response.data.stgeometry[0].boundary;
          this.leaderDetail = response.data.leaderDetail;
          this.raceDetail = this.leaderDetail;
          this.leaderDetail.boundary_type = response.data.boundary_type;
          this.setFormattedElectionDate();
          this.getEndorseAndFollowers();
          this.loaderShow=false;
          this.loadmap(response.data.mapZoomLevel);
          this.leaderEngagement();
          this.sendrequest.buttonClicked(this.user.id,'view_leader_detail');
        },
        error => {
        })
      }
    }
    
  } 
  getParams() {
    let position_id='';
    console.log("URL : ",document.URL.indexOf("?"));
    if (document.URL.indexOf("?") > 0) {
      let splitURL = document.URL.split("?");
      let splitParams = splitURL[1].split("&");
      console.log("splitParams : ",splitParams);
      let i: any;
      for (i in splitParams){
        let singleURLParam = splitParams[i].split('=');
        if (singleURLParam[0] == "position"){
          position_id= singleURLParam[1];
        }
      }
    }
    return position_id;
  }
  setFormattedElectionDate() {
    if(this.leaderDetail.phone !='' || this.leaderDetail.alt_phone!='' || this.leaderDetail.email!='' || this.leaderDetail.facebook!='' || this.leaderDetail.twitter!='' || (this.leaderDetail.instagram!='' && this.leaderDetail.instagram!=null) || (this.leaderDetail.website!='' && this.leaderDetail.website!='none')) {
      this.showContactDetailButton=true;
    }
    if(this.leaderDetail.next_general_election_date_format ==undefined) {
      if(this.leaderDetail.next_general_election_date != '') {
        this.election_date = this.leaderDetail.next_general_election_date;
        this.election_type = 'general';
        this.electionYear = moment(this.leaderDetail.next_general_election_date, "MM/DD/YYYY").format('YY');
      } else if(this.leaderDetail.next_primary_election_date != '') {
        this.election_date = this.leaderDetail.next_primary_election_date;
        this.election_type = 'primary';
        this.electionYear = moment(this.leaderDetail.next_primary_election_date, "MM/DD/YYYY").format('YY');
      } else if(this.leaderDetail.next_special_election_date != '') {
        this.election_date = this.leaderDetail.next_special_election_date;
        this.election_type = 'special';
        this.electionYear = moment(this.leaderDetail.next_special_election_date, "MM/DD/YYYY").format('YY');
      }
    } else {
      if(this.leaderDetail.next_general_election_date != '') {
        this.election_date = this.leaderDetail.next_general_election_date;
        this.election_type = 'general';
        this.electionYear = moment(this.leaderDetail.next_general_election_date, "YYYY-MM-DD").format('YY');
      } else if(this.leaderDetail.next_primary_election_date != '') {
        this.election_date = this.leaderDetail.next_primary_election_date;
        this.election_type = 'primary';
        this.electionYear = moment(this.leaderDetail.next_primary_election_date, "YYYY-MM-DD").format('YY');
      } else if(this.leaderDetail.next_special_election_date != '') {
        this.election_date = this.leaderDetail.next_special_election_date;
        this.election_type = 'special';
        this.electionYear = moment(this.leaderDetail.next_special_election_date, "YYYY-MM-DD").format('YY');
      }
    }
  }
  shareDetail() {

    let address = this.raceDetail.coverage_area+((this.raceDetail.coverage_area.trim()!='')?',':'')+" "+((this.raceDetail.jurisdiction_type =='City' || this.raceDetail.jurisdiction_type =='County')?this.raceDetail.jurisdiction_name:'')+" "+this.raceDetail.state;

    this.socialSharing.share(this.leaderDetail.first_name+" "+this.leaderDetail.last_name+" \n"+this.leaderDetail.position_title+" \n"+address+" \n"+this.leaderDetail.phone+" \n"+this.leaderDetail.email+"\n", '', null, "https://empowrd.app/leader-detail/"+this.leaderDetail.position_id).then((result:any) => {
      this.sendrequest.buttonClicked(this.user.id,'Leader Detail Share');
    },
    error => {
          console.log('share error ', error)
    })
    
  }
  ionViewWillEnter() {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
  }
   callToNumber(number) {
      this.callNumber.callNumber(number, true)
      .then((res:any) => { this.leaderEngament('phone'); })
      .catch(err => console.log('Error launching dialer', err));
    //  Request URL: https://prod1.empowrd.com/api/office/callEmailButtonLeader/phone/1505798595/1132

   }
  leaderEngament(type) {
     this.sendrequest.getResult('office/callEmailButtonLeader/'+type+'/'+this.leaderDetail.position_id+'/'+ this.user.id , 'get', { }).then((response: any) => {
      this.updatePowerPoint();
    },
      error => {
      })
  }
  sendEmail(email) {
    let emailOptions = {
      to: email,
    };
    // Send a text message using default options
    this.emailComposer.open(emailOptions);
    this.leaderEngament('email');
  }
  showMapDirections(address) {
    let lat=parseFloat(localStorage.getItem('latitude'));
    let lon=parseFloat(localStorage.getItem('longitude'));
    let options: LaunchNavigatorOptions = { 
      start:[lat, lon]
      // app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    console.log("address", address);
    console.log(options);
    this.launchNavigator.navigate(address, options)
    .then(
      (success:any) => console.log('Launched navigator'),
      (error:any) => console.log('Error launching navigator', error)
    );
  }
  openMapPopUp(type) {
    let address = '';
    if(type=='primary') {
      address = this.leaderDetail.address_line1+' '+this.leaderDetail.address_line2+ ' '+this.leaderDetail.city+ ' '+this.leaderDetail.state+' '+this.leaderDetail.zip;
    } else {
      address = this.leaderDetail.alt_address_line1+' '+this.leaderDetail.alt_address_line2+ ' '+this.leaderDetail.alt_city+ ' '+this.leaderDetail.alt_state+' '+this.leaderDetail.alt_zip;
    }
    let alert = this.alertCtrl.create({
      title: 'Would you like driving directions to this location?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.showMapDirections(address);
          }
        }
      ]
    });
    alert.present();
  }
  getEndorseAndFollowers() {
    this.sendrequest.getResult('user/getEndorseAndFollowers', 'post', {'leader_id': this.raceDetail.position_id }).then((response: any) => {
      this.endorsementLength = response.data.endorsement.length;
      this.followersLength = response.data.followers.length;
      
      if(response.data.voterPledgesBallot!=0) {
        this.ballot=true;
      }
        this.followerText = this.followersLength+" MEMBERS";

      if(response.data.followed!=0) {
        this.endorsed=true;
        this.followEndorseText = (response.data.followedData[0].status=='0')?'FOLLOWED!':'ENDORSED!';
      }
        this.endorsementText = this.endorsementLength+" MEMBERS";
      this.allendorsement = response.data.endorsement;
      if(this.endorsementLength > 0) {
        this.endorsement.push(this.allendorsement[0]);
        /* this.endorseFirstname = this.endorsement[0].first_name;
        this.endorseLastname = this.endorsement[0].last_name;
        this.endorseStatement = this.endorsement[0].statement;
        this.endorseParty = this.endorsement[0].party;
        this.endorsementImage = this.endorsement[0].photo_url; */
      }
      this.followers = response.data.followers;
    },
    error => {
    })
  }
  viewAllEndorse() {
    this.endorsement = this.allendorsement;
  }
  leaderEngagement() {
    this.sendrequest.getResult('office/leaderengagement', 'post', { 'position_id': this.leaderDetail.position_id, 'user_id': this.user.id }).then((response: any) => {
        this.updatePowerPoint();
      },
      error => {
      })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderDetailPage');

    this.mainCarousel.centeredSlides = false;
    this.mainCarousel.direction = 'horizontal';
    this.mainCarousel.autoHeight = false;
    this.mainCarousel.pager = false;
    this.mainCarousel.loop = false;
    this.mainCarousel.zoom = false;
    this.mainCarousel.speed = 500;
    this.mainCarousel.spaceBetween = 24;
    this.mainCarousel.slidesPerView = 1.3;
    this.mainCarousel.slidesOffsetBefore = 0;
    this.mainCarousel.effect = 'slide';

    let boundary: any;
    if (this.leaderDetail.position_id != '') {
      this.sendrequest.getResult('office/getCdmOfficeBoundaryByPositionId/' + this.leaderDetail.position_id, 'get', {}).then((response: any) => {
        this.boundarylat = response.data.stgeometry[0].latitude;
        this.boundarylon = response.data.stgeometry[0].longitude;
        this.boundarydata = response.data.stgeometry[0].boundary;
        this.leaderDetail.boundary_type = response.data.boundary_type;
        this.loaderShow=false;
        this.loadmap(response.data.mapZoomLevel);
      },
      error => {
      })
      this.leaderEngagement();
      
      this.sendrequest.buttonClicked(this.user.id,'view_leader_detail');
    } 

    
    //  
    this.events.publish('incresedWidth:created', 'false', Date.now());
  }
  updatePowerPoint() {
    this.sendrequest.updatePowerScore(this.user.id);
  }
  ionViewDidEnter() {
    console.log("ionViewDidEnter New Home Page");

  }
  ionViewCanLeave() {
    // document.getElementById("mapIdLeader").outerHTML = "";
    document.getElementById("mapIdLeader").outerHTML = "";
    console.log(this.map);
    this.events.publish('incresedWidth:created', 'true', Date.now());
  }
  goBack() {
    if(this.navCtrl.canGoBack()) {
      this.navCtrl.pop({ animate: true, animation: 'ios-transition' });
    } else {
      this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
    }
  }
  
  loadmap(zoomSize) {
    let lat = this.boundarylat; // latitude
    let lon = this.boundarylon; // longitude
    let latLng = new google.maps.LatLng(lat, lon);
    
    

    console.log("zoomSize : ",zoomSize);
    console.log(this.leaderDetail.boundary_type);

    let mapOptions = {
      zoom: zoomSize,
      gestureHandling: 'none',
      zoomControl: false,
      fullscreenControl: false,
      mapTypeControl: false,
      center: new google.maps.LatLng(lat, lon), // New York
      styles: [{"featureType":"road","stylers":[{"hue":"#5e00ff"},{"saturation":-79}]},{"featureType":"poi","stylers":[{"saturation":-78},{"hue":"#6600ff"},{"lightness":-47},{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"lightness":22}]},{"featureType":"landscape","stylers":[{"hue":"#6600ff"},{"saturation":-11}]},{},{},{"featureType":"water","stylers":[{"saturation":-65},{"hue":"#1900ff"},{"lightness":8}]},{"featureType":"road.local","stylers":[{"weight":1.3},{"lightness":30}]},{"featureType":"transit","stylers":[{"visibility":"simplified"},{"hue":"#5e00ff"},{"saturation":-16}]},{"featureType":"transit.line","stylers":[{"saturation":-72}]},{}]
    };

    var infowindow = new google.maps.InfoWindow({
      content: '<div class="popup-contentBox"><img src="./assets/img/map-marker-square.png" alt=""/><div class="popup-content-info"><div class="badge">UNVERIFIED</div><div class="title">' + localStorage.getItem('addresstext') + '</div></div></div>'
    });
    console.log('this.mapElement');
    console.log(this.mapElement);
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    var jsondata = JSON.parse(this.boundarydata);
    var geojson = {
      "type": "FeatureCollection",
      "features": [
        {
          "type": "Feature",
          "id": 0,
          "properties": {
            'color': "purple",
            "popupContent": "This is the Auraria West Campus",
            "style": {
              weight: 1,
              fillOpacity: 0.7,
              fillColor: '#6a88b9',
              strokeWeight: 1,
              opacity: 0.3,
              strokeColor: '#ffffff'
            }
          },
          "geometry": {
            "type": jsondata.type,
            "coordinates": jsondata.coordinates
          }
        }
      ]
    };
    //var geojson = JSON.parse(geoString);
    this.map.data.addGeoJson(geojson)
    this.map.data.setStyle({
      weight: 1,
      fillOpacity: 0.7,
      fillColor: '#6a88b9',
      strokeWeight: 1,
      opacity: 0.3,
      strokeColor: '#ffffff'
    });

    /*var marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lon),
      map: this.map,
      icon:'assets/img/map-marker.svg',
    });
    marker.addListener('click', function() {
      infowindow.open(this.map, marker);
    });*/

  }
  goToPage(page) {
    this.navCtrl.push(page);
  }
  logScrollStart() {
    // console.log("Scroll Start");
    // document.getElementById("leaderdetailsheaderNavbar").classList.add("fadeUp");
    // document.getElementById("hidetabs").classList.add("inProgress");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("leaderdetailsheaderNavbar").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
    this.scrallDirection = $event.directionY;
    /* if (this.scrallDirection == "down") {
      document.getElementById("hidetabs").classList.add("inProgress");
      document.getElementById("leaderdetailsheaderNavbar").classList.add("fadeUp");
    }
    if (this.scrallDirection == "up") {
      document.getElementById("hidetabs").classList.remove("inProgress");
      document.getElementById("leaderdetailsheaderNavbar").classList.remove("fadeUp");
    } */
    /* if ($event.scrollTop > 100) {
      document.getElementById("leaderdetailsheaderNavbar").classList.add("shadow");
    } else {
      document.getElementById("leaderdetailsheaderNavbar").classList.remove("shadow");
    }  */

  }
  showContacts() {
    console.log("clicked");
    let profileModal = this.modalCtrl.create('LeaderPopupPage', { 'leaderDetail': this.leaderDetail });
    profileModal.present();
  }
  showEndorsePopup() {
    console.log("clicked");
    let profileModal = this.modalCtrl.create('EndorseModalPage', {  'leaderDetail': this.leaderDetail });
    profileModal.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null && data != '') {
        this.endorsed=true;
        this.submitEndorseDetails(data);     
      }
    });
    profileModal.present();
  }
  removeBallot() {
    this.ballot=false;
    let params =  { 'position_id': this.leaderDetail.position_id, 'election_date': this.election_date , 'election_type': this.election_type };
    this.sendrequest.getResult('user/removeVoterBallot', 'post', params).then((response: any) => {

      this.followers = response.data.followers;
      this.followersLength = this.followers.length;
      this.followerText = this.followersLength +" MEMBERS"; 
    },
    error => {
    })
  }
  addBallot() {
    this.ballot=true;
    let params =  { 'position_id': this.leaderDetail.position_id, 'election_date': this.election_date , 'election_type': this.election_type };
    this.sendrequest.getResult('user/addVoterBallot', 'post', params).then((response: any) => {

      this.followers = response.data.followers;
      this.followersLength = this.followers.length;
      this.followerText = this.followersLength +" MEMBERS"; 
    },
    error => {
    })
  }
  unendorse() {
    
    let params =  {};
    this.endorsed=false;
    if(this.followEndorseText=='ENDORSED!') {
      if( this.endorsement.length == this.allendorsement.length) {
        this.allendorsement = this.removeByKey(this.allendorsement, this.user.id);
        this.endorsement = this.allendorsement;
      } else {
        this.allendorsement = this.removeByKey(this.allendorsement, this.user.id);
        if(this.endorsement.id==this.user.id) {
          this.endorsement = [];
          this.endorsement.push(this.allendorsement[0]);
        }
      }
      
      this.endorsementLength = this.allendorsement.length;
      this.endorsementText = this.endorsementLength+" MEMBERS";
    }
    this.sendrequest.getResult('user/unfollowLeaders/'+this.leaderDetail.position_id+'/'+this.user.id, 'get', params).then((response: any) => {
      
     
     
    },
    error => {
    })

  }
  removeByKey(array, id){
    array.some(function(item, index) {
    if(array[index].user_id == id){
        array.splice(index, 1);
    }
    });
    return array;
  }
  submitEndorseDetails(data) {
    let status= (data.endorseFollow==false)?'0':'1';
    this.followEndorseText = (status=='0')?'FOLLOWED!':'ENDORSED!';
    let params =  { 'leader_id': this.leaderDetail.position_id, 'statement': data.statement, 'status': status};
    this.sendrequest.getResult('user/followLeaders/'+this.user.id, 'post', params).then((response: any) => {
      console.log("response.data.endorse.length");
      console.log(response.data.endorse.length);
      console.log(response.data);
      console.log(response.data.endorse);
      if(response.data.endorse.first_name != undefined) {
        this.allendorsement.push(response.data.endorse);
        if(this.endorsement.length==0) {
          this.endorsement.push(this.allendorsement[0]);
        }
        this.endorsementLength = this.allendorsement.length;
        this.endorsementText = this.endorsementLength +" MEMBERS";
      }
     
    },
    error => {
    })
  }

  
}
