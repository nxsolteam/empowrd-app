import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content, ModalController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the CivicProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-civic-profile',
  templateUrl: 'civic-profile.html',
})
export class CivicProfilePage {

  powerScore='0';
  civicData:any;
  organizationCount=0;
  causesText='';
  profileCompletedCount=0;
  organizationText='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public events: Events, public modalCtrl: ModalController) {
    this.powerScore = localStorage.getItem('powerScore');
    events.subscribe('powerScoreUpdate:created', (user, time) => {
      this.powerScore = localStorage.getItem('powerScore');
    });
    events.subscribe('updateCivicProfile:data', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
      this.updateData();
    });
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    this.updateData();
  }
  updateData() {
    this.profileCompletedCount=0;
    if(this.civicData.userData.date_of_birth != '') {
      console.log(" 1 ");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.photo_url != null && this.civicData.userData.photo_url != '') {
    console.log(" 2 ");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.prononouns != null && this.civicData.userData.prononouns !='') {
    console.log(" 3 ");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.party != null && this.civicData.userData.party != '' && this.civicData.userData.party != 'notselected') {
    console.log(" 4");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.disabled != '' && this.civicData.userData.disabled != null) {
    console.log(" 5 ");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.employment != '' && this.civicData.userData.employment != null) {
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.most_interest_in != '' && this.civicData.userData.most_interest_in != null) {
    console.log(" 6");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.engagement_level != '' && this.civicData.userData.engagement_level != null) {
    console.log("7 ");
      this.profileCompletedCount++;
    }
    if(this.civicData.userData.gender != null ) {
    console.log(" 8 ");
      this.profileCompletedCount++;
    }
    

    
    // acronym 
    this.organizationText = '';
    let orgLength =this.civicData.organizations.length;
    this.organizationCount = orgLength;
    if(orgLength > 0) {
      if(orgLength > 4) {
        for(var i=0; i < 4; i++) {
          this.organizationText = this.organizationText +""+this.civicData.organizations[i].acronym+", ";
        }
        this.organizationText = this.organizationText.substring(0, this.organizationText.length - 2);
        this.organizationText = this.organizationText + " + "+(orgLength-4).toString();
      } else {
        for(var i=0; i < orgLength; i++) {
          this.organizationText += this.civicData.organizations[i].acronym+", ";
        }
        this.organizationText = this.organizationText.substring(0, this.organizationText.length - 2);
      }
    }
    this.causesText ='';
    let causeLength =this.civicData.causes.length;
    if(causeLength > 0) {
      if(causeLength > 4) {
        for(var i=0; i < 4; i++) {
          this.causesText = this.causesText +""+this.civicData.causes[i].name+", ";
        }
        this.causesText = this.causesText.substring(0, this.causesText.length - 2);
        this.causesText = this.causesText + " + "+(causeLength-4).toString();
      } else {
        for(var i=0; i < causeLength; i++) {
          this.causesText += this.civicData.causes[i].name+", ";
        }
        this.causesText = this.causesText.substring(0, this.causesText.length - 2);
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CivicProfilePage');
  }
  showPowerScore() {
    let powerScoreModal = this.modalCtrl.create('PowerscorePage', {});
    powerScoreModal.present();
  }
  goToPage(page) {
    this.events.publish('gotoPage:created', page, {});
  }
  goBack() {
    this.navCtrl.pop({ animate:false });
  }
}
