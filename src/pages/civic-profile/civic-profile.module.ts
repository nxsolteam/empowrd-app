import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivicProfilePage } from './civic-profile';

@NgModule({
  declarations: [
    CivicProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(CivicProfilePage),
  ],
})
export class CivicProfilePageModule {}
