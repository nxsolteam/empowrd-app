import { Component, ViewChild } from '@angular/core';
import { IonicPage, Nav, NavController, NavParams, LoadingController, Events, ModalController, Platform, ViewController, App } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { Keyboard } from '@ionic-native/keyboard';
import { StatusBar } from '@ionic-native/status-bar';
/* declare var cordova: any;
declare var window: any;
declare var document: any;
var SMSRetriever = window['SMSRetriever'];
 */
/**
 * Generated class for the MobileNumberVerificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mobile-number-verification',
  templateUrl: 'mobile-number-verification.html',
})
export class MobileNumberVerificationPage {
  responseData:any;
  @ViewChild(Nav) nav: Nav;
  @ViewChild('phone1') phone1;
  @ViewChild('phone2') phone2;
  @ViewChild('phone3') phone3;
  public smsTextmessage: string = '';
  public appHashString: string = '';
  supported=true;
  phoneNumber1='';
  phoneNumber2='';
  phoneNumber3='';
  userEmail:string;
  showError=false;
  showContent=true;
  sendButtonEnabled=true;
  backEnabled=false;
  forgotEmail=false;
  showLoader=false;
  errorMessage = 'THIS NUMBER EXISTS';
  social='';
  agreePolicy=false;
  showErrorMessage=false;
  startedSmsWatch=false;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public loadingCtrl: LoadingController, public keyboard: Keyboard, public events: Events, public modalCtrl: ModalController, private statusBar: StatusBar, public platform: Platform, public viewCtrl: ViewController, public app: App) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }

    if(navParams.get('forgotEmail') != null && navParams.get('forgotEmail') != undefined) {
      this.backEnabled=true;
      this.forgotEmail=true;
    }
    if(navParams.get('response') != null && navParams.get('response') != undefined) {
    	this.responseData = navParams.get('response');
      this.userEmail = this.responseData.user.email;
    }
    if(navParams.get('fromLogin') != null && navParams.get('fromLogin') != undefined) {
      this.backEnabled=true;
      this.errorMessage='THIS NUMBER NOT ASSOCIATED WITH THIS ACCOUNT';
    }
    if(navParams.get('email') != null && navParams.get('email') != undefined) {
      this.userEmail = navParams.get('email');
    }
    if(navParams.get('social') != null && navParams.get('social') != undefined) {
      this.social = navParams.get('social');
    }
    events.subscribe('hideMobileContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.showContent=false;
      setTimeout(() => {
        this.showContent=true;
      }, 500);
    });
    events.subscribe('stopSmsWatch:created', (user, time) => {
     this.stopSmsWatch();
    });
  }
  ionViewWillEnter()
  {

  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.phone1.setFocus();
    }, 1200);
    /* if(SmsReceiver != undefined) {
      SmsReceiver.isSupported((supported) => {
       this.supported=supported;
      }, (err) => {
        console.log(err);
      });
    }
    if(this.supported==true && SmsReceiver != undefined) {
      SmsReceiver.requestPermission((request) => {
        console.log(request);
      }, (err) => {
        console.log(err);
      });
    } */
    console.log('ionViewDidLoad MobileNumberVerificationPage');
    
  }
  startSmsWatch() {
    /* SMSRetriever.getHashString(function(hash) {
      // Hash string returned OK
      console.log('hash');
      alert(hash);
       SMSRetriever.fireOnSmsArrive(function (message) {
          console.log(message);
          cordova.fireDocumentEvent('onSMSArrive', {
              'message': message
          });
      });
    }, function(err) {
      // Error retrieving hash string
      console.error(err);
    });
    SMSRetriever.startWatch(function(msg) {
      // Wait for incoming SMS
      console.log('messageBody');
      alert(msg);
    }, function(err) {
      // Failed to start watching for SMS
      console.error(err);
    });
    document.addEventListener('onSMSArrive', function(args) {
      // SMS arrived, get its contents
      console.info('onSMSArrive');
      console.info(args);
      console.info(args.message);

      // To Do: Extract the received one-time code and verify it on your server
    }); */
    /* if(this.supported==true && this.startedSmsWatch==false && SmsReceiver != undefined) {
      this.startedSmsWatch=true;
      SmsReceiver.startReception(({messageBody, originatingAddress}) => {
        console.log(messageBody);
        console.log(originatingAddress);
        if('Empowrd verification code is '+localStorage.getItem('code')) {
          this.events.publish('smsReaded:created', messageBody, Date.now());
         // this.stopSmsWatch();
        }
      }, (err) => {
          console.log(err);
      })
    } */
  }
  stopSmsWatch() {
    /* if(this.startedSmsWatch==true) {
      this.startedSmsWatch=false;
      if(SmsReceiver != undefined) {
        SmsReceiver.stopReception(() => {
          console.log("Correctly stopped");
        }, (err) => {
          console.log("Error while stopping the SMS receiver",err);
        });
      }
    } */
  }
  setFocusElement() {
    this.phone1.setFocus();
  }
  openPolicy() {
  	console.log("open policy");
  	let profileModal = this.modalCtrl.create('PrivacyPolicyPage', { 'openModal': '1' });
    profileModal.onDidDismiss((data:any) => {
      
    });
    profileModal.present();
  }
  goBack() {
    this.events.publish('loginPageContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    if(this.navCtrl.canGoBack()) {
      if(localStorage.getItem('modelRegistration')=='true') {
        this.viewCtrl.dismiss();
      } else {
        this.navCtrl.popToRoot({animate:false});
      }
    } else {
      this.navCtrl.setRoot('LandingPage',{},{animate:false});
    }
  }
  verifyMobile(){
    let phoneNumber = this.phoneNumber1.replace("-",'');
    if(this.forgotEmail==true) {
      this.sendrequest.sendEmailVerificationLink({
        phoneNumber:phoneNumber.replace("-",'')
      }).then((response:any) => { 
         
      },
      error => {
        
      });
      setTimeout(() => {
       this.showContent=false;
      }, 100);
      this.navCtrl.push('NumberSubmittedPage',{'phoneNumber':this.phoneNumber1+''+this.phoneNumber2+''+this.phoneNumber3,'fromLogin':this.backEnabled, 'forgotEmail':this.forgotEmail },{animation: 'ios-transition', duration:900,animate:true});
   
      setTimeout(() => {
       this.showContent=true;
      }, 1500);
    } else {
    	if(this.agreePolicy==false && this.backEnabled==false) {
    		this.showErrorMessage=true;
    		return;
    	}
      this.showError=false;
      this.showLoader=true;
      this.startSmsWatch();
      this.sendrequest.sendVerificationCode({
        email:this.userEmail, 
        fromLogin:this.backEnabled,
        'social': this.social,
        phoneNumber:phoneNumber.replace("-",'')
      }).then((response:any) => { 
         this.showLoader=false;
         // 'code':response.data.code, 'email':response.data.email
         localStorage.setItem('code',response.data.code);
        setTimeout(() => {
         this.showContent=false;
        }, 100);
        //this.app.getRootNav().push('VerifyMobilePage',{'phoneNumber':this.phoneNumber1+''+this.phoneNumber2+''+this.phoneNumber3,'fromLogin':this.backEnabled, 'forgotEmail':this.forgotEmail,'social': this.social },{animation: 'ios-transition', duration:900,animate:true});

        this.navCtrl.push('VerifyMobilePage',{'phoneNumber':this.phoneNumber1+''+this.phoneNumber2+''+this.phoneNumber3,'fromLogin':this.backEnabled, 'forgotEmail':this.forgotEmail,'social': this.social },{animation: 'ios-transition', duration:900,animate:true});
      
        setTimeout(() => {
         this.showContent=true;
        }, 1500);
      },
      (error:any) => {
        // console.log(error);
        this.showLoader=false;
        this.showError=true;
        //  this.sendrequest.presentToast("Your Login Credentials Failed. Try Again.");
        //  this.goToRoot();
      });
      
    }
    
  }
  
 
  checkLength(event,type)
  {
    /*
     if(event.key != "Backspace" )//MAULIK::ADDED
     {
       var numbers=this.phoneNumber1;
       
       if(numbers.length > 9)
       {
        return false;
       }
        if(numbers.length == 3)
       {
         this.phoneNumber1= numbers+'-';
       }
       if(numbers.length == 7)
       {
         this.phoneNumber1= numbers+'-';
       } 
     } */   
     /* if(this.phoneNumber1.length ==11)
     {
       this.sendButtonEnabled=true;            
     }
     else
     {
       this.sendButtonEnabled=false;
     }      
       */
  }

}
