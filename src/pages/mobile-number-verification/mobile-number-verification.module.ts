import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MobileNumberVerificationPage } from './mobile-number-verification';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

@NgModule({
  declarations: [
    MobileNumberVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(MobileNumberVerificationPage),
    NgxMaskIonicModule,
  ],
})
export class MobileNumberVerificationPageModule {}
