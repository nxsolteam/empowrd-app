import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistereventPage } from './registerevent';

@NgModule({
  declarations: [
    RegistereventPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistereventPage),
  ],
})
export class RegistereventPageModule {}
