import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
/**
 * Generated class for the RegistereventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registerevent',
  templateUrl: 'registerevent.html',
})
export class RegistereventPage {
@ViewChild('phone1') phone1;

  phoneNumber1='';
  userEmail='';
  sendButtonEnabled=false;
  firstDashOptionClass="disabled";
  secondDashOptionClass="disabled";
  emailMessageShow=false;
  codeMessageShow=false;
  eventCodeTrue=false;
  confirmationEventsId:any=[];
  selectedEvent:any=[];
  events:any=[];
  event:any=[];
  user:any;
  eventName='';
  currentSelectedEventIndex:number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public sendrequest: SendrequestProvider) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.confirmationEventsId = navParams.get('confirmationEventsId');
    // this.events = navParams.get('events');
    this.event = navParams.get('event');
    // this.selectedEvent = navParams.get('selectedEvent');
    this.currentSelectedEventIndex=0;
    this.eventName= this.event.event_name;
    // this.getEventName();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistereventPage');
  }
  getEventName() {
    this.events.forEach(function(item){ 
      if(this.confirmationEventsId[this.currentSelectedEventIndex]==item.id ){
        this.eventName= item.event_name;
      } 
    }, this);
  }
  goBackButton() {
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  addToProfile(){
    this.emailMessageShow=false;
    this.codeMessageShow=false;
    if(this.userEmail!='') {
    	this.sendrequest.getResult('user/verifyUserEmailForEvent','post',{
        email:this.userEmail,
        eventCode:this.phoneNumber1,
        eventid:this.event.id
      }).then((response:any) => { 
        this.emailMessageShow=false;
        this.codeMessageShow=false;
        if(response.data.emailnotExist=='0'){
          this.sendrequest.updateCivicProfileData();
          this.sendrequest.updatePowerScore(this.user.id);
          this.navCtrl.push('SelectedEventsPage', { 'event':this.event },{ animate:true, animation: 'ios-transition' });
          /* this.currentSelectedEventIndex++;
          if(this.currentSelectedEventIndex==this.confirmationEventsId.length) {
            this.deselectedEventDelete();
            
            this.currentSelectedEventIndex--;
            if(this.navParams.get('powerScore')==undefined) {
              this.navCtrl.push('AddEventsActivitiesPage', { 'selectedEvent': this.selectedEvent, 'events': this.events },{animate:true, animation: 'ios-transition'});
            } else {
              this.navCtrl.push('AddEventsActivitiesPage', { 'selectedEvent': this.selectedEvent, 'events': this.events, 'powerScore': this.navParams.get('powerScore') },{animate:true, animation: 'ios-transition'});

            }
          } else {
            this.userEmail='';
            this.phoneNumber1='';
            this.getEventName();
          } */

        } else {
          this.emailMessageShow=true;
        }
      },
    	error => {
    		this.sendrequest.presentToast("Error occured. Please try again");
    	});
    } else {
      var codeValid=false;
      // this.events.forEach(function(item){ 
        if(this.event.code==this.phoneNumber1){
          codeValid=true;
          this.addUserToEventGroup(this.event.id);
          this.navCtrl.push('SelectedEventsPage', { 'event':this.event },{ animate:true, animation: 'ios-transition' });
          
          /* this.currentSelectedEventIndex++;
          if(this.currentSelectedEventIndex==this.confirmationEventsId.length) {
            this.deselectedEventDelete();
           
            this.currentSelectedEventIndex--;
            if(this.navParams.get('powerScore')==undefined) {
              this.navCtrl.push('AddEventsActivitiesPage', { 'selectedEvent': this.selectedEvent, 'events': this.events },{animate:true, animation: 'ios-transition'});
            } else {
              this.navCtrl.push('AddEventsActivitiesPage', { 'selectedEvent': this.selectedEvent, 'events': this.events, 'powerScore': this.navParams.get('powerScore') },{animate:true, animation: 'ios-transition'});

            }
          } else {
            this.userEmail='';
            this.phoneNumber1='';
            this.getEventName();
          } */
        } 
      // }, this);
      this.codeMessageShow =!codeValid;
    }
  }
  deselectedEventDelete() {
    this.sendrequest.getResult('user/deselectedEventDelete','post',{
      selectedEvents:this.selectedEvent
    }).then((response:any) => {    
    },
    error => {
      
    });
  }
  addUserToEventGroup(eventId) {
    this.sendrequest.getResult('user/addEmailToUserEventGroup','post',{
      eventid:eventId
    }).then((response:any) => {   
      this.sendrequest.updateCivicProfileData();
      this.sendrequest.updatePowerScore(this.user.id); 
    },
    error => {
      
    });
  }

}
