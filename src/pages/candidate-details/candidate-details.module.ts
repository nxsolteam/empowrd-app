import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CandidateDetailsPage } from './candidate-details';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CandidateDetailsPage,
  ],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(CandidateDetailsPage),
  ],
})
export class CandidateDetailsPageModule {}
