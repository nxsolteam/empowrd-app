import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
/**
 * Generated class for the CandidateDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-candidate-details',
  templateUrl: 'candidate-details.html',
})
export class CandidateDetailsPage {
  officeid:string;
  electionYear:string;
  headerBackround:string;
  headerRightBackround:string;
  user:any;
  candidateList:any;
  leaderdata:any=[];
   constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private iab: InAppBrowser) {
  	this.user = JSON.parse(localStorage.getItem('user'));
  	//this.officeid=this.navParams.get('id');
  	//this.electionYear=this.navParams.get('electionYear');
  	this.officeid='43464';
  	this.electionYear='2020';
  	let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      spinner: 'dots'
    });
    loading.present();
    this.sendrequest.getResult("user/getCandidates/" + this.user.id,'post',{ leader_id: this.officeid}).then((response:any) => {
      //loading.dismiss();
      this.candidateList=response.data;
    },
    error => {
       // loading.dismiss();
        this.sendrequest.presentToast("Error occured. Please try again");
    });
  	this.sendrequest.getResult('office/' + this.officeid + '/' + this.user.id,'get',{}).then((response:any) => {

  		
      	this.leaderdata = response.data.position_information;
        var statePositionTitle = ['Attorney General', 'Supreme Court Clerk', 'Court of Appeals Judge', 'Supreme Court Reporter of Decisions', 'Supreme Court Judge', 'Labor Commissioner', 'Insurance Commissioner', 'School Superintendent', 'Secretary of State', 'Agriculture Commissioner', 'Lieutenant Governor', 'Governor', 'U.S. Representative', 'State Senator', 'State Representative', 'Attorney General', 'Superintendent of Public Instruction', 'State Court Judge', 'State Representative', 'State Senator', 'Public Service Commissioner', 'Superior Court Judge', 'Chief Superior Court Judge', 'Public Service Commissioner'];
        var countyPositionTitle = ['County Commission Chairperson', 'Coroner', 'Solicitor General', 'Sheriff', 'Assoc. Probate Court Judge', 'Interim Probate Court Judge', 'Probate Court Judge', 'Chief Magistrate Judge', 'School Board Member', 'Magistrate Judge', 'State Court Judge', 'County Commissioner'];
        var cityPositionTitle = ['City Council Member', 'Mayor', 'Chairman', 'Commissioner', 'School Board Member','Chairperson'];
        var usPositionTitle = ['Attorney General', 'Vice President', 'President', 'U.S. Senator', 'U.S. Representative', 'Congressmember'];
        if (cityPositionTitle.indexOf(this.leaderdata[0].position_title) >= 0) {
            this.headerBackround = "#6844b7";
            this.headerRightBackround = "#503192";
        } else if (countyPositionTitle.indexOf(this.leaderdata[0].position_title) >= 0) {
            this.headerBackround = "#b74444";
            this.headerRightBackround = "#9a3737";
        } else if (statePositionTitle.indexOf(this.leaderdata[0].position_title) >= 0) {
            this.headerBackround = "#a944b7";
            this.headerRightBackround = "#752a7f";
        } else if (usPositionTitle.indexOf(this.leaderdata[0].position_title) >= 0) {
            this.headerBackround = "#449ab7";
            this.headerRightBackround = "#246b83";
        }
        else {
            this.headerBackround = "#b7445b";
            this.headerRightBackround = "a43d51";
        }
        loading.dismiss();
    },
    error => {
        loading.dismiss();
        this.sendrequest.presentToast("Error occured. Please try again");
    });
       
  }
  getParty(party) {
	   
	    if(party=='Republican' || party=='Republican Party'){
	        return 'REPUBLICAN';
	    } else if( party=='Democrat' || party=='Democratic') {
	        return 'DEMOCRAT';
	    } else if(party=='Non-partisan' || party=='') {
	        return '';
	    } else {
	        return party.toUpperCase()+" PARTY";
	    }
  }
  twitterFilter(str) {
        str = str.trim();
        str = str.replace("@", "");
        return str;
  }
  showRunForThisOffice(){
    let modal = this.modalCtrl.create('ContentModalPage', {
            data: this.leaderdata[0],
            officeData:true,
        },{
      cssClass: 'content-modal'
    });
    modal.onDidDismiss(() => {
        // Nothing
    });
    modal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CandidateDetailsPage');
  }
  gowebsiteUrl(url){
    this.goPageFromUrl('http://'+url);
  }
  gofacebookUrl(url){
    this.goPageFromUrl('http://'+url);
  }
  goTwitterUrl(url){
    this.goPageFromUrl('https://twitter.com/'+this.twitterFilter(url));
  }
  goPageFromUrl(url){
    //window.location.href=url;
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(url,target,options);
  }
}
