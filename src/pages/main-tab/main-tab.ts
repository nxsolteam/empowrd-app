import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs, Tab, Events, Platform, MenuController, AlertController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { StatusBar } from '@ionic-native/status-bar';
import { MyCommunityProvider } from '../../providers/my-community/my-community';
/**
 * Generated class for the MainTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main-tab',
  templateUrl: 'main-tab.html',
})
export class MainTabPage {
  @ViewChild('mainTabs') mainTabs: Tabs;
  newHomeRoot = "NewHomePage";
  tab1Root = "HomeOpportunityPage";
  tab2Root = "OfficialsPage";
  tab6Root = "ElectionsPage";
  tab3Root = "NewHomePage";
  electionRacesParams={};
  tab4Root = "RaceDetailsPage";
  tabMessageRoot = "MsglistPage";
  // tab5Root = "CandidatesPage"; // "AccountPage";
  tab5Root = "TakeActionPage"; // "AccountPage";
  selectedIndex:any;
  activeTab: any;
  showsHomeTab=true;
  showDetailsTab=false;
  countNewMessage='0';
  loaded:   boolean = false;
  tabIndex: number  = 0;
  powerScore='0';
  user:any;
  initial='';
  showRepsTab=true;
  showOpportunity=true;
  engagedNewMessage=0;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public plt: Platform, public menuCtrl: MenuController, public sendrequest: SendrequestProvider, private nativePageTransitions: NativePageTransitions, private statusBar: StatusBar, private alertCtrl: AlertController, public myCommunityProvider: MyCommunityProvider) {
    this.statusBar.overlaysWebView(false);
    if(this.plt.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.user = JSON.parse(localStorage.getItem('user'));
    if(this.user==null) {
      localStorage.setItem('modelRegistration','true');
      this.navCtrl.setRoot('MobileNumberVerificationPage',{},{animate:false});
    } else {
      this.initial=this.user.first_name.charAt(0)+""+this.user.last_name.charAt(0);
      this.initial = this.initial.toUpperCase();
      if(navParams.get('powerScore') != undefined) {
        this.powerScore = navParams.get('powerScore').toString();
        localStorage.setItem('powerScore', this.powerScore);
      } else {
        if(localStorage.getItem('powerScore')==undefined) {
          localStorage.setItem('powerScore', '0');  
          this.sendrequest.updatePowerScore(this.user.id);
        } else {
          let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
          if(totalpowerScore > 999) {
            this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
          } else {
            this.powerScore = localStorage.getItem('powerScore');
          }
        }
      }
      events.subscribe('setLatLon:created', (user, time) => {
        
          this.showRepsTab=true;
        
        
      });
      console.log("stateShortName : ", localStorage.getItem('stateShortName'));
      if(localStorage.getItem('stateShortName')!=undefined && localStorage.getItem('stateShortName')!=null) {
        
          this.showRepsTab=true;
        
      }
      events.subscribe('powerScoreUpdate:created', (user, time) => {
        let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
        if(totalpowerScore > 999) {
          this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
        } else {
          this.powerScore = localStorage.getItem('powerScore');
        }
      });
      console.log(" onboardingStart ");

      console.log(localStorage.getItem('onboardingStart') );
       
      if(localStorage.getItem('onboardingStart') != undefined) {
        console.log(" onboardingStart call");
        this.sendrequest.getResult('user/getFriendNotification','get', {}).then((response:any) => { 
          if(response.data.message!="") {
            let alert = this.alertCtrl.create({
              title: 'EMPOWRD',
              cssClass: 'confirm-register-modal',
              subTitle: response.data.message,
              buttons: ['OK']
            });
            alert.present();
          }
        },
        error => {
        });
        this.myCommunityProvider.myCommunityRefresh();
        localStorage.removeItem('onboardingStart');
      }
      events.subscribe('countNewMessage:created', (user, time) => {
        let getNewMailByTypes = JSON.parse(localStorage.getItem('getNewMailByTypes'));
        this.engagedNewMessage = 0;
        for(var i=0; i< getNewMailByTypes.length; i++) {
          this.engagedNewMessage += getNewMailByTypes[i].new_message_count;
        }
      });
      events.subscribe('messageCountChange:created', (user, time) => {
        let getNewMailByTypes = JSON.parse(localStorage.getItem('getNewMailByTypes'));
        this.engagedNewMessage = 0;
        for(var i=0; i< getNewMailByTypes.length; i++) {
          this.engagedNewMessage += getNewMailByTypes[i].new_message_count;
        }
      });
      if(localStorage.getItem('getNewMailByTypes') != undefined) {
        let getNewMailByTypes = JSON.parse(localStorage.getItem('getNewMailByTypes'));
        this.engagedNewMessage = 0;
        for(var i=0; i< getNewMailByTypes.length; i++) {
          this.engagedNewMessage += getNewMailByTypes[i].new_message_count;
        }
      }
    }
    if(navParams.get('selectedIndex')==undefined) {
      this.selectedIndex = 2;
      this.tabIndex = this.selectedIndex;
    } else {
      this.selectedIndex = parseInt(navParams.get('selectedIndex'));
      this.tabIndex = this.selectedIndex;
    }
      
  }
  private getAnimationDirection(index):string {
    var currentIndex = this.tabIndex;

    this.tabIndex = index;

    switch (true){
      case (currentIndex < index):
        return('left');
      case (currentIndex > index):
        return ('right');
    }
  }
  public transition(e):void {
   
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MainTabPage');
     console.log("this.mainTabs");
    console.log(this.mainTabs);
    this.events.subscribe('selectedTabs:data', (page, data) => {
        console.log(data);
        this.electionRacesParams ={};
        if(data.date!=undefined){
          this.electionRacesParams=data;
        }
        if (this.plt.is('android')) {
          if(document.getElementById("tab-t0-"+page)) {
            setTimeout(() => {
            document.getElementById("tab-t0-"+page).click();
            },450);
          }
          if(document.getElementById("tab-t1-"+page)) {
            setTimeout(() => {
              document.getElementById("tab-t1-"+page).click();
            },450);
          }
          if(document.getElementById("tab-t2-"+page)) {
            setTimeout(() => {
              document.getElementById("tab-t2-"+page).click();
            },450);
          }
        } else {
        console.log(parseInt(page));
          this.mainTabs.select(parseInt(page));
        }
        if(page != '5') {
          this.showDetailsTab=false;
        } else {
          this.showDetailsTab=true;
        }
        if(page== '3') {
          this.showOpportunity==false;
        } else if(page== '2') {
          this.showOpportunity==true;
        }
        
      });
    if(localStorage.getItem('registereddevice') != undefined) {
      let typeDevice='';
      if (this.plt.is('ios') || this.plt.is('ipad'))  {
        typeDevice='ios';
      } else {
        typeDevice='android';
      }
      let registrationId = localStorage.getItem('registereddevice');
      this.sendrequest.getResult('user/updateDeviceId/' + typeDevice + '/' + registrationId,'get', {}).then((response:any) => { 
      },
      error => {
      });
    }
    if(this.user!=null) {
      if(localStorage.getItem('civicProfile') == undefined) {
        this.sendrequest.updateCivicProfileData();
        
      }
    }
  }
  goBack(page) {
    // this.navCtrl.push(page,{},{animate:true, duration:800, direction: 'back', animation: 'ios-transition'});
    // this.showsHomeTab=true;
    this.events.publish('selectedTabs:data', '2', Date.now());
  }
  openLeadersTab() {
    // this.showsHomeTab=false;
    this.events.publish('selectedTabs:data', '5', Date.now());
  }
  goToProfilePage() {
   this.events.publish('gotoPage:created', 'CivicProfilePage', {});
  }
  goToPage(page) {
    this.navCtrl.push(page,{},{animate:true, animation: 'ios-transition'});
  }
  openMenu(){
    if(this.user!=null) {
      this.menuCtrl.open();
    }
  }

}
