import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainTabPage } from './main-tab';
// import { SuperTabsModule } from 'ionic2-super-tabs';

@NgModule({
  declarations: [
    MainTabPage,
  ],
  imports: [
    IonicPageModule.forChild(MainTabPage),
    // SuperTabsModule,
  ],
})
export class MainTabPageModule {}
