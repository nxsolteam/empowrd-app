import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';

/**
 * Generated class for the HelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

   text='';
  problem='';
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider) {
    this.user =  JSON.parse(localStorage.getItem('user'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

  sendCommunity() {
    this.sendrequest.getResult('user/sendhelp/' + this.user.id,'post',{ 'problem': this.problem, 'text': this.text, 'user': this.user }).then((response:any) => { 

    },
    error => {
      
    })
    this.problem='';
    this.text='';
    this.sendrequest.presentToast('Thank you for given feedback.');
    this.navCtrl.pop();
  }
  goBack() {
  	this.navCtrl.pop();
  }

}
