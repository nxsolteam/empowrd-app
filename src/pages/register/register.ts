import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, AlertController, Platform, ViewController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { CryptProvider } from '../../providers/crypt/crypt';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Cache, CacheService } from 'ionic-cache-observable';
import { AddresslistProvider } from '../../providers/addresslist/addresslist';
import { Observable } from 'rxjs/Observable';
import { Addresslist } from '../../providers/addresslist/addresslist.model';
import { Subscription } from 'rxjs/Subscription';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { ElectionRacesProvider } from '../../providers/election-races/election-races';
import { StatusBar } from '@ionic-native/status-bar';
declare var cordova: any;

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  @ViewChild('email') firstNameInput;
  public submitAttempt=false;
  registerMobileShowContent=true;
  failure=false;
  firstnameFocusClass='';
  successFirstnameClass='';
  successLastnameClass='';
  successEmailClass='';
  lastnameFocusClass='';
  emailFocusClass='';
  showLoader=false;
  phoneNumber;
  errorMessage='THIS EMAIL EXISTS';
  showError=false;
  email;
  public member:any={'email':''};
  showAppleLoginButton=false;
  addresslist:any;
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams,public fb: FormBuilder, public sendrequest: SendrequestProvider, public crypt: CryptProvider, public loadingCtrl: LoadingController, public events: Events, private googlePlus: GooglePlus, private facebook: Facebook, private alertCtrl: AlertController, public platform: Platform, private addresslistProvider: AddresslistProvider, private cacheService: CacheService, private leaderlistProvider: LeaderlistProvider, private electionRacesProvider: ElectionRacesProvider, private statusBar: StatusBar, public viewCtrl: ViewController) {
    this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    if(navParams.get('phoneNumber') != null && navParams.get('phoneNumber') != undefined) {
      this.phoneNumber = navParams.get('phoneNumber');
    }
    if (this.platform.is('ios') || this.platform.is('ipad')) {
      this.showAppleLoginButton=true;
    }
    localStorage.setItem('onboardingStart','1'); 
    events.subscribe('registerContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.registerMobileShowContent=false;
      setTimeout(() => {
        this.registerMobileShowContent=true;
      }, 500);
    });
  }
  
  confirmPopUp() {
    let alert = this.alertCtrl.create({
      title: "If you leave now it will cancel your member registration process.",
      subTitle: "Please confirm below.",
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'DO NOT REGISTER',
          handler: () => {
            this.goToRoot();
          }
        },
        {
          text: 'CONTINUE',
          handler: () => {
            this.registerMember(this.registerForm.value);
          }
        }
      ]
    });
    alert.present();
  }
  public registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$")]],
    first_name: ['', Validators.required],
    last_name: ['', Validators.required]
  });
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        let passwordInput = group.controls[passwordKey],
            passwordConfirmationInput = group.controls[passwordConfirmationKey];
        if (passwordInput.value !== passwordConfirmationInput.value) {
            return passwordConfirmationInput.setErrors({notEquivalent: true})
        }
        else {
            return passwordConfirmationInput.setErrors(null);
        }
    }
  }

  googleLogin(){
    console.log("Google Login");
    this.googlePlus.login(
      {
    })
    .then((res:any) => {
      // alert(JSON.stringify(res));
      let data = {
        email:res.email,
        first_name:res.givenName,
        last_name:res.familyName,
        home:"ecu",
        phone: this.phoneNumber,
        photo_url: res.imageUrl,
        social: 'google',
        google: res.userId,
      }
      this.checkEmailExist(data);
      /* setTimeout(() => {
        this.registerMobileShowContent=false;
      }, 100);
      this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
      setTimeout(() => {
        this.registerMobileShowContent=true;
      }, 1500);
      */
    })
    .catch(err =>  this.sendrequest.presentToast('Unable to login with google'));
  }
  fbLogin() {
    this.facebook.login(['public_profile', 'email'])
    .then((res: FacebookLoginResponse) => {
      // alert(JSON.stringify(res));
      this.facebook.api('me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)', []).then((profile:any) => {
        
        let data = {
          email:profile.email,
          first_name:profile.first_name,
          last_name:profile.last_name,
          home:"ecu",
          phone: this.phoneNumber,
          photo_url: profile.picture_large.data.url,
          social: 'facebook',
          facebook: profile.id,
        }
        this.checkEmailExist(data);
      });
    })
    .catch(e => this.sendrequest.presentToast('Unable to login with facebook'));
  }
  

 appleLogin() {
   console.log("Apple Login"); 
    /* let appleLoginResponse = { 'email' : '12jatin.b.php@gmail.com', 'fullName': {'givenName':'Jatin', 'familyName':'Bhatt' }, 'user': '0001866.dbd168c82c944455b60f1bec53d15b58.0518', 'authorizationCode': 'nsdfgbusbguiguifg' };
   console.log(appleLoginResponse);
   
      alert(JSON.stringify(appleLoginResponse));
      let data = {
        email:appleLoginResponse.email,
        first_name:appleLoginResponse.fullName.givenName,
        last_name:appleLoginResponse.fullName.familyName,
        home:"ecu",
        phone: this.phoneNumber,
        social: 'apple',
        apple: appleLoginResponse.user,
      }
      this.checkEmailExist(data); */
   cordova.plugins.SignInWithApple.signin(
    { requestedScopes: [0, 1] },
    (appleLoginResponse: any) => {
      console.log(appleLoginResponse);
      let data = {
        email:appleLoginResponse.email,
        first_name:appleLoginResponse.fullName.givenName,
        last_name:appleLoginResponse.fullName.familyName,
        home:"ecu",
        phone: this.phoneNumber,
        social: 'apple',
        apple: appleLoginResponse.user,
      }
      this.checkEmailExist(data);
    },
    (err:any) => {
      console.error(err);
      console.log(JSON.stringify(err));
    }
  ) 

 }

  checkEmailExist(data) {
    this.showLoader=true;
    this.showError=false;
    this.sendrequest.checkEmailExist(data).then((responseData:any) => {
      if(responseData.data.login=='1') {
        this.sendrequest.authtoken = responseData.data.token;
        localStorage.setItem('user', JSON.stringify(responseData.data.user));
        localStorage.setItem('auth-token', responseData.data.token);
        console.log( JSON.parse(localStorage.getItem('user')));
        this.addresslistProvider.refresh();

        //const sourceData = this.addresslistProvider.random();
        this.cacheService
        .get('addresslist')
        .mergeMap((cache: Cache<Addresslist[]>) => {
            this.cache = cache;
            
            return this.cache.get$;
        })
        .subscribe((addresslist:any) => {
            this.addresslist=addresslist;
           
            if(localStorage.getItem('addressId')!=this.addresslist.data.addresses[0].id) {
              localStorage.setItem('headerAddressTitle' , this.addresslist.data.addresses[0].type);
              localStorage.setItem('addresstext' , this.addresslist.data.addresses[0].address); 
              localStorage.setItem('addressId' , this.addresslist.data.addresses[0].id); 
              localStorage.setItem('city' , this.addresslist.data.addresses[0].city); 
              localStorage.setItem('county' , this.addresslist.data.addresses[0].county); 
              localStorage.setItem('state' , this.addresslist.data.addresses[0].state); 
              localStorage.setItem('validateAddress',this.addresslist.data.addresses[0].validateAddress);
            
              this.events.publish('setAddressTitle:created', 'user', Date.now());
              
              this.sendrequest.getResult('office/findlatlong/'+ localStorage.getItem('addresstext') + '/' + responseData.data.user.id,'get',{}).then((response:any) => {     
                var results = response.data.response.results[0];
                localStorage.setItem('latitude',results.location.lat.toString());
                localStorage.setItem('longitude',results.location.lng.toString());
                this.events.publish('setLatLon:created', 'user', Date.now());
                this.leaderlistProvider.leaderlistRefresh();
                this.electionRacesProvider.electionRaceRefresh();
              });
            }
           
        });
        this.showError=false; 
        this.showLoader=false;
         setTimeout(() => {
         this.registerMobileShowContent=false;
        }, 100);
        this.navCtrl.setRoot('MainTabPage',{},{animate:true, duration:900,  direction: 'forward', animation: 'ios-transition'});
        setTimeout(() => {
         this.registerMobileShowContent=true;
        }, 1200);

      } else {

        this.showError=false; 
        this.showLoader=false;
        setTimeout(() => {
          this.registerMobileShowContent=false;
        }, 100);
        this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
        setTimeout(() => {
          this.registerMobileShowContent=true;
        }, 1500);
      }
    },
    error => {
      this.showLoader=false;
      this.showError=true;
      // this.sendrequest.presentToast("Email address is already associated with another account");
    });
   }
  //MAULIK
  inputFocusFirstname() {
    this.firstnameFocusClass='has-focus';
  }
  inputBlurLastname() {
    this.lastnameFocusClass='';
    this.successLastnameClass='';
    if(this.registerForm.controls['last_name'].valid){
      this.successLastnameClass='has-success';
    }
    if(this.registerForm.controls['last_name'].errors ){
      this.successLastnameClass='has-error';
    }
  }
  inputFocusLastname() {
    this.lastnameFocusClass='has-focus';
  }
  inputBlurFirstname() {
    this.firstnameFocusClass='';
    this.successFirstnameClass='';
    if(this.registerForm.controls['first_name'].valid){
      this.successFirstnameClass='has-success';
    }
    if(this.registerForm.controls['first_name'].errors ){
      this.successFirstnameClass='has-error';
    }
  }
  //MAULIK

  inputFocusEmail() {
     this.emailFocusClass='has-focus';
  }
  inputBlurEmail() {
    this.emailFocusClass='';
    this.successEmailClass='';
    if(this.registerForm.controls['email'].valid){
      this.successEmailClass='has-success';
    }
    if(this.registerForm.controls['email'].errors) {
      this.successEmailClass='has-error';
    }
  }
 
  ionViewDidLoad() {
    setTimeout(() => {
      this.firstNameInput.setFocus();
    }, 1100);
     
    console.log('ionViewDidLoad RegisterPage');
  }
  goBack() {
    // this.events.publish('verifyMobileContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#282333');
    if(localStorage.getItem('modelRegistration')=='true') {
      this.viewCtrl.dismiss();
    } else {
      this.navCtrl.popToRoot({animate:false});
    }
  }
  
  /*
  ORIGINAL::MAULIK
  registerMember(form){
    console.log(form);
    return false;
    this.submitAttempt = true;
    
    setTimeout(() => {
      this.registerMobileShowContent=false;
    }, 100);
    this.navCtrl.push('RegisterStepTwoPage',{'phoneNumber': this.phoneNumber,'email': form.email },{ animate:true, duration:900, animation: 'ios-transition'});
    setTimeout(() => {
      this.registerMobileShowContent=true;
    }, 1500); 
  }
  ORIGINAL::MAULIK
  */

  registerMember(form){ 
    this.submitAttempt = true;
    
    if(this.registerForm.valid){
      //console.log("Valid Form");
      //console.log(form);
      //return false;
      let data = {
        email:form.email,
        first_name:form.first_name,
        last_name:form.last_name,
        home:"ecu",
        phone: this.phoneNumber
      }
      this.checkEmailExist(data);
      /* setTimeout(() => {
        this.registerMobileShowContent=false;
      }, 100);
      this.navCtrl.push('SteptwoprofilePage',{'data':data},{ animate:true, duration:900, animation: 'ios-transition'});
      setTimeout(() => {
        this.registerMobileShowContent=true;
      }, 1500); */
      
    } else {
      
    }
    
  }
}
