import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, PopoverController, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { SelectedEventActivitiesProvider } from '../../providers/selected-event-activities/selected-event-activities';
import { NewMessageMenuComponent } from '../../components/new-message-menu/new-message-menu';

/**
 * Generated class for the ActivityDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activity-detail',
  templateUrl: 'activity-detail.html',
})
export class ActivityDetailPage {

  activityDetail:any;
  questionFirstObj:any = [];
  questionSecondObj:any = [];
  footerButtonShow:boolean=false;
  footerButtonDisabled:boolean=false;
  footerButtonTestboolean=false;
  footerButtonTest:string='';
  user:any;
  eventName='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private alertCtrl: AlertController, private selectedEventActivitiesProvider: SelectedEventActivitiesProvider, public popoverCtrl: PopoverController, public events: Events) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.activityDetail = navParams.get('activity');
    this.eventName=localStorage.getItem('selectedEventName');
    this.sendrequest.getResult('user/activityDetail/'+this.activityDetail.id+'/' + this.user.id,'get',{ }).then((response:any) => { 
		this.activityDetail = response.data.ActivityData;
		this.questionFirstObj = response.data.questionFirstObj;
		this.questionSecondObj = response.data.questionSecondObj;
		this.footerButtonShow=response.data.footerButtonShow;
		this.footerButtonDisabled=response.data.footerButtonDisabled;
		this.footerButtonTest=response.data.footerButtonTest;
    },
    error => {
      
    })
  }
  openMenuPopup(myEvent, activity) {
    let index=0;
    var popover = this.popoverCtrl.create(NewMessageMenuComponent, {  'activity' : activity, 'index':index }, { cssClass: 'new-message-secondary-menu' });
    popover.present({
        ev: myEvent 
    });
    popover.onDidDismiss((data:any) => {
      console.log(data);
      if(data != null) {
        this.confirmDeleteMessagepopup(data.message, data.index);
        
      }
    });
  }
  confirmDeleteMessagepopup(activity,index) {
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteActivity(activity,index);
          }
        }
      ]
    });
    alert.present();
  }
  deleteActivity(activity, index) {
      console.log(activity);
      
      this.sendrequest.getResult('user/deleteUserActivity','post',{ 'event_id': activity.event_id, 'activity_id' : activity.id }).then((response:any) => { 
        this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
      },
      error => {
      })
      this.goBack();
  }
  goToEvaluation() {
  	this.navCtrl.push('ActivityEvaluationPage',{ 'activity':this.activityDetail, 'questionFirstObj': this.questionFirstObj,'questionSecondObj':this.questionSecondObj, 'footerButtonShow': this.footerButtonShow,'footerButtonDisabled': this.footerButtonDisabled, 'footerButtonTest': this.footerButtonTest } ,{animate:true, animation: 'ios-transition'});
  }
  showConfirmationPopup() {
  	let alert = this.alertCtrl.create({
      title: 'Are you sure to delete this activity?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.deleteEventActivity();
          }
        }
      ]
    });
    alert.present();
  }
  goBack() {
    this.events.publish('selectedEventContent:created', 'user', Date.now());
    this.footerButtonShow=false;
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  
  deleteEventActivity() {
    this.sendrequest.getResult('user/deleteactivity/'+this.activityDetail.id+'/' + this.user.id,'get',{ }).then((response:any) => { 
		this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    },
    error => {
      
    });
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  } 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivityDetailPage');
  }

}
