import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivityDetailPage } from './activity-detail';
import { ComponentsModule }from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    ActivityDetailPage,
  ],
  imports: [
  	DirectivesModule,
  	ComponentsModule,
    IonicPageModule.forChild(ActivityDetailPage),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class ActivityDetailPageModule {}
