import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewSignUpWebPage } from './new-sign-up-web';

@NgModule({
  declarations: [
    NewSignUpWebPage,
  ],
  imports: [
    IonicPageModule.forChild(NewSignUpWebPage),
  ],
})
export class NewSignUpWebPageModule {}
