import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
declare var google: any;

/**
 * Generated class for the NewSignUpWebPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-sign-up-web',
  templateUrl: 'new-sign-up-web.html',
})
export class NewSignUpWebPage {

  @ViewChild('Address') Address;
  user:any ={}; // $localStorage.user;
  voterValidationNotConfirm =false;
  voterValidationConfirm=false;
  empowrdcheckbox=0;
  disableButton=true;
  voter_validation = 0;
  address:string='';
  unit:string='';
  party:string='';
  name:string='';
  lastname:string='';
  email:string='';
  number:string='';
  privacycheckbox =false;
  showvalidatebutton=false;
  validateVoterValidation=0;
  insert=false;
  edit=false;
  address_type='';
  partnerID='';
  date='';
  month='';
  addressObj:any;
  jurisdictions:any
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public sendrequest: SendrequestProvider) {
  	this.user = JSON.parse(localStorage.getItem('user'));
    console.log(navParams.get('jurisdictions'));
    if(navParams.get('jurisdictions')!=undefined) {
      this.edit=true;
      this.jurisdictions = navParams.get('jurisdictions');
      this.address = this.jurisdictions.address;
      this.addressObj = this.jurisdictions.address;
      this.unit = this.jurisdictions.unit;
      this.party = this.jurisdictions.party;
      this.name = this.jurisdictions.fullname;
      this.lastname = this.jurisdictions.lastname;
      this.email = this.jurisdictions.email;
      this.number = this.jurisdictions.phone_number;  
    } else {
      this.insert=true;

    }
  }
  dismiss(params) {
	this.viewCtrl.dismiss(params);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad NewSignUpWebPage');
  }
  ionViewWillEnter() {
     // Google Places API auto complete
    let input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];
    let autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        // retrieve the place object for your use
        let place = autocomplete.getPlace();
        this.addressObj =place;
        let state = '';
        for(var i=0; i < place.address_components.length; i++) {
          if(place.address_components[i].types[0] =='administrative_area_level_1') {
            state = place.address_components[i].short_name;
          }
        }
    });
  }
  privacyChange(){
    if(this.privacycheckbox ==true){
        this.disableButton=false;
    } else{
        this.disableButton=true;
    }
  }
  checkVoterValidation() {
   this.sendrequest.getResult('user/checkvotervalidation/' + this.user.id,'post',{
        address: this.address,
        unit:this.unit,
        party:this.party,
        fullname:this.name,
        lastname:this.lastname,
        email:this.email,
        phone_number:this.number,
        addressId:localStorage.getItem('addressId')
    }).then((response:any) => { 
      	this.showvalidatebutton=false;
        if(response.data.record ==0){
            this.voterValidationNotConfirm =true;
            this.voterValidationConfirm=false;
            this.validateVoterValidation=0;
        } else {
            this.voterValidationConfirm=true;
            this.voterValidationNotConfirm =false;
            this.validateVoterValidation=1;
        }
    },
    error => {
      
    });
    
  }
  cancelregister() {   
    this.sendrequest.getResult('user/newSignup/' + this.user.id,'post',{
      address: this.addressObj,
      unit:this.unit,
      party:this.party,
      fullname:this.name,
      lastname:this.lastname,
      email:this.email,
      phone_number:this.number,
      id:'',
      addressId:localStorage.getItem('addressId'),
      validate_address:this.validateVoterValidation,
      privacy:this.privacycheckbox,
      empowrd_about:this.empowrdcheckbox,
      send_email:'1',
      address_type:'n'
    }).then((response:any) => {
    });   
   this.dismiss({});
  }
  registered() {
    this.sendrequest.getResult('user/newSignup/' + this.user.id,'post',{
      address: this.addressObj,
      unit:this.unit,
      party:this.party,
      fullname:this.name,
      lastname:this.lastname,
      email:this.email,
      phone_number:this.number,
      id:'',
      addressId:localStorage.getItem('addressId'),
      validate_address:this.validateVoterValidation,
      privacy:this.privacycheckbox,
      empowrd_about:this.empowrdcheckbox,
      send_email:'1',
     // partnerId:this.partnerID,
      address_type:'r'
    }).then((response:any) => {
    });  
    this.dismiss({});
  }
  newSignup() {
    if(this.insert==false) {
      this.sendrequest.getResult('user/newSignup/' + this.user.id,'post',{
        address: this.addressObj,
        unit:this.unit,
        party:this.party,
        fullname:this.name,
        lastname:this.lastname,
        email:this.email,
        phone_number:this.number,
        id:'',
        addressId:localStorage.getItem('addressId'),
        validate_address:this.validateVoterValidation,
        privacy:this.privacycheckbox,
        empowrd_about:this.empowrdcheckbox,
        address_type:this.address_type
      }).then((response:any) => {
      });  
    } else {
      this.sendrequest.getResult('user/newSignup/' + this.user.id,'post',{
        address: this.addressObj,
        unit:this.unit,
        party:this.party,
        fullname:this.name,
        lastname:this.lastname,
        email:this.email,
        phone_number:this.number,
        id: this.jurisdictions.id,
        addressId:localStorage.getItem('addressId'),
        validate_address:this.validateVoterValidation,
        privacy:this.privacycheckbox,
        empowrd_about:this.empowrdcheckbox,
        send_email:'1',
   //     partnerId:this.partnerID,
        address_type:'v'
      }).then(function(response) {
         
      });
    }
    this.dismiss({});
  }
  cancelInEdit() {
    this.dismiss({});
  }
  
  delete() {
   this.sendrequest.getResult('user/newSignupDelete/' + this.user.id,'post',{
        id:this.jurisdictions.id
    }).then((response:any) => { 

    });
    this.dismiss({});
  }
  
  /*
  
  checkVoterValidation(){
    API.all("user/checkvotervalidation/"+this.user.id).post({
        address: this.address,
        unit:this.unit,
        party:this.party,
        fullname:this.name,
        lastname:this.lastname,
        email:this.email,
        palStorage.editNewSignup,
        addressId:localStorage.getItem('addressId'),
        validate_address:this.validateVoterValidation,
        privacy:this.privacycheckbox,
        empowrd_about:this.empowrdcheckbox,
        send_email:'1',
        partnerId:this.partnerID,
        address_type:'n'
    }).then(function(response) {
        $localStorage.editNewSignup = '';
        $state.go('app.canvastool');
    });
  }
   */

}
