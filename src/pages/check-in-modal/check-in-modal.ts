import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
declare var google: any;

/**
 * Generated class for the CheckInModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-check-in-modal',
  templateUrl: 'check-in-modal.html',
})
export class CheckInModalPage {

  checkedIn=false;
  checkinTime=0;
  latitude='';
  longitude='';
  formatted_address='';
  turnOffLocation=true;
  selectedEvent:any={};
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public sendrequest: SendrequestProvider, public viewCtrl: ViewController) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.selectedEvent = this.navParams.get('selectedevent');
  }
  checkIn() {
  	this.sendrequest.getResult('user/eventAddressCheckIn/' + this.user.id,'post',{ 'event_id': this.selectedEvent.id, 'address': this.formatted_address, 'latitude': this.latitude, 'longitude': this.longitude }).then((response:any) => { 
  		this.checkedIn = response.data.status;
  		this.checkinTime++;
    },
    error => {
      
    })
  }
  dismiss() {
   	let data = { 'checkedin': this.checkedIn };
	this.viewCtrl.dismiss(data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckInModalPage');
    this.getCurrentAddress();

  }
  getCurrentAddress() {
  	this.geolocation.getCurrentPosition().then((response) => {
      var geocoder = new google.maps.Geocoder();
      	this.latitude= response.coords.latitude.toString();
      	this.longitude= response.coords.longitude.toString();
       
        var latlng = new google.maps.LatLng(response.coords.latitude, response.coords.longitude);
        var request = {
            latLng: latlng
        };
        geocoder.geocode(
	        {'latLng': latlng}, 
	         (results, status) => {
	         	console.log(results);
	            if (status == google.maps.GeocoderStatus.OK) {
                  	this.turnOffLocation=false;
                    this.formatted_address=results[0].formatted_address;
	            }
	            else {
	            	this.turnOffLocation=true;
	            	this.sendrequest.presentToast("Location can not getting. Please try again.")
	            }

	           
	        }
	        
	    );

    });
  }

}
