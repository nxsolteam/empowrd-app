import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckInModalPage } from './check-in-modal';

@NgModule({
  declarations: [
    CheckInModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckInModalPage),
  ],
})
export class CheckInModalPageModule {}
