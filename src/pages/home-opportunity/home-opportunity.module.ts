import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeOpportunityPage } from './home-opportunity';
import { ComponentsModule }from '../../components/components.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    HomeOpportunityPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeOpportunityPage),
    ComponentsModule,
    DirectivesModule,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class HomeOpportunityPageModule {}
