import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Events, Content, Select, AlertController, Platform } from 'ionic-angular';
import { Map, tileLayer, marker, geoJSON, LayerOptions, icon, circleMarker } from 'leaflet';
//import leafletImage from 'leaflet-image';

import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { Slides } from 'ionic-angular';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header'
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { PlaceholderProvider } from '../../providers/placeholder/placeholder';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';
import domtoimage from 'dom-to-image';
import { SocialSharing } from '@ionic-native/social-sharing';
import html2canvas from 'html2canvas';
declare var google: any;
/**
 * Generated class for the HomeOpportunityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({  
  segment: 'home-opportunity/:card'
})
@Component({
  selector: 'page-home-opportunity',
  templateUrl: 'home-opportunity.html',
})
export class HomeOpportunityPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  @ViewChild('electionDrownDown') electionDrownDown: Select;

  map: any;
  messageCount='0';
  loadPage=false;
  latLons:any=[];
  maillatLons:any=[];
  spaceBetween:any;
  virtualTranslate:any;
  options:any;
  sliderLength=2;
  PrimarylatLons:any=[];
  @ViewChild('mainSlider') mainCarousel: Slides;
  @ViewChild('mainOneSlider') mainOneCarousel: Slides;
  showLoader=true;
  slideSecondMapElement:any=[];
  slideFirstMap: any;
  slideFiveMap: any;
  slideSecondMap: any=[];
  slideThirdMap: any=[];
  slideSixMap: any=[];
  slideFourMap: any=[];
  newMarker:any;
  shortAddress='';
  addressOnHeader='';
  votingaddressdate='';
  mailInvotingaddressdate='';
  electionDateType='';
  mailInelectionDateType='';
  activeIndexOfSlide=0;//MAULIK
  scrallDirection='';//MAULIK
  user:any;
  validateAddress=false;
  showLobbyCard=true;
  forceHideCard=true;
  showLoaderCard=true;
  validAddressIcon="assets/img/group-5.svg";
  eventData:any = [];
  votingLocationData:any = [];
  mailInVotingLocationData:any = [];
  primaryLocationData:any = [];
  allElectionsCards:any = [];
  localLocationData:any = [];
  generalLocationData:any = [];
  specialLocationData:any = [];
  primarySelectedData:any = {};
  generalSelectedData:any = {};
  localSelectedData:any = {};
  specialSelectedData:any = {};
  voting:any = {};
  mailInVoting:any = {};

  primarySelectedIndex=0;
  votingSelectedIndex=0;
  mailInVotingSelectedIndex=0;
  specialSelectedIndex=0;
  generalSelectedIndex=0;
  localSelectedIndex=0;
  firstmoredays=0;
  earlyVotingDays='';
  earlyPrimaryDays='';
  earlyGeneralDays='';
  earlyLocalDays='';
  specialElectionDays='';
  generalElectionDate='';
  localElectionDate='';
  primaryElectionDate='';
  specialElectionDate='';
  showDropDown = false;
  lobbyCardMoreDaysText='';
  lobbyMessageCount='0';
  lobbyNewMessageCount='0';
  lobbyCardState='';
  lobbyCardDate='';
  nonVerifyUrl='';
  hideFirstSlide=false;
  earlyVotingLocationEnable=false;
  earlyMailInVotingLocationEnable=false;
  earlyPrimaryEnable=true;
  earlySpecialEnable=true;
  earlyGeneralEnable=true;
  earlyLocalEnable=true;
  firstSlideText='';
  firstSlideTitle='CHECK YOUR VOTER REGISTRATION STATUS';
  powerScore='0';
  earlyVotingLocationDate='';
  earlyMailInVotingLocationDate='';
  votingOpportunity = '0';
  suportOpportunity = '0';
  empowrdUnivesity= '0';
  messages = '0';
  initialName = '';
  messageTitle = '';
  messageText = '';
  navMenu='support';
  //navMenu='getEngaged';
  empowrdUnivesityMessageData:any=[];
  text='';
  problem='';
  cardCount=0;
  getNewMailByTypes:any=[];
  getNewMailListOrg:any=[];
  currentDay:any;
  currentDate:any;
  engagedNewMessage=0;
  primaryElectionData:any =[];
  specialElectionData:any =[];
  generalElectionData:any =[];
  localElectionData:any =[];
  firstmoredaysText='';
  earlyVotingDaysText='';
  earlyMailInVotingDaysText='';
  loadedOrgMessageSlider=false;

  congrationalCardMoreDaysText='';
  congrationalCardDate='';
  congressionalTextTitle='';
  congressionalText='';
  lobbyTextTitle='';
  lobbyText='';
  showCardLoader=false;
  eventCard=false;
  showCongrationalCard=false;
  showCongrassionCardSpinner=false;
  showLobbyCardSpinner=false;
  showVoterCardSpinner=false;
  showEarlyVotingCardSpinner=false;
  showEarlyMailInVotingCardSpinner=false;
  cardResponseData:any={};
  loadSpecialElectionMap=false;
  loadPrimaryElectionMap=false;
  loadGeneralElectionMap=false;
  loadLocalElectionMap=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public events: Events, private iab: InAppBrowser, private placeholderProvider: PlaceholderProvider, private launchNavigator: LaunchNavigator, private alertCtrl: AlertController, private el:ElementRef, private statusBar: StatusBar, public platform: Platform, private sanitizer: DomSanitizer, private socialSharing: SocialSharing) {
    //console.log("card values ", navParams.get('card'));
   this.statusBar.overlaysWebView(false);
    if(this.platform.is('android')) {
      this.statusBar.backgroundColorByHexString('#282333');
    } else {
      this.statusBar.backgroundColorByHexString('#F9F0E0');
    }
    this.user =  JSON.parse(localStorage.getItem('user'));
    if(this.user==null) {
      localStorage.setItem('modelRegistration','true');
      //this.navCtrl.setRoot('MobileNumberVerificationPage',{},{animate:false});
      /*
      let registerModal = this.modalCtrl.create('MobileNumberVerificationPage', {  });
      registerModal.onDidDismiss((data:any) => {
        console.log(data);
        if(data != null && data != '') {

        } else {
          this.navCtrl.setRoot('LandingPage');
        }
      });
      registerModal.present();*/
    } else {
      this.messageCount = localStorage.getItem('messageCount');
      this.currentDay = moment().format("dddd");
      this.currentDate = moment().format("MMM. DD, YYYY");

      events.subscribe('countNewMessage:created', (user, time) => {
        this.suportOpportunity = localStorage.getItem('suportOpportunity');
        this.messages = localStorage.getItem('messages');
        this.empowrdUnivesity = localStorage.getItem('empowrdUnivesity');
        this.votingOpportunity = localStorage.getItem('votinDateCount');
        this.engagedNewMessage = 0;
        for(var i=0; i< this.getNewMailByTypes.length; i++) {
          this.engagedNewMessage += this.getNewMailByTypes[i].new_message_count;
          if(this.getNewMailByTypes[i].message_type_inbox=='LOBBY'){
            this.lobbyNewMessageCount=this.getNewMailByTypes[i].new_message_count.toString();
            this.lobbyMessageCount=this.getNewMailByTypes[i].message_count.toString();
          }
        }
        
      });    
     
      if(localStorage.getItem('votinDateCount') != undefined) {
       this.votingOpportunity = localStorage.getItem('votinDateCount');
      }
      if(localStorage.getItem('suportOpportunity') != undefined) {
        this.suportOpportunity = localStorage.getItem('suportOpportunity');
      }
      if(localStorage.getItem('empowrdUnivesity') != undefined) {
        this.empowrdUnivesity = localStorage.getItem('empowrdUnivesity');
      }
      if(localStorage.getItem('messages') != undefined) {
        this.messages = localStorage.getItem('messages');
      }
      let totalpowerScore = parseInt(localStorage.getItem('powerScore'));
      if(totalpowerScore > 999) {
        this.powerScore = Math.floor(totalpowerScore/1000).toString()+"k+";
      } else {
        this.powerScore = localStorage.getItem('powerScore');
      }
      events.subscribe('setLatLon:created', (user, time) => {
        if(localStorage.getItem('validateAddress')!= undefined) {
          if(localStorage.getItem('validateAddress')=='0') {
            this.validateAddress=false;
            this.validAddressIcon="assets/img/group-5.svg";
          } else {
            this.validateAddress=true;
            this.validAddressIcon="assets/img/group-5.svg";
          }
        }
        this.lobbyCardState=localStorage.getItem('stateShortName');
        if(localStorage.getItem('addresstext')!= undefined) {
        console.log('addresstext', localStorage.getItem('addresstext'));
          this.addressOnHeader = localStorage.getItem('addresstext');
          this.shortAddress = this.addressOnHeader.substring(0, 22);
        }
        this.earlyVotingLocationEnable=false;
        this.sliderLength=2;
        this.loadSlidesMap();
        
      });
      if(localStorage.getItem('validateAddress')!= undefined) {
        if(localStorage.getItem('validateAddress')=='0') {
          this.validateAddress=false;
        } else {
          this.validateAddress=true;
        }
      }
      if(localStorage.getItem('addresstext')!= undefined) {
        this.addressOnHeader = localStorage.getItem('addresstext');
        this.shortAddress = this.addressOnHeader.substring(0, 22);
        this.lobbyCardState=localStorage.getItem('stateShortName');
      }
      this.initialName = this.user.first_name[0]+""+this.user.last_name[0];
    }
  }
  removeEventCard(election, index) {
    this.allElectionsCards.splice(index, 1);
    this.sendrequest.getResult('user/eventAttend','post',{ 'event_id': election.id }).then((response:any) => { 
    },
    error => {
    })
  }
  async shareCard(id,text,cardObj) {
    
    
    var type='none';
    this.showCardLoader=true;
    function filter (node) {
     // return true;
      return (node.id !== type);
    }
    var node = document.getElementById(id);
   
    if(id=='congrationalCard') {
      this.showCongrassionCardSpinner=true;
    }
    else if(id=='lobbyCard') {
      this.showLobbyCardSpinner=true;
    }
    else if(id=='voterCard' || id=='voterCardNew') {
      this.showVoterCardSpinner=true;
    }
    else if(id=='earlyVoting') {
      type='slideFirstMapElement';
      this.showEarlyVotingCardSpinner=true;
    }
    else if(id=='earlyMailInVoting') {
      type='slideFiveMapElement';
      this.showEarlyMailInVotingCardSpinner=true;
    } else {
        var res = id.split("_");
        if(res[0]=='event') {
          type='none';
          this.eventCard=true;
        } else if(res[0]=='generalelection') {
          type='slideThirdMapElement_'+cardObj.id;
        } else if(res[0]=='localelection') {
          type='slideSixMapElement_'+cardObj.id;
        } else if(res[0]=='specialelection') {
          type='slideSecondMapElement_'+cardObj.id;
        } else if(res[0]=='primaryelection') {
          type='slideFourthMapElement_'+cardObj.id;
        }
      cardObj.showLoader=true;
    }
    if(type!='none') {
      var mapElement = document.getElementById(type); 
     await html2canvas(mapElement, { useCORS: true }).then(canvas => {
        var imgData = canvas.toDataURL("image/png");
        console.log(imgData);
        node.querySelector("#"+type+"_image").setAttribute('src', imgData);
        node.querySelector("#"+type+"_image").setAttribute('style', 'display:block');
        //.style.display = "block"; 
        node.querySelector("#"+type).setAttribute('style', 'display:none'); // .style.display = "none";
      });
    }
    setTimeout(() => {
      
      domtoimage.toPng(node, {filter: filter})
      .then((dataUrl:any) =>{
        if(type!='none') {
          node.querySelector("#"+type).setAttribute('style', "display:block;overflow: hidden;"); //.style.display = "block";
          node.querySelector("#"+type+"_image").setAttribute('style', 'display:none'); // .style.display = "none"; 
        }
        console.log(dataUrl);
        if(id=='congrationalCard') {
          this.showCongrassionCardSpinner=false;
        }
        else if(id=='lobbyCard') {
          this.showLobbyCardSpinner=false;
        }
        else if(id=='voterCard' || id=='voterCardNew') {
          this.showVoterCardSpinner=false;
        }
        else if(id=='earlyVoting') {
          this.showEarlyVotingCardSpinner=false;
        }
        else if(id=='earlyMailInVoting') {
          this.showEarlyMailInVotingCardSpinner=false;
        } else {
          cardObj.showLoader=false;
        }

        this.socialSharing.share(text, '', dataUrl, "https://empowrd.app/main-tab/tab-2/home-opportunity/"+id).then((result:any) => {
          this.eventCard=false;
          this.showCardLoader=false;
          this.sendrequest.buttonClicked(this.user.id,'Share Card '+id);
          this.sendrequest.updateCivicProfileData();
        },
        error => {
          this.eventCard=false;
          this.showCardLoader=false;
          console.log('share error ', error);
          //console.log(error.path[0].src);
        });
      })
      .catch((error) => {
        this.eventCard=false;
        this.showCardLoader=false;
        if(id=='congrationalCard') {
          this.showCongrassionCardSpinner=false;
        }
        else if(id=='lobbyCard') {
          this.showLobbyCardSpinner=false;
        }
        else if(id=='voterCard' || id=='voterCardNew') {
          this.showVoterCardSpinner=false;
        }
        else if(id=='earlyVoting') {
          this.showEarlyVotingCardSpinner=false;
        }
        else if(id=='earlyMailInVoting') {
          this.showEarlyMailInVotingCardSpinner=false;
        } else {
          cardObj.showLoader=false;
        }
          console.error('oodfgdgdfgdfps, something went wrong!', error);
          console.log('sssssss : ', error.path[0].src);
      });
    },50); 
  }
  getMessageType(type) {
      if(type=='ANNOUNCEMENT'){
        return 'BULLETINS';
      } else if(type=='MEMBERSHIP INVITE') {
        return 'INVITES';
      } else {
        return type;
      }
  }
  handleClick(event) {
    console.log(event);
    console.log(event.target.parentNode.tagName);
    let url='';
    if (event.target.tagName == "A") { 
      // url = event.target.innerText.trim();
      url = event.target.getAttribute("open-externalurl");

    } else if (event.target.parentNode.tagName == "A") { 
      url = event.target.parentNode.getAttribute("open-externalurl");

      //url = event.target.parentNode.innerText.trim();
    }
    console.log(url);
    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    if(url != '' && url != undefined) {
      this.iab.create(url,target,options); 
    }
  }
  backAddress(type) {
    let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
    if(type=='voting') {
      if(this.votingSelectedIndex!=0) {
        this.votingSelectedIndex--;
      }
      if(this.votingSelectedIndex < this.votingLocationData.length) {
        this.voting = this.votingLocationData[this.votingSelectedIndex];
        
        let earlylatVoting=this.voting.latitude;
        let earlylonVoting=this.voting.longitude;
        let mapOptionsVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatVoting, earlylonVoting), // New York
          styles: mapStyle
        };
        this.slideFirstMap = new google.maps.Map(document.getElementById('slideFirstMapElement'), mapOptionsVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatVoting, earlylonVoting),
            map: this.slideFirstMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    }
    else if(type=='mailInVoting') {
      if(this.mailInVotingSelectedIndex!=0) {
        this.mailInVotingSelectedIndex--;
      }
      if(this.mailInVotingSelectedIndex < this.mailInVotingLocationData.length) {
        this.mailInVoting = this.mailInVotingLocationData[this.mailInVotingSelectedIndex];
        
        let earlylatmailVoting=this.mailInVoting.latitude;
        let earlylonmailVoting=this.mailInVoting.longitude;
        let mapOptionsmailVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting), // New York
          styles: mapStyle
        };
        this.slideFiveMap = new google.maps.Map(document.getElementById('slideFiveMapElement'), mapOptionsmailVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting),
            map: this.slideFiveMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    }
  }
  nextAddress(type) {
    let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
    if(type=='voting') {
      this.votingSelectedIndex++;
      if(this.votingSelectedIndex < this.votingLocationData.length) {
        this.voting = this.votingLocationData[this.votingSelectedIndex];
        
        let earlylatVoting=this.voting.latitude;
        let earlylonVoting=this.voting.longitude;
        let mapOptionsVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatVoting, earlylonVoting), // New York
          styles: mapStyle
        };
        this.slideFirstMap = new google.maps.Map(document.getElementById('slideFirstMapElement'), mapOptionsVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatVoting, earlylonVoting),
            map: this.slideFirstMap,
            icon:'assets/img/early_vote_3.svg',
          });
        }
      }
    } else if(type=='mailInVoting') {
      this.mailInVotingSelectedIndex++;
      if(this.mailInVotingSelectedIndex < this.mailInVotingLocationData.length) {
        this.mailInVoting = this.mailInVotingLocationData[this.mailInVotingSelectedIndex];
        
        let earlylatmailVoting=this.mailInVoting.latitude;
        let earlylonmailVoting=this.mailInVoting.longitude;
        let mapOptionsmailVoting = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting), // New York
          styles: mapStyle
        };
        this.slideFiveMap = new google.maps.Map(document.getElementById('slideFiveMapElement'), mapOptionsmailVoting);
        if(this.latLons.length > 0) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting),
            map: this.slideFiveMap,
            icon:'assets/img/early_vote_4.svg',
          });
        }
      }
    } else if(type=='primary') {
      this.primarySelectedIndex++;
      if(this.primarySelectedIndex < this.primaryLocationData.length) {
        this.primarySelectedData = this.primaryLocationData[this.primarySelectedIndex];
      }
      let primarylatVoting=this.primarySelectedData.latitude;
      let primarylonVoting=this.primarySelectedData.longitude;
      
      let primaryEleMapOptions = {
        zoom: 16,
        gestureHandling: 'none',
        zoomControl: false,
        fullscreenControl: false,
        mapTypeControl: false, 
        center: new google.maps.LatLng(primarylatVoting, primarylonVoting), // New York
        styles: mapStyle
      };
      this.slideFourMap = [];
      for(var j=0; j < this.primaryElectionData.length; j++) {
        this.slideFourMap[j] = new google.maps.Map(document.getElementById('slideFourthMapElement_'+this.primaryElectionData[j].id), primaryEleMapOptions);
      }
      for(var i=0; i< this.PrimarylatLons.length; i++) {
                  
        
        for(var k=0; k < this.slideFourMap.length; k++) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(this.PrimarylatLons[i].latitude, this.PrimarylatLons[i].longitude),
            map: this.slideFourMap[k],
            icon:'assets/img/early_vote_4.svg',
          });
        } 
      }

    } else if(type=='special') {
      this.specialSelectedIndex++;
      if(this.specialSelectedIndex < this.specialLocationData.length) {
        this.specialSelectedData = this.specialLocationData[this.specialSelectedIndex];
        let speciallatVoting=this.specialSelectedData.latitude;
        let speciallonVoting=this.specialSelectedData.longitude;
        let specialEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(speciallatVoting, speciallonVoting), // New York
          styles: mapStyle
        };
        this.slideSecondMap=[];
        for(var j=0; j < this.specialElectionData.length; j++) {
          this.slideSecondMap[j] = new google.maps.Map(document.getElementById('slideSecondMapElement_'+this.specialElectionData[j].id), specialEleMapOptions);
        }
        
        for(var k=0; k < this.slideSecondMap.length; k++) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(speciallatVoting, speciallonVoting),
            map: this.slideSecondMap[k],
            icon:'assets/img/early_vote_4.svg',
          });
        }
      }
    } else if(type=='local') {
      this.localSelectedIndex++;
      if(this.localSelectedIndex < this.localLocationData.length) {
        this.localSelectedData = this.localLocationData[this.localSelectedIndex];
        let locallatVoting=this.localSelectedData.latitude;
        let locallonVoting=this.localSelectedData.longitude;
        let localEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(locallatVoting, locallonVoting), // New York
          styles: mapStyle
        };
        this.slideSixMap=[];
        for(var j=0; j < this.localElectionData.length; j++) {
          this.slideSixMap[j] = new google.maps.Map(document.getElementById('slideSixMapElement_'+this.localElectionData[j].id), localEleMapOptions);
        }
        
        for(var k=0; k < this.slideSixMap.length; k++) {
          var markerTwo = new google.maps.Marker({
            position: new google.maps.LatLng(locallatVoting, locallonVoting),
            map: this.slideSixMap[k],
            icon:'assets/img/early_vote_4.svg',
          });
        }
      }
    } else {
      this.generalSelectedIndex++;
      if(this.generalSelectedIndex < this.generalLocationData.length) {
        this.generalSelectedData = this.generalLocationData[this.generalSelectedIndex];
      }
      let generallatVoting=this.generalSelectedData.latitude;
      let generallonVoting=this.generalSelectedData.longitude;

      let generalEleMapOptions = {
        zoom: 16,
        gestureHandling: 'none',
        zoomControl: false,
        fullscreenControl: false,
        mapTypeControl: false, 
        center: new google.maps.LatLng(generallatVoting, generallonVoting), // New York
        styles: mapStyle
      };
      this.slideThirdMap=[];
      for(var j=0; j < this.generalElectionData.length; j++) {
        this.slideThirdMap[j] = new google.maps.Map(document.getElementById('slideThirdMapElement_'+this.generalElectionData[j].id), generalEleMapOptions);
      }
      for(var k=0; k < this.slideThirdMap.length; k++) {
        var markerTwo = new google.maps.Marker({
          position: new google.maps.LatLng(generallatVoting, generallonVoting),
          map: this.slideThirdMap[k],
          icon:'assets/img/early_vote_4.svg',
        });
      }
    }
  }
  
  segmentChanged(event) {

  }
  editAddress() {
    this.events.publish('gotoPage:created', 'AddEditAddressPage', {});
  }
  sendCommunity() {
    this.sendrequest.getResult('user/sendhelp/' + this.user.id,'post',{ 'problem': this.problem, 'text': this.text, 'user': this.user }).then((response:any) => { 

    },
    error => {
      
    })
    this.problem='';
    this.text='';
    this.sendrequest.presentToast('Thank you for given feedback.');
  }
  ionViewDidEnter() {
    console.log("ionViewDidEnter New Home Page");
    if(
    (this.loadSpecialElectionMap==false || this.loadPrimaryElectionMap==false || this.loadGeneralElectionMap==false || this.loadLocalElectionMap==false) && this.cardResponseData.data !=undefined) {
      this.loadElectionCardMap();
    }
    //this.events.publish('homeOpportunityScreen:created', 'true', Date.now());
  }
  slideChanged(slides)
  {
    this.activeIndexOfSlide=this.mainCarousel._activeIndex;
    this.scrallDirection=this.mainCarousel.swipeDirection;
  }
  public getSafehtml(html:string){
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  viewBallotDetails(date, type) {
    date =  moment(date, "MM.DD.YYYY").format('YYYY-MM-DD');
    this.events.publish('gotoPage:created', 'ElectionsPage',  { 'date': date, 'type': type, 'menuType' : 'UPCOMING RACES'});

    //this.events.publish('selectedTabs:data','4', { 'date': date, 'type': type, 'menuType' : 'UPCOMING RACES'});
    setTimeout(() => {
      this.events.publish('changeElectionNavTab:created','UPCOMING RACES',{ 'date': date, 'type': type});
    },400);
  }
  confirmEvent(event) {
    // this.navCtrl.push('RegistereventPage',{'confirmationEventsId':event.id, 'event':event },{animate:true, animation: 'ios-transition'});
    this.events.publish('gotoPage:created', 'RegistereventPage', {'confirmationEventsId':event.id, 'event':event});
  }
  gotoEvent(event) {
    // this.navCtrl.push('SelectedEventsPage',{'event':event },{animate:true, animation: 'ios-transition'});
    this.events.publish('gotoPage:created', 'SelectedEventsPage', {'event':event });
  }
  getCardId(election) {
    if(election.card_type=='primary' || election.card_type=='local' || election.card_type=='general' || election.card_type=='special') {
      return election.card_type+"election_"+election.id;
    }
    else if(election.card_type=='event') {
      return election.card_type+"_"+election.id;
    } else {
      return election.card_type;
    }
    /* 
    else if(election.card_type=='earlyVoting') {
      return election.card_type;
    }
    else if(election.card_type=='earlyMailInVoting') {
      return election.card_type;
    }
    else if(election.card_type=='lobbyCard') {
      return election.card_type;
    } */ 
  }
  loadSlidesMap() {
    if(localStorage.getItem('latitude') != undefined && localStorage.getItem('latitude') !==null) {
      this.showLoader=true;
      let lat= localStorage.getItem('latitude') // '33.6830929';
      let lon= localStorage.getItem('longitude') // '-84.1405661';
      let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
      /* Voting Location Start */
      
      let latVoting = localStorage.getItem('latitude'); // latitude
      let lonVoting = localStorage.getItem('longitude'); // longitude
      //let latLng = new google.maps.LatLng(latVoting, lonVoting);
      this.sendrequest.getResult('office/getCardDetails/' + this.user.id,'post',{ 'address': this.addressOnHeader, 'latitude': localStorage.getItem('latitude'), 'longitude': localStorage.getItem('longitude'),'state_short_name': localStorage.getItem('stateShortName') }).then((cardResponse:any) => {

        this.cardResponseData=cardResponse;
        this.eventData=this.cardResponseData.data.eventData;

        console.log("response : ",cardResponse);
        this.showLoader=false;
        let sliderLength=0;
        this.allElectionsCards = cardResponse.data.allElectionsCards;
        this.specialElectionData = cardResponse.data.specialElectionData;
        this.generalElectionData = cardResponse.data.generalElectionData;
        this.localElectionData = cardResponse.data.localElectionData;
        console.log("this.generalElectionData : ",this.generalElectionData);
        this.primaryElectionData = cardResponse.data.primaryElectionData;
        this.hideFirstSlide = cardResponse.data.hideFirstSlide;
        this.showLobbyCard = cardResponse.data.showLobbyCard;
        this.showCongrationalCard = cardResponse.data.showCongrationalCard;
        this.earlyVotingLocationEnable = cardResponse.data.earlyVotingLocationEnable;
        this.earlyMailInVotingLocationEnable = cardResponse.data.earlyMailInVotingLocationEnable;
        
        sliderLength = (this.hideFirstSlide==false)?sliderLength+1:sliderLength;
        sliderLength = (this.earlyVotingLocationEnable==true)?sliderLength+1:sliderLength;
        sliderLength = (this.earlyMailInVotingLocationEnable==true)?sliderLength+1:sliderLength;
        sliderLength = (this.showLobbyCard==true)?sliderLength+1:sliderLength;
        sliderLength = (this.showCongrationalCard==true)?sliderLength+1:sliderLength;
        sliderLength = sliderLength+this.eventData.length;
        sliderLength = sliderLength+this.primaryElectionData.length;
        sliderLength = sliderLength+this.specialElectionData.length;
        sliderLength = sliderLength+this.generalElectionData.length;
        sliderLength = sliderLength+this.localElectionData.length;
        this.sliderLength = sliderLength;
        console.log(" this.sliderLength  in api ", this.sliderLength);
        this.cardCount = cardResponse.data.cardCount;
        this.votingLocationData = cardResponse.data.votingLocationData;
        this.mailInVotingLocationData = cardResponse.data.mailInVotingLocationData;
        this.primaryLocationData = cardResponse.data.primaryLocationData;
        this.generalLocationData = cardResponse.data.primaryLocationData;
        this.localLocationData = cardResponse.data.localElectionLocationData;
        this.specialLocationData = cardResponse.data.primaryLocationData;
        console.log("this.generalLocationData : ",this.generalLocationData);
        this.congrationalCardMoreDaysText = cardResponse.data.congrationalCardMoreDaysText;
        this.congrationalCardDate = cardResponse.data.congrationalCardDate;
        this.congressionalTextTitle = cardResponse.data.congressionalTextTitle;
        this.lobbyTextTitle = cardResponse.data.lobbyTextTitle;
        this.lobbyText = cardResponse.data.lobbyText;
        this.congressionalText = cardResponse.data.congressionalText;
        this.firstmoredays = cardResponse.data.firstmoredays; 
        this.firstmoredaysText = cardResponse.data.firstmoredaysText; 
        this.lobbyCardMoreDaysText = cardResponse.data.lobbyCardMoreDaysText; 
        this.lobbyCardDate = cardResponse.data.lobbyCardDate; 
        this.earlyVotingLocationDate = cardResponse.data.earlyVotingLocationDate;
        this.earlyMailInVotingLocationDate = cardResponse.data.earlyMailInVotingLocationDate;
       
        this.firstSlideText= cardResponse.data.firstSlideText;
        this.firstSlideTitle= cardResponse.data.firstSlideTitle;
        this.nonVerifyUrl = cardResponse.data.link;
        this.earlyVotingDays = cardResponse.data.earlyVotingDays;
        this.earlyVotingDaysText = cardResponse.data.earlyVotingDaysText;
        this.earlyMailInVotingDaysText = cardResponse.data.earlyMailInVotingDaysText;
        this.earlyPrimaryDays = cardResponse.data.earlyPrimaryDays;
        this.earlyGeneralDays = cardResponse.data.earlyGeneralDays;
        this.earlyLocalDays = cardResponse.data.earlyLocalDays;
        this.specialElectionDays = cardResponse.data.specialElectionDays;
        this.generalElectionDate = cardResponse.data.generalElectionDate;
        this.localElectionDate = cardResponse.data.localElectionDate;
        this.primaryElectionDate = cardResponse.data.primaryElectionDate;
        this.specialElectionDate = cardResponse.data.specialElectionDate;
        
        this.showLoaderCard = false;
        setTimeout(() => {
          this.forceHideCard=false;
          },1000);
        if(this.earlyVotingLocationEnable==true) {
          let earlylatVoting=latVoting;
          let earlylonVoting=lonVoting;
          if(cardResponse.data.latLons.length > 0) {
            earlylatVoting=cardResponse.data.latLons[0].latitude;
            earlylonVoting=cardResponse.data.latLons[0].longitude;
          }
          let mapOptionsVoting = {
            zoom: 16,
            gestureHandling: 'none',
            zoomControl: false,
            fullscreenControl: false,
            mapTypeControl: false, 
            center: new google.maps.LatLng(earlylatVoting, earlylonVoting),
            styles: mapStyle
          };

          setTimeout(() => {
            this.slideFirstMap = new google.maps.Map(document.getElementById('slideFirstMapElement'), mapOptionsVoting);
            if(cardResponse.data.latLons.length > 0) {
              var markerTwo = new google.maps.Marker({
                position: new google.maps.LatLng(cardResponse.data.latLons[0].latitude, cardResponse.data.latLons[0].longitude),
                map: this.slideFirstMap,
                icon:'assets/img/early_vote_3.svg',
              });
            }
          },2000);
          
        }
        if(this.earlyMailInVotingLocationEnable==true) {
          let earlylatmailVoting=latVoting;
          let earlylonmailVoting=lonVoting;
          if(cardResponse.data.maillatLons.length > 0) {
            earlylatmailVoting=cardResponse.data.maillatLons[0].latitude;
            earlylonmailVoting=cardResponse.data.maillatLons[0].longitude;
          }
          let mapOptionsmailVoting = {
            zoom: 16,
            gestureHandling: 'none',
            zoomControl: false,
            fullscreenControl: false,
            mapTypeControl: false, 
            center: new google.maps.LatLng(earlylatmailVoting, earlylonmailVoting),
            styles: mapStyle
          };
          
          setTimeout(() => {
            this.slideFiveMap = new google.maps.Map(document.getElementById('slideFiveMapElement'), mapOptionsmailVoting);
            if(cardResponse.data.maillatLons.length > 0) {
              var markerTwo = new google.maps.Marker({
                position: new google.maps.LatLng(cardResponse.data.maillatLons[0].latitude, cardResponse.data.maillatLons[0].longitude),
                map: this.slideFiveMap,
                icon:'assets/img/early_vote_4.svg',
              });
            }
          },2000);
          
        }
        this.PrimarylatLons = cardResponse.data.PrimarylatLons;
        this.latLons = cardResponse.data.latLons;
        /* Voting Location End */
        
         
       
        /* Special Election Map End */
        
        
        if(this.votingLocationData.length > 0) {
          this.voting = this.votingLocationData[0];
          this.votingSelectedIndex=0;
        } else {
          this.voting={};
        }
        if(this.mailInVotingLocationData.length > 0) {
          this.mailInVoting = this.mailInVotingLocationData[0];
          this.mailInVotingSelectedIndex=0;
        } else {
          this.mailInVoting={};
        }
        this.loadSpecialElectionMap=false;
        this.loadPrimaryElectionMap=false;
        this.loadGeneralElectionMap=false;
        this.loadLocalElectionMap=false;
        this.loadElectionCardMap();
        this.votingaddressdate=cardResponse.data.votingaddressdate;
        this.mailInvotingaddressdate=cardResponse.data.mailInvotingaddressdate;
        this.electionDateType=cardResponse.data.electionDateType;
        this.mailInelectionDateType=cardResponse.data.mailInelectionDateType;
      },
      error => {
        
      })
      
    }
  }
  loadElectionCardMap() {
    let lat= localStorage.getItem('latitude');
    let lon= localStorage.getItem('longitude');
    let mapStyle =[{"featureType": "all","elementType": "all","stylers": [{"hue": "#ffaa00"},{"saturation": "-33"},{"lightness": "10"}]},{"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#9c5e18"}]},{"featureType": "landscape.natural.terrain","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "poi.park","elementType": "geometry.fill","stylers": [{"color": "#e0ebd4"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "road.highway","elementType": "labels.text","stylers": [{"visibility": "on"}]},{"featureType": "road.arterial","elementType": "geometry","stylers": [{"visibility": "simplified"}]},{"featureType": "transit.line","elementType": "all","stylers": [{"visibility": "off"}]},{"featureType": "water","elementType": "geometry.fill","stylers": [{"saturation": "-23"},{"gamma": "2.01"},{"color": "#f2f6f6"}]},{"featureType": "water","elementType": "geometry.stroke","stylers": [{"saturation": "-14"}]}];
    setTimeout(() => {
        /* Special Election Map */

        let speciallatVoting=lat;
        let speciallonVoting=lon;
        if(this.cardResponseData.data.PrimarylatLons.length > 0) {
          speciallatVoting=this.cardResponseData.data.PrimarylatLons[0].latitude;
          speciallonVoting=this.cardResponseData.data.PrimarylatLons[0].longitude;
        }

        let specialEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(speciallatVoting, speciallonVoting), // New York
          styles: mapStyle
        };
        let primarylatVoting=lat;
        let primarylonVoting=lon;
        if(this.cardResponseData.data.PrimarylatLons.length > 0) {
          primarylatVoting=this.cardResponseData.data.PrimarylatLons[0].latitude;
          primarylonVoting=this.cardResponseData.data.PrimarylatLons[0].longitude;
        }
        let primaryEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(primarylatVoting, primarylonVoting), // New York
          styles: mapStyle
        };
        let generallatVoting=lat;
        let generallonVoting=lon;
        
        if(this.cardResponseData.data.PrimarylatLons.length > 0) {
          generallatVoting=this.cardResponseData.data.PrimarylatLons[0].latitude;
          generallonVoting=this.cardResponseData.data.PrimarylatLons[0].longitude;
        }
        let generalEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(generallatVoting, generallonVoting), // New York
          styles: mapStyle
        };
        let locallatVoting=lat;
        let locallonVoting=lon;
        if(this.cardResponseData.data.PrimarylatLons.length > 0) {
          locallatVoting=this.cardResponseData.data.localElectionlatLons[0].latitude;
          locallonVoting=this.cardResponseData.data.localElectionlatLons[0].longitude;
        }
        let localEleMapOptions = {
          zoom: 16,
          gestureHandling: 'none',
          zoomControl: false,
          fullscreenControl: false,
          mapTypeControl: false, 
          center: new google.maps.LatLng(locallatVoting, locallonVoting), // New York
          styles: mapStyle
        };
        if(this.loadSpecialElectionMap==false) {
          if(this.specialElectionData.length==0) {
            this.loadSpecialElectionMap=true;
          }
          for(var j=0; j < this.specialElectionData.length; j++) {
            if(document.getElementById('slideSecondMapElement_'+this.specialElectionData[j].id)!=null) {
              this.loadSpecialElectionMap=true;
              this.slideSecondMap[j] = new google.maps.Map(document.getElementById('slideSecondMapElement_'+this.specialElectionData[j].id), specialEleMapOptions);
            } else {
              this.loadSpecialElectionMap=false;
            }
          } 
        }
        if(this.loadPrimaryElectionMap==false) {
          if(this.primaryElectionData.length==0) {
            this.loadPrimaryElectionMap=false;
          }
          for(var j=0; j < this.primaryElectionData.length; j++) {
            if(document.getElementById('slideFourthMapElement_'+this.primaryElectionData[j].id)!=null) {
              this.loadPrimaryElectionMap=true;
              this.slideFourMap[j] = new google.maps.Map(document.getElementById('slideFourthMapElement_'+this.primaryElectionData[j].id), primaryEleMapOptions);
            } else {
              this.loadPrimaryElectionMap=true;
            }
          }
        }
        console.log(" slideThirdMapElement : ");
        console.log(" this.loadGeneralElectionMap : ", this.loadGeneralElectionMap);
        if(this.loadGeneralElectionMap==false) {
          if(this.generalElectionData.length==0) {
            this.loadGeneralElectionMap=false;
          }
          for(var j=0; j < this.generalElectionData.length; j++) {
            if(document.getElementById('slideThirdMapElement_'+this.generalElectionData[j].id)!=null) {
              console.log(document.getElementById('slideThirdMapElement_'+this.generalElectionData[j].id));
              this.loadGeneralElectionMap=true;
              this.slideThirdMap[j] = new google.maps.Map(document.getElementById('slideThirdMapElement_'+this.generalElectionData[j].id), generalEleMapOptions);
            } else {
              this.loadGeneralElectionMap=true;
            }
          } 
        }
        if(this.loadLocalElectionMap==false) {
          if(this.localElectionData.length==0) {
            this.loadLocalElectionMap=false;
          }
          for(var j=0; j < this.localElectionData.length; j++) {
            if(document.getElementById('slideSixMapElement_'+this.localElectionData[j].id)!=null) {
              console.log(document.getElementById('slideSixMap_'+this.localElectionData[j].id));
              this.loadLocalElectionMap=true;
              this.slideSixMap[j] = new google.maps.Map(document.getElementById('slideSixMapElement_'+this.localElectionData[j].id), localEleMapOptions);
            } else {
              this.loadLocalElectionMap=true;
            }
          } 
        }
        console.log("this.slideThirdMap ggggggg : ", this.slideThirdMap);

         console.log("this.cardResponseData.data.PrimarylatLons : ", this.cardResponseData.data.PrimarylatLons);
        if(this.cardResponseData.data.PrimarylatLons.length > 0) {
          for(var k=0; k < this.slideThirdMap.length; k++) {
            var markerTwo = new google.maps.Marker({
              position: new google.maps.LatLng(this.cardResponseData.data.PrimarylatLons[0].latitude, this.cardResponseData.data.PrimarylatLons[0].longitude),
              map: this.slideThirdMap[k],
              icon:'assets/img/early_vote_4.svg',
            });
          } 
          for(var k=0; k < this.slideFourMap.length; k++) {
            var markerTwo = new google.maps.Marker({
              position: new google.maps.LatLng(this.cardResponseData.data.PrimarylatLons[0].latitude, this.cardResponseData.data.PrimarylatLons[0].longitude),
              map: this.slideFourMap[k],
              icon:'assets/img/early_vote_4.svg',
            });
          } 
          for(var k=0; k < this.slideSecondMap.length; k++) {
            
            var markerTwo = new google.maps.Marker({
              position: new google.maps.LatLng(this.cardResponseData.data.PrimarylatLons[0].latitude, this.cardResponseData.data.PrimarylatLons[0].longitude),
              map: this.slideSecondMap[k],
              icon:'assets/img/early_vote_4.svg',
            });
          }
        }
        if(this.cardResponseData.data.localElectionlatLons.length > 0) {
          for(var k=0; k < this.slideSixMap.length; k++) {
            var markerTwo = new google.maps.Marker({
              position: new google.maps.LatLng(this.cardResponseData.data.localElectionlatLons[0].latitude, this.cardResponseData.data.localElectionlatLons[0].longitude),
              map: this.slideSixMap[k],
              icon:'assets/img/early_vote_4.svg',
            });
          } 
          
        }
        
    },3000);
    if(this.primaryLocationData.length > 0) {
      this.primarySelectedData = this.primaryLocationData[0];
      this.primarySelectedIndex=0;
    } else {
      this.primarySelectedData={};
    }
    if(this.specialLocationData.length > 0) {
      this.specialSelectedData = this.specialLocationData[0];
      this.specialSelectedIndex=0;
    } else {
      this.specialSelectedData={};
    }
    if(this.generalLocationData.length > 0) {
      this.generalSelectedData = this.generalLocationData[0];
      this.generalSelectedIndex=0;
    } else {
      this.generalSelectedData={};
    } 
    if(this.localLocationData.length > 0) {
      this.localSelectedData = this.localLocationData[0];
      this.localSelectedIndex=0;
    } else {
      this.localSelectedData={};
    }
  }
  goToContactUs() {
   this.events.publish('gotoPage:created', 'HelpPage', {});
  }
  showMapDirections(address) {
    let lat=parseFloat(localStorage.getItem('latitude'));
    let lon=parseFloat(localStorage.getItem('longitude'));
    let options: LaunchNavigatorOptions = { 
      start:[lat, lon]
      // app: this.launchNavigator.APP.GOOGLE_MAPS
    };
    console.log("address", address);
    console.log(options);
    this.launchNavigator.navigate(address, options)
    .then(
      (success:any) => console.log('Launched navigator'),
      (error:any) => console.log('Error launching navigator', error)
    );
  }
  openMapPopUp(address) {
    let alert = this.alertCtrl.create({
      title: 'Would you like driving directions to this location?',
      cssClass: 'confirm-register-modal',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.showMapDirections(address);
          }
        }
      ]
    });
    alert.present();
  }
  messageDetail(umid) {
    this.events.publish('gotoPage:created', 'MsgDetailPage', { 'msgId': umid });
    this.placeholderProvider.refresh();
  }
  showOpportunity() {
    this.events.publish('selectedTabs:data','3', Date.now());
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeOpportunityPage');
    if(this.user!=null) {
      this.loadSlidesMap();
    }
    
    this.mainCarousel.freeMode = false;
    this.mainCarousel.centeredSlides = true;
    this.mainCarousel.direction = 'horizontal';
    this.mainCarousel.autoHeight = false;
    this.mainCarousel.pager = false;
    this.mainCarousel.loop = false;
    this.mainCarousel.spaceBetween = 26;
    this.mainCarousel.zoom = false;
    this.mainCarousel.speed = 500;
    this.mainCarousel.slidesPerView = 'auto';
    this.mainCarousel.effect = 'slide';
    // this.mainCarousel.slidesOffsetBefore = '42';
    // this.mainCarousel.slidesOffsetAfter = '42';

    this.mainOneCarousel.freeMode = false;
    this.mainOneCarousel.centeredSlides = true;
    this.mainOneCarousel.virtualTranslate = false;
    this.mainOneCarousel.direction = 'horizontal';
    this.mainOneCarousel.autoHeight = false;
    this.mainOneCarousel.pager = false;
    this.mainOneCarousel.loop = false;
    this.mainOneCarousel.spaceBetween = 0;
    this.mainOneCarousel.zoom = false;
    this.mainOneCarousel.speed = 500;
    this.mainOneCarousel.slidesPerView = '1.3';
    this.mainOneCarousel.effect = 'slide';

    setTimeout(() => {this.loadPage=true},1000);
  }
  gotoElectionsPage() {
    this.events.publish('selectedTabs:data','6', Date.now());
  }
  toggleDropDown() {
    this.showDropDown = !this.showDropDown;
    this.events.publish('addressDropDown:created', {'status':this.showDropDown }, Date.now());
  }
  logScrollStart() {
    // console.log("Scroll Start");
    // document.getElementById("headerNavbarHomeOpportunity").classList.add("fadeUp");
    // document.getElementById("hidetabs").classList.add("inProgress");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    // document.getElementById("headerNavbarHomeOpportunity").classList.remove("fadeUp");
    // document.getElementById("hidetabs").classList.remove("inProgress");
  }
  logScrolling($event) {
    /*
     * MAULIK
     * When scroll 'down' remove the footer
     * When scroll 'up' show the footer
     */
    this.scrallDirection = $event.directionY;
    if (this.scrallDirection == "down") {
      // document.getElementById("hidetabs").classList.add("inProgress");
      // document.getElementById("headerNavbarHomeOpportunity").classList.add("fadeUp");
    }
    if (this.scrallDirection == "up") {
      // document.getElementById("hidetabs").classList.remove("inProgress");
      // document.getElementById("headerNavbarHomeOpportunity").classList.remove("fadeUp");
    }
    if ($event.scrollTop > 100) {
      // document.getElementById("headerNavbarHomeOpportunity").classList.add("shadow");
    } else {
      // document.getElementById("headerNavbarHomeOpportunity").classList.remove("shadow");
    }

  }
  verifyLocation() {

    const options : InAppBrowserOptions = {
      location : 'yes',//Or 'no' 
      hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only 
      fullscreen : 'yes',//Windows only    
    };
    let target = "_system";
    this.iab.create(this.nonVerifyUrl,target,options);
  }
  showMessages(type, count) {
    //type
    if(count!='0') {
      localStorage.setItem('messageSelectedType', type);
      this.events.publish('messageSelectedType:created', type, Date.now());
      this.events.publish('gotoPage:created', 'MsglistPage', {});
    }
  }
}
