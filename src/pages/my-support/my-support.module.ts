import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySupportPage } from './my-support';

@NgModule({
  declarations: [
    MySupportPage,
  ],
  imports: [
    IonicPageModule.forChild(MySupportPage),
  ],
})
export class MySupportPageModule {}
