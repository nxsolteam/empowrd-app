import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header'
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';
import { Cache, CacheService } from 'ionic-cache-observable';
import { GetOrgList } from '../../providers/get-org-list/get-org-list.model';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the MySupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-support',
  templateUrl: 'my-support.html',
})
export class MySupportPage {

  @ViewChild(Content) content: Content;
  @ViewChild(ParallaxHeaderDirective) directive = null;
  public getOrgListCache: Cache<GetOrgList[]>;
  public getOrgList$: Observable<GetOrgList[]>;
  workwithOrganizationContent = true;
  totalOrgSelected=0;
  searchBarSection=false;
  showOrganizationHeader=true;
  showbuttons = true;
  searchValue='';  
  showall='0';

  orgCount=0;
  user:any;
  Organization:any;
  allOrganization:any;
  Events:any;
  selectedArray:any=[];
  selectedArrayId:any=[];
  selectedEvent:any=[];
  previousSelectedEvent:any=[];
  previouspowerScore=0;
  selectedOrganizationArray=[];
  civicData:any;
  updatedOrg=false;
  loaderShow=true;
  powerScore='0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public sendrequest: SendrequestProvider, private getOrgListProvider: GetOrgListProvider, private cacheService: CacheService) {
    this.user =  JSON.parse(localStorage.getItem('user'));
    this.powerScore = localStorage.getItem('powerScore');
    this.civicData = JSON.parse(localStorage.getItem('civicProfile'));
    for(var i=0; i < this.civicData.organizations.length; i++) {
      this.selectedOrganizationArray.push(this.civicData.organizations[i].id);
    }
    /*
    for(var i=0; i< this.civicData.events.length; i++) {
      this.previousSelectedEvent.push(this.civicData.events[i].event_id);
      this.selectedEvent.push(this.civicData.events[i].event_id);
    }*/
    this.getAllOrganizations();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySupportPage');
  }
  logScrollStart() {
    // console.log("Scroll Start");
    //document.getElementById("headerStrocke").classList.add("animate");
  }
  logScrollEnd() {
    // console.log("Scroll End ");
    //document.getElementById("headerStrocke").classList.remove("animate");
  }
  logScrolling($event) {
    //console.log($event);
  }
  AddOrganization() {
    setTimeout(() => {
      this.workwithOrganizationContent=false;
    }, 100);
    console.log(this.selectedArray);
    this.navCtrl.push('AddOrganizationPage',{'organization': this.Organization,'selectedarray':this.selectedArray,'selectedarrayid':this.selectedArrayId},{animate:true, duration:900, animation: 'ios-transition'});
    setTimeout(() => {
       this.workwithOrganizationContent=true;
    }, 1000);
  }
  getAllOrganizations() {
    /* this.sendrequest.getResult('user/getOrganizationFour/' + this.user.id,'post',{}).then((response:any) => {     
      this.Organization = response.data.certified;  
        this.Organization.forEach(function(item){ 
            if(this.selectedOrganizationArray.indexOf(item.id) !== -1){
	          item.isChecked=true;
	          this.addOrg(item);
	        } else {
	          item.isChecked=false;
	        }
        }, this);
        this.allOrganization=this.Organization;
        
        // this.Events = response.data.events;  
        // this.Events.forEach(function(item){ 
        //   if(this.selectedEvent.indexOf(item.id) !== -1){
        //     item.isChecked=true;
        //   } else {
        //     item.isChecked=false;
        //   }
        // }, this); 
        this.allOrganization=this.Organization;
    },
    error => {
      
    })  */
    this.getOrgListProvider.getOrgListRefresh();
    this.cacheService
      .get('getOrgList')
      .mergeMap((getOrgListCacheResponse: Cache<GetOrgList[]>) => {
          this.getOrgListCache = getOrgListCacheResponse;
          return this.getOrgListCache.get$;
      }).subscribe((response:any) => {
         this.Organization = response.data.certified;  
        this.Organization.forEach(function(item){ 
            if(this.selectedOrganizationArray.indexOf(item.id) !== -1){
            item.isChecked=true;
            this.addOrg(item);
          } else {
            item.isChecked=false;
          }
        }, this);
        this.allOrganization=this.Organization;
        this.Events = response.data.events;  
        this.Events.forEach(function(item){ 
          if(this.selectedEvent.indexOf(item.id) !== -1){
            item.isChecked=true;
          } else {
            item.isChecked=false;
          }
        }, this);
        this.loaderShow=false;
      });
  }
  getOrganizations() {
    this.sendrequest.getResult('user/getallOrganization/' + this.user.id,'get',{}).then((response:any) => {     
      this.Organization = response.data.organizations;  
      this.Organization.forEach(function(item){ 
        if(this.selectedOrganizationArray.indexOf(item.id) !== -1){
          item.isChecked=true;
          this.addOrg(item);
        } else {
          item.isChecked=false;
        }
      }, this);
      this.allOrganization = this.Organization;
      this.loaderShow=false;
      this.getAllOrganizations();
    },
    error => {
      
    })
  }
  removeOrg(org){
    org.isChecked=false;
    this.totalOrgSelected -=1;
    this.selectedOrganizationArray.splice(this.selectedOrganizationArray.indexOf(org.id), 1);
    this.selectedArrayId.splice(this.selectedArrayId.indexOf(org.id), 1);
    this.selectedArray =  this.removeByKey(this.selectedArray,{
        accronym: org.accronym, 
          name:  org.name,
          accronym_model:org.isChecked
    });
    //this.powerScore = this.previouspowerScore + (this.totalOrgSelected * 5);

  }
  removeByKey(array, params){
    array.some(function(item, index) {
    if(array[index].accronym === params.accronym){
        array.splice(index, 1);
    }
    });
    return array;
  }
  addRemoveOrg(org) {
    this.updatedOrg=true;
    if(org.isChecked) {
      this.removeOrg(org);
    } else {
      this.addOrg(org);
    }
  }
  addRemoveEvent(event) {
    this.updatedOrg=true;
    if(event.isChecked) {
     
      this.totalOrgSelected -=1;
      this.selectedEvent.splice(this.selectedEvent.indexOf(event.id), 1);
    } else {
      if(this.selectedEvent.indexOf(event.id) == -1){
        //localStorage.setItem('selectedEventId',event.id);
        //localStorage.setItem('selectedEventName',event.event_name);
        this.totalOrgSelected +=1;
        this.selectedEvent.push(event.id);
      }
    }
    console.log(this.selectedEvent);
    event.isChecked = !event.isChecked;
  }
  addOrg(org) {
    org.isChecked=true;
    if(this.selectedOrganizationArray.indexOf(org.id) == -1){
      this.selectedOrganizationArray.push(org.id);
    }
    if(this.selectedArrayId.indexOf(org.id) == -1){
	    this.totalOrgSelected +=1;
	    this.selectedArrayId.push(org.id);
	    this.selectedArray.push({
	        accronym: org.accronym, 
	        name:  org.name,
	        accronym_model:org.isChecked
	    });
    }
  }
  addSelected(){
    this.events.publish('workwithOrganizationContent:created', this.selectedArray, this.selectedArrayId);
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
    setTimeout(() => {
        this.showOrganizationHeader=false;
      }, 500);
  }
  showSelected(){
    this.showall='1';
    this.Organization = [];
    this.allOrganization.forEach(function(item){
        
        this.selectedArrayId.forEach(function(pitem){ 
            if(pitem == item.id) {
                item.isChecked=true;
                this.Organization.push(item);
            }
        }, this);
    }, this);
  }
  showAll() {
    this.showall='0';
    this.searchValue='';
    this.Organization=[];
    //this.Organization =this.allOrganization;
    this.allOrganization.forEach(function(item){ 
        
        if(item.certified !=2) {
          this.Organization.push(item);
        }
    }, this);
  }
  GoToNext(value){
    var confirmationEventsId=[];
    if(value =='yes') {
      this.sendrequest.getResult('user/addMultipleOrganization/' + this.user.id,'post',{
        organization:this.selectedArray
      }).then((response:any) => { 
        this.sendrequest.updateCivicProfileData();
        this.sendrequest.updatePowerScore(this.user.id);
        this.events.publish('setLatLon:created', this.user, Date.now());
        
      },
      error => {
       
      });
      if(this.selectedEvent.length > 0){
        if(this.previousSelectedEvent.length > 0){
           this.selectedEvent.forEach(function(item){ 
              if(this.previousSelectedEvent.indexOf(item) == -1){
               confirmationEventsId.push(item);
              } 
            }, this);
        } else {
          confirmationEventsId=this.selectedEvent;
        }
      
        if(confirmationEventsId.length==0) {
          this.navCtrl.push('AddEventsActivitiesPage',{ 'events':this.Events, 'selectedEvent':this.selectedEvent, 'fromSupportPage':'1' },{animate:true, animation: 'ios-transition'});
        } else {
          this.navCtrl.push('RegistereventPage',{'confirmationEventsId':confirmationEventsId, 'events':this.Events, 'selectedEvent':this.selectedEvent },{animate:true, animation: 'ios-transition'});
        }
      } else {
        this.navCtrl.pop({animate:true, animation: 'ios-transition'});
      }
    } else {

      this.navCtrl.pop({animate:true, animation: 'ios-transition'});
    }
  }
  goBack() {
    //this.events.publish('createProfileContent:created', 'user', Date.now());
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToRoot() {
    this.navCtrl.popToRoot({animate:false});
  }

}
