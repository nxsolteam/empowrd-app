import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs, Events, Tab } from 'ionic-angular';

/**
 * Generated class for the TabsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-home',
  templateUrl: 'tabs-home.html',
})
export class TabsHomePage {
	tab1Root = "HomePage";
  tab2Root = "HomePage";
  tab3Root = "LeadersPage";
  tab4Root = "HomePage"; // "AccountPage";

  @ViewChild('mainTabs') mainTabs: Tabs;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    events.subscribe('goToPage:params', (page, params) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      navCtrl.push(page,params);
    });
    events.subscribe('changeTab:created', (index, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.mainTabs.select(index);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsHomePage');
    
  }

}
