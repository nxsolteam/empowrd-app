import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PrivacyPolicyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {

  openModal='0';
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    if(navParams.get('openModal')!=undefined) {
      this.openModal = navParams.get('openModal');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivacyPolicyPage');
  }
  gotoView(id){
    document.getElementById("bookmark"+id).scrollIntoView();
  }
  goBack() {
    if(this.openModal=='0') {
      this.navCtrl.pop({ animate:true, direction: 'forward', animation: 'ios-transition'});
    } else {
      this.viewCtrl.dismiss();
    }
  }

}
