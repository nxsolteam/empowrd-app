import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, Content, LoadingController, Events, ModalController } from 'ionic-angular';
import { ParallaxHeaderDirective } from '../../directives/parallax-header/parallax-header'
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';
import { Cache, CacheService } from 'ionic-cache-observable';
import { GetOrgList } from '../../providers/get-org-list/get-org-list.model';
import { Observable } from 'rxjs/Observable';
import { SelectedEventActivitiesProvider } from '../../providers/selected-event-activities/selected-event-activities';
import { SelectedEventActivities } from '../../providers/selected-event-activities/selected-event-activities.model';
import { StatusBar } from '@ionic-native/status-bar';
import * as moment from 'moment';
import { Refresher } from 'ionic-angular';
import { PlaceholderProvider } from '../../providers/placeholder/placeholder';
import { Placeholder } from '../../providers/placeholder/placeholder.model';
/**
 * Generated class for the SelectedEventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selected-events',
  templateUrl: 'selected-events.html',
})
export class SelectedEventsPage {

  // @ViewChild(Content) content: Content;
  // @ViewChild(ParallaxHeaderDirective) directive = null;
  newMessage=false;
  selectedEvents:any={};
  selectedEventIds:any=[];
  civicData:any;
  eventName='';
  activityList:any=[];
  public selectedEventActivitiesCache: Cache<SelectedEventActivities[]>;
  public selectedEventActivities$: Observable<SelectedEventActivities[]>;
  selectedSection='agenda';
  currentDate = moment(). format('MM.DD.YY');
   messageList:any = [];
  allMessages:any = [];
  messageSelectedType = '';
  todayDate = '';
  selectedActivity:any={};
  lastMessage:any={};
  hideEventContent=false;
  public placeholder$: Observable<Placeholder[]>;

  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<Placeholder>}
   */
  public cache: Cache<Placeholder[]>;
  public getOrgListCache: Cache<GetOrgList[]>;
  public getOrgList$: Observable<GetOrgList[]>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private getOrgListProvider: GetOrgListProvider, private cacheService: CacheService, private selectedEventActivitiesProvider: SelectedEventActivitiesProvider, public loadingCtrl: LoadingController, public events: Events, private statusBar: StatusBar, private placeholderProvider: PlaceholderProvider,public modalCtrl: ModalController) {
    this.selectedEvents = navParams.get('event');
  	this.todayDate = moment(). format('MM/DD/YY');
      this.eventName=this.selectedEvents.event_name; 
    if(this.selectedEvents.checkin==false) {
      this.checkInModal();
    }
    this.getActivityData();
    this.getMessageList();
    events.subscribe('selectedEventContent:created', (user, time) => {
      this.hideEventContent=true;
      setTimeout(() => {
        this.hideEventContent=false;
      }, 500);
    });
  }
  public onRefresh(refresher: Refresher): void {
    this.placeholderProvider.refresh(refresher);
  }
  checkInModal() {
    console.log("dfgdfg");
    let activityModal = this.modalCtrl.create('CheckInModalPage', {'selectedevent': this.selectedEvents },{
      cssClass: 'check-in-modal'
    });
    activityModal.onDidDismiss(data => {
      console.log(data);
      if(data!=null) {
        this.selectedEvents.checkin = data.checkedin;
      }
    });
    activityModal.present();
  }
  gotoSelectedActivity() {
    this.hideEventContent=true;
     localStorage.setItem('selectedEventName',this.selectedEvents.event_name);
     localStorage.setItem('selectedEventId',this.selectedEvents.id);
     this.navCtrl.push('SelectedEventActivitiesPage', {},{animate:true, animation: 'ios-transition'});
    setTimeout(() => {
      this.hideEventContent=false;
    }, 900);
  }
  getMessageList() {
    this.placeholderProvider.refresh();
    this.cacheService
    .get('msglist')
    .mergeMap((cache: Cache<Placeholder[]>) => {
        this.cache = cache;
        return this.cache.get$;
    }).subscribe((response:any) => {
      this.allMessages = response.data;
      localStorage.setItem("lastupdated_message", this.allMessages[0].created_at);
      this.getMessageByType();
    })
  }
  gotoMailDetail(umid, usercomposed, msgid, mail) {
    console.log("Clicked : ");
    mail.status = 1;
    this.hideEventContent=true;
    if (usercomposed == false) {
      this.events.publish('gotoPage:created', 'MsgDetailPage', { 'msgId': umid });
    } else {
      this.events.publish('gotoPage:created', 'MsgDetailPage', { 'composeMessage': usercomposed, 'msgId': msgid });
    }
    setTimeout(() => {
      this.hideEventContent=false;
    }, 900);
  }
  gotoMessageList() {
    this.hideEventContent=true;
    //localStorage.setItem('messageSelectedType','ANNOUNCEMENT');
    this.navCtrl.push('MsglistPage', { 'event_id' : this.selectedEvents.id, 'eventMessages':true},{animate:true, animation: 'ios-transition'});
    setTimeout(() => {
      this.hideEventContent=false;
    }, 900);
  }
  getMessageByType() {
    this.messageList = [];
    // this.newMessage=false;
    for(var i = 0; i < this.allMessages.length; i++) {
      if(this.allMessages[i].type==2 && this.allMessages[i].event_id==this.selectedEvents.id) {
        
        this.messageList.push(this.allMessages[i]);
      }
    }
    this.lastMessage={};
    if(this.messageList.length > 0) {
      this.lastMessage=this.messageList[0];
    }
  }
  goBack() {
    this.navCtrl.pop({animate:true, animation: 'ios-transition'});
  }
  goToActivityList(event) {
    localStorage.setItem('selectedEventId',event.id);
    localStorage.setItem('selectedEventName',event.event_name);
  	/* 
   
    this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    this.navCtrl.push('SelectedEventActivitiesPage', {},{animate:true, animation: 'ios-transition'}); */
    
    let selectedEvent=[];
    selectedEvent.push(localStorage.getItem('selectedEventId'));
    this.hideEventContent=true;
    this.navCtrl.push('AddEventsActivitiesPage', {'selectedEvent': selectedEvent },{animate:true, animation: 'ios-transition'});
    setTimeout(() => {
      this.hideEventContent=false;
    }, 900);
  }

  editActivity() {
    localStorage.setItem('selectedEventId',this.selectedEvents.id);
    localStorage.setItem('selectedEventName',this.selectedEvents.event_name);
    let selectedEvent=[];
    selectedEvent.push(this.selectedEvents.id);
    this.navCtrl.push('AddEventsActivitiesPage', { 'selectedEvent': selectedEvent },{animate:true, animation: 'ios-transition',duration:900 });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectedEventsPage');
  }

  goToActivityDetails(activity) {
    localStorage.setItem('selectedEventId',this.selectedEvents.id);
    this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    this.hideEventContent=true;
    this.navCtrl.push('ActivityDetailPage', {'activity': activity},{animate:true, animation: 'ios-transition'});
    setTimeout(() => {
      this.hideEventContent=false;
    }, 900);
  }
  getActivityData() {
    localStorage.setItem('selectedEventId',this.selectedEvents.id);
    this.selectedEventActivitiesProvider.selectedEventActivitiesRefresh();
    this.cacheService
        .get('selectedEventActivities')
        .mergeMap((selectedEventActivitiesCacheResponse: Cache<SelectedEventActivities[]>) => {
            this.selectedEventActivitiesCache = selectedEventActivitiesCacheResponse;
            return this.selectedEventActivitiesCache.get$;
        }).subscribe((response:any) => {
            
            
            let activities = [];
            for(var i = 0; i < response.data.length; i++) {
              if(response.data[i].event_id==this.selectedEvents.id) {
                activities.push(response.data[i]);
              }
            }
            this.activityList = activities;
            this.selectedActivity={};
            if(this.activityList.length > 0) {
              this.selectedActivity=this.activityList[0];
            }
        });
  }

}
