import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectedEventsPage } from './selected-events';

@NgModule({
  declarations: [
    SelectedEventsPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectedEventsPage),
  ],
})
export class SelectedEventsPageModule {}
