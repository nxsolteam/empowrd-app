import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectivesModule } from '../../directives/directives.module';
import { WorkWithOrganizationsPage } from './work-with-organizations';

@NgModule({
  declarations: [
    WorkWithOrganizationsPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(WorkWithOrganizationsPage),
  ],
})
export class WorkWithOrganizationsPageModule {}
