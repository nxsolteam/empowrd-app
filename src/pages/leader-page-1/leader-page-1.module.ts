import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaderPage_1Page } from './leader-page-1';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    LeaderPage_1Page,
  ],
  imports: [
  	DirectivesModule,
    IonicPageModule.forChild(LeaderPage_1Page),
  ],
})
export class LeaderPage_1PageModule {}
