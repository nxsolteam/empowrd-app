import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../../providers/sendrequest/sendrequest';
/**
 * Generated class for the NotvalidatedaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notvalidatedaddress',
  templateUrl: 'notvalidatedaddress.html',
})
export class NotvalidatedaddressPage {
  data:any;
  addressDisplay:string='';
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider,public viewCtrl: ViewController, public loadingCtrl: LoadingController) {
  	 this.user =  JSON.parse(localStorage.getItem('user'));
  	this.data=navParams.get('addressDetail');
  	this.addressDisplay=this.data.address;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotvalidatedaddressPage');
  }
  registerAddress(type) {
  	localStorage.setItem('currentLocationClicked','false');
    if(type=="voter") {
        localStorage.setItem('headerAddressTitle',"PRIMARY RESIDENCE");
    } else {
        localStorage.setItem('headerAddressTitle',"Registered Voting Location");
    }
    let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
    this.sendrequest.getResult("user/saveAddress/" + this.user.id,'post',{
        address: this.addressDisplay,
        type: type
    }).then((response:any) => { 
      loading.dismiss();
    	let data={'nextStepPage':'StepthreeprofilePage'};
    	this.viewCtrl.dismiss(data);
    },
    error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
    });
  	
  }
  hideModal() {
    let data={};
   	this.viewCtrl.dismiss('');
  }

}
