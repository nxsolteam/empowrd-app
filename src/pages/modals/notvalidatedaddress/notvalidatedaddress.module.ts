import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotvalidatedaddressPage } from './notvalidatedaddress';

@NgModule({
  declarations: [
    NotvalidatedaddressPage,
  ],
  imports: [
    IonicPageModule.forChild(NotvalidatedaddressPage),
  ],
})
export class NotvalidatedaddressPageModule {}
