import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../../providers/sendrequest/sendrequest';
/**
 * Generated class for the LocationverifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-locationverify',
  templateUrl: 'locationverify.html',
})
export class LocationverifyPage {
  data:any;
  addressDisplay:string='';
  user:any;
  showValidateVoterButton:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider,public viewCtrl: ViewController, public loadingCtrl: LoadingController) {
    this.user =  JSON.parse(localStorage.getItem('user'));
  	this.data=navParams.get('addressDetail');
  	this.addressDisplay=this.data.address;
  	if(this.data.votervalidation.showVoterValidateButton == 1){
  		this.showValidateVoterButton=true;
  	}
  }
  validateAddress() {
	
    if(this.data.votervalidation.record != 0) {
      let loading = this.loadingCtrl.create({
          content: 'Please wait...',
          spinner: 'bubbles'
      });
      loading.present();
    	this.sendrequest.getResult("user/saveValidateAddress/" + this.user.id,'post',{
	        validate_address: '1'
	    }).then((response:any) => { 
          loading.dismiss();
          let data={'nextStepPage':'ValidatedaddressPage'};
          this.viewCtrl.dismiss(data);
      },
      error => {
        loading.dismiss();
        this.sendrequest.presentToast("Error occured. Please try again");
      });
    } else {
      let loading = this.loadingCtrl.create({
          content: 'Please wait...',
          spinner: 'bubbles'
      });
      loading.present();
    	this.sendrequest.getResult("user/saveValidateAddress/" + this.user.id,'post',{
	        validate_address: '0'
	    }).then((response:any) => { 
        loading.dismiss();
        let data={'nextStepPage':'NotvalidatedaddressPage'};
        this.viewCtrl.dismiss(data);
      },
      error => {
        loading.dismiss();
        this.sendrequest.presentToast("Error occured. Please try again");
      });
        
    }
  }
  registerAddress(type) {
  	
  	localStorage.setItem('currentLocationClicked','false');
    if(type=="voter") {
        localStorage.setItem('headerAddressTitle',"PRIMARY RESIDENCE");
    } else {
        localStorage.setItem('headerAddressTitle',"Registered Voting Location");
    }
    let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
    this.sendrequest.getResult("user/saveAddress/" + this.user.id,'post',{
        address: this.addressDisplay,
        type: type
    }).then((response:any) => { 
      loading.dismiss();
    	let data={'nextStepPage':'SteponeprofilePage'};
    	this.viewCtrl.dismiss(data);
    },
    error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
    });
  	
  }
  hideModal() {
    let data={};
   	this.viewCtrl.dismiss('');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationverifyPage');
  }

}
