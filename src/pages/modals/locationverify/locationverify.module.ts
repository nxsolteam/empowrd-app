import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationverifyPage } from './locationverify';

@NgModule({
  declarations: [
    LocationverifyPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationverifyPage),
  ],
})
export class LocationverifyPageModule {}
