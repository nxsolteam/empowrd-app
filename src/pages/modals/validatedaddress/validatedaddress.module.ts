import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ValidatedaddressPage } from './validatedaddress';

@NgModule({
  declarations: [
    ValidatedaddressPage,
  ],
  imports: [
    IonicPageModule.forChild(ValidatedaddressPage),
  ],
})
export class ValidatedaddressPageModule {}
