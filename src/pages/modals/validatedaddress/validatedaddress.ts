import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../../providers/sendrequest/sendrequest';
/**
 * Generated class for the ValidatedaddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-validatedaddress',
  templateUrl: 'validatedaddress.html',
})
export class ValidatedaddressPage {
  user:any;
  data:any;
  addressDisplay:string='';
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider,public viewCtrl: ViewController, public loadingCtrl: LoadingController) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  	this.data=navParams.get('addressDetail');
  	this.addressDisplay=this.data.address;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ValidatedaddressPage');
  }
  registerAddress(type) {
  	
  	localStorage.setItem('currentLocationClicked','false');
    localStorage.setItem('headerAddressTitle',"Validated Voting Address");
    let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
    this.sendrequest.getResult("user/saveAddress/" + this.user.id,'post',{
        address: this.addressDisplay,
        type: type
    }).then((response:any) => { 
      loading.dismiss();
    	let data={'nextStepPage':'StepthreeprofilePage'};
    	this.viewCtrl.dismiss(data);
    },
    error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
    });
  }
  hideModal() {
    let data={};
   	this.viewCtrl.dismiss('');
  }

}
