import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SteptwoprofilePage } from './steptwoprofile';

@NgModule({
  declarations: [
    SteptwoprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(SteptwoprofilePage),
  ],
})
export class SteptwoprofilePageModule {}
