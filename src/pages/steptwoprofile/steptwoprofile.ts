import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, Events, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
/**
 * Generated class for the SteptwoprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 declare var google: any;
@IonicPage()
@Component({
  selector: 'page-steptwoprofile',
  templateUrl: 'steptwoprofile.html',
})
export class SteptwoprofilePage {
  stepTwoShowContent=true;
  latitude=0;
  longitude=0;
  location='';
  user:any;
  userData:any;
  @ViewChild('Address') Address;
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, public sendrequest: SendrequestProvider, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private nativePageTransitions: NativePageTransitions, public events: Events, private alertCtrl: AlertController, public viewCtrl: ViewController) {
  	// this.user =  JSON.parse(localStorage.getItem('user'));
    if(navParams.get('data') != null && navParams.get('data') != undefined) {
      this.userData =  navParams.get('data');
    }
    events.subscribe('stepTwoContent:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      this.stepTwoShowContent=false;
      setTimeout(() => {
        this.stepTwoShowContent=true;
      }, 500);
    });
  }
  ionViewDidEnter() {
    let input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];
    let autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      // retrieve the place object for your use
      let place = autocomplete.getPlace();
      console.log(place);
      let state = '';
      for(var i=0; i < place.address_components.length; i++) {
        if(place.address_components[i].types[0] =='administrative_area_level_1') {
          state = place.address_components[i].short_name;
        }
      }
      // if(state=='GA' || state=='FL') {
        this.location=place.formatted_address;
        this.latitude = place.geometry.location.lat();
        this.longitude = place.geometry.location.lng();
        localStorage.setItem('latitude',this.latitude.toString());
        localStorage.setItem('stateShortName',state);
        localStorage.setItem('longitude',this.longitude.toString());
        localStorage.setItem('currentLocationClicked','false');
        localStorage.setItem('addresstext',place.formatted_address);

        localStorage.setItem('responseData',JSON.stringify(this.userData));
        localStorage.setItem('activePage','VerifyEmailPage');
        this.sendrequest.sendVerificationEmail(this.userData).then((response:any) => { 

        },
        (error:any) => {
        })
        //setTimeout(() => {
          this.stepTwoShowContent=false;
        //}, 100);
        this.navCtrl.push('VerifyEmailPage',{'userData' : this.userData},{ animate:true, duration:1000, animation: 'ios-transition'});
        setTimeout(() => {
          this.stepTwoShowContent=true;
        }, 1500);
      /* } else {
        this.location='';
        this.confirmPopUp();
      } */

    });
  }
  ionViewDidLoad() {
    setTimeout(() => {
      this.Address.setFocus();
    }, 1200);
    
    console.log('ionViewDidLoad SteptwoprofilePage');
  }
  confirmPopUp() {
    let alert = this.alertCtrl.create({
      title: "Today, EMPOWRD only engages via Florida and Georgia locations.",
      subTitle: "If you would like to continue, please provide a Florida or Georgia address.",
      cssClass: 'address-popup',
      buttons: [
        {
          text: 'DO NOT REGISTER',
          handler: () => {
            this.goToRoot();
          }
        },
        {
          text: 'CHANGE ADDRESS',
          handler: () => {
            
          }
        }
      ]
    });
    alert.present();
  }
  showModal(pageName,params){
   	let modal = this.modalCtrl.create(pageName, params,{
      cssClass: 'custom-modal'
    });
    modal.onDidDismiss((data) => {
    console.log("data");
    console.log(data);
        if(data!=null && data !=''){
        	if(data.nextStepPage =='ValidatedaddressPage' || data.nextStepPage =='NotvalidatedaddressPage'){
        		this.showModal(data.nextStepPage,{'addressDetail':{'address':localStorage.getItem('addresstext')}});
        	} else {
        		let options: NativeTransitionOptions = {
              direction: 'left',
              duration: 500
            }
            this.nativePageTransitions.slide(options);
            this.navCtrl.push(data.nextStepPage);
        	}
        }
    });
    modal.present();
  }
  goToPage(page){

    this.sendrequest.goToPage(page);
  }
  goBack() {
    // this.events.publish('registeStepTwoContent:created', 'user', Date.now());
    this.navCtrl.pop({ animate:true, duration:900, direction: 'back', animation: 'ios-transition'});
  }
  goToRoot() {
    if(localStorage.getItem('modelRegistration')=='true') {
      this.viewCtrl.dismiss();
    } else {
      this.navCtrl.popToRoot({animate:false});
    }
  }
  ionViewWillEnter() {
     // Google Places API auto complete
    
  }
  getAddressDetails(address){
    let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
  	this.sendrequest.getResult("user/getAddressDetails/" + this.user.id,'post',{
        'address': address,
        firstname:this.user.first_name,
        lastname:this.user.last_name,
        email:this.user.email
    }).then((response:any) => { 
        loading.dismiss();
        this.showModal('LocationverifyPage',{'addressDetail':{'address':address,'votervalidation':response.data}});
    },
    error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
    });
  }
  currentLocationGPSFromStepOne() {
  	localStorage.setItem('currentLocationClicked','true');
  	this.geolocation.getCurrentPosition().then((response) => {

      console.log(response);
      var geocoder = new google.maps.Geocoder();
        localStorage.setItem('latitude',response.coords.latitude.toString());
        localStorage.setItem('longitude',response.coords.longitude.toString());
        var latlng = new google.maps.LatLng(response.coords.latitude, response.coords.longitude);
        var request = {
            latLng: latlng
        };
        geocoder.geocode(
	        {'latLng': latlng}, 
	         (results, status) => {
	         	console.log(results);
	            if (status == google.maps.GeocoderStatus.OK) {
                   	localStorage.currentLocationClicked
                    localStorage.setItem('addresstext',results[0].formatted_address);
                    this.getAddressDetails(results[0].formatted_address);
	            }
	            else {
	            	this.sendrequest.presentToast("Location can not getting. Please try again.")
	            }

	           
	        }
	        
	    );

    });

  }

}
