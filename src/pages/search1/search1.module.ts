import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Search1Page } from './search1';
import { ComponentsModule }from '../../components/components.module';
@NgModule({
  declarations: [
    Search1Page,
   // CustomHeaderComponent,
  ],
  imports: [
    IonicPageModule.forChild(Search1Page),
   // CustomHeaderComponent,
   	ComponentsModule,
  ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class Search1PageModule {}
