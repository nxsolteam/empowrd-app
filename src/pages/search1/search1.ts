import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { LeaderlistProvider } from '../../providers/leaderlist/leaderlist';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Leaderlist } from '../../providers/leaderlist/leaderlist.model';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

/**
 * Generated class for the Search1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search1',
  templateUrl: 'search1.html',
})
export class Search1Page {
  header_data:any;
  resultData:any;
  user:any;
  headerAddressTitle='';
  addressOnHeader='';
  showEventAddress='false';
  yourEngagementStart='';
  public leaderlistCache: Cache<Leaderlist[]>;
  public leaderlist$: Observable<Leaderlist[]>;
  leaderlist:any;
  titleonHeader='';
  name='';
  textatlast = "Select any of the options below and begin your civic engagement process.";
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public sendrequest: SendrequestProvider, private leaderlistProvider: LeaderlistProvider, private cacheService: CacheService, public loadingCtrl: LoadingController, public events: Events, private nativePageTransitions: NativePageTransitions) {
  	this.header_data={ismenu:true,ishome:false,title:"HOME"};
  	this.user =  JSON.parse(localStorage.getItem('user'));
    this.name = this.user.first_name.charAt(0)+''+this.user.last_name.charAt(0)
    this.addressChangeUpdateText();
  	let loading = this.loadingCtrl.create({
        content: 'Please wait...',
        spinner: 'bubbles'
    });
    loading.present();
  	this.sendrequest.getResult('user/getNewNewsEventCount/' + this.user.id,'get',{}).then(result => {     
      loading.dismiss();
  	  console.log(result);
  	},
  	error => {
      loading.dismiss();
      this.sendrequest.presentToast("Error occured. Please try again");
  	})
    events.subscribe('addressChanged:created', (address, time) => {
      console.log('Welcome', address, 'at', time);
      this.addressChangeUpdateText();
    });
    //const leaderListSourceData = this.leaderlistProvider.random();
    this.cacheService.get('leaderlist').subscribe((leaderlistCache) => {
     // this.leaderlistCache = leaderlistCache;
      this.leaderlist$ = leaderlistCache.get$;
      console.log(" Search 1 leaderlist ");
      console.log(this.leaderlist$);
      console.log(this.leaderlist$ );

      
    });
  }
  addressChangeUpdateText(){
    if (localStorage.getItem('headerAddressTitle') !==null) {
      this.headerAddressTitle=localStorage.getItem('headerAddressTitle');
      this.titleonHeader = this.headerAddressTitle;
    }
    if (localStorage.getItem('addresstext') !==null) {
      this.addressOnHeader=localStorage.getItem('addresstext');
    }
    if(localStorage.getItem('showEventAddress') !== null){
      this.showEventAddress=localStorage.getItem('showEventAddress');
    }
    if(this.showEventAddress=='true'){
      this.yourEngagementStart = "ACTIVE EVENT:";
    } else {
      this.yourEngagementStart = "YOUR ENGAGEMENT STARTS AT:";
    }
  }
  gotoMessageList(){
    
    this.sendrequest.goToPage('MsglistPage');
  }
  showLeaders(){
    
    this.sendrequest.goToPage('LeadersPage');
  }
  showLeaderList(){
    
    this.sendrequest.goToPage('LeaderFilterPage');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Search1Page');
    
  }
  ionViewWillEnter() {

  }
  

}
