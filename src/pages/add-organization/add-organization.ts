import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { SendrequestProvider } from '../../providers/sendrequest/sendrequest';
import { GetOrgListProvider } from '../../providers/get-org-list/get-org-list';
import { Cache, CacheService } from 'ionic-cache-observable';
import { GetOrgList } from '../../providers/get-org-list/get-org-list.model';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the AddOrganizationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-organization',
  templateUrl: 'add-organization.html',
})
export class AddOrganizationPage {
  totalOrgSelected=0;
  searchBarSection=false;
  showOrganizationHeader=true;
  showbuttons = true;
  public getOrgListCache: Cache<GetOrgList[]>;
  public getOrgList$: Observable<GetOrgList[]>;
  user:any;
  showall='0';
  Organization:any;
  allOrganization:any;
  selectedArray:any = [];
  selectedArrayId:any =[];
  searchValue='';
  constructor(private navCtrl: NavController, public navParams: NavParams, public events: Events,  public sendrequest: SendrequestProvider, private getOrgListProvider: GetOrgListProvider, private cacheService: CacheService) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
  //  this.Organization = navParams.get('organization');
  	this.selectedArray = navParams.get('selectedarray');
  	console.log(this.selectedArray);
  	this.totalOrgSelected=this.selectedArray.length;
  	this.selectedArrayId = navParams.get('selectedarrayid');
  	//this.allOrganization = this.Organization;
  	
  }
  showSearchBarSection() {
    this.searchBarSection= !this.searchBarSection;
  }
  getOrganizations() {
      this.getOrgListProvider.getOrgListRefresh();
      this.cacheService
        .get('getOrgList')
        .mergeMap((getOrgListCacheResponse: Cache<GetOrgList[]>) => {
            this.getOrgListCache = getOrgListCacheResponse;
            return this.getOrgListCache.get$;
        }).subscribe((response:any) => {
           this.Organization = response.data.certified;  
          this.Organization.forEach(function(item){ 
              if(this.selectedArrayId.indexOf(item.id) !== -1){
              item.isChecked=true;
            } else {
              item.isChecked=false;
            }
          }, this);
          this.allOrganization=this.Organization;
        });
  	/* this.sendrequest.getResult('user/getallOrganization/' + this.user.id,'get',{}).then((response:any) => {     
	  	this.Organization = response.data.certified;  
        this.Organization.forEach(function(item){ 
            item.isChecked=false;
        });
        this.allOrganization=this.Organization;
	},
	error => {
		
	}) */
  }

  goToBack() {
     if(this.searchBarSection ==true){
      this.showSearchBarSection();
    } else if(this.searchValue !='') {
      this.showAll();
    }
    else {
     
      this.events.publish('workwithOrganizationContent:created', this.selectedArray, this.selectedArrayId);
      setTimeout(() => {
        this.showbuttons =false;
      }, 100);
      this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
      setTimeout(() => {
          this.showOrganizationHeader=false;
        }, 600);
    }
  }
  addSelected(){
  	this.events.publish('workwithOrganizationContent:created', this.selectedArray, this.selectedArrayId);
    this.navCtrl.pop({ animate:true, duration:1300, direction: 'back', animation: 'ios-transition'});
    setTimeout(() => {
        this.showOrganizationHeader=false;
      }, 500);
  }
  searchOrganization() {
    console.log("clicked");
    if(this.searchValue !=''){
      this.showSearchBarSection();
    	this.Organization = this.allOrganization.filter((v) => {
    		if(v.subaccount_type=='ORG') {
    		    if (v.name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
    		       return true;
    		    }
    		} else {
    			if (v.first_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1) {
               return true;
          }
          if(v.last_name != null){
            if(v.last_name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1)
              return true;
          }
    		}
    	    return false;
        })
    }
  };

  showSelected(){
    this.showall='1';
    this.Organization = [];
    this.allOrganization.forEach(function(item){
   			
        this.selectedArrayId.forEach(function(pitem){ 
            if(pitem == item.id) {
                item.isChecked=true;
                this.Organization.push(item);
            }
        }, this);
    }, this);
  }
  showAll() {
    this.showall='0';
    this.searchValue='';
    this.Organization=[];
    //this.Organization =this.allOrganization;
    this.allOrganization.forEach(function(item){ 
        
        if(item.certified !=2) {
          this.Organization.push(item);
        }
    }, this);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddOrganizationPage');

    this.sendrequest.getResult('user/getallOrganization/' + this.user.id,'post',{'allOrg':1}).then((response:any) => {     
      response.data.organizations.forEach(function(item){ 
        item.isChecked=false;
        this.selectedArrayId.forEach(function(pitem){ 
          if(pitem == item.id) {
              item.isChecked=true;
          }
        }, this);
        if(item.certified !=2) {
          this.Organization.push(item);
        }
      }, this);
     // this.Organization = this.Organization.concat(response.data.organizations); 
      this.allOrganization = this.allOrganization.concat(response.data.organizations);;
    },
    error => {
      
    })
  }
  removeOrg(org){
  	org.isChecked=false;
  	this.totalOrgSelected -=1;
  	this.selectedArrayId.splice(this.selectedArrayId.indexOf(org.id), 1);
	this.selectedArray =  this.removeByKey(this.selectedArray,{
	    accronym: org.accronym, 
        name:  org.name,
        accronym_model:org.isChecked
	});
  }
  removeByKey(array, params){
    array.some(function(item, index) {
    if(array[index].accronym === params.accronym){
        array.splice(index, 1);
    }
    });
    return array;
  }
  addOrg(org) {
  	org.isChecked=true;
  	this.totalOrgSelected +=1;
  	this.selectedArrayId.push(org.id);
    this.selectedArray.push({
        accronym: org.accronym, 
        name:  org.name,
        accronym_model:org.isChecked
    });
  }
  

}
