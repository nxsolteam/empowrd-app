import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { Platform, ToastController, App, AlertController, Events } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { CacheService } from 'ionic-cache-observable';
import { File } from '@ionic-native/file';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


/*
  Generated class for the SendrequestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SendrequestProvider {
	api_url: string = "https://prod1.empowrd.com/api/";
  //api_url: string = "https://prod3.empowrd.com/api/";
	authtoken:string;
  cacheDir: string;
  constructor(public http: HttpClient, public plt: Platform,
    public app: App, private toastCtrl: ToastController,private alertCtrl: AlertController,private network: Network,private file: File, private cacheService: CacheService, private nativePageTransitions: NativePageTransitions, private events: Events) {
    console.log('Hello SendrequestProvider Provider');
    if( localStorage.getItem('auth-token') !==null) {
    	let authtoken = localStorage.getItem('auth-token');
    	this.authtoken = authtoken;
    }
    // let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
    //   this.presentToast('network was disconnected.');
    // });
     this.plt.ready().then(ready => {
      this.cacheDir = this.file.cacheDirectory;
    })
    // stop disconnect watch
    //disconnectSubscription.unsubscribe();

  }
  getHeaderOptions(): any {
   
    var headers = new HttpHeaders().set('Authorization', `Bearer ${this.authtoken}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json');
   // var headers = new HttpHeaders().set('Authorization', `${this.authtoken}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json');
    return { headers }
  }
  getEmails(obj) {
    return new Promise((resolve, reject) => {
      let options = {headers: new HttpHeaders().set('Accept', 'application/json').set('Content-Type', 'application/json')};

      this.http.get("https://www.google.com/m8/feeds/contacts/default/full?access_token="+obj.accessToken+"&alt=json", options).subscribe((result: any) => {
          resolve(result);
        },
        err => {
          //this.getErrorMessage(err);
          reject(err);
        });
    });
  }
  buttonClicked(id,type) {
    let options = this.getHeaderOptions(); 
    this.http.get(this.api_url +'user/buttonClicked/' + id+'/'+type , options).subscribe((response: any) => {
     
    },
    err => {
      
    });
  }
  updateCivicProfileData() {
   let options = this.getHeaderOptions(); 
    this.http.get(this.api_url +'user/getCivicProfileData' , options).subscribe((response: any) => {
      console.log(response);
      localStorage.setItem('civicProfile', JSON.stringify(response.data)); 
      this.events.publish('updateCivicProfile:data', response.data, Date.now());
    },
    err => {
      
      
    });
  }
  sendFriendNotification() {
   let options = this.getHeaderOptions(); 
    this.http.get(this.api_url +'user/sendFriendNotification' , options).subscribe((response: any) => {
    },
    err => {
    });
  }
  updatePowerScore(userid) {
   let options = this.getHeaderOptions(); 
    this.http.get(this.api_url +'user/calculatePowerScore/'+userid , options).subscribe((response: any) => {
      console.log(response);
      
      localStorage.setItem('powerScore', response.data.powerScore.toString()); 
      localStorage.setItem('powerScorePoint',  JSON.stringify(response.data.powerScorePoint));  
      this.events.publish('powerScoreUpdate:created', response.data.powerScore, Date.now());
      
    },
    err => {
      
      
    });
  }
  getResult(url,method,param){
    return new Promise((resolve, reject) => {
  
      let options = this.getHeaderOptions(); 
      if(method=='post'){ 
        this.http.post(this.api_url +url , param,options)
          .subscribe((res: any) => {           
            resolve(res);
          },
          err => {
              
              reject(err);
        });
      } else {
        this.http.get(this.api_url +url , options).subscribe((result: any) => {
          resolve(result);
        },
        err => {
          
          reject(err);
        });
      }
    });
  }
  verifiedEmail(id){
    return new Promise((resolve, reject) => {
      let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.get(this.api_url +'auth/emailverified/'+id, options)
        .subscribe((res: any) => { 
          
          resolve(res);
        },
          err => {
             reject(err);
          });
    });
  }
  verifyEmail(data){
    return new Promise((resolve, reject) => {
      let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url +'auth/checkVerifyEmail',data, options)
        .subscribe((res: any) => { 
          
          resolve(res);
        },
          err => {
             reject(err);
          });
    });
  }
  appleLogin(user) {
   return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
     this.http.post(this.api_url + "auth/loginUsingApple", user,options)
       .subscribe((res: any) => {
         if(!res.errors) {
           console.log(res);
           /* this.authtoken = res.token;
           localStorage.setItem('user', JSON.stringify(res.user));
           localStorage.setItem('auth-token', res.token); */
         }
         resolve(res);
       },
         err => {
            reject(err);
         });
   });
  }
  login(user) {
    return new Promise((resolve, reject) => {
      let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url + "auth/loginUsingEmail", user,options)
        .subscribe((res: any) => { 
          if(!res.errors) {
            console.log(res);
            /* this.authtoken = res.token;
            localStorage.setItem('user', JSON.stringify(res.user));
            localStorage.setItem('auth-token', res.token); */
          }
          resolve(res);
        },
          err => {
             reject(err);
          });
    });
  }
  userData(user) {
    return new Promise((resolve, reject) => {
      let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url + "auth/getUserById", user,options)
        .subscribe((res: any) => { 
          if(!res.errors) {
            console.log(res);
            /* this.authtoken = res.token;
            localStorage.setItem('user', JSON.stringify(res.user));
            localStorage.setItem('auth-token', res.token); */
          }
          resolve(res);
        },
          err => {
             reject(err);
          });
    });
  }
  
  sendEmailVerificationLink(user) {
    return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
     if(this.plt.is('core') || this.plt.is('mobileweb')) {
        user.device='web';
      } else {
        user.device='app';
      }
      this.http.post(this.api_url + "auth/sendemailverificationlinknew", user,options)
        .subscribe((res: any) => { 
          resolve(res);
        },
        err => {
           reject(err);
        });
    });
  }
  sendVerificationCode(user) {
    return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url + "auth/sendmobileverificationcode", user,options)
        .subscribe((res: any) => { 
          resolve(res);
        },
          err => {
            // let message = this.getErrorMessage(err);
            //this.presentToast(message);
             reject(err);
          });
    });
  }
  checkEmailExist(user) {
    return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url + "auth/checkEmailExist", user,options)
        .subscribe((res: any) => { 
          resolve(res);
        },
          err => {
            // let message = this.getErrorMessage(err);
            //this.presentToast(message);
             reject(err);
          });
    });
  }
  sendVerificationEmail(data){
    return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
     
      if(this.plt.is('core') || this.plt.is('mobileweb')) {
        data.device='web';
      } else {
        data.device='app';
      }
      this.http.post(this.api_url + "auth/sendverificationEmailNew",data, options)
        .subscribe((res: any) => { 
          resolve(res);
        },
        err => {
          //let message = this.getErrorMessage(err);
          //this.presentToast(message);
           reject(err);
        });
    });
  }
  authregister(user) {
    return new Promise((resolve, reject) => {
     let options = {headers: new HttpHeaders().set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
      this.http.post(this.api_url + "auth/newregister", user,options)
        .subscribe((res: any) => { 
          if(!res.errors) {
            /* localStorage.setItem('registerStep','getstarted');
            this.authtoken = res.data.token;
            localStorage.setItem('user', JSON.stringify(res.data.user));
            localStorage.setItem('auth-token', res.data.token);*/
          }
          resolve(res);
        },
          err => {

            //let message = this.getErrorMessage(err);
            //this.presentToast(message);
             reject(err);
          });
    });
  }
  getErrorMessage(err){
    return err.error.errors.message[0];
  }
  signOut(){
    localStorage.removeItem('auth-token');
    localStorage.removeItem('user');
    localStorage.removeItem('registerStep');
    localStorage.clear();
    localStorage.clearCache();
    /*
    this.cacheService.unregister('leaderlist');
    this.cacheService.unregister('addresslist');
    this.cacheService.unregister('msglist');
    */
    let nav = this.app.getActiveNav();
    nav.setRoot('LandingPage',{});
  }
  
  goToPage(pagename:string,params:any={},setRoot:boolean=false) {
    if(pagename ==''){
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500
      }
      this.nativePageTransitions.slide(options);
      let nav = this.app.getActiveNav();
      nav.pop();
    } else {
      /* let options: NativeTransitionOptions = {
        direction: 'left',
        duration: 500
      }
      this.nativePageTransitions.slide(options); */
      let nav = this.app.getActiveNav(); 
      if(setRoot ==false) {

        nav.push(pagename,params,{    animation: 'ios-transition', // default for push is 'forward'
         
         });
      } else {
        nav.setRoot(pagename,params);
      }
    }
  }
  presentToast(message) {
    if(message != '' && message !='undefined' && message != undefined) {
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
      toast.present();
      /*
      let alert = this.alertCtrl.create({
            title: 'Empowrd',
            cssClass: 'confirm-register-modal',
            subTitle: `${message}`,
            buttons: ['OK']
          })
          alert.present();
          */
    }
  }
  cacheObject(fileFullPath: string, newFileName: string) {
    return new Promise((resolve, reject) => {

      let composed = fileFullPath.replace("file:///", "").split("/")
      let fileName = composed.pop()
      let filePath = "file:///" + composed.join("/")

      console.log(`copying ${fileFullPath} to ${this.cacheDir}/${newFileName}`)
      let localUri = this.cacheDir + newFileName

      this.file.checkFile(this.cacheDir, newFileName).then(result => {
        console.log('file exists ')
        resolve({ nativeURL: localUri })
      },
        error => {
          this.file.copyFile(filePath, fileName, this.cacheDir, newFileName).then(result => {

            resolve(result)
            console.log('copy file success ', result)

          },
            error => {
              reject(error)
              console.log('copy file failed ', JSON.stringify(error))
            })
        })
    })
  }
}
