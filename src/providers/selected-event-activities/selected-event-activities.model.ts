export interface SelectedEventActivities {
	activity_model: string;
	activity_title: string;
	address: string;
	admin_id: string;
	administator_name: string;
	checked: string;
	created_at: string;
	date: string;
	description:string;
	endtime: string;
	event_id: number;
	formatted_address: string;
	id: number;
	lat: string;
	location: string;
	lon:string;
	secondary_title: string;
	shorttype:string;
	starttime: string;
	type: string;
	updated_at: string;
	volunteer: string;
}
