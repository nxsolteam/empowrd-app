import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { SelectedEventActivities } from './selected-event-activities.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';

/*
  Generated class for the SelectedEventActivitiesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SelectedEventActivitiesProvider {

  user:any;
  
  public selectedEventActivitiesCache: Cache<SelectedEventActivities[]>;
  
  public selectedEventActivities$: Observable<SelectedEventActivities[]>;
  public selectedEventActivitiesrefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
    console.log('Hello selectedEventActivitiesProvider');
    this.user =  JSON.parse(localStorage.getItem('user'));
    const selectedEventActivitiesSourceData = this.random();
    this.cacheService.register('selectedEventActivities', selectedEventActivitiesSourceData).subscribe((selectedEventActivitiesCache) => {
      this.selectedEventActivitiesCache = selectedEventActivitiesCache;
      this.selectedEventActivities$ = selectedEventActivitiesCache.get$;
      //this.selectedEventActivitiesCache.refresh().subscribe();
      console.log(" selectedEventActivities ");
      console.log(this.selectedEventActivities$);
      //this.refresh();
    });
  }

  /**
   * Get some selectedEventActivities data.
   *
   * @returns {Observable<SelectedEventActivities>}
   */
  public get(): Observable<SelectedEventActivities[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.post<SelectedEventActivities[]>(this.sendrequest.api_url + 'events/getUserActivityList',
    { 'eventid' : localStorage.getItem('selectedEventId') }, httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<SelectedEventActivities>}
   */
  public random(): Observable<SelectedEventActivities[]> {
    return Observable.defer(() => {

       	return this.get();
    });
  }
  public selectedEventActivitiesRefresh(refresher?: Refresher): void {
    if (this.selectedEventActivitiesrefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.selectedEventActivitiesCache) {
      this.selectedEventActivitiesrefreshSubscription = this.selectedEventActivitiesCache.refresh()
        .finally(() => this.selectedEventActivitiesrefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}