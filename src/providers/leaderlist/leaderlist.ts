import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { Leaderlist } from './leaderlist.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';
/*
  Generated class for the LeaderlistProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
 declare var google: any;
@Injectable()
export class LeaderlistProvider {
  user:any;
  
  public leaderlistCache: Cache<Leaderlist[]>;
  
  public leaderlist$: Observable<Leaderlist[]>;
  
  public leaderlistrefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
    console.log('Hello LeaderlistProvider');
    this.user =  JSON.parse(localStorage.getItem('user'));
    const leaderListSourceData = this.random();
    this.cacheService.register('leaderlist', leaderListSourceData).subscribe((leaderlistCache) => {
      this.leaderlistCache = leaderlistCache;
      this.leaderlist$ = leaderlistCache.get$;
      //this.leaderlistCache.refresh().subscribe();
      console.log(" leaderlist ");
      console.log(this.leaderlist$);
      //this.refresh();
    });
  }

  /**
   * Get some Leaderlist data.
   *
   * @returns {Observable<Leaderlist>}
   */
  public get(lat:any, lon: any): Observable<Leaderlist[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.get<Leaderlist[]>(this.sendrequest.api_url + 'cdmleaders/fetch_rgalistdataupdated/'  + lon + ' ' + lat,httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<Leaderlist>}
   */
  public random(): Observable<Leaderlist[]> {
    return Observable.defer(() => {
	    let lat=localStorage.getItem('latitude');
	    let lon=localStorage.getItem('longitude');
       	return this.get(lat,lon);
    });
  }
  public leaderlistRefresh(refresher?: Refresher): void {
    if (this.leaderlistrefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.leaderlistCache) {
      this.leaderlistrefreshSubscription = this.leaderlistCache.refresh()
        .finally(() => this.leaderlistrefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}


