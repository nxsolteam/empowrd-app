export interface GetOrgList {
	id: number;
	message_count: number;
	followers: number;
	accronym:string;
	accronym_model:string;
	name:string;
	causes:string;
	description:string;
	short_description:string;
	show_short_description:boolean;
}
