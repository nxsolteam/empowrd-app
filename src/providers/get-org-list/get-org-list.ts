import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { GetOrgList } from './get-org-list.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';

/*
  Generated class for the GetOrgListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GetOrgListProvider {

  user:any;
  
  public getOrgListCache: Cache<GetOrgList[]>;
  
  public getOrgList$: Observable<GetOrgList[]>;
  
  public getOrgListrefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
    console.log('Hello getOrgListProvider');
   
    const getOrgListSourceData = this.random();
    this.cacheService.register('getOrgList', getOrgListSourceData).subscribe((getOrgListCache) => {
      this.getOrgListCache = getOrgListCache;
      this.getOrgList$ = getOrgListCache.get$;
      //this.getOrgListCache.refresh().subscribe();
      console.log(" getOrgList ");
      console.log(this.getOrgList$);
      //this.refresh();
    });
  }

  /**
   * Get some getOrgList data.
   *
   * @returns {Observable<getOrgList>}
   */
  public get(): Observable<GetOrgList[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
    let user =  JSON.parse(localStorage.getItem('user'));
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.get<GetOrgList[]>(this.sendrequest.api_url + 'user/getOrganizationFour/' + user.id,httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<getOrgList>}
   */
  public random(): Observable<GetOrgList[]> {
    return Observable.defer(() => {
       	return this.get();
    });
  }
  public getOrgListRefresh(refresher?: Refresher): void {
    if (this.getOrgListrefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.getOrgListCache) {
      this.getOrgListrefreshSubscription = this.getOrgListCache.refresh()
        .finally(() => this.getOrgListrefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}
