import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Placeholder } from './placeholder.model';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';
import { Refresher } from 'ionic-angular';
import { Cache, CacheService } from 'ionic-cache-observable';
import { SendrequestProvider } from '../sendrequest/sendrequest';
/*
  Generated class for the PlaceholderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class PlaceholderProvider {
  user:any;
  /**
   * The placeholder data to present.
   *
   * @type {Observable<Placeholder>}
   */
  public placeholder$: Observable<Placeholder[]>;

  /**
   * The cache instance for refreshing, etc.
   *
   * @type {Cache<Placeholder>}
   */
  public cache: Cache<Placeholder[]>;

  /**
   * Refresh subscription that can be cancelled when leaving the view.
   *
   * @type {Subscription}
   */
  public refreshSubscription: Subscription;

  /**
   * ContactPage constructor.
   *
   * @param {PlaceholderProvider} placeholderProvider
   * @param {CacheService} cacheService
   */
  constructor(public httpclient: HttpClient, public sendrequest: SendrequestProvider,  private cacheService: CacheService) {
    console.log('Hello PlaceholderProvider');
    // this.user =  JSON.parse(localStorage.getItem('user'));
    const sourceData = this.random();
    // Register the cache instance using 'contact' as identifier, so we get the same data on next load.
    this.cacheService.register('msglist', sourceData).subscribe((cache) => {
      this.cache = cache;
      this.placeholder$ = cache.get$;
      if(this.user!=null) {
        this.refresh();
      }
    })
  }

  /**
   * Get some placeholder data.
   *
   * @returns {Observable<Placeholder>}
   */
  public get(id:number, options: any): Observable<Placeholder[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
    return this.httpclient.post<Placeholder[]>(this.sendrequest.api_url + 'user/fetchNewMail/' + id, { 'created_at' : localStorage.getItem("lastupdated_message")},httpOptions)
      .delay(1000);
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<Placeholder>}
   */
  public random(): Observable<Placeholder[]> {
    return Observable.defer(() => {
    let options = this.sendrequest.getHeaderOptions();
    this.user =  JSON.parse(localStorage.getItem('user'));
      return this.get(this.user.id,options);
    });
  }
  /**
   * Perform refresh.
   *
   * @param {Refresher} refresher
   */
  public refresh(refresher?: Refresher): void {
    if (this.refreshSubscription) {
      console.warn('Already refreshing.');
      return;
    }
    if (this.cache) {
      this.refreshSubscription = this.cache.refresh()
        .finally(() => this.refreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}
