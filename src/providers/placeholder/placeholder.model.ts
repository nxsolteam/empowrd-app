export interface Placeholder {
	admin_id: number;
	body:string;
	created_at:string;
	date:string;
	message_id:number;
	mid:number;
	status:number;
	subject:string;
	subtitle:string;
	totalThread:number;
	type:number;
	umid:number;
	usercomposed:boolean;
}
