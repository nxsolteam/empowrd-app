import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AutoCompleteService} from 'ionic2-auto-complete';
import 'rxjs/add/operator/map';

/*
  Generated class for the OfficeSearchAutoCompleteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OfficeSearchAutoCompleteProvider  implements AutoCompleteService {

  constructor(public http: HttpClient) {
    console.log('Hello OfficeSearchAutoCompleteProvider Provider');
  }
  getResults(keyword:string) {
    return this.http.get("https://restcountries.eu/rest/v1/name/"+keyword)
      .map(
        (result:any) =>
        {
          return result.json()
            .filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()) )
        });
  }

}
