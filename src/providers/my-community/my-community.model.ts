export interface MyCommunity {
	id: number;
	type:string;
	address:string;
	admin_id:number;
	count:number;
}
