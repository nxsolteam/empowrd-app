import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { MyCommunity } from './my-community.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';
/*
  Generated class for the MyCommunityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyCommunityProvider {

  user:any;
  
  public myCommunityCache: Cache<MyCommunity[]>;
  
  public myCommunity$: Observable<MyCommunity[]>;
  
  public myCommunityrefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
    console.log('Hello MyCommunityProvider Provider');
    this.user =  JSON.parse(localStorage.getItem('user'));
    const myCommunitySourceData = this.random();
    this.cacheService.register('myCommunity', myCommunitySourceData).subscribe((myCommunityCache) => {
      this.myCommunityCache = myCommunityCache;
      this.myCommunity$ = myCommunityCache.get$;
      //this.myCommunityCache.refresh().subscribe();
      console.log(" myCommunity ");
      console.log(this.myCommunity$);
      //this.refresh();
    });
  }
/**
   * Get some myCommunity data.
   *
   * @returns {Observable<MyCommunity>}
   */
  public get(): Observable<MyCommunity[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.get<MyCommunity[]>(this.sendrequest.api_url + 'user/getMyCommunity',httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<MyCommunity>}
   */
  public random(): Observable<MyCommunity[]> {
    return Observable.defer(() => {
       	return this.get();
    });
  }
  public myCommunityRefresh(refresher?: Refresher): void {
    if (this.myCommunityrefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.myCommunityCache) {
      this.myCommunityrefreshSubscription = this.myCommunityCache.refresh()
        .finally(() => this.myCommunityrefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}
