import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Addresslist } from './addresslist.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Refresher } from 'ionic-angular';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';

/*
  Generated class for the AddresslistProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class AddresslistProvider {
  user:any;
   addresslist:any=[];
  public cache: Cache<Addresslist[]>;
  public addresslist$: Observable<Addresslist[]>;
  public refreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, public sendrequest: SendrequestProvider, private cacheService: CacheService) {
    console.log('Hello AddresslistProvider');
    //this.user =  JSON.parse(localStorage.getItem('user'));
    const sourceData = this.random();
    this.cacheService
    .register('addresslist',sourceData)
    .subscribe((leaderlistCache) => {
      this.cache = leaderlistCache;
      this.addresslist$ = leaderlistCache.get$;
      
    })

  }

  /**
   * Get some Addresslist data.
   *
   * @returns {Observable<Addresslist>}
   */
  public get(id:number, options: any): Observable<Addresslist[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.get<Addresslist[]>(this.sendrequest.api_url + 'user/getAddressAndEventsForNewApp/' + id,httpOptions)
      .delay(1000); // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<Addresslist>}
   */
  public random(): Observable<Addresslist[]> {
    return Observable.defer(() => {
    let options ={}; // this.sendrequest.getHeaderOptions();
    this.user =  JSON.parse(localStorage.getItem('user'));
    let id=this.user.id;
       return this.get(id,options);
    });
  }
  /**
   * Handle refresh event.
   *
   * @param {Refresher} refresher
   */
  public onRefresh(refresher: Refresher): void {
    this.refresh(refresher);
  }


  /**
   * Perform refresh.
   *
   * @param {Refresher} refresher
   */
   
  public refresh(refresher?: Refresher): void {
    if (this.refreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.cache) {
      this.refreshSubscription = this.cache.refresh()
        .finally(() => this.refreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}

