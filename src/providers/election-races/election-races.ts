import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { ElectionRaces } from './election-races.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';

/*
  Generated class for the ElectionRacesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ElectionRacesProvider {

  user:any;
  
  public electionRaceCache: Cache<ElectionRaces[]>;
  
  public electionRace$: Observable<ElectionRaces[]>;
  
  public electionRacerefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
  	this.user =  JSON.parse(localStorage.getItem('user'));
    const electionRaceSourceData = this.random();
    this.cacheService.register('electionRace', electionRaceSourceData).subscribe((electionRaceCache) => {
      this.electionRaceCache = electionRaceCache;
      this.electionRace$ = electionRaceCache.get$;
      console.log(" electionRace ");
      console.log(this.electionRace$);
    });
    console.log('Hello ElectionRacesProvider Provider');
  }
  /**
   * Get some electionRace data.
   *
   * @returns {Observable<ElectionRaces>}
   */
  public get(lat:any, lon: any): Observable<ElectionRaces[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};
    return this.httpclient.get<ElectionRaces[]>(this.sendrequest.api_url + 'cdmleaders/fetch_raceslistdata/'  + lon + ' ' + lat,httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<ElectionRaces>}
   */
  public random(): Observable<ElectionRaces[]> {
    return Observable.defer(() => {
	    let lat=localStorage.getItem('latitude');
	    let lon=localStorage.getItem('longitude');
       	return this.get(lat,lon);
    });
  }
  public electionRaceRefresh(refresher?: Refresher): void {
    if (this.electionRacerefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.electionRaceCache) {
      this.electionRacerefreshSubscription = this.electionRaceCache.refresh()
        .finally(() => this.electionRacerefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }

}
