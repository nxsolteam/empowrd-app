export interface ElectionRaces {
	id: number;
	type:string;
	address:string;
	admin_id:number;
	count:number;
}
