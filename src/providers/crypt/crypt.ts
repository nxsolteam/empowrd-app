import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
/*
  Generated class for the CryptProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CryptProvider {
  public JsonFormatter = {
    stringify: function(cipherParams) {
      // create json object with ciphertext
      let jsonObj:any;
      jsonObj = { ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64) };
  ​
      // optionally add iv or salt
      if (cipherParams.iv) {
        jsonObj.iv = cipherParams.iv.toString();
      }
  ​
      if (cipherParams.salt) {
        jsonObj.s = cipherParams.salt.toString();
      }
  ​
      // stringify json object
      return JSON.stringify(jsonObj);
    },
    parse: function(jsonStr) {
      // parse json string
      let jsonObj:any = JSON.parse(jsonStr);
  ​
      // extract ciphertext from json object, and create cipher params object
      var cipherParams = CryptoJS.lib.CipherParams.create({
        ciphertext: CryptoJS.enc.Base64.parse(jsonObj.ct)
      });
  ​
      // optionally extract iv or salt
  ​
      if (jsonObj.iv) {
        cipherParams.iv = CryptoJS.enc.Hex.parse(jsonObj.iv);
      }
  ​
      if (jsonObj.s) {
        cipherParams.salt = CryptoJS.enc.Hex.parse(jsonObj.s);
      }
  ​
      return cipherParams;
    }
  };
  private SECERET_KEY: string = 'tjtHiYVP6TZVCDES9MCqvJQB5AXxUVPU';
  constructor(public http: HttpClient) {
    console.log('Hello CryptProvider Provider');
  }
  encrypt(string) {
  	return CryptoJS.AES.encrypt(JSON.stringify(string), this.SECERET_KEY, {
          format: this.JsonFormatter
        }).toString();
  }

}
