export interface GetCausesList {
	id: number;
	accronym:string;
	accronym_model:string;
	name:string;
}
