import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import { Refresher } from 'ionic-angular';
import { GetCausesList } from './get-causes-list.model';
import { SendrequestProvider } from '../sendrequest/sendrequest';
import { Cache, CacheService } from 'ionic-cache-observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/finally';

/*
  Generated class for the GetCausesListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GetCausesListProvider {

  user:any;
  
  public getCausesListCache: Cache<GetCausesList[]>;
  
  public getCausesList$: Observable<GetCausesList[]>;
  
  public getCausesListrefreshSubscription: Subscription;
  constructor(public httpclient: HttpClient, private cacheService: CacheService, public sendrequest: SendrequestProvider) {
    console.log('Hello getCausesListProvider');
   
    const getCausesListSourceData = this.random();
    this.cacheService.register('getCausesList', getCausesListSourceData).subscribe((getCausesListCache) => {
      this.getCausesListCache = getCausesListCache;
      this.getCausesList$ = getCausesListCache.get$;
      //this.getCausesListCache.refresh().subscribe();
      console.log(" getCausesList ");
      console.log(this.getCausesList$);
      //this.refresh();
    });
  }

  /**
   * Get some getCausesList data.
   *
   * @returns {Observable<getCausesList>}
   */
  public get(): Observable<GetCausesList[]> {
  	 // const httpOptions = {headers: new HttpHeaders().set("token", this.token)};
    let user =  JSON.parse(localStorage.getItem('user'));
  	 let token = localStorage.getItem('auth-token');
  	  const httpOptions ={headers: new HttpHeaders().set('Authorization', `Bearer ${token}`).set('Accept', 'application/x.laravel.v1+json').set('Content-Type', 'application/json')};

    return this.httpclient.get<GetCausesList[]>(this.sendrequest.api_url + 'user/getcausesFour/' + user.id,httpOptions)
      ; // Enforce a delay to make the cache functionality more visible.
  }

  /**
   * Get some random placeholder data.
   *
   * @returns {Observable<getCausesList>}
   */
  public random(): Observable<GetCausesList[]> {
    return Observable.defer(() => {
       	return this.get();
    });
  }
  public getCausesListRefresh(refresher?: Refresher): void {
    if (this.getCausesListrefreshSubscription) {
      console.warn('Already refreshing.');

      return;
    }

    if (this.getCausesListCache) {
      this.getCausesListrefreshSubscription = this.getCausesListCache.refresh()
        .finally(() => this.getCausesListrefreshSubscription = null)
        .subscribe(() => {
          if (refresher) {
            refresher.complete();
          }
        }, (err) => {
          console.error('Something went wrong!', err);

          if (refresher) {
            refresher.cancel();
          }

          throw err;
        });
    } else {
      console.warn('Cache has not been initialized.');

      if (refresher) {
        refresher.cancel();
      }
    }
  }
}
